#!/bin/bash

# Use NCKS to extract only variables requested, and then chop out the 3x3 patch

CONFIG='VAR_CONFIG';       RUN_ID='VAR_RUN_ID'

# This is the window to ignore everything outside of.
START_YEAR='VAR_START_YEAR';      START_MONTH='VAR_START_MONTH';       START_DAY='VAR_START_DAY';
FINAL_YEAR='VAR_FINAL_YEAR';      FINAL_MONTH='VAR_FINAL_MONTH';       FINAL_DAY='VAR_FINAL_DAY';

# Stations / regions to be extracted
STATION_LIST='VAR_STATION_LIST'
SWITCH_USEFILENAME=VAR_SWITCH_USEFILENAME
	# csv file with station ID, lon/lat of nearest grid point, x,y coords of nearest point, and group (not necessarilyy used here)

# Input / Output directories and roots
DIR_SOURCE_DATA='VAR_DIR_SOURCE'	# Directory of links from the linking script
DIR_WORK='VAR_DIR_WORK'			# Directory for work space, cleared at the end of the script
DIR_SERIES='VAR_DIR_SERIES'		# Final output directory

# Coordinate files (also to be chopped)
FILE_COORD='VAR_FILE_COORD'
DIR_COORD='VAR_DIR_COORD'

INTERIM_FILE='VAR_FILE_INTERIM'		# temporary file used for concatenating patches

# All variables to be extracted.  Currently must include all variables present in a file - no variables deleted from schema.
GRID_T='VAR_GRID_T';   VARS_T='VAR_VARS_T';	GRID_T2D='VAR_GRID_T2D';   VARS_T2D='VAR_VARS_T2D';
GRID_U='VAR_GRID_U';   VARS_U='VAR_VARS_U';    	GRID_U2D='VAR_GRID_U2D';   VARS_U2D='VAR_VARS_U2D';
GRID_V='VAR_GRID_V';   VARS_V='VAR_VARS_V';    	GRID_V2D='VAR_GRID_V2D';   VARS_V2D='VAR_VARS_V2D';

# Switches
SWITCH_SERIES=VAR_SWITCH_SERIES		# If 1, compile the extracted patches into a single time series
SWITCH_RMINTERIM=VAR_SWITCH_RMINTERIM	# If 1, clean out all the interim patch files etc.  (final output is a time series file)
SWITCH_END_OF_DAY='VAR_END_OF_DAY'	# String with the timestamp for datemax

# Details about grids
DELIMITER='VAR_DELIMITER';		INTERVAL='VAR_INTERVAL';	MOORING_INTERVAL='1h' #'VAR_MOORING_INTERVAL'
XOFFSET=1;              YOFFSET=1;		# If full domain, set both offsets to 1, since Matlab is 1-indexed.
						# Not currently supported, but can extract things from already extracted region.	

# Metadata
NOTE='VAR_META_NOTE'                         # string for any relevant information
USER=`whoami`                   # attribution string

#-----------------------------------------------------------------------------------------------------------

# [ 0 ] Housekeeping

if [ ! -d $DIR_SERIES ] ; then; mkdir -p ${DIR_SERIES}; fi
if [ ! -d $DIR_COORD ] ; then; mkdir -p ${DIR_COORD}; fi
if [ ! -d $DIR_WORK ] ; then; mkdir -p ${DIR_WORK}; fi

NOTE=${NOTE// /_} # make sure the note is well behaved

# [ 1 ] List all files in the directory of links with a given delimiter (so one representative per time slice)

all_files=`ls -1 ${DIR_SOURCE_DATA}/*${INTERVAL}*${DELIMITER}*`
filelist='';

START_DATE=${START_YEAR}${START_MONTH}${START_DAY};			FINAL_DATE=${FINAL_YEAR}${FINAL_MONTH}${FINAL_DAY}
if [[ ${SWITCH_END_OF_DAY} == '00:00:00' ]]; then; 
	datemin=${START_YEAR}'-'${START_MONTH}'-'${START_DAY}' 00:00:01';	
	datemax=${FINAL_YEAR}'-'${FINAL_MONTH}'-'$((${FINAL_DAY} + 1))' 00:00:00'
else
	datemin=${START_YEAR}'-'${START_MONTH}'-'${START_DAY}' 00:00:00';	
	datemax=${FINAL_YEAR}'-'${FINAL_MONTH}'-'${FINAL_DAY}' '${SWITCH_END_OF_DAY} #${REF_END_OF_DAY}
fi

# [ 1.5 ] Aggregate some variables regarding the grids and which variables are requested from each. 
#         Allows for looping later on rather than bunches of if switches
GRIDS=''; VARS=''; VARSTR=''
if [[ ${#VARS_T2D} > 0 ]]; then; GRIDS=${GRIDS}' '${GRID_T2D}; VARS=${VARS}' '${VARS_T2D}; fi
if [[ ${#VARS_U2D} > 0 ]]; then; GRIDS=${GRIDS}' '${GRID_U2D}; VARS=${VARS}' '${VARS_U2D}; fi
if [[ ${#VARS_V2D} > 0 ]]; then; GRIDS=${GRIDS}' '${GRID_V2D}; VARS=${VARS}' '${VARS_V2D}; fi
if [[ ${#VARS_T}   > 0 ]]; then; GRIDS=${GRIDS}' '${GRID_T}; VARS=${VARS}' '${VARS_T}; fi
if [[ ${#VARS_U}   > 0 ]]; then; GRIDS=${GRIDS}' '${GRID_U}; VARS=${VARS}' '${VARS_U}; fi
if [[ ${#VARS_V}   > 0 ]]; then; GRIDS=${GRIDS}' '${GRID_V}; VARS=${VARS}' '${VARS_V}; fi
GRIDS=( ${GRIDS} );	VARS=( ${VARS} );	NGRIDS=${#GRIDS[@]};	NVARS=${#VARS[@]}
VARSTR=${VARS[@]}; VARSTR=${VARSTR//\ /,}



# [ 2 ] Loop through the total list, pick out the files that overlap with the specified window, and make a list of them

for f in ${all_files}; do
	inc=false
	# Parse window of days present in file
	# This structure requires input from FILES script, which creates links with a specific format and the window of data present explicit
	winstart=${f:$((${#f} - 20)):8}
	winfinal=${f:$((${#f} - 11)):8}

	# convert to comparable integers
	w1=`date '+%C%y%m%d' -d "${winstart}"`;		w2=`date '+%C%y%m%d' -d "${winfinal}"`
	f1=`date '+%C%y%m%d' -d "${START_DATE}"`;	f2=`date '+%C%y%m%d' -d "${FINAL_DATE}"`;

	# Now check
	inc=false
	if [[ ${w1} -ge ${f1} && ${w1} -le ${f2} ]]; then
		inc=true
	fi

	if [[ ${w2} -ge ${f1} && ${w2} -le ${f2} ]]; then
		inc=true
	fi

	# If true, add the file to the filelist
	if [ $inc = 'true' ]; then
		filelist=${filelist}' '${f}
	fi

done




# [ 3 ] Loop through the pared filelist, chop the full file name into relevant pieces, and then extract the data you need.

# Loop through files
for f in ${filelist}; do

	src_dir=${f%/*} 	# get the full path to the file
	fl_root=${f##*/} 	# chop everything before and including the final slash
	coord_root=${FILE_COORD##*/}; coord_root=${coord_root:0:$((${#coord_root}-3))} 	

	# chop at the delimiter so you can splice in more info
	ind=`grep -b -o ${DELIMITER} <<< ${fl_root}` 
	ind=`cut -d: -f1 <<< ${ind}`
	fl_root=${fl_root:0:$((${ind} - 1))}    

	suffix=${f:$((${#f}-20)):20}


	# Chop the parts out
	
        while IFS='' read -r line || [[ -n "$line" ]]; do        # loop over stations
		list=${line//,/ }
		read -A yarr <<< $list

		# All stations need these variables for the coordinate file chopping
		code=${yarr[1]}		# this is the short code
		id=${yarr[0]}		# this is the full string

		# get the indices to chop
		ix=${yarr[4]};	iy=${yarr[5]}
		x1=$((${ix} - 1)); x2=$((${ix} + 1));
		y1=$((${iy} - 1)); y2=$((${iy} + 1));


		# Chop the stations where code indicates needs to be extracted

		if [[ ${SWITCH_USEFILENAME} == True ]] || [[ ${code:0:2} == 'X-' ]] || [[ ${code:0:3} == 'TG-' ]] || [[ ${code:0:5} == 'BUOY-' ]] ; then
			#echo IN SWITCH, ${NGRIDS}
			# Chop dimensions and select variables
			# Nearest point determined with python, which is 0-indexed
			for ((i=0; i<${NGRIDS}; i++)); do
				ncks -O -d x,${x1},${x2} -d y,${y1},${y2} -v ${VARSTR} ${src_dir}/${fl_root}_${GRIDS[i]}_${suffix} ${DIR_WORK}/${fl_root}_${GRIDS[i]}_PATCH_${code}-09_${suffix}
				echo ${DIR_WORK}/${fl_root}_${GRIDS[i]}_PATCH_${code}-09_${suffix}
			done

		fi

		# Clip the coordinate file too - all stations requested need one (not just the extracted ones).
		ncks -O -d x,${x1},${x2} -d y,${y1},${y2} ${FILE_COORD} ${DIR_COORD}/${coord_root}_${code}'.nc'



	done <<< `cat $STATION_LIST` #`sed 1d $STATION_LIST`



done


# [ 4 ] If requested, compile the patches into a time series. The switch is probably useless here, no reason to not concatenate

if [[ $SWITCH_SERIES == 1 ]]; then
        while IFS='' read -r line || [[ -n "$line" ]]; do        # loop over stations
		list=(${line//,/ })
		code=${list[1]}
		id=${list[0]}

		# Only files that have been extracted have patch files
		if [[ ${SWITCH_USEFILENAME} == True ]] || [[ ${code:0:2} == 'X-' ]] || [[ ${code:0:3} == 'TG-' ]] || [[ ${code:0:5} == 'BUOY-' ]] ; then

			# Single time series for each grid
			for ((i=0; i<${NGRIDS}; i++)); do
				PATCHLIST=`ls -1 ${DIR_WORK}'/'*${GRIDS[i]}'_PATCH_'${code}'-09_'*`
				ncrcat -O -h $PATCHLIST $INTERIM_FILE
				ncks -O -h -d time_counter,"$datemin","$datemax" ${INTERIM_FILE} ${DIR_SERIES}/${fl_root}_SER_${GRIDS[i]}_${code}-09_${START_DATE}-${FINAL_DATE}.nc
				rm ${INTERIM_FILE}
			done

			# Now combine files with different grids so there's only one file for each station.  Only difference is no grid listing in the filename.

			for ((i=0; i<${NGRIDS}; i++)); do
				ncks -A ${DIR_SERIES}/${fl_root}_SER_${GRIDS[i]}_${code}-09_${START_DATE}-${FINAL_DATE}.nc ${DIR_SERIES}/${fl_root}_SER_${code}-09_${START_DATE}-${FINAL_DATE}.nc

				# Clean up the grid-specific files if requested
				if [[ $SWITCH_RMINTERIM == 1 ]]; then
					rm ${DIR_SERIES}/${fl_root}_SER_${GRIDS[i]}_${code}-09_${START_DATE}-${FINAL_DATE}.nc
				fi
			done
		
			# Add the metadata
			ncatted -h -a  'note',global,a,c,"${NOTE}" ${DIR_SERIES}/${fl_root}_SER_${code}-09_${START_DATE}-${FINAL_DATE}.nc
			ncatted -h -a  'obs_file',global,a,c,"${id}" ${DIR_SERIES}/${fl_root}_SER_${code}-09_${START_DATE}-${FINAL_DATE}.nc

		else

			mooring_root=${fl_root}
			if [[ ${INTERVAL} != ${MOORING_INTERVAL} ]]; then
				mooring_root=${mooring_root/_${INTERVAL}_/_${MOORING_INTERVAL}_}
			fi

			# Single time series for each grid.  
			# If there is more than one mooring file (ie one per grid) then will need to snag the second for loop in other part of the if statement too
			#for ((i=0; i<${NGRIDS}; i++)); do
			PATCHLIST=`ls -1 ${DIR_SOURCE_DATA}'/'*_${code}'-09_'*`
			ncrcat -O -h $PATCHLIST $INTERIM_FILE
			ncks -O -h -d time_counter,"$datemin","$datemax" ${INTERIM_FILE} ${DIR_SERIES}/${mooring_root}_SER_${code}-09_${START_DATE}-${FINAL_DATE}.nc
			rm ${INTERIM_FILE}
			#done


			

		fi
		
	done <<< `cat $STATION_LIST` #`sed 1d $STATION_LIST`

fi


# [ 5 ] Clean up the intermediary files

if [[ $SWITCH_RMINTERIM == 1 ]]; then
	filelist=${filelist:2:${#filelist}}
	rm ${DIR_WORK}/*_PATCH_*
fi

