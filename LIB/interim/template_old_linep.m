
% Reads in observation and model data for Line P stations (one cruise at a time), constructs nice plots with full range of information available.
% Station 1 is on the right, station 35 on the left - matches how it looks on the map.

clear all

p = genpath ('VAR_PATH_FXNS'); addpath(p); warning ('off', 'all');


% INPUT INFO
dir_data = 'VAR_DIR_DATA';		root_data = 'VAR_ROOT_DATA';	% both observation and model data available in single file, with metadata attached

bathy_file = 'VAR_FILE_BATHY';

runid = 'VAR_RUN_ID';


% OUTPUT INFO
dir_output = 'VAR_DIR_OUTPUT';		root_output = 'VAR_ROOT_OUTPUT';


% PLOTTING PARAMETERS - SET IN SCRIPT

% Winter Cruise
%cruise = '2016-02';
%nc = 8;        % number of colours / contours
%mint = 5.5;       maxt = 10;      difft = 2;      nct = 9;
%mins = 32;      maxs = 34.5;      diffs = 1;      ncs = 10;
%minr = 1024.5;    maxr = 1028;    diffr = 1;      ncr = 14;

% Summer Cruise
cruise = '2016-06';
nc = 8;        % number of colours / contours
mint = 5;       maxt = 12;      difft = 2;      nct = 14;
mins = 31;      maxs = 34;      diffs = 1;      ncs = 12;
minr = 1023;    maxr = 1028;    diffr = 1;      ncr = 10;

% Summer Cruise
%cruise = '2016-08';
%nc = 8;        % number of colours / contours
%mint = 6;       maxt = 18;      difft = 2;      nct = 12;
%mins = 31.75;      maxs = 34.5;      diffs = 1;      ncs = 11;
%minr = 1023;    maxr = 1028;    diffr = 1;      ncr = 10;




%------------------------------------------------------------------------------------------------------------------
%--- END OF USER-SUPPLIED INFO ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------

% [ 0 ] HOUSEKEEPING

% read in bathy data here, set some parameters

bathy  = double(ncread(bathy_file, 'Bathymetry'));
navlat = double(ncread(bathy_file, 'nav_lat'));
navlon = double(ncread(bathy_file, 'nav_lon'));

land = bathy; land(land>0)=NaN; land(land==0)=1;
bathy(bathy==0) = NaN;



xl = -143.0;	xr = -126.0;		% left and right edges of horizontal axis for sections

% Initialize arrays to store sorted data
%linep_stns = [26,35,25,24,23,22,21,20,19,18,17,16,15,14,13,12,10,9,8,7,6,5,4,3,2,1];
ncasts = zeros (27,1);
ct_mod = zeros(27,75); sa_mod = zeros(27,75); rho_mod = zeros(27,75);
ct_obs = zeros(27,75); sa_obs = zeros(27,75); rho_obs = zeros(27,75);

obs_lon = zeros(27,75);		obs_lat = zeros(27,75);
mod_lon = zeros(27,75);		mod_lat = zeros(27,75);
obs_depths = zeros(27,75);	mod_depths = zeros(27,75);



% [ 1 ] READ IN DATA


load ([dir_data '/' root_data '_CTD_summary']);	% contains summary_mod, _obs, _meta

[~, nprofiles ] = size(summary_meta);

% Go profile by profile and sort data into spatial order
for n=1:nprofiles

	% Parse the station number (chop P, check for letter, chop to dash)
	station_str = summary_meta(n).station;
	loc = strfind (station_str, '-');
	stn = station_str(2:(loc(1)-1)  );
	if (isletter(stn(1)) );	stn = stn(2:end); end	% some indicated by PA rather than just P


	% stn is then the numerical station code for the profile
	% note that stn 26 is the end and 35 is between 26 and 25.  (for some baffling and obscure reason)
	cast_ct_mod = summary_mod(n).ct;	cast_sa_mod = summary_mod(n).sa;	cast_rho_mod = summary_mod(n).rho;	cast_depths_mod = summary_mod(n).depths;
	cast_ct_obs = summary_obs(n).ct;	cast_sa_obs = summary_obs(n).sa;	cast_rho_obs = summary_obs(n).rho;	cast_depths_obs = summary_obs(n).depths;


	if     (str2num(stn) == 35);	ind = 26;
	elseif (str2num(stn) == 26);	ind = 27;
	elseif (str2num(stn) <= 25);	ind = str2num(stn);
	else
		display  (['Unrecognized station id: ' stn ])
		stop;
	end

	ct_mod  (ind,:) = cast_ct_mod (:,1); sa_mod  (ind,:) = cast_sa_mod (:,1); rho_mod (ind,:) = cast_rho_mod(:,1);

	% Obs stations may have > 1 per day - ensure that only one day is included in the average by filtering directory contents by hand
	ct_obs  (ind,:) = ct_obs (ind,:) + cast_ct_obs'; 
	sa_obs  (ind,:) = sa_obs (ind,:) + cast_sa_obs'; 
	rho_obs (ind,:) = rho_obs(ind,:) + cast_rho_obs';
	ncasts  (ind)   = ncasts(ind) + 1;


	% spatial info
	obs_lon (ind,:) = summary_meta(n).obs_lon;	obs_lat(ind,:) = summary_meta(n).obs_lat;
	mod_lon (ind,:) = summary_meta(n).mod_lon;	mod_lat(ind,:) = summary_meta(n).mod_lat;

	obs_depths (ind,:) = cast_depths_obs;		mod_depths (ind,:) = cast_depths_mod;

end

% Average the observations by number of casts in each location, NaN anything that doesn't have observations
for s=1:27
	if (ncasts(ind) > 0)
		ct_obs  (s,:) = ct_obs (s,:) / ncasts(s);
		sa_obs  (s,:) = sa_obs (s,:) / ncasts(s);	
		rho_obs (s,:) = rho_obs(s,:) / ncasts(s);
	else
		ct_obs  (s,:) = NaN;
		sa_obs  (s,:) = NaN;
		rho_obs (s,:) = NaN;
	end
end


% NaN out anything that's set to zero
ct_obs (ct_obs==0) = NaN; ct_mod (ct_mod==0) = NaN;
sa_obs (sa_obs==0) = NaN; sa_mod (sa_mod==0) = NaN;
rho_obs (rho_obs==0) = NaN; rho_mod (rho_mod==0) = NaN;



% So now have daily average profiles for stations sorted into appropriate bins. Observation data is already interpolated to model grid depths

% [ 2 ] Plot! That! Data!!!

fig = figure;
subplot (4,3,[1,3]); hold on
pcolor (navlon,navlat,bathy);	shading flat; colorbar		% bathymetry
colormap(gca,cmocean('-deep'))
scatter(obs_lon(:,1),obs_lat(:,1), 50, 'm',  'filled')	% station locations (label them somehow)
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);
xlim ([xl xr]);	ylim ([48.5 50]);

% Temperature
subplot (4,3,4);
ints = linspace(mint, maxt, nct+1);
contourf  (mod_lon, mod_depths, ct_mod, ints); shading flat; colorbar;
%for s=1:27
%	scatter (obs_lon(s,:), obs_depths(s,:), 1, ct_obs(s,:), 'filled'); 
%end
colormap(gca,cmocean('thermo',nct))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([mint maxt])
xlabel('Longitude'); ylabel('Depth (m)');	
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);
tlabel = text (-149,100, 'Con. Temperature', 'HorizontalAlignment', 'center', 'Rotation', 90, 'FontSize', 22);
mlabel = text ((xr+xl)/2,-25, 'Model', 'HorizontalAlignment', 'center', 'FontSize', 22);


subplot (4,3,5);
contourf  (obs_lon, obs_depths, ct_obs, ints); shading flat; colorbar;
colormap(gca,cmocean('thermo',nct))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([mint maxt])
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);
olabel = text ((xr+xl)/2,-25, 'Observations', 'HorizontalAlignment', 'center', 'FontSize', 22);

ct_diff = ct_mod - ct_obs;
subplot (4,3,6);
ints = linspace(-difft, difft, 2*(nc+1));
contourf  (mod_lon, mod_depths, ct_diff, ints); shading flat; colorbar; 
colormap(gca,cmocean('balance',nc))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([-difft difft])
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);
dlabel = text ((xr+xl)/2,-25, 'Model - Obs', 'HorizontalAlignment', 'center', 'FontSize', 22);


% Salinity

subplot (4,3,7);
ints = linspace(mins, maxs, ncs+1);
%[C,h] =contourf  (mod_lon, mod_depths, sa_mod, ints); set (h, 'LineColor', 'none'); 
contourf  (mod_lon, mod_depths, sa_mod, ints);  
shading flat; colorbar;
colormap(gca,cmocean('haline', ncs))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([mins maxs	])
ylabel('Depth (m)');	
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);
slabel = text (-149,100, 'Abs. Salinity', 'HorizontalAlignment', 'center', 'Rotation', 90, 'FontSize', 22);

subplot (4,3,8);
[C,h] =contourf  (obs_lon, obs_depths, sa_obs, ints); %set (h, 'LineColor', 'none'); 
shading flat; colorbar;
colormap(gca,cmocean('haline',ncs))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([mins maxs])
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);

subplot (4,3,9);
ints = linspace(-diffs, diffs, 2*(nc+1));
contourf  (mod_lon, mod_depths, (sa_mod - sa_obs), ints); shading flat; colorbar;
colormap(gca,cmocean('balance', nc))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([-diffs diffs])
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);


% Density
subplot (4,3,10);
ints = linspace(minr, maxr, ncr+1);
contourf  (mod_lon, mod_depths, rho_mod, ints); shading flat; colorbar;
colormap(gca,cmocean('dense', ncr))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([minr maxr])
xlabel('Longitude'); ylabel('Depth (m)');	
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);
rlabel = text (-149,100, 'In Situ Density', 'HorizontalAlignment', 'center', 'Rotation', 90, 'FontSize', 22);

subplot (4,3,11);
contourf  (obs_lon, obs_depths, rho_obs, ints); shading flat; colorbar;
colormap(gca,cmocean('dense',ncr))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([minr maxr])
xlabel('Longitude'); 	
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);

subplot (4,3,12);
ints = linspace(-diffr, diffr, 2*(nc+1));
contourf  (mod_lon, mod_depths, (rho_mod - rho_obs), ints); shading flat; colorbar; 
colormap(gca,cmocean('balance',nc))
xlim ([xl xr]);	ylim ([0 200]); set(gca, 'YDir','reverse'); caxis ([-diffr, diffr])
xlabel('Longitude');	
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);

annotation('textbox', [0 0.875 1 0.1], 'String', ['Line P  Run ID: ' runid '  Cruise: ' cruise '   Near Surface'  ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 28)



% Output the figure

%width=6400*1.75; height=4800*1.75; resolution=300;
width=5600; height=4200; resolution=300;
set(gcf,'paperunits','inches');  set(gcf,'paperposition',[0 0 width height]./resolution);
set(gcf,'color',[1 1 1]);set(gcf,'inverthardcopy','off');

pngname = [dir_output, '/', root_output '_LineP_', cruise,  ];
print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname);
close(fig);

