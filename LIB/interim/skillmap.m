% PLOTTING AND STATISTICS FOR TIDE GAUGE ANALYSIS
% S Taylor
% Last structural changes: September 20, 2018

% USED WITH: process_tidegauge.sh
% REQUIRES : Matlab 2013+
% PURPOSE  : Reads in .mat file from tidegauge script, which contains harmonic constituents.  Plots them nicely
%            in a variety of ways (line plots, maps)

% INPUT    : List of tide gauge stations, .mat files generated with same list (order is important)

% OUTPUT   : Set of plots (phaseamp line plots, with a map showing all stations considered).  Basic stats
%            to help evaluate how good the tidal analysis is)


%  ------------------------------------------------------------------------------------------------------
%  --- INPUT INFO ---------------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------


clear all
p = genpath ('/space/hall0/sitestore/dfo/odis/stt001/PACKAGE/ETC/FXNS'); addpath(p);
warning('off', 'all');


% INPUT DATA
config = 'NEP36'; run_id = 'DI02c';
input_analysis = ['/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TG/DI02c' '/' 'TG_NOSASSA_SNR0_NEP36-DI02c_TG' '_summary'];
dir_output     = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TG/DI02c';
root_plot = 'TG_NOSASSA_SNR0_NEP36-DI02c_TG';

% Reference data
TC = {'Q1', 'O1', 'P1', 'K1', 'N2', 'M2', 'S2', 'K2'};
TC_names =['Q1'; 'O1';'P1';'K1';'N2';'M2';'S2';'K2'];		% non-cell format - some housekeeping to harmonize this with TC{}.
bathy_file = '/space/hall0/sitestore/dfo/odis/stt001/PACKAGE/ETC/REF/Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc';		% only used for landmask
switch_remove_ib = 1;

% COLORS
clr_o = [0.2 0.2 0.2];


% Load the summary file (has all tidal constituents in one place)
load (input_analysis);

%  ------------------------------------------------------------------------------------------------------
%  --- END INPUT INFO -----------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------


% [ 0 ] HOUSEKEEPING

% Constituents:
% Q1 = 1, O1 = 2,  P1 = 3,  K1 = 4,  N2 = 5,  M2 = 6, S2 = 7, K2 = 8

% Get the number of stations and constituents
[~,nstns] = size (summary_mod);		[~,ncons]  = size (TC);
es = linspace(1,nstns,nstns);

% Read in the land mask and lat lone on file
bathy = double(ncread(bathy_file, 'Bathymetry'));
lat   = double(ncread(bathy_file, 'nav_lat'));
lon   = double(ncread(bathy_file, 'nav_lon'));

land = bathy; land(land>0)=NaN; land(land==0)=-1;

stn={}; xt={};


% [ 1 ] READ IN DATA

ao = zeros(nstns,ncons);    am = zeros(nstns,ncons);    po = zeros(nstns,ncons);    pm = zeros(nstns,ncons);    %stn= zeros(nstns,1);

for n=1:nstns
	% Station
	stn{n}  = cellstr (summary_obs(n).station);		xt{n} = stn{1,n}{:}(4:9);
	mlon(n) = summary_meta(n).mod_longitude;		mlat(n) = summary_meta(n).mod_latitude;

	% Data
	for con =1:ncons
		% Now need to search the tidal constituent structure for the matching constituent.

                % First, pad the string with spaces since they're all four characters long
%                con_str = TC{1,con}
%                [~,ll] = size (con_str)
%                if     (ll == 2);   con_str = [con_str , '  ']
%                elseif (ll == 3);   con_str = [con_str , ' ']
%                end

                con_name = TC_names(con,:);

                ind = strmatch(con_name,summary_obs(n).tidal_cons.name,'exact');  
                ao (n,con) = summary_obs(n).tidal_cons.tidecon(ind,1);
                aoe(n,con) = summary_obs(n).tidal_cons.tidecon(ind,2);
                po (n,con) = summary_obs(n).tidal_cons.tidecon(ind,3);
                poe(n,con) = summary_obs(n).tidal_cons.tidecon(ind,4);


		% Same but for the model data
                ind = strmatch(con_name,summary_mod(n).tidal_cons.name,'exact');   
		am (n,con) = summary_mod(n).tidal_cons.tidecon(ind,1);
		ame(n,con) = summary_mod(n).tidal_cons.tidecon(ind,2);
		pm (n,con) = summary_mod(n).tidal_cons.tidecon(ind,3);
		pme(n,con) = summary_mod(n).tidal_cons.tidecon(ind,4);
	
	end

	skillscore(n) = summary_statistics(n).skill;
	
end

display ('[ 1 ] Data read in')
	
% [ 2 ] PLOTTING

% Make line plots of amplitude, phase, and complex difference for model, observations, and difference

% parameters for annotations / header
rayleigh = summary_meta.rayleigh;
sig_to_noise = summary_meta.sig_to_noise;
has_filters = summary_meta.filters;


for con =1:ncons

	% [ 2.1 ] Amplitude
	fig = figure;
	plot(es,0.0*es, 'Color', [0.8 0.8 0.8], 'Linewidth', 2, 'HandleVisibility','off'); hold on
	errorbar(es,ao(:,con),aoe(:,con), 'Color', clr_o, 'Linewidth', 2);
	%plot(es,ao(:,con), 'Color', clr_o, 'Linewidth', 2);
	%errorbar(es,am(:,con),ame(:,con), 'r', 'Linewidth', 2);
	plot(es,am(:,con), 'r', 'Linewidth', 2);

	annotation('textbox', [0 0.9 1 0.1], 'String', ['Amplitude of ' TC{1,con} '     Run ID: ' run_id ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)
	xlabel('Tide Gauge');   ylabel('Amplitude (m)');
	legend ('Observations', run_id , 'Mod-Obs', 'Location', 'best');
	xlim ([1,nstns]); set(gca,'xtick',es,'xticklabel',xt', 'XTickLabelRotation', 45);

	L = get(gca,'YLim'); 
        if     (L(2) >= 1.5);   set(gca,'YTick',L(1):0.2:L(2) );
        elseif (L(2) >= 1.0);   set(gca,'YTick',L(1):0.1:L(2) );
        elseif (L(2) >= 0.5);   set(gca,'YTick',L(1):0.05:L(2) );
        else;                   set(gca,'YTick',L(1):0.02:L(2)); end
	grid on;
	
	set(gca,'FontSize', 15); %set(gcf,'inverthardcopy','off');

	%annotation('textbox', [0.01 0.85 0.1 0.08] , 'String', {['Ray.: ' num2str(rayleigh)], ['SNR: ' num2str(sig_to_noise)], ['Filter: ', num2str(has_filters)] }, 'FontSize', 16, 'EdgeColor', 'none');
	resolution=100; width = 1600; height=900; set(gcf,'paperunits','inches');
	set(gcf,'paperposition',[0 0 width height]./resolution);
	pngname = [dir_output '/' root_plot '_' TC{1,con} '_amp.png'];
	print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname);
	close (fig);
	
	
	% [ 2.2 ] Phase
	fig = figure;
	
	plot(es,0.0*es, 'Color', [0.8 0.8 0.8], 'Linewidth', 2, 'HandleVisibility','off'); hold on
	errorbar(es,po(:,con),poe(:,con), 'Color', clr_o, 'Linewidth', 2);
	%plot(es,po(:,con), 'Color', clr_o, 'Linewidth', 2);
	%errorbar(es,pm(:,con),pme(:,con), 'r', 'Linewidth', 2);
	plot(es,pm(:,con), 'r', 'Linewidth', 2);

	annotation('textbox', [0 0.9 1 0.1], 'String', ['Phase of ' TC{1,con} '     Run ID: ' run_id ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)
	xlabel('Tide Gauge');   ylabel('Phase (deg)');
	legend ('Observations', run_id , 'Mod-Obs', 'Location', 'best');
	xlim ([1,nstns]); set(gca,'xtick',es,'xticklabel',xt', 'XTickLabelRotation', 45);


%	L = get(gca,'YLim') 
        a = min(min(po(:,con)));        b = min(min(pm(:,con)));        c = min(a,b);   L(1) = floor(c/5.0) * 5.0;
        a = max(max(po(:,con)));        b = max(max(pm(:,con)));        c = max(a,b);   L(2) = ceil(c/5.0) * 5.0;
        ylim (L);
        if     (L(2) - L(1) >= 200); set(gca,'YTick',L(1):20:L(2));
        elseif (L(2) - L(1) >= 100); set(gca,'YTick',L(1):10:L(2));
        elseif (L(2) - L(1) >= 50); set(gca,'YTick',L(1):5:L(2));
        elseif (L(2) - L(1) >= 20); set(gca,'YTick',L(1):2:L(2));
        else; set(gca,'YTick',L(1):1:L(2)); end

	%L(1) = round(c/5.0) * 5.0;
	%if (L(2)) > 360;	L(2) = 360;	ylim (L);	end

	%if (L(2) == 360);	set(gca,'YTick',L(1):20:L(2) );	end
	%else;			set(gca,'YTick',L(1):0.1:L(2) );	end
	grid on;
	
	set(gca,'FontSize', 15); %set(gcf,'inverthardcopy','off');
	%annotation('textbox', [0.01 0.85 0.1 0.08] , 'String', {['Ray.: ' num2str(rayleigh)], ['SNR: ' num2str(sig_to_noise)], ['Filter: ', num2str(has_filters)] }, 'FontSize', 16, 'EdgeColor', 'none');
	resolution=100; width = 1600; height=900; set(gcf,'paperunits','inches');
	set(gcf,'paperposition',[0 0 width height]./resolution);
	pngname = [dir_output '/' root_plot '_' TC{1,con} '_phase.png'];
	print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname);
	close (fig);



	% [ 2.3] Map of Phaseamp Results (amplitude is size of scatter dot, phase is its color)

	load('nclcolormap');
	

	fig = figure;
	% First the background map
	pcolor (lon,lat,land); shading flat; hold on
	cmap = (nclcolormap.NCV_bright);	cmap(1,:) = [0.6 0.6 0.6]; colormap(cmap);	caxis([0 360]);

	% scale the amplitudes by 3m
	ammax = max(max(am(:,con))); ammin = min(min(am(:,con)));

	% Multiple bubbles for the legend
	a = round(ammax, 2);
	bubsizes = a*[0.01 0.25 0.5 0.75 1.0 ];
	for ind = 1:numel(bubsizes)
	   bubleg(ind) = plot(NaN,NaN,'ro','markersize',sqrt(100*ammax*bubsizes(ind)),'MarkerFaceColor','red');
	   legentry{ind} = [num2str(round(bubsizes(ind), 2)) ' m'];
	end
	% Now plot the actual data
	h1=scatter (mlon,mlat,am(:,con)*100*ammax, pm(:,con)', 'o', 'filled'); colormap(cmap); caxis([0 360]); colorbar;
	legend([bubleg], legentry, 'Location', 'southwest', 'FontSize', 16)


	annotation('textbox', [0 0.9 1 0.1], 'String', [TC{1,con} ' Amplitude (size) and Phase (colour)    Run ID: ' run_id ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)
	%if (switch_remove_ib == 1)
	%	title (['Amplitude (size) and Phase (colour) of ' TC{1,con} ' for run ' run_id '  IB Removed'], 'FontSize', 24);
	%else
	%	title (['Amplitude (size) and Phase (colour) of ' TC{1,con} ' for run ' run_id ], 'FontSize', 24);
	%end
	xlim ([-139 -121]);

	set(gca,'FontSize', 18); %set(gcf,'inverthardcopy','off');
	resolution=200; width = 1600; height=1600; set(gcf,'paperunits','inches');
	set(gcf,'paperposition',[0 0 width height]./resolution);
	pngname = [dir_output '/' root_plot '_' TC{1,con} '_map_abs.png'];
	print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname);
	close (fig);


end

% [ 2.4 ] Map of Skill scores (uniform size, colour indicates skill)

fig = figure;
% First the background map
pcolor (lon,lat,land); shading flat; hold on
%cmap = (nclcolormap.NCV_bright);	cmap(1,:) = [0.6 0.6 0.6]; colormap(cmap);	caxis([0 1]);
cmap = cmocean ('matter',8);	cmap(1,:) = [0.6 0.6 0.6]; colormap(cmap);	caxis([0.84 1]);
cbr = colorbar;  set(cbr, 'YTick',[0.84:0.02:1.0]);

h1=scatter (mlon,mlat,100, skillscore, 'o', 'filled'); %colormap(cmap); caxis([0.85 1]); colorbar;

annotation('textbox', [0 0.9 1 0.1], 'String', ['Skill scores '  '  Run ID: ' run_id ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)

xlim ([-139 -121]);

set(gca,'FontSize', 18); %set(gcf,'inverthardcopy','off');
resolution=200; width = 1600; height=1600; set(gcf,'paperunits','inches');
set(gcf,'paperposition',[0 0 width height]./resolution);
pngname = [dir_output '/' root_plot '_map_skill.png'];
print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname);
close (fig);




% Salish Sea only

fig = figure;
% First the background map
pcolor (lon,lat,land); shading flat; hold on
cmap = cmocean ('matter',8);	cmap(1,:) = [0.6 0.6 0.6]; colormap(cmap);	caxis([0.84 1]);
cbr = colorbar;  set(cbr, 'YTick',[0.84:0.02:1.0]);

h1=scatter (mlon,mlat,125, skillscore, 'o', 'filled'); %colormap(cmap); caxis([0.85 1]); colorbar;

annotation('textbox', [0 0.9 1 0.1], 'String', ['Skill scores '  '  Run ID: ' run_id ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)

xlim ([-129 -122]); ylim ([47.5 51]);

set(gca,'FontSize', 18); %set(gcf,'inverthardcopy','off');
resolution=200; width = 1600; height=1600; set(gcf,'paperunits','inches');
set(gcf,'paperposition',[0 0 width height]./resolution);
pngname = [dir_output '/' root_plot '_map_skill_salish.png'];
print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname);
close (fig);



display ('[ 2 ] Plots done')
pause(5);

% [ 3 ] STATISTICS

% [ 3.1 ] Summary Files

apd = (am(:,:) - ao(:,:)) ./ ao(:,:);		ppd = (pm(:,:) - po(:,:)) ./ po(:,:);

% By station
for n=1:nstns
	filename = [dir_output '/' root_plot '_' stn{1,n}{:} '_summary.csv']; fid = fopen(filename, 'w');
	fprintf(fid,'#%s, %s, %s, %s, %s, %s, %s, %s, %s,\n','Constituent','Amp Obs','AmpErr Obs', 'Amp Mod','AmpErr Mod', 'Pha Obs','PhaErr Obs', 'Pha Mod', 'PhaErr Mod');
	fprintf (fid,'# Rayleigh: %f   SNR: %f    Has filters: %f \n', rayleigh,sig_to_noise,has_filters); 
	for con=1:ncons
		fprintf(fid,'%s, %f, %f, %f, %f, %f, %f, %f, %f,\n',TC{1,con}, ao(n,con),aoe(n,con), am(n,con),ame(n,con), po(n,con),poe(n,con), pm(n,con),pme(n,con) );
	end
	fclose(fid);
end


% By constituent

for con=1:ncons
	%apd = (am(:,con) - ao(:,con)) ./ ao(:,con);		ppd = (pm(:,con) - po(:,con)) ./ po(:,con);
	filename = [dir_output '/' root_plot '_' TC{1,con} '_summary.csv']; fid = fopen(filename, 'w');
	fprintf(fid,'#%s, %s, %s, %s, %s, %s, %s, %s, %s,\n','Station','Amp Obs','AmpErr Obs', 'Amp Mod','AmpErr Mod', 'Pha Obs','PhaErr Obs', 'Pha Mod', 'PhaErr Mod');
	fprintf (fid,'# Rayleigh: %f   SNR: %f    Has filters: %f \n', rayleigh,sig_to_noise,has_filters); 
	for n=1:nstns
		fprintf(fid,'%s, %f, %f, %f, %f, %f, %f, %f, %f,\n', stn{1,n}{:}, ao(n,con),aoe(n,con), am(n,con),ame(n,con), po(n,con),poe(n,con), pm(n,con),pme(n,con) );
	end
	fclose(fid);
end
pause(5)

% Skill scores
filename = [dir_output '/' root_plot '_skillscores.csv']; fid = fopen(filename, 'w');
fprintf(fid,'#%s, %s,\n','Station','Skill score');
fprintf (fid,'# Rayleigh: %f   SNR: %f    Has filters: %f \n', rayleigh,sig_to_noise,has_filters); 
for n=1:nstns
	fprintf(fid,'%s, %f, \n',stn{1,n}{:}, skillscore(n) );
end
fclose(fid);




% [ 3.2 ] Basic Statistics

% mean difference, sigma difference, skewness, mean % difference, correlations?, rmse

% By constituent only way that makes sense
filename = [dir_output '/' root_plot '_' 'statistics.csv']; fid = fopen(filename, 'w');
fprintf(fid,'#%s, %s, %s, %s, %s, %s, %s, %s,\n','Constituent','Mean Amp Diff','Mean Amp \% Diff','Mean Phase Diff', 'SD Amp %diff', 'SD Phase %diff', 'Skew Amp', 'Skew Phase');

for con=1:ncons
	fprintf(fid,'%s, %f, %f, %f, %f, %f, %f, %f,\n', TC{1,con}, mean(am(:,con) - ao(:,con)),mean(apd(:,con)), mean(ppd(:,con)), std(apd(:,con)), std(ppd(:,con)), skewness(apd(:,con)), skewness(ppd(:,con)) ) ;
end

pause(5)

display ('[ 3 ] Statistics output, script complete')


