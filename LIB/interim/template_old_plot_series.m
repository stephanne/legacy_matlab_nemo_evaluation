% PLOTTING AND STATISTICS FOR THINGS WITH A TIME SERIES
% S Taylor
% Last structural changes: September 20, 2018

% USED WITH: process_tidegauge.sh, process_adcp.sh
% REQUIRES : Matlab 2013+
% PURPOSE  : Reads in .mat file with time series info of some sort (tide gauge, adcp, etc). Plots them 
%            nicely, calculates a frequency spectra (currently no filters applied) and does some basic 
%            stats on the series.

% INPUT    : List of tide gauge stations, .mat files generated with same list (order is important)

% OUTPUT   : Set of plots, .csv with stats etc


%  ------------------------------------------------------------------------------------------------------
%  --- INPUT INFO ---------------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

clear all

p = genpath ('VAR_PATH_FXNS');
addpath(p);
warning('off', 'all');


% INPUT INFO
input_summary    = ['VAR_DIR_OUTPUT' '/' 'VAR_ROOT_OUTPUT' '_summary'];
output_directory = 'VAR_DIR_OUTPUT';
output_fileroot  = 'VAR_ROOT_OUTPUT';

bathy_file = 'VAR_FILE_BATHY';

runid = 'VAR_RUN_ID';


% PANELS
instrument = 'TIDG'	% 4 characters, TIDG, ADCP, CURM, MCTD, CCTD, BUOY, 
panels = [1,0,0,1]	% generally, u,v,ke, spectra, or t,s,rho, spectra, or ssh,spectra,

% PARAMETERS
%TC = {VAR_TIDAL_CONSTITUENTS};
nhours = VAR_NHOURS;  			% interval for n-hour averages
cast_sz = 50;
sz = 16;
%switch_remove_ib = VAR_SWITCH_REMOVE_IB;

% TIMING INFO
%obs_interval = VAR_INTERVAL_OBS;        mod_interval = VAR_INTERVAL_MODEL;
eval_start = datenum ('VAR_DATE_START', 'yyyymmdd');  str_start = 'VAR_DATE_START';
eval_final = datenum ('VAR_DATE_FINAL', 'yyyymmdd');  str_final = 'VAR_DATE_FINAL';

% COLOURS
clr_o = [0.2 0.2 0.2];


%  ------------------------------------------------------------------------------------------------------
%  --- END INPUT INFO -----------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------


% [ 0 ] LOAD THE DATA

load (input_summary);		% contains summary_obs, summary_mod, summary_stats, summary_meta.  

% Coastline / land data
bathy = double(ncread(bathy_file, 'Bathymetry'));
lat   = double(ncread(bathy_file, 'nav_lat'));
lon   = double(ncread(bathy_file, 'nav_lon'));

land = bathy; land(land>0)=NaN; land(land==0)=1;

% Info on panels
npanels = sum(panels);

% TIGHTEN THIS
if (output_fileroot(1:2) == 'TG')
	[~,nstns] = size (summary_mod);
elseif (output_fileroot(1:4) == 'ADCP')
	[nstns,~] = size (summary_mod);		% presence of multiple vertical levels causes the flip.  CM and other 1-level files should follow TG.
end

% PUT THIS IN A CONFIG FILE


if (instrument == 'TIDG')
	npanels = 2;	str_sp = [211, 000, 000, 212];		% strings labelling the subplots
	spx = 6;	spy = 2;	
	spm1 = [1,2]; 	spm2= [7,8];	spp1 = [3,6]; spp2=0; spp3=0; spp4 = [9,12];	
	xax = {'Time', [],[], 'Period (h)'};	
	yax = {'Water Level (m)', [],[], 'Power'};
	width = 3200;  height = 2400; resolution = 300;
	inc_legend = 1;
elseif (instrument == 'ADCP')
	npanels = 4;	%str_sp = [411,412,413,414];	
	spx = 6;	spy = 4;	%sp3 = [ [1 6], [11 16], [2 3 4 5], [7 8 9 10], [12 13 14 15], [17 18 19 20] ];
	spm1=[1,8];	spm2=[13,20];	spp1 = [3,6];	spp2=[9,12];	spp3=[15,18];	spp4=[21,24];
	xax = { [],[],'Time', 'Period (h)'};	
	yax = {'U (m/s)', 'V (m/s)', 'KE (m^2/s^2)', 'Power'};
	width = 3200;  height = 3600; resolution = 300;
	inc_legend = 1;
elseif (instrument == 'CURM')
	npanels = 4;	str_sp = [411,412,413,414];	
	spx = 6;	spy = 4;	%sp3 = [ [1 6], [11 16], [2 3 4 5], [7 8 9 10], [12 13 14 15], [17 18 19 20] ];
	spm1=[1,8];	spm2=[13,20];	spp1 = [3,6];	spp2=[9,12];	spp3=[15,18];	spp4=[21,24];
	xax = { [],[],'Time', 'Period (h)'};	
	yax = {'U (m/s)', 'V (m/s)', 'KE (m^2/s^2)', 'Power'};
	width = 3200;  height = 3600; resolution = 300;
	inc_legend = 1;
elseif (instrument == 'MCTD')
	npanels = 4;	str_sp = [411,412,413,414];	
	spx = 6;	spy = 4;	%sp3 = [ [1 6], [11 16], [2 3 4 5], [7 8 9 10], [12 13 14 15], [17 18 19 20] ];
	spm1=[1,8];	spm2=[13,20];	spp1 = [3,6];	spp2=[9,12];	spp3=[15,18];	spp4=[21,24];
	xax = { [],[],'Time', 'Period (h)'};	
	yax = {'Cons. Temp.', 'Abs. Salt', 'In-situ Dens.', 'Power'};
	width = 3200;  height = 3600; resolution = 300;
	inc_legend = 1;
elseif (instrument == 'BUOY')
	npanels = 2;	str_sp = [211, 000, 000, 212];		% strings labelling the subplots
	spx = 6;	spy = 2;	
	spm1 = [1,2]; 	spm2= [7,8];	spp1 = [3,6]; spp2=0; spp3=0; spp4 = [9,12];	
	xax = {'Time', [],[], 'Period (h)'};	
	yax = {'In-situ Temp.', [],[], 'Power'};
	width = 3200;  height = 2400; resolution = 300;
	inc_legend = 1;
else
	display ('Set plot parameters before running.')
	stop
end



% Use summary.meta to get the individual files with the full processed info
for s = 1:nstns

	station = summary_meta(1,s).station;

	src_filename = [output_directory, '/', output_fileroot, '_', station, '_', str_start, '-', str_final, '.mat']
	load (src_filename);
	
	out_filename = [output_directory, '/' output_fileroot '_' station];


	% Location info for the data
	olon = meta.obs_lon;	olat = meta.obs_lat;
	mlon = meta.mod_lon;	mlat = meta.mod_lat;
	nlevels = count(meta.levels);


	% Instrument switch for different quantities to plot
	% Plot labels etc here too - actually no, put that in a config file that gets called at the beginning of each figure?
	switch instrument
		case 'TIDG'
			obs_time = observations.time_reg(:);	mod_time = model.time(:);
			obs_1(1,:) = observations.residual(:);	mod_1(1,:) = model.residual(:);
			obs_2 = NaN*obs_time;			mod_2 = NaN*mod_time;
			obs_3 = NaN*obs_time;			mod_3 = NaN*mod_time;
			

		case 'ADCP', 'CURM'
			obs_time = observations.time_reg(:);	mod_time = model.time(:);
			for l=1:nlevels
				obs_1(l,:) = real(observations(l).residual(:));	mod_1(l,:) = real(model(l).residual(:));
				obs_2(l,:) = imag(observations(l).residual(:));	mod_2(l,:) = imag(model(l).residual(:));
				obs_3(l,:) = obs_1(l,:).*obs_1(l,:) + obs_2(l,:).*obs_2(l,:);
				mod_3(l,:) = mod_1(l,:).*mod_1(l,:) + mod_2(l,:).*mod_2(l,:);
			end

		case 'MCTD'
			obs_time = observations.time_reg(:);	mod_time = model.time(:);
			obs_1(1,:) = observations.ct(:);	mod_1(1,:) = model.ct(:);
			obs_2(1,:) = observations.sa(:);	mod_2(1,:) = model.sa(:);
			obs_3(1,:) = observations.rho(:);	mod_3(1,:) = model.rho(:);
	
		case 'BUOY'
			obs_time = observations.time_reg(:);	mod_time = model.time(:);
			obs_1(1,:) = observations.t(:);		mod_1(1,:) = model.t(:);
			obs_2 = NaN*obs_time;			mod_2 = NaN*mod_time;
			obs_3 = NaN*obs_time;			mod_3 = NaN*mod_time;

		case default
			display (['Unrecognized instrument: ' instrument ])
			stop
	end
		



	% [ 1 ]  PREPARE THE TIME SERIES.  Prepare isn't quite r ight - should have regularized / windowed / whatever the time series already before you get here.
	% also you're going to read in all the statistics from the individual file - no sense in duplicating things.


	for l=1:nlevels

		% Calculate how much of the observations are present (ie, how much missing data is there?)
		[npts,~] = size (obs_resid);	[nnans,~] = size (find( (isnan(obs_resid) == 1) ));	pcmissing = 100.0*nnans / npts;

		% Frequency spectra of panel 3 or 1 (if 3 is just NaNs)
	
		a = [1./240, 1./24, 1./12, 1./6, 1./3];
		TL = {'240', '24', '12', '6', '3'};

		if (max(max(obs_3)) == NaN)	% use the first panel
			win = round(length(obs_1)/8);	[fo,po] = spec1(win,obs_1);
			win = round(length(mod_1)/8);	[fm,pm] = spec1(win,mod_1);
		else
			win = round(length(obs_3)/8);	[fo,po] = spec1(win,obs_3);
			win = round(length(mod_3)/8);	[fm,pm] = spec1(win,mod_3);
		end
		

		% [ 2 ] PLOT!!!  Unhardwire as much of this as possible with a config file


		fig = figure;

		% First the maps
		subplot (spy,spx,spm1)
	        pcolor (lon,lat,land); shading flat; colormap([0.7 0.7 0.7]);   hold on;        % coastline
		xlim ([-142 -120]);	ylim ([44 60]);

		xl = round(olon,0)-2;	xr = round(olon,0)+2;
		yb = round(olat,0)-2;	yt = round(olat,0)+2;
		xc = [xl xl xl xl xl   xl+1 xl+2 xl+3   xr xr xr xr xr   xr-1 xr-2 xr-3];	
		yc = [yb yb+1 yb+2 yb+3 yt   yt yt yt   yt yt-1 yt-2 yt-3 yb   yb yb yb];
		h3 = scatter(xc,yc, cast_sz/4, 'b', '.');

	        h1 = scatter (olon,olat, cast_sz, [0.2 0.2 0.2], 'filled');
	        h2 = scatter (mlon,mlat, cast_sz, 'r', 'filled');

		if (inc_legend == 1);	legend ([h1 h2], 'Obs.', 'Model', 'Location', 'northeast');	end
	        set(gca, 'TickLength', [0.025 0.025], 'layer','top', 'Linewidth', 1.5, 'FontSize', sz);
		pos = get(gca, 'Position');	pos(1) = pos(1) - 0.04;	set(gca, 'Position', pos)



		subplot (spy,spx,spm2)
        	pcolor (lon,lat,land); shading flat; colormap([0.7 0.7 0.7]);   hold on;        % coastline
        	xlim ([xl xr]); ylim ([yb yt]);
	        h1 = scatter (olon,olat, cast_sz, clr_o, 'filled');
	        h2 = scatter (mlon,mlat, cast_sz, 'r', 'filled');
	        set(gca, 'TickLength', [0.025 0.025], 'layer','top', 'Linewidth', 1.5, 'FontSize', sz);
		pos = get(gca, 'Position');	pos(1) = pos(1) - 0.04;	set(gca, 'Position', pos)



	
		% Now thetime series

		subplot (spy,spx, spp1)
		plot (obs_time, mod_1(l,:), 'Color', clr_o, 'Linewidth', 1); hold on
		plot (mod_time, mod_1(l,:), 'r', 'Linewidth', 1);
		datetick ('x', 'mmm'); axis tight; ylabel (yax{1,1}); xlabel (xax{1,1});
		set (gca, 'Linewidth', 2, 'FontSize', 16, 'TickLength', [0.025 0.025]);
		pos = get(gca, 'Position');	pos(1) = pos(1) + 0.04;	set(gca, 'Position', pos)
		%pos = get(gca, 'Position');	pos(3:4) = 0.8*pos(3:4);	set(gca, 'Position', pos)
		grid on;

		if (npanels == 4)
			subplot (spy,spx,spp2)
			plot (obs_time, obs_2(l,:), 'Color', clr_o, 'Linewidth', 1); hold on
			plot (mod_time, mod_2(l,:), 'r', 'Linewidth', 1);
			datetick ('x', 'mmm'); axis tight; ylabel (yax{1,2}); xlabel (xax{1,2});
			set (gca, 'Linewidth', 2, 'FontSize', 16, 'TickLength', [0.025 0.025]);
			pos = get(gca, 'Position');	pos(1) = pos(1) + 0.04;	set(gca, 'Position', pos)

			subplot (spy,spx,spp3)
			plot (obs_time, obs_3(l,:), 'Color', clr_o, 'Linewidth', 1); hold on
			plot (mod_time, mod_3(l,:), 'r', 'Linewidth', 1);
			datetick ('x', 'mmm'); axis tight; ylabel (yax{1,3}); xlabel (xax{1,3});
			set (gca, 'Linewidth', 2, 'FontSize', 16, 'TickLength', [0.025 0.025]);
			pos = get(gca, 'Position');	pos(1) = pos(1) + 0.04;	set(gca, 'Position', pos)
		end

	subplot (spy,spx,spp4)
	plot(log(fo), po, 'Color', clr_o, 'LineWidth', 2); hold on;		% hacky semilog plot so can have only specified ticks
	plot(log(fm), pm, 'r', 'LineWidth', 2);
	set (gca, 'xtick', log(a), 'xticklabel', TL);
	ylabel (yax{1,4}); xlabel (xax{1,4});
	%xlim([0 0.2]); %ylim ([0 8.0e-4]);
	set (gca, 'Linewidth', 2, 'FontSize', 16, 'TickLength', [0.025 0.025]);
	pos = get(gca, 'Position');	pos(1) = pos(1) + 0.04;	set(gca, 'Position', pos)
	grid on;

	% text box with parameter values (use annotation since scale on plot will change)
	rayleigh = meta.rayleigh;
	sig_to_noise = meta.sig_to_noise;
	has_filters = meta.filters;
	%annotation('textbox', [0.66 0.35 0.2 0.08] , 'String', {['% Missing: ', num2str(round(pcmissing,2))], ['Skill: ', num2str(round(summary_statistics(s).skill , 3) )] }, 'FontSize', 16, 'EdgeColor', 'none');
	%annotation('textbox', [0.66 0.37 0.1 0.08] , 'String', {['Ray.: ' num2str(rayleigh)], ['SNR: ' num2str(sig_to_noise)], ['Filter: ', num2str(has_filters)], ['% Missing: ', num2str(pcmissing)] }, 'FontSize', 16, 'EdgeColor', 'none');



	% Title and annotations with info about data / statistics
	str_title = [station '   Run ID: ' runid '  ]
	annotation('textbox', [0 0.9 1 0.1], 'String', str_title, 'EdgeColor','none', 'HorizontalAlignment','center', 'FontSize',20 )
	

	%if (output_fileroot(1:2) == 'TG')
	%	annotation('textbox', [0 0.9 1 0.1], 'String', [station ' Residual   Run ID: ' runid '   % Missing: ' num2str(round(pcmissing,2)) '   Skill: ' num2str(round(summary_statistics(s).skill , 3)) ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)
	%	depth_string = '';
	%elseif (output_fileroot(1:4) == 'ADCP')
	%	annotation('textbox', [0 0.9 1 0.1], 'String', ['Residual    Run ID: ' runid '    ' instrument ': ' station '    Inst. Depth: ' num2str(meta.inst_depth) ' m    Analysis Depth: ' num2str(meta.depth(l)) ' m'], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)
	%	depth_string = ['_' num2str(meta.depth(l))]
	%elseif (output_fileroot(1:2) == 'CM')
	%	annotation('textbox', [0 0.9 1 0.1], 'String', ['Residual    Run ID: ' runid '    ' instrument ': ' station '    Inst. Depth: ' num2str(meta.inst_depth) ' m    Analysis Depth: ' num2str(meta.depth) ' m'], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)
	%	depth_string = ['_' num2str(meta.depth(l))]
	%end



	% PRINT!
	set(gcf,'paperunits','inches'); set(gcf,'paperposition',[0 0 width height]./resolution);
	print('-dpng', ['-r' num2str(resolution)], '-zbuffer', [out_filename depth_string '_res-spec']);
	pause(3);	close(fig);






	% Clear some variables
	clear obs_time_i  obs_total  obs_total_i  obs_resid  obs_resid_i
	clear oa tea


	end % level loop

	display (['[ ' num2str(s) ' ] Station ' station ' complete.']  )


end
