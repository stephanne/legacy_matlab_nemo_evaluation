% SST BUOY ANALYSIS AND COMPARISON
% S Taylor
% Last structural changes: Stephember 25, 2018

% USED WITH: process_ctd.sh
% REQUIRES : Matlab 2013+, gsw package
% PURPOSE  : Uses a time series file (single file only, currently) with in-situ surface temperature
%            and compares it with model data. 

% INPUT    : Takes time series files generated by SER and observation files for each tide gauge 
%            station requested, and does a comparison between the two. 

% OUTPUT   : All output in a .mat file for plotting.


%  ------------------------------------------------------------------------------------------------------
%  --- INPUT INFO ---------------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

clear all
p = genpath ('/space/hall0/sitestore/dfo/odis/stt001/PACKAGE/ETC/FXNS');
addpath(p);


% Directories and filename roots
dir_obs    = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/DATA/BUOY';
dir_model  = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/SER_DI02d';		root_model  = 'NEP36-DI02d_1h';
dir_output = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/BUOY';		root_output = 'BUOY_NEP36-DI02d';


% Input info on buoys
station_list = 'info_buoy.csv'	% contains the location info etc

% Timing and variables. var_dim can be either depth (for a cast) or time (for a mooring).
var_mod_time = 'time_counter';		var_lat  = 'nav_lat';
var_mod_temp = 'tos';	var_mod_salt = 'sos';	var_obs_temp = 'Temp';	

% Parameters
eval_start = datenum ('20160101', 'yyyymmdd');  str_start = '20160101';
eval_final = datenum ('20161231', 'yyyymmdd');  str_final = '20161231';
[sy,sm,sd] = datevec(eval_start);               [fy,fm,fd] = datevec(eval_final);
ix = 2;	iy = 2;
%nnans = 120;	% minimum number of 10 min intervals that, if all nans, need to be painted back in to the interpolated variables.
single_file = 1;

switch_careful_time = 0;

% Metadata
note='surface_buoys';
user='stt001';

%  ------------------------------------------------------------------------------------------------------
%  --- END OF INPUT INFO --------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

% [ 0 ] HOUSEKEEPING AND INITIAL INPUT 

% Get station codes.  Use the same file as everything else to stay consistent
tmp = readtable(station_list, 'Delimiter',',','ReadVariableNames',false);  % need read variable names switch if there isn't a header line
codeinfo = table2struct (tmp);
[nfiles,~] = size(codeinfo);	% can have more than one observation file for a mooring station (ie with the same short code)



% Loop over stations, get the data, interpolate, run through ttide.

for n = 1:nfiles

	% Get the station code	
	station = codeinfo(n).Var2


	% [ 1 ] OBSERVATIONS

	obs_filename = [dir_obs '/' codeinfo(n).Var1 '.nc']; % info_obs_stn{n,1};

	obs_time  = double(ncread(obs_filename, 't'));
	obs_dt    = double(ncread(obs_filename, 'dt'));
	obs_lon   = double(ncread(obs_filename, 'longitude')); if (obs_lon > 0); obs_lon = -obs_lon; end	% some buoys in deg W not deg E
	obs_lat   = double(ncread(obs_filename, 'latitude'));
	obs_temp  = double(ncread(obs_filename, 'Temp')); %var_obs_temp));


	% Time info is a buried in the "units" attibute of the variable time.  (Time is integer number of seconds
	% from the [specified] start of the data collection.)
	obs_time_str = ncreadatt (obs_filename, 't', 'units');
	obs_time_str = obs_time_str(14:end);
	%obs_stamp = datenum (obs_time_str, 'yy-mm-dd HH:MM:SS');	%ctds have a single time stamp
        obs_start = datenum (obs_time_str, 'yy-mm-dd HH:MM:SS');
        obs_time = obs_time/86400 +obs_start;   % datenum is in days, t is in seconds



	% Convert the pressure to a depth, using the GSW package.  Note no ssh variable available.
	% This routine assumes standard seawater at 0 degrees.  Check with Hauke about how appropriate this is - 
	% probably need to use all three thermodynamic variables (and so a more sophisticated routine).  For testing, this
	% is fine.

	[nt,~] = size(obs_time);

	for t = 1:nt
		obs_time_abs(t) = obs_start + obs_time(t)/86400;	% obs_t is in seconds, need it in datenum.
	end

	% Set the window of the moored data
	%obs_start = obs_time(1);	q=find(~isnan(obs_time)); obs_final = obs_time(q(end));
	% if last entry in the dataset is a nan, this causes problems. 

        ind = find (obs_time >= eval_start & obs_time < eval_final);
        obs_time = obs_time(ind);	obs_temp = obs_temp(ind);


		% often have large gaps in data - check if one at beginning or end of evaluation window
		q=find(~isnan(obs_time));	obs_time = obs_time(q);	obs_temp = obs_temp(q);
		obs_start = obs_time(1);	q=find(~isnan(obs_time)); obs_final = obs_time(q(end));

        	obs_time_orig = obs_time;

	        % Adjust the observations as needed - NaN wonky values.  
	        obs_temp (obs_temp> 100) = NaN; obs_temp (obs_temp< -100) = NaN;

	if (switch_careful_time == 1)		% if doing a very precise analysis - likely no need, but leave it in case you need it
        	% Gaps in data need to be filled with NaNs too.
	        obs_time_reg = datenum(obs_time(1)):datenum(0,0,0,0,60,0):datenum(obs_time(end));
	        obs_temp_reg = NaN .* obs_time_reg;
	        [~,inda,indb] = intersect (round(obs_time,3), round(obs_time_reg,3));
	        obs_temp_reg(indb) = obs_temp(inda);
	        %indc = find(isnan(obs_ssh_reg));  % only needed is need to reimpose NaNs after some sort of filtering - unlikely here

	%if (switch_careful_time == 1)		% if doing a very precise analysis - likely no need, but leave it in case you need it
		% Save to structures
		observations.t      = obs_temp_reg;
		observations.time   = obs_time_reg;
	end

	rawobs.pt     = obs_temp;
	rawobs.time   = obs_time;


	% Something in here to check if there's a big gap and then the series continues but not strictly on the hour
	% buoy 46211, for ex, has a bit of data, then a two month gap, and starts up again in early March for the rest of the year.
	% want to take the longer of the two sections
	





% NOV 5: THIS BIT BELOW IS BANANAS, DO NOT USE IT
	
%	% Obserations are in-situ temperature, but no salinity data to convert to potential temperature
%	%obs_pt     = gsw_pt0_from_t (obs_sa, obs_t, obs_pres);		% reference pressure at surface set to 0 dbar - may not the the case (ask Hauke)
%	%obs_ct     = gsw_CT_from_pt (obs_sa, obs_pt);			% need for gsw calculations
%
%	% Calculate in-situ density
%	%obs_rho    = gsw_rho (obs_sa, obs_ct, obs_pres);
%	
%
%	% Interpolate to standard model grid so that differences / bias / etc can be calculated.
%	
%	mod_filename = dir([dir_model, '/', root_model, '_SER_', station, '-09*.nc' ])
%	mod_filename = mod_filename(1).name;
%	mod_filename = [dir_model, '/', mod_filename]
%	mod_time     = double(ncread(mod_filename, var_mod_time));
%	mod_time = datenum(f_datestr(mod_time));
%
%
%	%obs_t_int   = interp1 (obs_time_abs, obs_t, mod_time, 'spline');
%	
%	k = find(~isnan(obs_temp));
%	obs_temp_int = interp1 (obs_time_abs(k),obs_temp(k),mod_time);
%	% find large gaps in original data, set to NaN between those time stamps
%	% find consecutive strings of nans, any longer than a set value are reset to NaN in the interpolated variable.
%	a = diff(k);
%	b = find([a' inf]>1)	
%	c = diff([0 b])		% length of the sequences
%	d = cumsum(c) 		% endpoints of the sequences
%	e = d - c		% startpoints of the sequences
%	f = find(c>nnans)	% which sequences match the criteria
%	g = e(f); g=g(g>0);
%	h = d(f); h=h(h>0);	% start and end points of bands to set to Nan
%	i = obs_time_abs(g)
%	j = obs_time_abs(h)
%	for m = 1:numel(k)
%		k(m) = find(mod_time == round (i,1))
%		l(m) = find(mod_time == round (j,1))
%		obs_temp_int(k(m):l(m)) = NaN;
%	end
	
%	stop

%  END OF PART TO CHOP








	
	% [ 2 ] MODEL

	% The structure of this requires all the variables to be in a single file.  (This is the the usual output from EXT and ROT scripts - 
	% errors are thrown if variables are missing, and presumably you addressed that before running this.)


	% Series is already compiled in an earlier chunk of the process_script - just need to read in the one file
	mod_filename = [dir_model, '/', root_model, '_SER_', station, '-09_', str_start, '-', str_final, '.nc' ];
	mod_time   = double(ncread(mod_filename, var_mod_time));
	mod_pt     = double(ncread(mod_filename, var_mod_temp));
	mod_s      = double(ncread(mod_filename, var_mod_salt));	% need salt so can get to in-situ temperature
	mod_ib     = double(ncread(mod_filename, 'ssh_ib')); 		% variability of surface pressure
	mod_lat    = double(ncread(mod_filename, 'nav_lat'));
	mod_lon    = double(ncread(mod_filename, 'nav_lon'));


	% Take only the middle point
	mod_pt  = squeeze (mod_pt(ix,iy,:,:));	mod_s   = squeeze (mod_s(ix,iy,:,:));
	mod_lat = squeeze (mod_lat(ix,iy));	mod_lon = squeeze (mod_lon(ix,iy));

	% Take only the day(s) specified by the observations
	% Chop first to evaluateion window, then by window of observations
        mod_time = datenum(f_datestr(mod_time));
        ind = find (mod_time >= eval_start & mod_time < eval_final);
        mod_time = mod_time(ind);       mod_pt = mod_pt(ind);     mod_s = mod_s(ind);

	if (switch_careful_time == 1)
        	ind = find (mod_time >=  datenum(obs_time(1)) & mod_time <= datenum(obs_time(end)) );
	        mod_time = mod_time(ind);       mod_pt = mod_pt(ind);     mod_s = mod_s(ind);
	else
		% want everything in terms of full days
        	ind = find (mod_time >=  round(datenum(obs_time(1))) & mod_time <= round(datenum(obs_time(end))) );
	        mod_time = mod_time(ind);       mod_pt = mod_pt(ind);     mod_s = mod_s(ind);
	end
        [mod_nt,~] = size (mod_time);
	%stop
	%mod_time = mod_time(ind);
	%mod_pt   = mod_pt  (ind);
	%mod_s    = mod_s   (ind);

	% If doing the very precise analysis, already interpolated observation data to match 1h time axis, so no need to interpolate in time here.
	% Otherwise, interpolate observations to model time axis
	if (switch_careful_time ~= 1)

		obs_time_reg = mod_time;
		obs_time_av = datenum(dateshift(datetime(datestr(obs_time)), 'start', 'hour', 'nearest') + minutes(30));	
		[~,inda,indb] = intersect (obs_time_av, mod_time);
                %obs_temp_reg(indb) = obs_temp(inda);

                %[~,inda,indb] = intersect (round(obs_time,3), round(obs_time_reg,3));
                %obs_temp_reg(indb) = obs_temp(inda);

		obs_temp_reg = interp1 (obs_time,obs_temp, mod_time, 'spline');

		% Paint NaNs back in - interp just glosses over them.
		obs_mask = NaN*obs_time_reg;	% missing time intervals
		obs_mask(indb) = 1.0;

		indd = find(isnan(obs_temp));	% explicit NaNs in observation data
		vals = datenum(dateshift(datetime(datestr(obs_time(indd))), 'start', 'hour', 'nearest')+minutes(30));
		[nv,~] = size(vals)
		if (nv > 0)
			for v = 1:nv
				inde = find( mod_time== vals(v,1));	% round obs_time to the nearest half hour (hourly interval)
				obs_mask(inde) = NaN;
			end
		end

		obs_temp_reg = obs_temp_reg .* obs_mask;
		%obs_temp_reg(inde) = NaN;
	

		observations.t      = obs_temp_reg;
		observations.time   = obs_time_reg;
	end
	
	% Convert salinity to absolute salinity.
	mod_pres = 0;
	mod_sa = gsw_SA_from_SP (mod_s, mod_pres, mod_lon, mod_lat);

	% Calculate in-situ temperature to compare with observations
	% this requires surface air pressure - unclear if available.  Use an absolute value for now - can diagnose from IB if have a zero point
	% impact of specifc value of pressure is very small - absolute value is fine for this analysis
	% units are decibars
	mod_t = gsw_t_from_pt0 (mod_sa, mod_pt,  10.1325);
	

	% Save to structures
	model.t    = mod_t;
	model.pt   = mod_pt;
	model.s    = mod_s;
	model.sa   = mod_sa;
	model.time = mod_time;


	% [ 3 ] DAILY AVERAGES
	
	mod_t_av = reshape (mod_t, 24,[]);	obs_t_av = reshape (obs_temp_reg, 24,[]);	mod_time_av = reshape (mod_time, 24, []);
	mod_t_av = nanmean (mod_t_av, 1);	obs_t_av = nanmean (obs_t_av, 1);		mod_time_av = nanmean (mod_time_av, 1);
	
	model.daily_t = mod_t_av;
	model.daily_time = mod_time_av;
	observations.daily_t = obs_t_av;
	observations.daily_time = mod_time_av;


	% [ 4 ] STATISTICS
	% Comparative stats
	% mod and obs not necessarily on same time axis - consider interpolating obs to mod for the sake of statistics.
	%obs_res_forstats = interp1(obs_time_reg_1h, obs_res, mod_time, 'spline');

	if (switch_careful_time == 1)
		mod_temp_forstats = interp1(mod_time, mod_t, obs_time_reg, 'spline');
	else
		mod_temp_forstats = mod_t;
	end

	% average every n hours (try 3 initially)
	nav = 3;
	mod_temp_forstats_av = movmean(mod_temp_forstats, nav, 'omitnan');
	obs_temp_forstats_av = movmean(obs_temp_reg, nav, 'omitnan');

	[mean_diff_temp, std_diff_temp, rmse_diff_temp, rae_diff_temp, corr_temp, skew_temp, skill_temp] = f_residual_stats( mod_temp_forstats_av, obs_temp_forstats_av );
	[skill_susan, skill_gamma] = f_skill_scores (mod_temp_forstats_av, obs_temp_forstats_av );
	summary_statistics(n).mean_bias = mean_diff_temp;
	summary_statistics(n).std_bias  = std_diff_temp;
	summary_statistics(n).rmse      = rmse_diff_temp;
	summary_statistics(n).relaverr  = rae_diff_temp;
	summary_statistics(n).correlation = corr_temp;
	summary_statistics(n).skewness    = skew_temp;
	summary_statistics(n).skill       = skill_temp;
	summary_statistics(n).skill_susan = skill_susan;
	summary_statistics(n).skill_gamma = skill_gamma;




	% [ 5 ] METADATA AND SUMMARY

        meta.input_obs = obs_filename;
        meta.input_mod = mod_filename;
	meta.station   = station;
        meta.obs_start = datestr(obs_start);
        meta.obs_final = datestr(obs_final);
        meta.obs_lat = obs_lat; meta.obs_lon = obs_lon;
        meta.mod_lat = mod_lat; meta.mod_lon = mod_lon;
        meta.note = note;
        meta.user = user;

	summary_meta(n) = meta;


	% [ 6 ] SAVE AND RESET FOR NEXT PASS

        % Save everything for one mooring in a .mat file
        file_output = [dir_output '/' root_output '_BUOY_'  station '_' str_start '-' str_final ]

        save (file_output, 'observations', 'model', 'rawobs', 'meta');

	summary_mod(n) = model;
	summary_obs(n) = observations;
	summary_rawobs(n) = rawobs;
	summary_meta(n) = meta;

        % Clear the variables
        clear model observations rawobs meta 
	clear obs_t obs_time mod_t mod_time mod_s
	clear indd inde


end


if (single_file == 1)
	file_output = [dir_output '/' root_output '_BUOY_summary' ]
	save (file_output, 'summary_mod', 'summary_obs', 'summary_rawobs', 'summary_meta', 'summary_statistics');
end
