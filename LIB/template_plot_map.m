% PLOTTING A STATISTIC ON A MAP
% S Taylor
% Last structural changes: December 19, 2018

% USED WITH: 
% REQUIRES : Matlab 2013+
% PURPOSE  : Reads in .mat file from tidegauge script, which contains harmonic constituents.  Plots them nicely
%            in a variety of ways (line plots, maps)

% INPUT    : List of tide gauge stations, .mat files generated with same list (order is important)

% OUTPUT   : Set of plots (phaseamp line plots, with a map showing all stations considered).  Basic stats
%            to help evaluate how good the tidal analysis is)


%  ------------------------------------------------------------------------------------------------------
%  --- INPUT INFO ---------------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------


clear all

p = genpath ('VAR_PATH_FXNS');
addpath(p);
warning('off', 'all');


% INPUT INFO
input_summary    = ['VAR_DIR_INPUT' '/' 'VAR_ROOT_INPUT' '_summary'];
input_directory  = 'VAR_DIR_INPUT';  input_fileroot   = 'VAR_ROOT_INPUT';
output_directory = 'VAR_DIR_OUTPUT'; output_fileroot  = 'VAR_ROOT_OUTPUT';

bathy_file = 'VAR_FILE_BATHY';

run_id = 'VAR_RUN_ID';


% CONFIGURATION
type_analysis    = 'VAR_TYPE_ANALYSIS';
str_start = 'VAR_DATE_START';   str_final = 'VAR_DATE_FINAL';
VAR_FILE_CONFIG_REF;
VAR_FILE_CONFIG_SPECIFIC;

%  ------------------------------------------------------------------------------------------------------
%  --- END INPUT INFO -----------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------


% [ 0 ] HOUSEKEEPING

% Load the data 
load ([input_directory '/' input_fileroot '_statistics.mat']);

% harmonize the variables ( in case you want to use it for non-ciops variables)
summary_meta = ciops_meta;	[~,nstns] = size(summary_meta);
stats_temp = ss_temp;	% this presumes that only thermodynamic variables are being evaluated.  Instrument switch here???
if (exist('ss_salt', 'var') == 1);	stats_salt = ss_salt;	else;	stats_salt = NaN;	end;
if (exist('ss_dens', 'var') == 1);	stats_dens = ss_dens;	else;	stats_dens = NaN;	end;

% Read in all requested variables from the structure(s) - add in support for salt / density (inelegant, ugh)
for s = 1:nstns
       	mlon(s) = summary_meta(s).mod_lon;	mlat(s) = summary_meta(s).mod_lat;
	for v = 1:nvars
		%varname {1,v}	% CHECK HOW TO DO THIS PROPERLY

		statistics (v,s) = stats_temp(s).(varnames{1,v});
	end
end

% remove the ones that are very short, sporadic, or otherwise not great
% loop is clunky but clear
statistics (1,inds_nan) = NaN;		% inds is specified in the config_plot_map file.  NaNing for one variable means naning for all variables.
inda = find(~isnan(statistics(1,:)));
statistics = statistics (:,inda);	% check this works
mlon = mlon (inda);	mlat = mlat (inda);


% PLOT!  One per requested variable

for v=1:nvars
	fig = figure;
	pngname = [output_directory '/' output_fileroot '_map_' varnames{1,v} '.png'];
	f_titleannot ([vartitles{1,v}  '  Run ID: ' run_id ], 20);

	pcolor (lon,lat,land); shading flat; hold on
	cmap = cmocean (clr_bar{v}, nclrs(v));	
	% set land value in here if needed since you can't figure out how ot have two colour bars
	cmap(1,:) = clr_land;
	colormap(cmap);	cbr = colorbar; 
	
	caxis (cax{v});	set(cbr, 'YTick', ctk{v} );

	h1=scatter (mlon,mlat,2*sz_scatter, statistics(v,:), 'o', 'filled');

	curax = gca;	f_fmtaxes (curax, 'Longitude', 'Latitude', 0, 0, sz_font, tk, +0.0);	xlim (xm); ylim(ym);
	curfig = gcf;	f_figprint (curfig, pngname, width,height,resolution)

	close(fig)

end

