
% Calculate a series of standard statistics on a set of data.  Data is by station, and can be either a time
% series or a depth profile.  Either way, calculate the series along that axis, do some aggreagate stats as
% well (coming soon???) and generate both .mat and .csv files with the data.  (Add statistics structures to
% input files, maybe??? (throw an error / stop if you're overwriting)

clear all

p = genpath ('VAR_PATH_FXNS');addpath(p);
warning('off','all');

% ADD CONFIG AND REFERENCE FILES HERE
file_reference = 'VAR_FILE_REFERENCE';
file_config = 'VAR_FILE_CONFIG';


% INPUT DATA:  not entirely generalized but one thing at a time

switch_ciops = VAR_SWITCH_CIOPS;	file_ciops = ['VAR_DIR_CIOPS' '/' 'VAR_ROOT_CIOPS' '_summary.mat' ];
switch_riops = VAR_SWITCH_RIOPS;	file_riops = ['VAR_DIR_RIOPS' '/' 'VAR_ROOT_RIOPS' '_summary.mat' ];
switch_hindcast = VAR_SWITCH_HINDCAST;	file_hindcast = ['VAR_DIR_HINDCAST' '/' 'VAR_ROOT_HINDCAST' '_summary.mat' ];
switch_ref   = VAR_SWITCH_REF;		file_ref   = ['VAR_DIR_REF' '/' 'VAR_ROOT_REF' '_summary.mat'];


% OUTPUT DIRECTORY:
dir_output = 'VAR_DIR_OUTPUT';		root_output = 'VAR_ROOT_OUTPUT';


% PARAMETERS
type_analysis = 'VAR_TYPE_ANALYSIS'	% SST, TSD, CURRENT, WATERLEVEL
%switch_chop = VAR_SWITCH_CHOP;	date_start = datenum('VAR_DATE_START', 'yyyymmdd');	date_final = datenum('VAR_DATE_FINAL', 'yyyymmdd');


% ----------------------------------------------------------------------------------------------------
% ----------------------------------------------------------------------------------------------------
% ----------------------------------------------------------------------------------------------------

% more necessary for plots at this point - revisit if needed
%load (file_reference);
%load (file_config);


% [ 0 ] Read in the data from the metadata file.  All initialized with NaN in file_reference.

if (switch_ciops == 1);		load (file_ciops);	ciops_meta = summary_meta;	ciops_obs = summary_obs;	ciops_mod = summary_mod;	end
if (switch_riops == 1);		load (file_riops);	riops_meta = summary_meta;	riops_obs = summary_obs;	riops_mod = summary_mod;	end
if (switch_hindcast == 1);	load (file_hindcast);	hindcast_meta = summary_meta;	hindcast_obs = summary_obs;	hindcast_mod = summary_mod;	end
if (switch_ref == 1);		load (file_ref);	ref_meta = summary_meta;	ref_obs = summary_obs;		ref_mod = summary_mod;		end

[~,nstns] = size(ciops_meta);

% [ 0.5 ] Chop to the specified dates, if requested. NOPE.

%if (switch_chop == 1)
%	if (switch_ciops == 1);		
%		for s = 1:nstns
%			ind = find(ciops_obs(s).time(:) >=date_start & ciops_obs(s).time(:) <= date_final);
%			% this gets really clunky realyl quickly.  remove this whole functionality
%end


% Check that all the same station - if commonly a problem then add a thing to map one to the other before the loop?


% [ 1 ] Individual station statistics

switch type_analysis 

	case 'SST'

		for s =1:nstns
			if (switch_ciops == 1);		ss_temp(s)       = f_stats_single (ciops_obs(s).t, ciops_mod(s).t);		end
			if (switch_riops == 1);		ss_temp_riops(s) = f_stats_single (ciops_obs(s).t, riops_mod(s).t);	 	end
			if (switch_hindcast == 1);	ss_temp_hc(s)    = f_stats_single (ciops_obs(s).t, hindcast_mod(s).t);	end
			if (switch_ref  == 1);		ss_temp_ref(s)   = f_stats_single (cmc_sst_obs(s).t, ciops_mod(s).t);		end
		end

		if (switch_ciops ~= 1);		ss_temp = NaN;		end
		if (switch_riops ~= 1);		ss_temp_riops = NaN;	end
		if (switch_hindcast ~= 1);	ss_temp_hc = NaN;	end
		if (switch_ref ~= 1);		ss_temp_ref = NaN;	end


	case 'TSD'	% temp, salinity, density
		for s =1:nstns
			if (switch_ciops == 1)
				ss_temp(s) = f_stats_single (ciops_obs(s).obs_ct, ciops_mod(s).mod_ct);
				ss_salt(s) = f_stats_single (ciops_obs(s).obs_sa, ciops_mod(s).mod_sa);
				ss_dens(s) = f_stats_single (ciops_obs(s).obs_rho, ciops_mod(s).mod_rho);
			end
			if (switch_riops == 1)
				ss_temp_riops(s) = f_stats_single (riops_obs(s).obs_ct, riops_mod(s).mod_ct);
				ss_salt_riops(s) = f_stats_single (riops_obs(s).obs_sa, riops_mod(s).mod_sa);
				ss_dens_riops(s) = f_stats_single (riops_obs(s).obs_rho, riops_mod(s).mod_rho);
			end
			if (switch_hindcast == 1)
				ss_temp(s) = f_stats_single (hindcast_obs(s).obs_ct, hindcast_mod(s).mod_ct);
				ss_salt(s) = f_stats_single (hindcast_obs(s).obs_sa, hindcast_mod(s).mod_sa);
				ss_dens(s) = f_stats_single (hindcast_obs(s).obs_rho, hindcast_mod(s).mod_rho);
			end
			if (switch_ref == 1)
				ss_temp(s) = f_stats_single (ref_obs(s).obs_ct, ref_mod(s).mod_ct);
				ss_salt(s) = f_stats_single (ref_obs(s).obs_sa, ref_mod(s).mod_sa);
				ss_dens(s) = f_stats_single (ref_obs(s).obs_rho, ref_mod(s).mod_rho);
			end
		end

	case 'CURRENT'	% u,v velocities, KE
	case 'WATERL_LEVEL'	% water level
	otherwise
		display (['Unrecognized value of type_analysis:  ' type_analysis]);
		stop
		


end

% [ 2 ] Aggregate statistics?

% [ 3 ] Metadata?


% [ 4 ] Save .mat and .csv files


switch type_analysis
	case 'SST'

		filename = [dir_output '/' root_output '_statistics_temp.csv'];		f_stats_csv (filename, ss_temp, ciops_meta );
		
		filename  = [dir_output '/' root_output '_statistics.mat'];
		save (filename, 'ss_temp', 'ss_temp_riops', 'ss_temp_hc', 'ss_temp_ref', 'ciops_meta');


	case 'TSD' 

		filename = [dir_output '/' root_output '_statistics_temp.csv'];		f_stats_csv (filename, ss_temp, ciops_meta );
		filename = [dir_output '/' root_output '_statistics_salt.csv'];		f_stats_csv (filename, ss_salt, ciops_meta );
		filename = [dir_output '/' root_output '_statistics_density.csv'];	f_stats_csv (filename, ss_dens, ciops_meta );
		filename  = [dir_output '/' root_output '_statistics.mat'];
		save (filename, 'ss_temp', 'ss_temp_riops', 'ss_temp_hc', 'ss_temp_ref', 'ss_salt', 'ss_salt_riops', 'ss_salt_hc', 'ss_salt_ref',  'ss_dens', 'ss_dens_riops', 'ss_dens_hc', 'ss_dens_ref',    'ciops_meta');


	case 'CURRENT'
		display ('write this');

	case 'WATER_LEVEL'
		display ('write this');

	otherwise
		display ('This should never appear')

end

















		


        % [ 3 ] BASIC STATISTICS

        % mean, bias, correlation.  Should probably do the "ensure they're on the same time axis" thing earlier
%        obs_temp_mean = nanmean (obs_temp_i);           mod_temp_mean = nanmean (mod_temp_i);
%        obs_salt_mean = nanmean (obs_salt_i);           mod_salt_mean = nanmean (mod_salt_i);
%        obs_rho_mean  = nanmean (obs_rho_i);            mod_rho_mean  = nanmean (mod_rho_i);

%        corr_temp = corrcoef (mod_temp_i,obs_temp_i);   diff_temp = mod_temp_i - obs_temp_i;    bias_temp = nanmean (diff_temp);
%        corr_salt = corrcoef (mod_salt_i,obs_salt_i);   diff_salt = mod_salt_i - obs_salt_i;    bias_salt = nanmean (diff_salt);
%        corr_rho  = corrcoef (mod_rho_i ,obs_rho_i );   diff_rho  = mod_rho_i  - obs_rho_i ;    bias_rho  = nanmean (diff_rho );

%        mod_temp_mean = 0; corr_temp = 0; diff_temp = 0; bias_temp = 0;
%        save ([out_filename, '_statistics'], 'obs_temp_mean','mod_temp_mean', 'obs_salt_mean','mod_salt_mean', 'obs_rho_mean', 'mod_rho_mean', 'corr_temp', 'corr_salt', 'corr_rho', 'diff_temp', 'diff_salt', 'diff_rho', 'bias_temp', 'bias_salt', 'bias_rho');

%        save ([out_filename, '_consistentseries'], 'obs_time_av','obs_salt_av','obs_rho_av', 'mod_time_av','mod_salt_av','mod_rho_av' )





