%
% ROTATE PAIRS OF VARIABLES OF FULL DOMAIN
% S Taylor, based on script from Li Zhai.  stephanne.taylor@dfo-mpo-gc.ca
% Last structural changes: May 3, 2018.  Harmonized with template_extract_rotate.sh

% USED WITH: process_rotate.sh
% REQUIRES : Matlab 2013+, opa_angle_zhai.m, rot_rep_2017.m
% PURPOSE  : Rotate u/v to true east/north at each point over the full model domain.  

% INPUT : Raw model files.  Be very careful running concurrently with the model, as script modifies the input files.
%         At each time interval (which can be one day or more than one day) three files are read in: t grid for 
%         coordinates (which is probably not needed) and u and v grid files for variables.

% OUTPUT: Amended raw model files.  Adds rotated variables to the original files, does not alter them further.  (New
%         variables come with attributes indicates that it is, and all attributes etc from original file are maintained.)


% Paths etc
clear
addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts/mat_BoF/function
addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts/m_map_1.4g

% Batch Info
input_root  = 'VAR_FLD_INPUT_ROOT';
output_root = 'VAR_FLD_OUTPUT_ROOT';

% Grids
gridT='VAR_GRIDT';	gridU='VAR_GRIDU';		gridV='VAR_GRIDV';

% Velocity pairs to be rotated.
npairs   = VAR_NPAIRS;
u_invar  = {VAR_U_INVARS};  v_invar  = {VAR_V_INVARS};
u_outvar = {VAR_U_OUTVARS}; v_outvar = {VAR_V_OUTVARS};

% Window of dats to be rotated.  Handles dates longer than the month alright (ie, rolls them over to the next month.)
start_day   = VAR_START_DAY;	start_month = VAR_START_MONTH;	start_year  = VAR_START_YEAR;
final_day   = VAR_FINAL_DAY;	final_month = VAR_FINAL_MONTH;	final_year  = VAR_FINAL_YEAR;

% Number of days grouped in a file
interval  = VAR_INTERVAL;

% Metadata
note = 'VAR_META_NOTE';
user = 'VAR_META_USER';


%----------------------------------------------------------------------------------------------------------------------
%--- End of user-supplied variables -----------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------------------------

% Error checking
nfiles = (datenum([final_year final_month final_day]) - datenum ([start_year start_month start_day]) + 1 ) / interval
if (floor(nfiles) ~= nfiles)
	display ('Non-integer number of intervals. Quitting')
	exit; %return;
end


% Generate the filenames (less of a headache than sifting through a dir listing)

% Initialize the day variables that change with each slice considered.  Numerical value needed for iteratin by a 
% specified number of days.
dae = datenum([start_year start_month start_day]);


% Get coordinate file set up
firstday = datestr (dae, 'yyyymmdd');
seconday = datestr (dae + interval - 1, 'yyyymmdd');
u_input_filename = [input_root '_' gridU '_' firstday '-' seconday '.nc'];
v_input_filename = [input_root '_' gridV '_' firstday '-' seconday '.nc'];
t_input_filename = [input_root '_' gridT '_' firstday '-' seconday '.nc'];


% Check here if ruo and rvo already exist. If they do, just copy the files.
switch_skip = 0
u_inputinfo = ncinfo(u_input_filename); u_inputvars = {u_inputinfo.Variables.Name};
v_inputinfo = ncinfo(u_input_filename); v_inputvars = {v_inputinfo.Variables.Name};
[~, nuinputvars] = size(u_inputvars); [~, nvinputvars] = size(v_inputvars);

umatch = zeros (npairs,nuinputvars); vmatch = zeros (npairs,nvinputvars);
u_oneslice = zeros (npairs, 1); v_oneslice = zeros(npairs,1);

for np = 1:npairs
	umatch (np,:)= strncmpi(u_inputvars,u_outvar{1,np},length(u_outvar{1,np}));
	vmatch (np,:)= strncmpi(v_inputvars,v_outvar{1,np},length(u_outvar{1,np}));
end


for q=1:nuinputvars
	if ( (sum(umatch(:,q)) == 1) || (sum(vmatch(:,q)) == 1) )
		display ('File already has ruo and/or rvo')
		switch_skip = 1; exit;
	end
end


% Error checking for u and v
for np = 1:npairs
	nudimensions = length(u_inputinfo.Dimensions);
	for n=1:nudimensions
		if (u_inputinfo.Dimensions(n).Unlimited)
			if (u_inputinfo.Dimensions(n).Length == 1); u_oneslice (np) = 1; end
		end
	end
	
	nvdimensions = length(v_inputinfo.Dimensions);
	for n=1:nvdimensions
		if (v_inputinfo.Dimensions(n).Unlimited); 
			if (v_inputinfo.Dimensions(n).Length == 1); v_oneslice (np)= 1; end
		end
	end
	
end

if (u_oneslice ~= v_oneslice)
	display ('Time dimensions are of different lengths')
	exit;
end


% create coordinate file
coord_filename = 'VAR_COORD_FILENAME' ; copyfile(t_input_filename,coord_filename);
glamt=ncread(coord_filename,'nav_lon'); gphit=ncread(coord_filename,'nav_lat');

[m n] =size(gphit) ;
nccreate(coord_filename,'glamt','Dimensions',{'x' m 'y' n}); ncwrite(coord_filename,'glamt',glamt);
nccreate(coord_filename,'glamu','Dimensions',{'x' m 'y' n}); ncwrite(coord_filename,'glamu',glamt);
nccreate(coord_filename,'glamv','Dimensions',{'x' m 'y' n}); ncwrite(coord_filename,'glamv',glamt);
nccreate(coord_filename,'glamf','Dimensions',{'x' m 'y' n}); ncwrite(coord_filename,'glamf',glamt);

nccreate(coord_filename,'gphit','Dimensions',{'x' m 'y' n}); ncwrite(coord_filename,'gphit',gphit);
nccreate(coord_filename,'gphiu','Dimensions',{'x' m 'y' n}); ncwrite(coord_filename,'gphiu',gphit);
nccreate(coord_filename,'gphiv','Dimensions',{'x' m 'y' n}); ncwrite(coord_filename,'gphiv',gphit);
nccreate(coord_filename,'gphif','Dimensions',{'x' m 'y' n}); ncwrite(coord_filename,'gphif',gphit);



for n = 1:nfiles

	firstday = datestr (dae, 'yyyymmdd');
	seconday = datestr (dae + interval - 1, 'yyyymmdd');

	u_input_filename = [input_root '_' gridU '_' firstday '-' seconday '.nc'];
	v_input_filename = [input_root '_' gridV '_' firstday '-' seconday '.nc'];

	u_output_filename = [output_root '_' gridU '_' firstday '-' seconday '.nc'];
	v_output_filename = [output_root '_' gridV '_' firstday '-' seconday '.nc'];

	% Copy the files to the new interim directory 
	[copystatus, copyerror] = copyfile (u_input_filename, u_output_filename);
	if (copystatus ~= 1)
		display ('Error in copying file')
		display (copyerror)
		exit;
	end

	[copystatus, copyerror] = copyfile (v_input_filename, v_output_filename);
	if (copystatus ~= 1)
		display ('Error in copying file')
		display (copyerror)
		exit;
	end


	% Open the file, grab the data, rotate, output

	% This is, other than the change of filename variable names,  directly from Li's script
	% Allocation of ruo and rvo also new.

	for np = 1:npairs
		uu =ncread(u_input_filename,u_invar{1,np}) ; vv =ncread(v_input_filename,v_invar{1,np}) ;
		ndimensions = ndims(uu);
		if (u_oneslice(np) == 1); ndimensions = ndimensions + 1; nt = 1; end

		display (size(uu))
		if (ndimensions == 4)
			[ni nj nz nt] =size(uu); ruo = zeros (ni,nj,nz,nt); rvo = zeros (ni,nj,nz,nt);
			% rotate u/v to true north/east
			for z=1:nz
			for it =1:nt
				ruo(:,:,z,it) = rot_rep_2017(squeeze(uu(:,:,z,it)),squeeze(vv(:,:,z,it)), 'T','ij->e',coord_filename);
				rvo(:,:,z,it) = rot_rep_2017(squeeze(uu(:,:,z,it)),squeeze(vv(:,:,z,it)), 'T','ij->n',coord_filename);
			end
			end
		elseif (ndimensions == 3)
			[ni nj nt] = size(uu);
			ruo = zeros (ni,nj,nt);
			rvo = zeros (ni,nj,nt);
			% rotate u/v to true north/east
			for it =1:nt
				ruo(:,:,it) = rot_rep_2017(squeeze(uu(:,:,it)),squeeze(vv(:,:,it)), 'T','ij->e',coord_filename);
				rvo(:,:,it) = rot_rep_2017(squeeze(uu(:,:,it)),squeeze(vv(:,:,it)), 'T','ij->n',coord_filename);
			end
		else
			display ('Unexpected number of dimensions.')
			exit;
		end
        

		% write rotated u/v
		try 
			ncinfo(u_output_filename,u_outvar{1,np});
		catch err            
			if (ndimensions == 4)
				nccreate(u_output_filename,u_outvar{1,np},'Dimensions',{'x' ni 'y' nj 'depthu' nz 'time_counter' nt});
			elseif (ndimensions == 3)
				nccreate(u_output_filename,u_outvar{1,np},'Dimensions',{'x' ni 'y' nj 'time_counter' nt});
			end
		end

		ncwrite(u_output_filename,u_outvar{1,np},ruo);
		ncwriteatt(u_output_filename,u_outvar{1,np},'long_name','ocean current along true east');
		ncwriteatt(u_output_filename,u_outvar{1,np},'units','m/s');    
		ncwriteatt(u_output_filename,'/','source',input_root);
		ncwriteatt(u_output_filename,'/','note',note);
		ncwriteatt(u_output_filename,'/','generated_by',user);    
		ncwriteatt(u_output_filename,'/','timestamp',date(now));    

		try 
			ncinfo(v_output_filename,v_outvar{1,np});
		catch err            
			if (ndimensions == 4)
				nccreate(v_output_filename,v_outvar{1,np},'Dimensions',{'x' ni 'y' nj 'depthv' nz 'time_counter' nt});
			elseif (ndimensions == 3)
				nccreate(v_output_filename,v_outvar{1,np},'Dimensions',{'x' ni 'y' nj 'time_counter' nt});
			end
		end

		ncwrite(v_output_filename,v_outvar{1,np},rvo);
		ncwriteatt(v_output_filename,v_outvar{1,np},'long_name','ocean current along true north');
		ncwriteatt(v_output_filename,v_outvar{1,np},'units','m/s');    
		ncwriteatt(v_output_filename,'/','source',input_root);
		ncwriteatt(v_output_filename,'/','note',note);
		ncwriteatt(v_output_filename,'/','generated_by',user);    
		ncwriteatt(v_output_filename,'/','timestamp',date(now));    


	end % pair loop

	% Set next start day
	dae = dae + interval -1 + 1;

end % file loop


%-------------------------------------------------------------------------------------------------------------------------------



