%
% ROTATE PAIRS OF VARIABLES OF SMALL SUBDOMAINS WITH SPECIFIED ID CODES
% S Taylor, based on script from Li Zhai.  stephanne.taylor@dfo-mpo-gc.ca
% Last structural changes: Sept. 6, 2018.  Adjusted for single file input, more complex ID codes, and not using xml file for input.

% USED WITH: process_rotate.sh
% REQUIRES : Matlab 2013+, opa_angle_ST.m, rot_rep_ST.m
% PURPOSE  : Rotate u/v to true east/north at each point over the full model domain.  

% INPUT : Raw model files.  Be very careful running concurrently with the model, as script modifies the input files.
%         At each time interval (which can be one day or more than one day) three files are read in: t grid for 
%         coordinates (which is probably not needed) and u and v grid files for variables.

% OUTPUT: Amended raw model files.  Adds rotated variables to the original files, does not alter them further.  (New
%         variables come with attributes indicates that it is, and all attributes etc from original file are maintained.)


% Paths etc
clear all
%addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts/mat_BoF/function
%addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts/m_map_1.4g
%addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts/

p = genpath ('VAR_PATH_FXNS');
addpath(p);


% Batch Info

dir_series = 'VAR_DIR_SERIES';		%root_series = 'VAR_ROOT_SERIES';
%dir_output = 'VAR_DIR_OUTPUT';		root_output = 'VAR_ROOT_OUTPUT';

%station_list = 'VAR_STATION_LIST';
station_info = 'VAR_STATION_LIST';


%vel_gridset = VAR_VEL_GRIDSET;	% 1: vels are both on T grid already.  0: vels are on U,V grids.
npairs   = VAR_NPAIRS;	
delimiter = 'VAR_DELIMITER';	% typically SER or some such - mooring files don't have grid_X in the file name
u_invar  = {'VAR_U_INVARS'};	v_invar  = {'VAR_V_INVARS'};
u_outvar = {'VAR_U_OUTVARS'};	v_outvar = {'VAR_V_OUTVARS'};

% Cordinate info
dir_coord  = 'VAR_DIR_COORD';	root_coord = 'VAR_ROOT_COORD';



% Metadata
note = 'VAR_META_NOTE';		strrep (note, ' ', '_');
user = 'VAR_META_USER';		strrep (user, ' ', '_');
timestamp =  datestr(datetime);	strrep (timestamp, ' ', '_');




%----------------------------------------------------------------------------------------------------------
%--- End of user-specifed information ---------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------------


% Get all the possible files
allfiles = dir ([dir_series '/*_' delimiter '_*']);
[nallfiles,~] = size(allfiles); inds=linspace(1,nallfiles,nallfiles); locs= zeros(nallfiles, 1);

% Read in the .csv with the ID strings and the short codes.  Don't need most of the info, but this is the same file that all the other ones read in so be consistent
tmp = readtable(station_info, 'Delimiter',',','ReadVariableNames',false);
codeinfo = table2struct (tmp);

% Get the number of files 
[nstns,~] = size(codeinfo);


for s=1:nstns

	station = codeinfo(s).Var2;

	% This is bananas, but: need to check if there are trailing lower case letters on station code, and trim them off if there are
	% If extracting variables will get the coordinate file too, but mooring files rely on the process_coordinate script (which
	% does not account for the subsets of mooring files, since it takes info straight from the xml file.

	if ( isstrprop(station(end),'lower') )
		if ( isstrprop(station(end-1),'lower') )
			station_trimmed = station(1:end-2);
		else
			station_trimmed = station(1:end-1);
		end
	else
		station_trimmed = station;
	end
	


	% Get the coordinate file for the mooring
	tmp = dir([dir_coord '/' root_coord '_' station_trimmed '.nc']);	coord_filename = [dir_coord, '/', tmp.name];

	% find all files in list that match that station code
	%isstn = contains(allfiles.name, station);  % unavailable in versions installed on GPSC
	%nfiles = sum(isstn);	locs = inds(isstn==1);

	% Clunky work around.  This is hideous, argh
	is_stn= [];
	for f=1:nallfiles; 
		if (strfind(allfiles(f).name, station ) > 0 ); is_stn(f) = 1; 
		else;	is_stn(f) = 0; 
		end
	end

	nfiles = sum(is_stn);	locs = inds(is_stn==1);



	% loop over the files, check if rotated vars present, if not, check that both u and v are present, if so, rotate.  else skip

	for f=1:nfiles

		% Get the data to be rotated
		mod_filename = allfiles(locs(f)).name;
		filename =[dir_series, '/',  mod_filename];

		% Loop over pairs (typically only one, but may as well include more)
		for np = 1:npairs

			go_on = 0;		% only do the rotation if the variables aren't already present (ie, error is thrown)

			% Check that no rotated variables are present
			try; ncinfo(filename,u_outvar{1,np}); catch err; go_on = 1; end
			try; ncinfo(filename,v_outvar{1,np}); catch err; go_on = 1; end

			if (go_on == 1)		% need to do the rotation

				% Check that both input variables are present in the file. If not, 
				try; ncinfo(filename,u_invar{1,np}); 
				catch err; 
					display (['Variable ' u_invar{1,np} ' is missing from ' filename '.'])
					break;
				end

				try; ncinfo(filename,v_invar{1,np}); 
				catch err; 
					display (['Variable ' u_invar{1,np} ' is missing from ' filename '.'])
					break;
				end

		

				% Have verified that data exists and rotation needs to be done.

				% Read data to be rotated
				uu =ncread(filename,u_invar{1,np}) ; vv =ncread(filename,v_invar{1,np}) ; ndimensions = ndims(uu);


				% Now need to sort out what grid variables are on. This is still kinda clunky but it's general enough to handle a lot of cases.
				% Get variable info, find which attribute is the one with the coordinates listed
				uin = ncinfo(filename, u_invar{1,np});		uind = find(strcmp({uin.Attributes.Name}, 'coordinates')==1);
				ustr = uin.Attributes(uind).Value;		uind = strfind (uin.Attributes(uind).Value, 'depth');
				uchar = upper (ustr (uind +5) );		udepthvar = ustr (uind:uind+5) ; 

				vin = ncinfo(filename, v_invar{1,np});		vind = find(strcmp({vin.Attributes.Name}, 'coordinates')==1);
				vstr = vin.Attributes(vind).Value;		vind = strfind (uin.Attributes(vind).Value, 'depth');
				vchar = upper (vstr (vind + 5));		vdepthvar = vstr (vind:vind+5);

	
				if (ndimensions == 4)
					[ni nj nz nt] =size(uu); ruo = zeros (ni,nj,nz,nt); rvo = zeros (ni,nj,nz,nt);
					% rotate u/v to true north/east
					for z=1:nz
						for it =1:nt
							ruo(:,:,z,it) = rot_rep_ST(squeeze(uu(:,:,z,it)),squeeze(vv(:,:,z,it)), uchar,'ij->e',coord_filename);
							rvo(:,:,z,it) = rot_rep_ST(squeeze(uu(:,:,z,it)),squeeze(vv(:,:,z,it)), vchar,'ij->n',coord_filename);
						end
					end

				elseif (ndimensions == 3)
					[ni nj nt] = size(uu); ruo = zeros (ni,nj,nt); rvo = zeros (ni,nj,nt);
					% rotate u/v to true north/east
					for it =1:nt
						ruo(:,:,it) = rot_rep_ST(squeeze(uu(:,:,it)),squeeze(vv(:,:,it)), uchar,'ij->e',coord_filename);
						rvo(:,:,it) = rot_rep_ST(squeeze(uu(:,:,it)),squeeze(vv(:,:,it)), vchar,'ij->n',coord_filename);
					end

				else
					display ('Unexpected number of dimensions.')
					exit;
				end


				% Write rotated u/v to the original files
				if (ndimensions == 4);
					nccreate(filename,u_outvar{1,np},'Dimensions',{'x' ni 'y' nj udepthvar nz 'time_counter' nt});
					nccreate(filename,v_outvar{1,np},'Dimensions',{'x' ni 'y' nj vdepthvar nz 'time_counter' nt});
				elseif (ndimensions == 3);
					nccreate(filename,u_outvar{1,np},'Dimensions',{'x' ni 'y' nj 'time_counter' nt});
					nccreate(filename,v_outvar{1,np},'Dimensions',{'x' ni 'y' nj 'time_counter' nt}); 
				end

				ncwrite(filename,u_outvar{1,np},ruo);		ncwrite(filename,v_outvar{1,np},rvo);
				ncwriteatt(filename,u_outvar{1,np},'long_name','ocean current along true east');	ncwriteatt(filename,u_outvar{1,np},'units','m/s');
				ncwriteatt(filename,v_outvar{1,np},'long_name','ocean current along true north');	ncwriteatt(filename,v_outvar{1,np},'units','m/s');

			end % switch to check if rotation already present

		end % pair loop


		% Global metadata
		ncwriteatt(filename,'/','note',note);	ncwriteatt(filename,'/','generated_by',user); ncwriteatt(filename,'/','timestamp',timestamp);



	end % file loop

%	end % error checking

end % station loop







