
% Read in observation and model tidal analysis, and generate tidal ellipse plots for each constituent.
% Plots observation, model, and difference ellipse in black,red, and blue respectively.

clear all

p = genpath ('VAR_PATH_FXNS'); addpath(p);
warning ('off', 'all');

%  ------------------------------------------------------------------------------------------------------
%  --- INPUT INFO ---------------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

% INPUT DATA

input_analysis   = ['VAR_DIR_OUTPUT' '/' 'VAR_ROOT_OUTPUT' '_summary'];
output_directory = 'VAR_DIR_OUTPUT';
output_fileroot  = 'VAR_ROOT_OUTPUT';

bathy_file = 'VAR_FILE_BATHY';

runid = 'VAR_RUN_ID';


% Reference data
TC = {VAR_TIDAL_CONSTITUENTS};	
TC_names =['Q1'; 'O1';'P1';'K1';'N2';'M2';'S2';'K2'];	% housekeeping task: harmonize this with TC.
axlim = [0.1, 0.1, 0.1, 0.2, 0.1, 0.2, 0.1, 0.1];
%axlim = [0.04, 0.03, 0.15, 0.7, 0.09];	% specify the size of the axes
gap  = VAR_GAP;				% number of degrees gap to leave in the ellipse

%  ------------------------------------------------------------------------------------------------------
%  --- END OF INPUT INFO --------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

% [ 0 ] HOUSEKEEPING AND INITIAL INPUT 


% Load the summary file (has all tidal constituents in one place)
load (input_analysis);

[~,nstns] = size (summary_meta);
[~,ncon]  = size (TC);

bathy = double(ncread(bathy_file, 'Bathymetry'));
lat   = double(ncread(bathy_file, 'nav_lat'));
lon   = double(ncread(bathy_file, 'nav_lon'));

land = bathy; land(land>0)=NaN; land(land==0)=1;


% [ 1 ] LOOP THROUGH STATIONS.  Looping through constituents first makes dot indexing very difficult.

for s = 1:nstns

        % Get the station code
       	station = summary_meta(s).station;  %codeinfo(n).Var2

	% Get the depths that the analysis is done at
	depths = summary_meta(s).depth;
	[~,nlevels] = size (depths);


	
	% [ 2 ] LOOP OVER DEPTHS THEN CONSTITUENTS

	for l = 1:nlevels

		for n = 1:ncon
	                con_name = TC_names(n,:);

        	        oind = strmatch(con_name,summary_obs(s,l).tidal_cons.name,'exact');
        	        mind = strmatch(con_name,summary_mod(s,l).tidal_cons.name,'exact');

			%if (summary_mod(s,l).tidal_cons.tidecon(mind,1) ~= 0)	% if major axis is 0, something's gone sideways in the ADCP script

				% This is a function that set the three ellipses: observations, model, and difference
				% First argument is model tidal constituent structure, second is observations, third is the indexes for the columns to use
				% (even columns hold error data, and honestly that can probably be unhardwired but leave it for now.)
				%[qmod, qobs,qdiff] = ellipse_difference (summary_mod(1,s).tidal_cons.tidecon(n,:), summary_obs(1,s).tidal_cons.tidecon(n,:) , [1,3,5,7]);
				[qmod, qobs,qdiff] = ellipse_difference (summary_mod(s,l).tidal_cons.tidecon(mind,:), summary_obs(s,l).tidal_cons.tidecon(oind,:) , [1,3,5,7]);

				% [ 3 ] PLOT

				fig1=figure;
				set(fig1,'Units','Inches','OuterPosition',[1 1 8 8]);
	
				% Grab the major / minor axes, phase and inclination data, and calculates eccentricity (which is not currently used for anything
				% but may be interesting to list on the plot?)
		        	%omaj = summary_obs(1,s).tidal_cons.tidecon(n,1); omin = summary_obs(1,s).tidal_cons.tidecon(n,3); oecc = omin/omaj;
			        %oinc = summary_obs(1,s).tidal_cons.tidecon(n,5); opha = summary_obs(1,s).tidal_cons.tidecon(n,7);
		        	%mmaj = summary_mod(1,s).tidal_cons.tidecon(n,1); mmin = summary_mod(1,s).tidal_cons.tidecon(n,3); mecc = mmin/mmaj;
			        %minc = summary_mod(1,s).tidal_cons.tidecon(n,5); mpha = summary_mod(1,s).tidal_cons.tidecon(n,7);

		        	omaj = summary_obs(s,l).tidal_cons.tidecon(oind,1); omin = summary_obs(s,l).tidal_cons.tidecon(oind,3); oecc = omin/omaj;
			        oinc = summary_obs(s,l).tidal_cons.tidecon(oind,5); opha = summary_obs(s,l).tidal_cons.tidecon(oind,7);

		        	mmaj = summary_mod(s,l).tidal_cons.tidecon(mind,1); mmin = summary_mod(s,l).tidal_cons.tidecon(mind,3); mecc = mmin/mmaj;
			        minc = summary_mod(s,l).tidal_cons.tidecon(mind,5); mpha = summary_mod(s,l).tidal_cons.tidecon(mind,7);
	
		        	hold on
			        ax = gca; ax.Visible = 'off';

		        	% Cross for scale, size hardwired in input section
		        	he4a = plot_tidalellipses(0,0,'EP',axlim(n),1,0,0, 'PlotType','cross');		set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);

				% Plot the ellipses, with dots at the end to indicate the particle tracing out the ellipse. Use . rather than o to avoid hexagon markers
				he1a  = plot (qobs(gap:360)); he1b = scatter (real(qobs(360:360)), imag(qobs(360:360)), 250, 'k', '.');
		        	he2a  = plot (qmod(gap:360)); he2b = scatter (real(qmod(360:360)), imag(qmod(360:360)), 250, 'r', '.');
			        he3a  = plot (qdiff(gap:360)); he3b = scatter (real(qdiff(360:360)), imag(qdiff(360:360)), 250, 'b', '.');

				% Save the ellipses into a structure
				ell(n).obs  = qobs;
				ell(n).mod  = qmod;
				ell(n).diff = qdiff;


		        	set(he1a, 'LineWidth', 2, 'Color', [0 0 0]);%set(he1b, 'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor', [0 0 0]); %, 'Marker', 'o');
		        	set(he2a, 'LineWidth', 2, 'Color', [1 0 0]);%set(he2b, 'MarkerEdgeColor', [1 0 0], 'MarkerFaceColor', [1 0 0], 'Marker', 's');
			        set(he3a, 'LineWidth', 2, 'Color', [0 0 1]);%set(he3b, 'MarkerEdgeColor', [0 0 1], 'MarkerFaceColor', [0 0 1], 'Marker', '.');


				% Set the size of the plot so things stay consistent
		        	xlim ([-axlim(n)*1.2 axlim(n)*1.2]); ylim ([-axlim(n)*1.2 axlim(n)*1.2]);%axis tight

				% Label the plot with the constituent and the size of the cross
		        	text(-axlim(n)*0.6, -axlim(n)*0.6, TC{n}(1:2), 'FontSize', 16);
			        text( axlim(n)*0.6, -axlim(n)*0.6, num2str(axlim(n)), 'FontSize', 16);

			        % set background color (outside axes)
		        	set(gcf,'color',[1 1 1]); set(gcf,'inverthardcopy','off');

			        % set size of frame to be written
			        width=900; height=900; resolution=300;
	        		set(gcf,'paperunits','inches');  set(gcf,'paperposition',[0 0 width height]./resolution);

			        % write .png file
			        %pngname = [output_directory, '/', output_fileroot, '_PLT-ELL_' match_str '_', TC{n}(1:2), '.png' ]
	        		pngname = [output_directory, '/', output_fileroot, '_ELL_' TC{n}(1:2) '_', station, '_', num2str(depths(l)) 'm.png' ];

			        %print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname);
		        	close(fig1);
			%end % error switch
		end % constituent loop


		% [ 4 ] SUMMARY PLOT

		olon = summary_meta(s).obs_lon;      olat = summary_meta(s).obs_lat;
		mlon = summary_meta(s).mod_lon;      mlat = summary_meta(s).mod_lat;
		cast_sz = 50;	sz=16;


		fig = figure();

		subplot (6,10,[1,23])	% Map 1
		pcolor (lon,lat,land); shading flat; colormap([0.7 0.7 0.7]);   hold on;        % coastline
        	xlim ([-142 -120]);     ylim ([44 60]);

	        xl = round(olon,0)-2;   xr = round(olon,0)+2;
       		yb = round(olat,0)-2;   yt = round(olat,0)+2;
	        xc = [xl xl xl xl xl   xl+1 xl+2 xl+3   xr xr xr xr xr   xr-1 xr-2 xr-3];
	       	yc = [yb yb+1 yb+2 yb+3 yt   yt yt yt   yt yt-1 yt-2 yt-3 yb   yb yb yb];
        	h3 = scatter(xc,yc, cast_sz/4, 'b', '.');

	        h1 = scatter (olon,olat, cast_sz, 'k', 'filled');
	       	h2 = scatter (mlon,mlat, cast_sz, 'r', 'filled');

        	subplot (6,10,[31,53])	% Map 2
	       	pcolor (lon,lat,land); shading flat; colormap([0.7 0.7 0.7]);   hold on;        % coastline
	        xlim ([round(olon,0)-2 round(olon,0)+2]); ylim ([round(olat,0)-2 round(olat,0)+2]);
	       	h1 = scatter (olon,olat, cast_sz, 'k', 'filled');
	        h2 = scatter (mlon,mlat, cast_sz, 'r', 'filled');
	       	set(gca, 'TickLength', [0.025 0.025], 'layer','top','FontSize', sz);

		a1 = round(max(abs(ell(6).obs))/2, 2);	a2 = round(max(abs(ell(4).obs))/2, 2);	
		axe = max (a1,a2);

		subplot (6,10,[4,26])	% M2
		hold on;	ax = gca;	ax.Visible = 'off';	loc=0.8;
		%axe = round(max(abs(ell(6).obs)), 2);	
		scale=2.5; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(6).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(6).obs(360:360)), imag(ell(6).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(6).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(6).mod(360:360)), imag(ell(6).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(6).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(6).diff(360:360)), imag(ell(6).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{6}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);

		subplot (6,10,[34,56])	% K1
		hold on;	ax = gca;	ax.Visible = 'off';
		%axe = round(max(abs(ell(4).obs)), 2);	
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(4).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(4).obs(360:360)), imag(ell(4).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(4).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(4).mod(360:360)), imag(ell(4).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(4).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(4).diff(360:360)), imag(ell(4).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{4}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);

		subplot (6,10,[7,18])	% S2
		hold on;	ax = gca;	ax.Visible = 'off';
		%axe = round(max(abs(ell(7).obs)), 2);	
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(7).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(7).obs(360:360)), imag(ell(7).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(7).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(7).mod(360:360)), imag(ell(7).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(7).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(7).diff(360:360)), imag(ell(7).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{7}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);

		subplot (6,10,[9,20])	% N2
		hold on;	ax = gca;	ax.Visible = 'off';
		%axe = round(max(abs(ell(5).obs)), 2);	
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(5).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(5).obs(360:360)), imag(ell(5).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(5).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(5).mod(360:360)), imag(ell(5).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(5).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(5).diff(360:360)), imag(ell(5).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{5}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);

		subplot (6,10,[27,38])	% O1
		hold on;	ax = gca;	ax.Visible = 'off';
		%axe = round(max(abs(ell(2).obs)), 2);	
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(2).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(2).obs(360:360)), imag(ell(2).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(2).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(2).mod(360:360)), imag(ell(2).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(2).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(2).diff(360:360)), imag(ell(2).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{2}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);
	
		subplot (6,10,[29,40])	% P1
		hold on;	ax = gca;	ax.Visible = 'off';
		%axe = round(max(abs(ell(3).obs)), 2);	
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(3).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(3).obs(360:360)), imag(ell(3).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(3).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(3).mod(360:360)), imag(ell(3).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(3).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(3).diff(360:360)), imag(ell(3).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{3}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);
	
		subplot (6,10,[47,58])	% K2
		hold on;	ax = gca;	ax.Visible = 'off';
		%axe = round(max(abs(ell(8).obs)), 2);	
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(8).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(8).obs(360:360)), imag(ell(8).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(8).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(8).mod(360:360)), imag(ell(8).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(8).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(8).diff(360:360)), imag(ell(8).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{8}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);
	
		subplot (6,10,[49,60])	% Q1
		hold on;	ax = gca;	ax.Visible = 'off';
		%axe = round(max(abs(ell(1).obs)), 2);	
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(1).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(1).obs(360:360)), imag(ell(1).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(1).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(1).mod(360:360)), imag(ell(1).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(1).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(1).diff(360:360)), imag(ell(1).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{1}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);



		% Print the figure
	        annotation('textbox', [0 0.9 1 0.1], 'String', ['Run: ' runid '  ADCP: ' station '  Inst.: ' num2str(summary_meta(s).inst_depth) 'm  Depth: ' num2str(depths(l)) 'm  Axis: ' num2str(axe)  ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 20)

		% set size of frame to be written
		width=2700; height=1800; resolution=300;
		set(gcf,'paperunits','inches');  set(gcf,'paperposition',[0 0 width height]./resolution);

		pngname = [output_directory, '/', output_fileroot, '_ELL_', station, '_',  num2str(depths(l)), 'm_summary.png' ];
		print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname);
		close(fig);

	end % depth 


        display (['[ ' num2str(s) ' ] Station ' station ' complete.']  )
end

