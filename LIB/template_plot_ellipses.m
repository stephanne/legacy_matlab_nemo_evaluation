
% Read in observation and model tidal analysis, and generate tidal ellipse plots for each constituent.
% Plots observation, model, and difference ellipse in black,red, and blue respectively.

clear all

p = genpath ('VAR_PATH_FXNS'); addpath(p);
warning ('off', 'all');

%  ------------------------------------------------------------------------------------------------------
%  --- INPUT INFO ---------------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

% INPUT DATA

input_analysis   = ['VAR_DIR_OUTPUT' '/' 'VAR_ROOT_OUTPUT' '_summary'];
output_directory = 'VAR_DIR_OUTPUT';
output_fileroot  = 'VAR_ROOT_OUTPUT';

bathy_file = 'VAR_FILE_BATHY';

runid = 'VAR_RUN_ID';


% Reference data
TC = {VAR_TIDAL_CONSTITUENTS};	
TC_names =['Q1'; 'O1';'P1';'K1';'N2';'M2';'S2';'K2'];	% housekeeping task: harmonize this with TC.
gap  = VAR_GAP;				% number of degrees gap to leave in the ellipse

%  ------------------------------------------------------------------------------------------------------
%  --- END OF INPUT INFO --------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

% [ 0 ] HOUSEKEEPING AND INITIAL INPUT 


% Load the summary file (has all tidal constituents in one place)
load (input_analysis);

[~,nstns] = size (summary_meta);
[~,ncon]  = size (TC);

bathy = double(ncread(bathy_file, 'Bathymetry'));
lat   = double(ncread(bathy_file, 'nav_lat'));
lon   = double(ncread(bathy_file, 'nav_lon'));

land = bathy; land(land>0)=NaN; land(land==0)=1;


% [ 1 ] LOOP THROUGH STATIONS.  Looping through constituents first makes dot indexing very difficult.

for s = 1:nstns

        % Get the station code
       	station = summary_meta(s).station;  %codeinfo(n).Var2

	% Get the depths that the analysis is done at
	depths = summary_meta(s).depth;
	[~,nlevels] = size (depths);


	
	% [ 2 ] LOOP OVER DEPTHS THEN CONSTITUENTS

	for l = 1:nlevels

		for n = 1:ncon
	                con_name = TC_names(n,:);

        	        oind = strmatch(con_name,summary_obs(s,l).tidal_cons.name,'exact');
        	        mind = strmatch(con_name,summary_mod(s,l).tidal_cons.name,'exact');


			% This is a function that set the three ellipses: observations, model, and difference
			% First argument is model tidal constituent structure, second is observations, third is the indexes for the columns to use
			% (even columns hold error data, and honestly that can probably be unhardwired but leave it for now.)
			%[qmod, qobs,qdiff] = ellipse_difference (summary_mod(1,s).tidal_cons.tidecon(n,:), summary_obs(1,s).tidal_cons.tidecon(n,:) , [1,3,5,7]);
			[qmod, qobs,qdiff] = ellipse_difference (summary_mod(s,l).tidal_cons.tidecon(mind,:), summary_obs(s,l).tidal_cons.tidecon(oind,:) , [1,3,5,7]);

			% Save the ellipses into a structure
			ell(n).obs  = qobs;	 ell(n).mod  = qmod; 	ell(n).diff = qdiff; 

		end % constituent loop


		% [ 4 ] SUMMARY PLOT

		olon = summary_meta(s).obs_lon;      olat = summary_meta(s).obs_lat;
		mlon = summary_meta(s).mod_lon;      mlat = summary_meta(s).mod_lat;
		%cast_sz = 50;	sz=16;


		fig = figure();

		subplot (3,4,1)	% Map 1
		pcolor (lon,lat,land); shading flat; colormap(clr_land);   hold on;        % coastline
		xlim ([-142 -120]);        ylim ([44 60]);

		[xm ym xd yd] = f_mapzoom(olon,olat,2);
		h0 = scatter(xd,yd, sz_scatter/4, clr_zoom, '.');
		h1 = scatter (olon,olat, sz_scatter, clr_obs, 'filled');
		h2 = scatter (mlon,mlat, sz_scatter, clr_mod, 'filled');

		f_fmtaxes (gca, '', '', 0, 0, sz_font, tk, -0.04)

		subplot (3,4,5)	% Map 2
		pcolor (lon,lat,land); shading flat; colormap(clr_land);   hold on;
		h1 = scatter (olon,olat, sz_scatter, clr_obs, 'filled');
		h2 = scatter (mlon,mlat, sz_scatter, clr_mod, 'filled');
		f_fmtaxes (gca, '', '', 0, 0, sz_font, tk, -0.04)
		xlim (xm); ylim (ym);


		% Set the axis length of the scales
                a1 = round(max(abs(ell(6).obs))/2, 2);  a2 = round(max(abs(ell(4).obs))/2, 2);
                a3 = round(max(abs(ell(6).mod))/2, 2);  a4 = round(max(abs(ell(4).mod))/2, 2);
                axe = (max ([a1,a2,a3,a4]));



		subplot (3,4,2)	% M2
		f_pltcross (gca, axe, 2.0,  TC{6}(1:2),0.8, clr_cross);
		f_pltellipse (ell(6).obs, clr_obs, gap, sz_pha);
		f_pltellipse (ell(6).mod, clr_mod, gap, sz_pha);
		f_pltellipse (ell(6).diff, clr_wt, gap, sz_pha);	% need to loop over this to add sources.  also webtide is not handled the same way
		pbaspect([1 1 1])	% unclear if can specify this earlier and still have it hold, or if it needs to be at the end.


		subplot (3,4,3)	% K1
		hold on;	ax = gca;	ax.Visible = 'off';
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(4).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(4).obs(360:360)), imag(ell(4).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(4).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(4).mod(360:360)), imag(ell(4).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(4).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(4).diff(360:360)), imag(ell(4).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{4}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);

		subplot (3,4,4)	% S2
		f_pltcross (gca, axe, 1.0,  TC{7}(1:2),0.8, clr_cross);
		f_pltellipse (ell(7).obs, clr_obs, gap, sz_pha);
		f_pltellipse (ell(7).mod, clr_mod, gap, sz_pha);
		f_pltellipse (ell(7).diff, clr_wt, gap, sz_pha);
		pbaspect([1 1 1]);

		subplot (3,4,6)	% N2
		hold on;	ax = gca;	ax.Visible = 'off';
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(5).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(5).obs(360:360)), imag(ell(5).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(5).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(5).mod(360:360)), imag(ell(5).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(5).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(5).diff(360:360)), imag(ell(5).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{5}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);

		subplot (3,4,7)	% O1
		hold on;	ax = gca;	ax.Visible = 'off';
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(2).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(2).obs(360:360)), imag(ell(2).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(2).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(2).mod(360:360)), imag(ell(2).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(2).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(2).diff(360:360)), imag(ell(2).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{2}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);
	
		subplot (3,4,8)	% P1
		hold on;	ax = gca;	ax.Visible = 'off';
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(3).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(3).obs(360:360)), imag(ell(3).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(3).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(3).mod(360:360)), imag(ell(3).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(3).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(3).diff(360:360)), imag(ell(3).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{3}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);
	
		subplot (3,4,11)	% K2
		hold on;	ax = gca;	ax.Visible = 'off';
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(8).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(8).obs(360:360)), imag(ell(8).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(8).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(8).mod(360:360)), imag(ell(8).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(8).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(8).diff(360:360)), imag(ell(8).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{8}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);
	
		subplot (3,4,12)	% Q1
		hold on;	ax = gca;	ax.Visible = 'off';
		scale=1.4; xlim ([-scale*axe scale*axe]); ylim ([-scale*axe scale*axe]);
	        he4a = plot_tidalellipses(0,0,'EP',axe,1,0,0, 'PlotType','cross');         set (he4a, 'Linewidth', 1.5, 'Color', [0.5 0.5 0.5]);
		he1a  = plot (ell(1).obs(gap:360), 'LineWidth', 2, 'Color', [0 0 0]); he1b = scatter (real(ell(1).obs(360:360)), imag(ell(1).obs(360:360)), 250, 'k', '.');
		he2a  = plot (ell(1).mod(gap:360), 'LineWidth', 2, 'Color', [1 0 0]); he2b = scatter (real(ell(1).mod(360:360)), imag(ell(1).mod(360:360)), 250, 'r', '.');
		he3a  = plot (ell(1).diff(gap:360),'LineWidth', 2, 'Color', [0 0 1]); he3b = scatter (real(ell(1).diff(360:360)), imag(ell(1).diff(360:360)), 250, 'b', '.');
		hold off;
		text(-axe*loc, -axe*loc, TC{1}(1:2), 'FontSize', 16); %text( axe*loc, -axe*loc, num2str(axe), 'FontSize', 16);



                % ANNOTATIONS ABOUT THE PARAMETERS THAT GO INTO THIS
                subplot (3,4,[9 10])
                ax=gca; ax.Visible = 'off';
                str_window1   = ['Start date: ' summary_meta(s).adcp_start_date ];
                str_window2   = ['End date: ' summary_meta(s).adcp_final_date];
                str_obsbounds = ['Obs. limits: ' num2str(round(summary_meta(s).obs_bins(1),1)) 'm - ' num2str(round(summary_meta(s).obs_bins(end),1)) 'm'];
                str_cross     = ['Cross scale length: ' num2str(axe)];
                str_instdepth = ['Instrument depth: ' num2str(round(summary_meta(s).inst_depth)) 'm' ];
                str_waterdepth= ['Water depth: ' num2str(round(summary_meta(s).water_depth)) 'm'];
                str_modbathy  = ['Model bathymetry: ' num2str(round(summary_meta(s).mod_water_depth)) 'm'];


		% Title the figure
	        f_titleannot (['Run: ' runid '  ADCP: ' station '  Depth: ' num2str(depths(l)) 'm  Axis: ' num2str(axe)  ],  20)

		% Print the figure
		pngname = [output_directory, '/', output_fileroot, '_ELL_', station, '_',  num2str(depths(l)), 'm_summary.png' ];
		f_figprint (gcf, pngname, 2700,1800,300);
		close(fig);

	end % depth 


        display (['[ ' num2str(s) ' ] Station ' station ' complete.']  )
end

