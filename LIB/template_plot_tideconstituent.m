% PLOTTING AND STATISTICS FOR TIDE GAUGE ANALYSIS
% S Taylor
% Last structural changes: September 20, 2018

% USED WITH: process_tidegauge.sh
% REQUIRES : Matlab 2013+
% PURPOSE  : Reads in .mat file from tidegauge script, which contains harmonic constituents.  Plots them nicely
%            in a variety of ways (line plots, maps)

% INPUT    : List of tide gauge stations, .mat files generated with same list (order is important)

% OUTPUT   : Set of plots (phaseamp line plots, with a map showing all stations considered).  Basic stats
%            to help evaluate how good the tidal analysis is)


%  ------------------------------------------------------------------------------------------------------
%  --- INPUT INFO ---------------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------


clear all
p = genpath ('VAR_PATH_FXNS');	addpath(p);	warning('off', 'all');


% INPUT DATA
config = 'VAR_CONFIG'; run_id = 'VAR_RUN_ID';
input_analysis = ['VAR_DIR_OUTPUT' '/' 'VAR_ROOT_OUTPUT' '_summary'];
dir_output     = 'VAR_DIR_OUTPUT';
root_plot = 'VAR_ROOT_OUTPUT';

% PARAMETERS
VAR_FILE_CONFIG_REFERENCE;
VAR_FILE_CONFIG_SPECIFIC;


%  ------------------------------------------------------------------------------------------------------
%  --- END INPUT INFO -----------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------


% [ 0 ] HOUSEKEEPING

% THIS NEEDS TO BE GENERALIZED FOR MULTIPLE SOURCES
load (input_analysis);


% Get the number of stations and constituents
[~,nstns] = size (summary_mod);		[~,ncons]  = size (TC);

amp = zeros(ndata,nstns,ncons);		pha = zeros(ndata,nstns,ncons);

es = linspace(1,nstns,nstns);		stn={}; xt={};




% [ 1 ] READ IN DATA

for n = 1:ndata
for s = 1:nstns
	% Station
	stn{n}  = cellstr (summary_obs(n).station);		xt{n} = stn{1,n}{:}(4:9);

	% Data
	for con =1:ncons
		% Now need to search the tidal constituent structure for the matching constituent.

                %con_name = TC_names(con,:);
                con_name = TC{con};

		ind = NaN;	ind = strmatch(con_name,summary_obs(n).tidal_cons.name,'exact');  
                amp (n,s,con) = summary_obs(n).tidal_cons.tidecon(ind,1);
                ampe(n,s,con) = summary_obs(n).tidal_cons.tidecon(ind,2);
                pha (n,s,con) = summary_obs(n).tidal_cons.tidecon(ind,3);
                phae(n,s,con) = summary_obs(n).tidal_cons.tidecon(ind,4);


	end
end
end

	
% [ 2 ] PLOTTING

% Make line plots of amplitude, phase, and complex difference for model, observations, and difference


for con =1:ncons

	% [ 2.1 ] Amplitude
	fig = figure;
	plot(es,0.0*es, 'Color', [0.8 0.8 0.8], 'Linewidth', 2, 'HandleVisibility','off'); hold on

	if (switch_errbar_obs == 1);	errorbar(es,ao(:,con),aoe(:,con), 'Color', clr_o, 'Linewidth', 2);
	else;				plot(es,ao(:,con), 'Color', clr_obs, 'Linewidth', 2);	end

	if (switch_errbar_mod == 1);	errorbar(es,am(:,con),ame(:,con), 'r', 'Linewidth', 2);
	else;				plot(es,am(:,con), 'Color', clr_mod, 'Linewidth', 2);		end

	f_titleannot (['Amplitude of ' TC{1,con} '     Run ID: ' run_id ], 20);

	legend ('Observations', run_id , 'Location', 'best');

	xlabel('Tide Gauge');   ylabel('Amplitude (m)');
	xlim ([1,nstns]); set(gca,'xtick',es,'xticklabel',xt', 'XTickLabelRotation', 45);

	% Set the ytics depending on the typical amplitude
	L = get(gca,'YLim'); 
        if     (L(2) >= 1.5);   set(gca,'YTick',L(1):0.2:L(2) );
        elseif (L(2) >= 1.0);   set(gca,'YTick',L(1):0.1:L(2) );
        elseif (L(2) >= 0.5);   set(gca,'YTick',L(1):0.05:L(2) );
        else;                   set(gca,'YTick',L(1):0.02:L(2)); end
	grid on;
	
	set(gca,'FontSize', 15); 


	pngname = [dir_output '/' root_plot '_' TC{con} '_amp.png'];
	f_figprint (gcf, pngname, 4800,2700,300);
	close (fig);
	
	
	% [ 2.2 ] Phase
	fig = figure;
	
	plot(es,0.0*es, 'Color', [0.8 0.8 0.8], 'Linewidth', 2, 'HandleVisibility','off'); hold on

	if (switch_errbar_obs == 1);	errorbar(es,po(:,con),poe(:,con), 'Color', clr_obs, 'Linewidth', 2);
	else;				plot(es,po(:,con), 'Color', clr_obs, 'Linewidth', 2);	end
	if (switch_errbar_obs == 1);	errorbar(es,pm(:,con),pme(:,con), 'Color', clr_mod, 'Linewidth', 2);
	else;				plot(es,pm(:,con), 'Color', 'clr_mod, 'Linewidth', 2);	end

	f_titleannot (['Phase of ' TC{con} '     Run ID: ' run_id ], 20);

	legend ('Observations', run_id , 'Location', 'best');

	xlabel('Tide Gauge');   ylabel('Phase (deg)');
	xlim ([1,nstns]); set(gca,'xtick',es,'xticklabel',xt', 'XTickLabelRotation', 45);


%	L = get(gca,'YLim') 
        a = min(min(po(:,con)));        b = min(min(pm(:,con)));        c = min(a,b);   L(1) = floor(c/5.0) * 5.0;
        a = max(max(po(:,con)));        b = max(max(pm(:,con)));        c = max(a,b);   L(2) = ceil(c/5.0) * 5.0;
        ylim (L);
        if     (L(2) - L(1) >= 200); set(gca,'YTick',L(1):20:L(2));
        elseif (L(2) - L(1) >= 100); set(gca,'YTick',L(1):10:L(2));
        elseif (L(2) - L(1) >= 50); set(gca,'YTick',L(1):5:L(2));
        elseif (L(2) - L(1) >= 20); set(gca,'YTick',L(1):2:L(2));
        else; set(gca,'YTick',L(1):1:L(2)); end

	grid on;
	
	set(gca,'FontSize', 15); 


	pngname = [dir_output '/' root_plot '_' TC{1,con} '_phase.png'];
	f_figprint (gcf, pngname, 4800,2700,300);
	close (fig);



	% [ 2.3] Map of Phaseamp Results (amplitude is size of scatter dot, phase is its color)
	% If you decide to put this back in, go look for the script in tan older verision of the package.


end


