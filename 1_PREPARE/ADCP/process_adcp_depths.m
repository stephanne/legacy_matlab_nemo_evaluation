% Script to determine what depths to choose for ADCP analysis.
% Reads in info on moorings, model bathy, and sorts stations accordingly.
% Then makes a best guess, outputs a text file with all the info for eyeballing

clear all
%p = genpath ('VAR_PATH_FXNS'); addpath(p);
%warning ('off', 'all');


% Directories and filename roots
dir_obs    = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TESTING/DATA/ADCP';
dir_model  = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TESTING/DATA/NEW_MODEL/nep36_SNDIST_02a';
root_model  = 'NEP36-DE01_1h';
%dir_output = 'VAR_DIR_OUTPUT';          root_output = 'VAR_ROOT_OUTPUT';
%dir_bathy  = 'VAR_DIR_BATHY';           root_bathy  = 'VAR_ROOT_BATHY';
str_ext_interval = '1d';                str_mooring_interval = '1h';

% Input info on stations
station_list = 'VAR_STATION_LIST';      % contains the mooring info


%  ------------------------------------------------------------------------------------------------------
%  --- END OF INPUT INFO --------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

% [ 0 ] HOUSEKEEPING AND INITIAL INPUT 

% Get station codes.  Use the same file as everything else to stay consistent
tmp = readtable(station_list, 'Delimiter',',','ReadVariableNames',false);  % need read variable names switch if there isn't a header line
codeinfo = table2struct (tmp);
[nfiles,~] = size(codeinfo);    % can have more than one observation file for a mooring station (ie with the same short code)

fid = fopen ('ADCP_mooring_info.csv', 'w')


for n = 1:nfiles

        % Get the station code
        station = codeinfo(n).Var2;

        % [ 1 ] OBSERVATIONS

        obs_filename = [dir_obs '/' codeinfo(n).Var1 '.nc']; % info_obs_stn{n,1};

        obs_bins  = double(ncread(obs_filename, 'bins')); 
        obs_instdepth = double(ncread(obs_filename, 'inst_depth'));
        water_depth = double(ncread(obs_filename, 'depth'));
        obs_lon   = double(ncread(obs_filename, 'longitude'));
        obs_lat   = double(ncread(obs_filename, 'latitude'));

	% [ 2 ] MODEL DATA

	%mod_filename = [dir_model, '/', root_model, '_', station, '-09_' str_start '-' str_final '.nc' ];
	mod_filename = [dir_model, '/', root_model, '_', station, '-09_20160101-20160101.nc' ];
        mod_lat    = double(ncread(mod_filename, 'nav_lat'));
        mod_lon    = double(ncread(mod_filename, 'nav_lon'));
        mod_depths = double(ncread(mod_filename, 'deptht'));

        % Take only the middle point
        mod_lat = squeeze (mod_lat(ix,iy));     mod_lon = squeeze (mod_lon(ix,iy));

        % Check if the model depth is less that the observation depth (argh)
        % find location of first 0 (ie first bin without data)
        mod_ind = find(~mod_u(:,1));


	% Get min/max of observation range, model depth at each point, and output to a text file.
	mind = min(min(obs_bins));	maxd = max(max(obs_bins))
	fprintf (fid, '%s, %f, %f,   %f, %f, %f,   %f,    %f, %f,\n', station, obs_lon,obs_lat, mind,maxd,obs_instdepth, water_depth, mod_depths(mod_ind-1), mod_depths(mod_ind) );
end
	
fclose(fid);	

	% Midpoint
        %obs_depth = obs_bins(int16(end/2));  % THIS NEEDS TO BE MORE SOPHISTICATED

