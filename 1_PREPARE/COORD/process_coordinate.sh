
# CLIP OUT SMALL COORDINATE FILES
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: August 31rd, 2018.

# USED WITH: NEMO iodef / domain_def files
# REQUIRES : bash, NCO operators
# PURPOSE  : Clips out small 3x3 patchs from coordinate file to match the mooring data output

###import numpy as np
###import writexml

set -a; source ../../config.ksh; set +a

# INPUT VARIABLES

FILE_COORDINATE=${REF_FILE_COORD}
DIR_OUTPUT=${REF_DIR_UMBRELLA}'/COORD'

if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p $DIR_OUTPUT; fi

# There are few enough stations that doing it by hand is less exasperating than wrangling an XML file.
# Okay maybe not but XML parsing is a headache and this is at least straightforward if longwinded and tedious.
id="estuary";	ib=80;	jb=558;		ie=220;	je=600;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="HAK1";	ib=478;	jb=494;		ie=480;	je=496; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="SCO1";	ib=463;	jb=421;		ie=465;	je=423; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="SCO2";	ib=436;	jb=442;		ie=438;	je=444; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="SRC1";	ib=420;	jb=526;		ie=422;	je=528; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="JUA1";	ib=354;	jb=534;		ie=356;	je=536; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="SRN1";	ib=386;	jb=587;		ie=388;	je=589; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="A1";	ib=572;	jb=270;		ie=574;	je=272; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="BP1";	ib=501;	jb=372;		ie=503;	je=374; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="EO1";	ib=553;	jb=322;		ie=555;	je=324; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="SOGN1";	ib=615;	jb=408;		ie=617;	je=410; 	ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="HEC1";	ib=408;	jb=569;		ie=410;	je=571;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="NEAH1";	ib=626;	jb=272;		ie=628;	je=274;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="BARK1";	ib=578;	jb=255;		ie=580;	je=257;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="BOPA1";	ib=687;	jb=297;		ie=689;	je=299;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="CAM2";	ib=429;	jb=580;		ie=431;	je=582;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="CMP1";	ib=428;	jb=587;		ie=430;	je=589;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="CLS1";	ib=547;	jb=277;		ie=549;	je=279;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="DEV1";	ib=438;	jb=636;		ie=440;	je=638;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="END1";	ib=468;	jb=219;		ie=470;	je=221;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="FOP1";	ib=604;	jb=292;		ie=606;	je=294;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="FGPD";	ib=604;	jb=293;		ie=606;	je=295;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="JFC2";	ib=646;	jb=264;		ie=648;	je=266;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="KSK1";	ib=426;	jb=625;		ie=428;	je=627;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="BACCH";	ib=583;	jb=259;		ie=585;	je=261;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="ENWF1";	ib=467;	jb=219;		ie=469;	je=221;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="S46087";	ib=626;	jb=272;		ie=628;	je=274;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="S46088";	ib=685;	jb=266;		ie=687;	je=268;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="S46099";	ib=638;	jb=168;		ie=640;	je=170;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="S46100";	ib=623;	jb=158;		ie=625;	je=160;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="RCNE1";	ib=468;	jb=220;		ie=470;	je=222;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="RCNW1";	ib=439;	jb=218;		ie=441;	je=220;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="RCSE1";	ib=468;	jb=218;		ie=470;	je=220;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="RCSW1";	ib=467;	jb=218;		ie=469;	je=220;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="AS04";	ib=676;	jb=263;		ie=678;	je=265;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="BACAX";	ib=578;	jb=255;		ie=580;	je=257;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="BACME";	ib=578;	jb=255;		ie=580;	je=257;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="BACWL";	ib=578;	jb=254;		ie=580;	je=256;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="BDYPM";	ib=687;	jb=297;		ie=689;	je=299;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="CRIPC2";	ib=600;	jb=381;		ie=602;	je=383;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="DIIPC2";	ib=371;	jb=678;		ie=373;	je=680;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="EF04";	ib=608;	jb=311;		ie=610;	je=313;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="KVIPC1";	ib=440;	jb=670;		ie=442;	je=672;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="LSBBL";	ib=675;	jb=319;		ie=677;	je=321;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="MAC";	ib=675;	jb=270;		ie=677;	je=272;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="NC27";	ib=518;	jb=211;		ie=520;	je=213;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="NC89";	ib=547;	jb=277;		ie=549;	je=279;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="SCHDW";	ib=672;	jb=316;		ie=674;	je=318;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="SCVIP";	ib=672;	jb=316;		ie=674;	je=318;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="TOF1";	ib=579;	jb=317;		ie=581;	je=319;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="USDDL";	ib=675;	jb=320;		ie=677;	je=322;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
id="ZUC1";	ib=555;	jb=349;		ie=557;	je=351;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'
#id="";	ib=;	jb=;		ie=;	je=;		ncks -F -d x,$ib,$ie -d y,$jb,$je $FILE_COORDINATE $DIR_OUTPUT'/coordinates_nep36_'$id'.nc'

 
