#!/bin/bash

# FILE COLLECTOR
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structural change: July 12 2018

# PURPOSE: Creates a series of links to files that can then be used as input for subsequent scripts.
#          Used for handling ECCC output, which has each (10 day, currently) file in a separate 
#          folder, and with a different filename structure.  Having a single directory with 
#          "filenames" with adjustable structure makes the whole package more easily adaptable.
# REQUIRES: Bash, nothing else

# INPUT: Nothing outside of this script

# OUTPUT: Series of links

# ECCC directory / filename structure: hindcast_nep36_9l/NEMO_14400/NEP369l_2015092500_1h_grid_T_2D.nc

# specify: root directory (hindcast_nep36_9l), prefix of looping directory (NEMO_), start number (14400), interval, end number
# parse: start and end date from files (if present - for now take for given that you have T U V and geo files)
# specify: end directory, grids needed, structure of final name,

#source ../../config_riops.ksh
source ../../config.ksh

# SOURCE
#UMBRELLA='/space/hall0/sitestore/dfo/odis/jpp001/ECCC/CIOPS-W/nep36_SNDIST_02c'		# D102c
#UMBRELLA='/space/hall0/sitestore/dfo/odis/jpp001/ECCC/CIOPS-W/nep36_SNFULL_02c'		# FU02c
#UMBRELLA='/space/hall0/sitestore/dfo/odis/jpp001/ECCC/CIOPS-W/nep36_SNFREE_02c'		# FR02c
#UMBRELLA='/space/group/dfo_odis/work/SeDOO/requests/RIOPS_rx_016'			# RIOPS rx10 (older RIOPS)
#UMBRELLA='/space/group/dfo_odis/work/SeDOO/requests/RIOPS_rx_010'			# RIOPS rx10 (older RIOPS)
#UMBRELLA='/space/hall0/sitestore/dfo/odis/jpp001/ECCC/CIOPS-W/nep36_SNDIST_02d'		# D102c
#UMBRELLA='/space/hall0/sitestore/dfo/odis/jpp001/ECCC/CIOPS-W/nep36_SNFULL_02c'		# D102c
UMBRELLA='/space/group/dfo_odis/work/SeDOO/requests/RIOPS_rx_021'			# RIOPS rx10 (older RIOPS)
#UMBRELLA='/gpfs/fs2/dfo/odis/liz001/NEP36/NEP36-OPM224-OUT/DIMG'			# Hindcast

#DIR_PREFIX='NEMO_';	FILE_PREFIX='FU02c';	FILE_INTERVAL='1d'		# CIOPSW
DIR_PREFIX='';	FILE_PREFIX='Creg12-CMC-ANAL';	FILE_INTERVAL='1d'		# RIOPS

# PARAMETERS  all three counts only needed if switch is either 1 or 2
COUNT_START=14400;	COUNT_INTERVAL=14400;	COUNT_FINAL=3;	
SWITCH_FINAL=0				# if 0: all available.  1: # of time slices.  2: # of files. 
FILENAME_STYLE=1			# 0: older style: T04_2015092200_1d_grid_T.nc, T04_1h_SCO2-09.nc
					# 1: newer style: DE01_1d_grid_V_20160306-20160306.nc, DE01_1h_SRC1-09_20160221-20160221.nc
DAYS_PER_FILE=1				# number of days per file
INTERVALS='1h 1d';	DOMAINS='grid_T_2D grid_T grid_U grid_U_2D grid_V grid_V_2D'
EXCLUDE='geo'; 				# only process files that do not contain this string

INCLUDE_MOORINGS=1			# set to 1 if there are mooring files to be linked (they lack a grid delimiter string)
MOORING_FILE='NEP36_moorings.txt'	# file with the strings to identify mooring files to be concatenated.

RUN_WINDOW=''
SWITCH_STEPH_CONVENTIONS=${REF_STEPH_CONVENTIONS}	# if 1, replaces all underscores in the middle of chunks of info with dashes, omits -09 from patch filenames
							# erratically implemented - don't futz with

# DESTINATION
#DESTINATION=${REF_DIR_UMBRELLA}'/DATA/DI02d'
#DEST_PREFIX='NEP36-DI02d'
DESTINATION=${REF_DIR_UMBRELLA}'/DATA/RIOPS_rx021'
DEST_PREFIX='RIOPS-rx021'
SWITCH_WIPE_DESTDIR=false




#-------------------------------------------------------------------------------------------
#--- End of input parameters ---------------------------------------------------------------
#-------------------------------------------------------------------------------------------

# [ 0 ] Preliminary housekeeping

if [ ! -d $DESTINATION ] ; then
	mkdir -p ${DESTINATION}
else
	if [[ $SWITCH_WIPE_DESTDIR ]] ; then
		rm ${DESTINATION}/*
	fi
fi
lendir=$((${#UMBRELLA} + 1));		lenconfig=$((${#FILE_PREFIX} + 1))
DAYS_PER_FILE=$((${DAYS_PER_FILE} - 1));		# 10 days file has nine days in filename (fenceposts)


# If mooring files are requested, read in the file with the ID strings in them
if [[ ${INCLUDE_MOORINGS} == 1 ]]; then
	moorings=()
	nm=0
        while IFS='' read -r line || [[ -n "$line" ]]; do        # loop over stations
                #list=(${line//_/ })     # split line (ie filename) by underscores
                #id=${list[0]}           # will need to be adjusted if the ID strings in the mooring outputs are not the ID strings from beginning of Hauke's files
		moorings+=(${line})
		nm=$((${nm} + 1))
        done <<< `cat ${MOORING_FILE}`

	#for i in "${moorings[@]}"; do echo "$i" ; done

fi	

echo "[ 1 ] Mooring ID strings read in"


# [ 1 ] Determine how many directories / files need to be linked.  depends on value of SWITCH_FINAL.

# For all cases, list the available subdirectories:
# Ignores any directory that does not follow the specified prefix with a numerical character
# May need a manual override?  Or, any alpha character after prefix means excluded?

listing=`ls ${UMBRELLA}/${DIR_PREFIX}* -d -v`  # -d grabs only the directory names, -v sorts them numerically
ind1=${#UMBRELLA}; 	ind2=${#DIR_PREFIX}; 	ind3=$((${ind1} + ${ind2}))
ALL_SUBDIRS='';		SUBDIRS=''

# Loop through the directories and check that the non-prefix part is all numeric characters
for slice in ${listing}; do
	ind4=${#slice};         suffix=${slice:ind3:ind4}
	if [[ ${suffix} =~ [a-zA-Z] ]] ; then
		#echo ${suffix} ' contains letters'
		ALL_SUBDIRS=${ALL_SUBDIRS}
	else
		ALL_SUBDIRS=${ALL_SUBDIRS}' '${slice}   
	fi
done

echo "[ 2 ] Directories listed"

case ${SWITCH_FINAL} in
	0 )	# All valid subdirectories in $UMBRELLA will be processed and linked
		SUBDIRS=${ALL_SUBDIRS}	
		#echo ${SUBDIRS}
		;;
	1 )	# A fixed number of time slices will be processed
		NDIR=$(( $((${COUNT_FINAL} - ${COUNT_START})) / ${COUNT_INTERVAL} ))
		ERR=$(( $((${COUNT_FINAL} - ${COUNT_START})) % ${COUNT_INTERVAL} ))
		if [ ${ERR} != 0 ] ; then
			echo "Non-integer number of files requested."
			echo "Number of time steps requested: " $((${COUNT_FINAL} - ${COUNT_START}))
			echo "Interval: " ${COUNT_INTERVAL}
			echo "Number of files: " ${NDIR}
			echo "Remainder: " ${ERR}
			exit
		fi

		count=0;
		for slice in ${SUBDIRS} ; do
			if [[ count > $NDIR ]]; then 
				break;
			fi
			SUBDIRS=${SUBDIRS}' '${slice}
			count=$((${count} + 1 ))
			
		done
		;;
	2 )	# A fixed number of files will be processed
		NDIR=${COUNT_FINAL}; 		count=0;
		for slice in ${SUBDIRS} ; do
			if [[ count > $NDIR ]]; then 
				break;
			fi
			SUBDIRS=${SUBDIRS}' '${slice};		count=$((${count} + 1 ))
			
		done
		;;
	*)	# Unclear option - throw an error.
		echo "Unknown option for SWITCH_FINAL: " ${SWITCH_FINAL}; exit
		;;
esac

echo "[ 3 ] After case switch"

# Duplicate for some options, but double checks number of directories that will be processed.
NDIR=`wc -w <<< ${SUBDIRS}`

echo "[ 4 ] Number of directories"


# [ 2 ] Loop through directories, parse out the relevant info

for slice in ${SUBDIRS}; do

	# Loop through the directory, toss out any files that match a given string (here, _geo_).  
	# Can do further error checking here too.
	#ALL_FILES=`ls -1 ${slice}/* -r`;	FILELIST='';
	ALL_FILES=`ls -1 ${slice}/*` ;	FILELIST='';

	echo "[ 5 ] $slice filelist generated."

	for f in ${ALL_FILES}; do
		inc=false
		test "${f#*$FILE_PREFIX}" != "$f" && inc=true	# Check if specified prefix is found in filename, set switch to true if it is.
		test "${f#*$EXCLUDE}" != "$f" && inc=false 	# Check if $word is found in filename, set switch to false if it is.

		if [ $inc = 'true' ]; then
                	FILELIST=${FILELIST}' '${f}
	        fi

	done

	#echo $slice,  $FILELIST
	#echo ""

	# Set grid_present to false (need to check if a grid_X file is present to avoid errors with mooring files)
	grid_present=false
	first=true


	# Have list of files that need linking to, now step through them, parse the dates, and link / copy / etc
	# At this point, have interval and grids specified manually (in a list, each of them).  If you start getting
	# weird combinations of files from JP, then come back to this.  Otherwise, it's more headache than it's worth.
	# Also since the parameters are position and extract length, ${#f} will always overshoot the end of the string 
	# (but it seems to be fine and doesn't add a ton of whitespace at the end)
	lenslice=$((${#slice} + 1 ))
	for f in ${FILELIST}; do
		#echo $slice
		#echo $f
		# Lop off the directory path
	        filename=${f:${lenslice}:${#f}}

		# Lop off the leading bit with the configuration info
		chop=${filename:${lenconfig}:${#filename}}

		echo $f



		# Switch to maintain capacity forless informative file names (which thankfully seem to not be being used anymore)

		case ${FILENAME_STYLE} in
			0 )	# Old style, no date stamps present in mooring file
				# Now get the various bits of info from the filename and check if you requested them

				# First first, check if it's a mooring file.
				ismooring=false
				for i in ${moorings[@]}; do
			                test "${f#*${i}}" != "$f" && ismooring=true   # Check if specified prefix is found in filename, set switch to true if it is.
				done

				#echo ${f}, ${ismooring}

				# First get the next eight digits, which give the end date of the data in the file.  
				# Again, if you start to get files that don't have a trailing 00 (ie, do have a trailing 12 or whatever)
				# then revisit this and put hour stamps in all your filenames I guess?
				# This is a bit dodgy if for some reason you have mooring files without grid files.  Since mooring files don't have 
				# any date information in the filename, this relies on having one of the grid files be processed first, and then just uses the
				# listedenddate from that.  If there's no grid files though this doesn't update and so doesn't work.  Need an override:
				if [[ ${ismooring} == 'false' ]]; then
					listedenddate=${chop:0:8}
					chop=${chop:11:${#chop}}
					#echo $chop, $listedenddate
					for i in ${DOMAINS}; do
						test "${f#*${i}}" != "$f" && grid_present=true   # Check if specified prefix is found in filename, set switch to true if it is.
					done

				elif [[ ${grid_present} == false && ${first} == true ]]; then	# need to manually update the date string. 
					first=false
					if [[ ${#listedenddate} > 0 ]]; then	# can just iterate listedenddate
						#echo ${listedenddate}
						listedenddate=`date '+%C%y%m%d' -d "$listedenddate+${DAYS_PER_FILE} days"`
					else					# no information to go on.  Pull from the file itself
						echo "You'll need to figure something out here.  Read in from prompt?"

					fi
				fi

				# Then the time step interval (1h, 1d, 60ts, etc)
				ind5=$((`expr index "$chop" _` - 1))
				int=${chop:0:$ind5}
				chop=${chop:$((${ind5} + 1)):${#chop}}

				# And lastly the domain / grid identifier.  
				# For the files you have so far, it's easiest and more robust to chop off the .nc rather than trying to sort
				# out how many underscores you have.  If later on there's a field after the domain / grid identifying, then
				# will need to revisit this.
				dom=${chop:0:$((${#chop} - 3))}
	
				finaldate=`date '+%C%y%m%d' -d "$listedenddate-1 days"`
				startdate=`date '+%C%y%m%d' -d "$finaldate-${DAYS_PER_FILE} days"`

				filewindow=${startdate}-${finaldate}
				;;

			1 )	# Current style, with dates present in the file listed as the last field in all files (including moorings)
				#DE01_1d_grid_V_20160306-20160306.nc, DE01_1h_SRC1-09_20160221-20160221.nc				

				#chop =1d_grid_V_20160306-20160306.nc
				ind1=$((${#chop}-20));		ind2=$((${#chop}-11))
				startdate=${chop:${ind1}:8};	finaldate=${chop:${ind2}:8}

				# Get the time string delimiter
				ind3=$((`expr index "$chop" _` - 1));	int=${chop:0:${ind3}};	 chop=${chop:$((${ind3} + 1)):${#chop}}

				# And the domain identifier.  Chop off the time window at the end rather than sussing out whether it's grid_T_2D or not 2D
				ind4=$((${#chop} - 21));		dom=${chop:0:${ind4}}

				#if [[ ${dom} == 'grid_T_2D' ]] && [[ ${int} == '1h' ]]; then
				#	echo $chop, $int, $dom
				#fi
				
				;;

			*)	# Unknown style
				echo "Unknown value for FILENAME_STYLE " ${FILENAME_STYLE}
				;;

		esac
		# Get the date string for X days before the end time stamp.
		# Bless you random Stack Exchange user for this bogglingly straightforward way to do this.

		#echo $listingenddate
		
		#finaldate=`date '+%C%y%m%d' -d "$listedenddate-1 days"`
		#startdate=`date '+%C%y%m%d' -d "$finaldate-${DAYS_PER_FILE} days"`

		filewindow=${startdate}-${finaldate}



		# Now, have all the pieces of info needed to generate your standardized filename.  (Not that the original
		# input isn't standardized, just in a very awkward way)
		# Eventually have it so the filename structure is user-specified up at the top. 
		# Check that both int and dom were actually requested before making links etc.
		# Use loops rather than if switches to avoid false positives if only grid_T_2D is requested but grid_T file exists.

		req_int=false;	req_dom=false

		for check_int in $INTERVALS; do
			if [[ $int == $check_int ]] ; then
				req_int=true
			fi	
		done

		for check_dom in $DOMAINS; do
			if [[ $dom == $check_dom ]] ; then
				req_dom=true
			fi	
		done

		for check_mooring in ${moorings[@]}; do
			# Check if filenames end in -09, since mooring codes do not include the -09 (just the station code)
			if [[ ${dom:$((${#dom}-3)):3} == '-09' ]]; then
				has_09=true
				if [[ $dom == ${check_mooring}-09 ]] ; then
					req_dom=true
				fi	
			else
				has_09=false
				if [[ $dom == $check_mooring ]] ; then
					req_dom=true
				fi	

			fi
		done

		
		if $req_int && $req_dom ; then

			if [[ $SWITCH_STEPH_CONVENTIONS == 1 ]] ; then
				dom=${dom//_/-}		# replace underscores with dashes
			else
				if [[ ${ismooring} == true ]]; then
					if [[ ${has_09} == false ]]; then
						dom=${dom}-09	# append -09 if it's a mooring file and ST conventions not applied.
					fi
				fi
			fi

			if [[ ${#RUN_WINDOW} > 0 ]] ; then
				linkname=${DESTINATION}/${DEST_PREFIX}_${int}_${RUN_WINDOW}_${dom}_${filewindow}.nc
			else
				linkname=${DESTINATION}/${DEST_PREFIX}_${int}_${dom}_${filewindow}.nc
			fi

			# And then either link, cp, rsync, mv, etc here.
			#echo ${f} ${linkname}
			ln -s ${f} ${linkname}

		fi

	done # loop over files in directory

done # loop over directories

