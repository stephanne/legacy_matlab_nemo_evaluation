
# FIND NEAREST GRID LOCATION OF DATA POINT 
# S Taylor,  stephanne.taylor@dfo-mpo.gc.ca
# Last structural changes: Sept. 4, 2018

# USED WITH: process_station.sh
# REQUIRES : Python 2.7 (likely works with earlier versions as well, untested with Python 3 but is probably fine)
#            Python functions in f_location.py and f_writexml.py
# PURPOSE  : Find the nearest location on model coordinate file from lat / lon specified in netcdf file. 

# INPUT    : A list of netcdf files.

# OUTPUT   : A .csv with the filename, the latlon coords, and the nearest grid point (0 indexed).

# NOTE     : This script is used for matching observations with lat/lon coordinates, and as such the domains are 
#            defined relative to the central point (which is the closest grid point to the requested lat/lon). 
#            Typically these will be the 3x3 patches used to match CTD casts etc.  For outputting larger regions (as 
#            in generating boundary conditions for other models, or river transects, etc), use the similar script RGN.

# NOTE #2  : RGN doesn't exist yet.

# NOTE #3  : This is also a test case for translating things into Python so they're more portable.



import numpy as np
from netCDF4 import Dataset
import sys

# Need to access environment variables to set the library location
import os
pkg_location = '..' #os.environ.get('PKG_LOCATION');	
lib_location = pkg_location + '/ETC/FXNS'; sys.path.append(lib_location)	# Add the library of functions
lib_location = pkg_location + '/ETC/DICTS'; sys.path.append(lib_location)	# Add the various dictionaries linking observations and model codes

import f_location as lcn

# INPUT DATA

import d_missing as d		# file contains variable "dictionary" which has the key:value pairs linking obs and mooring short codes

filelist = 'filelist_missing'		# list of observations files
dir_obs  = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/DATA'		# directory where all the listings in filelist are located

coord_file = '/space/hall0/sitestore/dfo/odis/stt001/PACKAGE/ETC/REF/coordinates_nep36.nc'		# Coordinate file
lmask_file = '/space/hall0/sitestore/dfo/odis/stt001/PACKAGE/ETC/REF/tlandmask_NEP36_tmpbathy.nc'		# Land mask file
bathy_file = '/space/hall0/sitestore/dfo/odis/stt001/PACKAGE/ETC/REF/Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc'		# Bathymetry file

dx = 0.10 # radius to look for nearest grid point

switch_usefilename = 0 # if set to true, will generate the short code rather than looking it up.
instrument = 'VAR_STR_INSTRUMENT'		# string indicating what sort of data this is (CastCTD etc).  Only used if switch_usefilenames is true.

# OUTPUT DATA

outfile = 'info_missing.csv'			# file containing id string, short code, lat/lon coords, 0-indexed location of nearest point, and distance to it


#-----------------------------------------------------------------------------------------------

# [ 1 ]  Read in coordinate grid and bathymetry.  
#        Using a temporary variable is a bit inefficient but allows the netcdf file to be closed 
#        cleanly, since tmp is the reference to the netcdf variable within the file.  Data is 
#        copied out, the reference is deleted, and the file can be closed without throwing warnings.  
#        nx and ny are determined from the coordinate file, and used to initialize all 2D arrays.

# Note the counterintuitive indexing: this Python library reads netcdf files as (y,x) rather than (x,y)

# Coordinate file
#ncid = netcdf.netcdf_file (coord_file, 'r')
ncid = Dataset (coord_file, 'r')
tmp = ncid.variables ['nav_lon']; 
nx = tmp.shape[1]; ny = tmp.shape[0]
glon = np.zeros ((ny,nx)); glat = np.zeros ((ny,nx)); lm = np.zeros((ny,nx))
glon[:,:] = tmp[:,:]
tmp = ncid.variables ['nav_lat']; glat[:,:] = tmp[:,:]; del tmp
ncid.close()

# Land mask file
#ncid = Dataset (lmask_file, 'r')
#tmp = ncid.variables ['tmask']; lm[:,:] = tmp[0,:,:]; del tmp
#ncid.close()

# Bathymetry file
ncid = Dataset (bathy_file, 'r')
tmp = ncid.variables ['Bathymetry']; bathy = tmp[:,:]; del tmp
ncid.close()

lm = np.where(bathy>0, 1,0);


# [ 3 ] Read in filenames, open, grab coordinates

filenames = [line.strip() for line in open(filelist)]
nfiles = len (filenames)

fid = open (outfile, 'w')

for n in range (nfiles):
	fn = filenames[n].split(' ')

	tmp = fn[0].split('/')
	tmp = tmp[len(tmp)-1]
	idstr = tmp[0:(len(tmp)-3)]

	filename = dir_obs + '/' + fn[0]
	
	ncid = Dataset(filename, 'r') #, format="NETCDF4")
	lat = ncid.variables['latitude'][:];
	lon = ncid.variables['longitude'][:];
	ncid.close()

	# HARDWIRED FOR THE WESTERN HEMISPHERE
	if (lon > 0 ):
		print "Station ", idstr, "has +'ve longitude."
		lon = -lon

	#q = lcn.find_nearest(input_data[n].id, 1 , float(input_data[n].lon), float(input_data[n].lat), glon, glat, lm, dx)
        #print n, lon,lat #, glon,glat, lm,dx
	(ilon,ilat,dist) = lcn.find_nearest_better(lon,lat, glon,glat, lm, dx)

	# Get the water depth at that point
	if not np.isnan(ilat):
		wd = bathy[ilat,ilon]
	else:
		wd = np.nan

	if idstr in d.dictionary:	# if entry has a mooring code assigned to it
		code = d.dictionary[idstr]
	elif switch_usefilename:	# if using filenames to generate short codes
		a=idstr.replace(instrument+'_', '')
		b=a.replace ('-', '')
		c=b.replace ('_', '-')
		code = c[0:len(b)-1] # .nc already removed. with CastCTD removed and no hyphens or trailing h
		#print code
	else:
		code='XXX'		# default string.

	# Write info to file. obs filename, short code for mooring (XXX if no mooring is available), lon/lat, nearest grid point (0-indexed), distance.
	if not np.isnan(ilat):
		fid.write ('%s, %s, %f, %f, %i, %i, %f, %f,\n' % (idstr, code, lon,lat, ilon,ilat, dist, wd))


fid.close()
