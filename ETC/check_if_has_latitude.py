
import numpy as np
from netCDF4 import Dataset
import shutil


filenames = [line.strip() for line in open('filelist')]
nfiles = len (filenames)

fid = open ('no_lattitude_present.txt', 'w')

for n in range (nfiles):
        fn = filenames[n].split(' ')
        filename = fn[0]

        ncid = Dataset(filename, 'r') #, format="NETCDF4")
	vars_avail = ncid.variables.keys()
	if 'latitude' not in vars_avail:
		print filename
		fid.write ('%s\n' %  filename )
		shutil.move(filename, 'NO_LATITUDE_LISTED')
        ncid.close()

