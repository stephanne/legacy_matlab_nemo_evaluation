#  Python translation of the Matlab function used to find the nearest point
#  in specified coordinate file to a specified lon, lat.

# lon,lat are the coordinates of the observation / point to be found.  
# glon, glat are model grids (from a coordinate file)
# bat is bathymetry (used as a land mask)
# dx is search radius, and should be slightly larger than model grid spacing

import numpy as np
import math
import collections 	# needed for named tuple output

def find_nearest (id,grp,lon,lat,glon,glat,bat,dx):

	# This funciton only supports lon/lat values that are single floats, not arrays
	# For multiple points, call this function repeatedly (once per point).  

	# Parameters
	pi = 2.0*math.asin(1.0); pi180 = pi/180.0
	earth_radius=6378.137;  # km

	# Find the points within dx degrees of the specified lon,lat
	b=np.nonzero( (glon[:,:] < lon+dx) & (glon[:,:] > lon-dx) & (glat[:,:] < lat+dx) & (glat[:,:]> lat-dx) & (bat[:,:]>0))
	if (len(b[0]) ==0) :
		return ([], [], [])
		
	npts = b[0].shape[0]	# how many points are in the range?
	dist = np.zeros(npts)	# initialize variable

	for n in range (npts):
		# Get the indices in b in a shorter form
		# note that since this uses netcdf 3 files and the scipy.io import regime, dimensions go (y,x) rather than (x,y).
		# Make sure this is consistent with the calling program - don't use without verifying that this is sensible
		# If you change the order here, change it a few lines down when you're getting the minimum distance too.
		ix = b[1][n];  iy = b[0][n]

		# Get the difference in longitude and latitude between the requested point and the current point in the search region
		# Make sure everything's in radians
		dlon = (lon - glon[iy,ix])*pi180 #long2 - long1;
		dlat = (lat - glat[iy,ix])*pi180 #lat2 - lat1;

		# Calculate the distance between the two points.  This is a direct translation from m_lldist in the m_map_1.4g package (same as is 
		# called from f_location.m).  m_lldist has an option to calculate geodetics as well as distance - it's not needed here.
		a = math.pow(math.sin(dlat/2.0),2.0) + math.cos(glat[iy,ix]) * math.cos(lat) * math.pow(math.sin(dlon/2.0),2.0)
		angles = 2.0 * math.atan2( math.sqrt(a), math.sqrt(1.0-a) )
		dist[n] = earth_radius * angles

	# Now get the minimum distance.
	ind= np.argmin(dist)			# get the index of the minimum value
	ix = b[1][ind]; iy = b[0][ind]		# get the indicies of the relevant point in the search regions

	# Return a named tuple with all the info: output
	#glon[iy,ix], glat[iy,ix], bat[iy,ix], dist[ind], ix,iy		# get the point in the domain.

	nearest_pt = collections.namedtuple('nearest_pt', ['id','grp', 'glon', 'glat', 'bat', 'dist', 'ix', 'iy'])
	p = nearest_pt (id,grp,glon[iy,ix], glat[iy,ix], bat[iy,ix], dist[ind], ix, iy)
	return p




def find_nearest_better (lon,lat,glon,glat,mask,dx):

	# This funciton only supports lon/lat values that are single floats, not arrays
	# For multiple points, call this function repeatedly (once per point).  

	# Parameters
	pi = 2.0*math.asin(1.0); pi180 = pi/180.0
	earth_radius=6378.137;  # km

	#lon=lon-0.04	# no clue why I put this in at some point

	# Find the points within dx degrees of the specified lon,lat
	b=np.nonzero( (glon[:,:] < lon+dx) & (glon[:,:] > lon-dx) & (glat[:,:] < lat+dx) & (glat[:,:]> lat-dx) & (mask[:,:]>0))
	if (len(b[0]) ==0) :
		return (np.nan, np.nan, np.nan) 
		
	npts = b[0].shape[0]	# how many points are in the range?
	dist = np.zeros(npts)	# initialize variable

	for n in range (npts):
		# Get the indices in b in a shorter form
		# note that since this uses netcdf 3 files and the scipy.io import regime, dimensions go (y,x) rather than (x,y).
		# Make sure this is consistent with the calling program - don't use without verifying that this is sensible
		# If you change the order here, change it a few lines down when you're getting the minimum distance too.
		ix = b[1][n];  iy = b[0][n]

		# Get the difference in longitude and latitude between the requested point and the current point in the search region
		# Make sure everything's in radians
		dlon = (lon - glon[iy,ix])*pi180 #long2 - long1;
		dlat = (lat - glat[iy,ix])*pi180 #lat2 - lat1;

		# Calculate the distance between the two points.  This is a direct translation from m_lldist in the m_map_1.4g package (same as is 
		# called from f_location.m).  m_lldist has an option to calculate geodetics as well as distance - it's not needed here.
		a = math.pow(math.sin(dlat/2.0),2.0) + math.cos(glat[iy,ix]) * math.cos(lat) * math.pow(math.sin(dlon/2.0),2.0)
		angles = 2.0 * math.atan2( math.sqrt(a), math.sqrt(1.0-a) )
		dist[n] = earth_radius * angles

		#print glon[iy,ix], glat[iy,ix], dist[n]

	# Now get the minimum distance.
	ind= np.argmin(dist)			# get the index of the minimum value
	ix = b[1][ind]; iy = b[0][ind]		# get the indicies of the relevant point in the search regions

	# Return a named tuple with all the info: output

	#nearest_pt = collections.namedtuple('nearest_pt', ['id','grp', 'glon', 'glat', 'mask', 'dist', 'ix', 'iy'])
	p = (ix,iy, dist[ind]) #nearest_pt (id,grp,glon[iy,ix], glat[iy,ix], mask[iy,ix], dist[ind], ix, iy)
	return p











