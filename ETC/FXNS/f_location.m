

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% find observed sites in the model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%lon,lat are observed locations
%glon, glat are model grids
%dep is model bathymetry
%dx is searching area, slightly bigger than model grids
%idx=[grid index, lat index, long index, lat,lon,depth,distance];
%dx=0.05 ; %unit in degree
%Li Zhai, 2017
function [idx]=f_location(lon,lat,glon,glat,dep,dx)


addpath('~/matlab_scripts/m_map_1.4g')

%!make sure lon is with glong
%if max(lon) <max(glon(:)) & min(lon) >min(glon(:))

idx=[];
%% create grid index
[m n]=size(glon);
[mlon mlat] =meshgrid([1:n], [1:m]) ;

for kk=1:length(lat)
%%%choose a small domain
xx=lon(kk);
yy=lat(kk);
ii=find(glon < xx+dx & glon > xx-dx & glat < yy+dx & glat > yy-dx & isnan(dep)==0);
 if length(ii)>1

%% compute distance
 dist=[];
 for jj=1:length(ii)
   dist(jj) = m_lldist([glon(ii(jj)) xx],[glat(ii(jj)) yy]);
 end

%% find the nearest location
 [mdist mm] = min(dist) ;
 idx(kk,:) = [ii(mm) mlat(ii(mm)) mlon(ii(mm)) glat(ii(mm)) glon(ii(mm)) dep(ii(mm)) mdist] ;
 else
    idx(kk,:) = nan(1,7) ;
 end
end % kk

%else
%    'observed location is outside model domain'    
%    idx=[];
%end
