function [Qmod1, Qmod2, vec_err] = ellipse_difference(mod_tidal_constit, obs_tidal_constit, inds)

%  VECTOR ERROR
%  Script to calculate vector error
%  Based on code provided by Yongsheng Wu
%  Simon Higginson, November 19, 2017 (re-written following system error)
%  Altered to output difference between the two ellipses by ST, May 14 2018

NN = 360;  % segment number in a tidal period
t=linspace(0,2*pi,NN);

i1 = inds(1,1); i2 = inds(1,2); i3 = inds(1,3); i4 = inds(1,4);

major = mod_tidal_constit(i1);
minor = mod_tidal_constit(i2);
inclination  = mod_tidal_constit(i3)*pi/180;
phase  = mod_tidal_constit(i4)*pi/180;
ecc = minor/major;
Qmod1 = major*(cos(t-phase) + 1i*ecc*sin(t-phase))*exp(1i*inclination);

major = obs_tidal_constit(i1);
minor = obs_tidal_constit(i2);
inclination  = obs_tidal_constit(i3)*pi/180;
phase  = obs_tidal_constit(i4)*pi/180;
ecc = minor/major;
Qmod2 = major*(cos(t-phase) + 1i*ecc*sin(t-phase))*exp(1i*inclination);

% Error
%vec_err = mean(abs(Qmod1 - Qmod2));
vec_err = Qmod1 - Qmod2;


end
