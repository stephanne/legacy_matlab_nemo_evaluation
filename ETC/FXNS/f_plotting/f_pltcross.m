function f_pltcross (curax, len_cross, scale,  clabel,pos, clr)
	hold on;
	curax.Visible = 'off'; 
	xlim ([-scale*len_cross scale*len_cross]); ylim ([-scale*len_cross scale*len_cross]);
	hcross = plot_tidalellipses(0,0,'EP',len_cross,1,0,0, 'PlotType','cross');
	set (hcross, 'Linewidth', 1.5, 'Color', clr);
	text(-len_cross*pos, -len_cross*pos, clabel, 'FontSize', 16);
end
