function f_figprint (curfig, pngname, width,height,resolution)

	set(curfig,'paperunits','inches'); set(curfig,'paperposition',[0 0 width height]./resolution);
	print('-dpng', ['-r' num2str(resolution)], '-zbuffer', pngname );
	pause(3); 

end

