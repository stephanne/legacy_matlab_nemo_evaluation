function  f_fmtaxes (curraxes, xax, yax, xlab, ylab, is_datetick, grid_on, sz_font, tl, offset)

set (curraxes, 'Linewidth', 2, 'FontSize', sz_font, 'TickLength', [tl tl], 'layer','top');
xlabel (xlab);	ylabel(ylab);	%axis tight;
if (~strcmp(class(xax),'char'));	xlim(xax);	end
if (~strcmp(class(yax),'char'));	ylim(yax);	end

if (is_datetick == 1)
	set(gca, 'XTickLabels', []);
	datetick ('x','mmm')
elseif (is_datetick ==2)
	set(gca, 'XTickLabels', []);
	datetick ('x', 'm')
end

pos = get(curraxes, 'Position');
pos(1) = pos(1) + offset; 
set(curraxes, 'Position', pos);

if (grid_on == 1 );	grid on;	end



end
