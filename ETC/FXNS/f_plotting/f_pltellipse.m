function f_pltellipse (ell, clr, gap, sz)

	plot (ell(gap:360), 'LineWidth', 2, 'Color', clr); 
	scatter (real(ell(360:360)), imag(ell(360:360)), sz, clr, '.');


end
