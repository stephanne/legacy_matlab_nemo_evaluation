function f_fmtcontour (curax, cmap, cax, ydir)

	shading (curax, 'flat'); colorbar;
	colormap(curax, cmap);	caxis([cax]);
	if (ydir == -1);	set(curax, 'YDir','reverse'); end


end
