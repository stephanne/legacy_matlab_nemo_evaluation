function [xm ym xd yd] = f_mapzoom (lon, lat, sz)

	pps = 5;	% points per side

	% corners
        xl = round(lon,0)-sz;   xr = round(lon,0)+sz;
        yb = round(lat,0)-sz;   yt = round(lat,0)+sz;

	% set the points
	dd = 2*sz / (pps-1);

	xd = zeros (1,4*pps);	yd = zeros (1,4*pps);

	% bottom
	for i=1:pps
		xd (1,i) = xl + (i-1)*dd;
		yd (1,i) = yb 		;
	end

	% right
	for i=1:pps
		xd (1,pps+i) = xr;
		yd (1,pps+i) = yb + (i-1)*dd;
	end

	% top
	for i=1:pps
		xd (1,2*pps+i) = xr - (i-1)*dd;
		yd (1,2*pps+i) = yt;
	end

	% left
	for i=1:pps
		xd (1,3*pps+i) = xl;
		yd (1,3*pps+i) = yt - (i-1)*dd;
	end

	% map limits
	xm = [xl xr];
	ym = [yb yt];

end
