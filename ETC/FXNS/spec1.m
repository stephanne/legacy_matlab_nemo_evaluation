function [ohm omPxx] = spec1(M,x);
%function out = spec1(M,x);

% Calculate and plot the power spectrum of time series x.
% Replaces missing values (indicated by NaN) by nanmean. Removes mean.
% Based on an M point Parzen window. Bandwidth is 12/M.

I=find(isnan(x)==1); x(I)=nanmean(x); x=x-mean(x);
n=length(x);
if rem(n,2)==1
    x=x(1:n-1);
end
x=x(:);
DX=fft(x); DX=DX(:);
n=length(DX);

delOmega=2*pi/n; p=floor(2*n/M);
Omega=(-p:p)*delOmega+0.00000000001;
Omega=Omega(:);
freq=[0:delOmega:pi]; freq=freq(:);
W=(sin(M*Omega/4)./sin(Omega/2)).^4.*(1-2/3*sin(Omega/2).^2).*6/(pi*M^3);
W=W/(sum(W)*delOmega);
W=W(:);

% Compute the power spectrum
INSTX=DX.*conj(DX);
INSTARTX=[INSTX(n-p+2:n);
    INSTX(1:n/2+p+1)];
Pxx=delOmega*conv(INSTARTX,W)/(2*pi*length(x));
Pxx=Pxx(:);
Pxx=2*Pxx(2*p:n/2+2*p);
om=freq/2/pi;
I=[2:length(om)];
%loglog(freq/2/pi,Pxx); ylim([0 max(Pxx)]); xlim([0 0.5]);
    
%semilogx(om(I),om(I).*Pxx(I)); xlim([0 0.2]); 
%out=om(I).*Pxx(I);
ohm=om(I);
omPxx=om(I).*Pxx(I);
