function num = f_datenum(str)

num = (datenum (str, 'yyyymmdd') - datenum ('19500101', 'yyyymmdd')) * 86400;

end
