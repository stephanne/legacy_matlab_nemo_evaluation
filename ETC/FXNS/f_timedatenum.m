function num = f_timedatenum(str)

num = (datenum (str, 'yyyymmddhhss') - datenum ('19500101', 'yyyymmdd')) * 86400;

end
