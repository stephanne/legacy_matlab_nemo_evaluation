function [ imin,imax,jmin,jmax ] = f_define_area(lon,lat,nlon,nlat,bathy,dx,buffer)
% NAME : f_define_area
% AUTHOR : J.-P. Paquin
% DATE : August 2017
% DESCRIPTION: identifies the min max for i,j for a region to include
%              a group of observation points
%
% CALLED BY : prepare_xml_files_BoF180.m
% FUNCTION CALLED: f_location
%
idx=[];
%idx=[grid index, lat index, long index, lat,lon,depth,distance];
idx=f_location(lon,lat,nlon,nlat,bathy,dx) ;
idx(3,:)=NaN;
imin=nanmin(squeeze(idx(:,2)))-buffer;
imax=nanmax(squeeze(idx(:,2)))+buffer;
jmin=nanmin(squeeze(idx(:,3)))-buffer;
jmax=nanmax(squeeze(idx(:,3)))+buffer;

end

