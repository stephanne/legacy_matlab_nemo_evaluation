%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Purpose:  rotate U/V to true east/north  hourly U/V T/S SSH in A1 zone of BoF180 defined in prepare_xml_files_BoF180.m
% Input:    BoF180-T10_1h_20150201_20150205_*-09.nc
% Output:   Rotated monthly file BoF180-T10_1h_20150201_20150228_code1-09.nc
%
% Author: Li Zhai 
% Date:   Aug 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-------Header information from prepare_xml_files_BoF180.m---------------
% CALLED PGM or FUNCTIONS: m_map package
%                          f_location
%
% NOTES: 
%   Inputs : codification of observation's priority
%           1- highest priority: mostly in Harbour 
%           2- mid level priority & modified "type 1" outside of harbour
%           3- Minas passage
%           0- on s'en fout
%   Added column "NEMO ID": with integer codes for stations following
%           999999 -> Smart buoy
%           8xxxxx -> Tide gauges (if outside of BoF180 => flag==0)
%           7xxxxx -> CTDs
%           6xxxxx -> ADPCs
%
%   Outputs : To avoid having a too large number of output files (>100) 
%            the approach is to output:
%               a- hourly data of the harbour area (#1), 
%               b- specific locations for #2 (3x3 centerd on closest model
%               point
%               c- small area in Minas passage (#3)
%
%  f_location
%        idx(:,2) => "i" location
%        idx(:,3) => "j" location
%
%
%----------End Header------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%   BEGIN Input   %%%%%%%%%%%%%%%%%%%%%%
clear
addpath /fs/vnas_Hdfo/odis/jpp001/matlab_scripts/mat_BoF/function
addpath /fs/vnas_Hdfo/odis/jpp001/matlab_scripts/m_map_1.4g

% input data directory
ddir = '/space/hall0/work/dfo/odis/jpp001/TMP_STORE/' ;
conf = 'SJAP100';
tst = 'U08' ;
indir1=[ddir conf '/' conf '-' tst '-OUT/A1/']
indir2=[ddir conf '/' conf '-' tst '-OUT/DIMG/'];
tfile = [conf '-' tst '*-09.nc']


% temporary directory
outdir = [ ddir conf '/' conf '-' tst '-OUT/tmpEVAL/'];
if ~exist(outdir,'dir')
  mkdir(outdir) 
end
icp=1;
% output directory 
outdir2 = [ddir conf '/' conf '-' tst '-OUT/EVAL/'];
if ~exist(outdir2,'dir')
  mkdir(outdir2)
end



% ---- define period used
year =2015 ; % beginning year
mth = 2 ; % beginning month
nmth = 4 ;%21 ; % total number of months
time_origin   = '1950-01-01 00:00:00' ;%% 
% ----   

%% ---  Observations file (export .xls file saved to .csv file with
%% modifications to input already included
%dateobs='20170728'; last='_Phase1plus_JP.csv';  
%formatread='%*s %*s %d %f %f %*s %*s %s %*s %*s %*s %*s %*s %d';
%fileObs=(['/fs/vnas_Hdfo/odis/liz001/from_JP/' dateobs '_NEMO-FVCOM_SaintJohn_BOF_Observations' last ]);
%HEADER=1 ; DELIM=',';

%%%%%%%%%%%%%%%%%%%  end of input %%%%%%%%%%%%%%

% [ 1 ] Load list of observation points
%display('[ 2 ] Load list of observation points')
%% Sample of input file:
%fileID = fopen(fileObs);
%summary= textscan(fileID,formatread,'headerlines',HEADER,'delimiter',DELIM);
%fclose(fileID);
%Iuse1=find(summary{1,5}==1);
%Iuse2=find(summary{1,5}==2);
%Iuse3=find(summary{1,5}==3);
%% Get only the flags 1, 2 and 3
%lat1=summary{1,2}(Iuse1); lon1=summary{1,3}(Iuse1);  date1=summary{1,4}(Iuse1); code1=summary{1,1}(Iuse1);
%lat2=summary{1,2}(Iuse2); lon2=summary{1,3}(Iuse2);  date2=summary{1,4}(Iuse2); code2=summary{1,1}(Iuse2);
%lat3=summary{1,2}(Iuse3); lon3=summary{1,3}(Iuse3);  date3=summary{1,4}(Iuse3);
%% clear summary


% copy all *-09.nc file to temporary directory
if icp ==1
display('COPY all -09 files')
copyfile([indir1 tfile],outdir)
copyfile([indir2 tfile],outdir)
end

jkc = dir([outdir conf '-' tst '_1h_' datestr(datenum([year mth 1]),'yyyymm') '*-09.nc'])
for itt = 1:length(jkc)
    clist(itt,:) = jkc(itt).name;
end

[czone jk] = size(clist);

for i=1:czone % go through each location
    jks = dir([outdir '*' clist(i,end-12:end) ]) ;
    clear list
    for itt = 1:length(jks)
        list(itt,:) = jks(itt).name;
    end
    [dnum jk] =size(list) ;
    % make a coordinate file
    ofile1 =[outdir list(2,:)]
    ofile =[outdir  list(1,end-12:end) 'coordinate']
    
    copyfile(ofile1,ofile)
    glamt=ncread(ofile,'nav_lon'); %longitude
    gphit=ncread(ofile,'nav_lat');

    [m n] =size(gphit) ;
    nccreate(ofile,'glamt','Dimensions',{'x' m 'y' n});
    ncwrite(ofile,'glamt',glamt)
    nccreate(ofile,'glamu','Dimensions',{'x' m 'y' n});
    ncwrite(ofile,'glamu',glamt)
    nccreate(ofile,'glamv','Dimensions',{'x' m 'y' n});
    ncwrite(ofile,'glamv',glamt)
    nccreate(ofile,'glamf','Dimensions',{'x' m 'y' n});
    ncwrite(ofile,'glamf',glamt)
    
    nccreate(ofile,'gphit','Dimensions',{'x' m 'y' n});
    ncwrite(ofile,'gphit',gphit)
    nccreate(ofile,'gphiu','Dimensions',{'x' m 'y' n});
    ncwrite(ofile,'gphiu',gphit)
    nccreate(ofile,'gphiv','Dimensions',{'x' m 'y' n});
    ncwrite(ofile,'gphiv',gphit)
    nccreate(ofile,'gphif','Dimensions',{'x' m 'y' n});
    ncwrite(ofile,'gphif',gphit)
    
    for j=1:dnum % number of files for each location
        infile = [outdir list(j,:)] 
        uu =ncread([infile],'uo') ;        
        vv =ncread([infile],'vo') ;
        [ni nj nz nt] =size(uu);
        
        % rotate u/v to true north/east
        for k=1:nz
            for it =1:nt
                ruo(:,:,k,it) = rot_rep_2017(squeeze(uu(:,:,k,it)),squeeze(vv(:,:,k,it)), 'T','ij->e',ofile);
                rvo(:,:,k,it) = rot_rep_2017(squeeze(uu(:,:,k,it)),squeeze(vv(:,:,k,it)), 'T','ij->n',ofile);
            end
        end 
        
        % write rotated u/v
        try 
            ncinfo([infile],'ruo');
        catch err            
            nccreate([infile],'ruo','Dimensions',{'x' ni 'y' nj 'depthu' nz 'time_counter' nt});
        end

        %nccreate([infile],'ruo','Dimensions',{'x' ni 'y' nj 'depthu' nz 'time_counter' nt});
        ncwrite([infile],'ruo',ruo)
        ncwriteatt([infile],'ruo','long_name','ocean current along true east');
        ncwriteatt([infile],'ruo','units','m/s');    
        
        try 
            ncinfo([infile],'rvo');
        catch err            
            nccreate([infile],'rvo','Dimensions',{'x' ni 'y' nj 'depthu' nz 'time_counter' nt});
        end
        %nccreate([infile],'rvo','Dimensions',{'x' ni 'y' nj 'depthv' nz 'time_counter' nt});
        ncwrite([infile],'rvo',rvo)
        ncwriteatt([infile],'rvo','long_name','ocean current along true north');
        ncwriteatt([infile],'rvo','units','m/s');    
        
    end    
end


%%% combine files into monthly files

for i=1:czone % number of stations    
    for j=mth:nmth % number of months
        rvo=[];
        ruo=[];
        vo=[];
        uo=[];
        so=[];
        thetao=[];
        zos=[];
        time_counter=[];
        time_counter_bounds=[];
        time_centered=[];
        time_centered_bounds=[];
        
        %find files in a given month
        dstr = datestr(datenum([year j 1 ]),'yyyymm')
        %list = ls([outdir '*' dstr '*' num2str(code(i)) '*'])
        jkk = dir([outdir '*' dstr '*' clist(i,end-12:end) ])
        clear list
        for itt = 1:length(jkk)
	    list(itt,:) = jkk(itt).name;
	end

        [ilist jk] = size(list) ;
        
        % read an example file to define the size 
        [ia ib iz it] = size(ncread([outdir list(1,:)],'rvo') ) ;
        
        % read all files in a given month        
        for k=1:ilist
            infile = [list(k,:)] ;
            ii=[it*(k-1)+1:it*k] ;
            rvo(:,:,:,ii) = ncread([outdir infile],'rvo') ;
            ruo(:,:,:,ii) = ncread([outdir infile],'ruo') ;
            vo(:,:,:,ii) = ncread([outdir infile],'vo') ;
            uo(:,:,:,ii) = ncread([outdir infile],'uo') ;
            so(:,:,:,ii) = ncread([outdir infile],'so') ;
            thetao(:,:,:,ii) = ncread([outdir infile],'thetao') ;        
            zos(:,:,ii) = ncread([outdir infile],'zos') ;        
            time_counter(ii) = ncread([outdir infile],'time_counter') ;
            time_counter_bounds(:,ii) = ncread([outdir infile],'time_counter_bounds') ;
            time_centered(ii)= ncread([outdir infile],'time_centered') ;        
            time_centered_bounds(:,ii) = ncread([outdir infile],'time_centered_bounds') ;
        end
        
        % change zeros to nan;
        ruo(ruo==0) = nan;
        rvo(rvo==0) = nan;
        uo(uo==0) = nan;
        vo(vo==0) = nan;
        so(so==0) = nan;
        thetao(thetao==0) = nan;
        zos(zos==0) = nan;
        
        
        % find data in month j        
        tvec = datevec(time_counter/86400+datenum(time_origin));
        jkmm = datevec(datestr([year, j,1,0,0,0]))
        jj=find(tvec(:,2)==jkmm(2)) ;       
        
        tstr1 =  datestr(datenum(tvec(jj(1),:)),'yyyymmdd')
        tstr2 =  datestr(datenum(tvec(jj(end),:)),'yyyymmdd')
        
        % define monthly file name
        pre = infile(1:end-30);
        apx = infile(end-12:end);
        namefile=[pre tstr1 '_' tstr2 apx] ;
        copyfile([outdir infile],[outdir2 namefile])
        ncwrite([outdir2 namefile],'ruo',ruo(:,:,:,jj))
        ncwrite([outdir2 namefile],'rvo',rvo(:,:,:,jj))
        ncwrite([outdir2 namefile],'uo',uo(:,:,:,jj))
        ncwrite([outdir2 namefile],'vo',vo(:,:,:,jj))
        ncwrite([outdir2 namefile],'so',so(:,:,:,jj))
        ncwrite([outdir2 namefile],'thetao',thetao(:,:,:,jj))
        ncwrite([outdir2 namefile],'zos',zos(:,:,jj))
        ncwrite([outdir2 namefile],'time_counter',time_counter(jj))
        ncwrite([outdir2 namefile],'time_counter_bounds',time_counter_bounds(:,jj))
        ncwrite([outdir2 namefile],'time_centered',time_centered(jj))
        ncwrite([outdir2 namefile],'time_centered_bounds',time_centered_bounds(:,jj))
        ncwriteatt([outdir2 namefile],'/','modification_date',datestr(now))
        ncwriteatt([outdir2 namefile],'/','name',namefile)
        
    end
end
