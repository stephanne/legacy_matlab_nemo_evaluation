%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Purpose: extract hourly U/V T/S SSH in A1 zone of BoF180 defined in prepare_xml_files_BoF180.m
% Input:    BoF180-T10_1h_20150201_20150205_A1_T.nc
% Output:   BoF180-T10_1h_20150201_20150205_code1-09.nc 
%
% Author: Li Zhai 
% Date:   Aug 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-------Header information from prepare_xml_files_BoF180.m---------------
% CALLED PGM or FUNCTIONS: m_map package
%                          f_location
%
% NOTES: 
%   Inputs : codification of observation's priority
%           1- highest priority: mostly in Harbour 
%           2- mid level priority & modified "type 1" outside of harbour
%           3- Minas passage
%           0- on s'en fout
%   Added column "NEMO ID": with integer codes for stations following
%           999999 -> Smart buoy
%           8xxxxx -> Tide gauges (if outside of BoF180 => flag==0)
%           7xxxxx -> CTDs
%           6xxxxx -> ADPCs
%
%   Outputs : To avoid having a too large number of output files (>100) 
%            the approach is to output:
%               a- hourly data of the harbour area (#1), 
%               b- specific locations for #2 (3x3 centerd on closest model
%               point
%               c- small area in Minas passage (#3)
%
%  f_location
%        idx(:,2) => "i" location
%        idx(:,3) => "j" location
%
%
%----------End Header------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%   BEGIN Input   %%%%%%%%%%%%%%%%%%%%%%
clear
addpath /fs/vnas_Hdfo/odis/jpp001/matlab_scripts/mat_BoF/function


timebeg=clock;
display([datestr(timebeg)])

% ---  Observations file (export .xls file saved to .csv file with
% modifications to input already included
dateobs='20170728'; last='_Phase1plus_JP.csv';  
formatread='%*s %*s %d %f %f %*s %*s %s %*s %*s %*s %*s %*s %d';
fileObs=(['/home/jpp001/matlab_scripts/mat_BoF/mat/' dateobs '_NEMO-FVCOM_SaintJohn_BOF_Observations' last ]);
HEADER=1 ; DELIM=',';

% input data
ddir = '/space/hall0/sitestore/dfo/odis/jpp001/' ;
conf = 'SJAP100';
tst = 'P11' ;
indir=[ddir conf '/' conf '-' tst '-OUT/DIMG/']
tfile = [conf '-' tst '*A1_T.nc']
ufile = [conf '-' tst '*A1_U.nc']
vfile = [conf '-' tst '*A1_V.nc']
% list of file 
jkt = dir([indir tfile])  ;
jku = dir([indir ufile])  ;
jkv = dir([indir vfile])  ;
%clear tlist ;ulist;vlist;
for itt = 1:length(jku)
    tlist(itt,:) = jkt(itt).name;
    ulist(itt,:) = jku(itt).name;
    vlist(itt,:) = jkv(itt).name;
end

coord = [indir 'SJAP100-P11_1h_20150301_20150303_A1_T.nc'];   %JPP--keep as T files zos is read later!
%bathy = ['/home/jpp001/disk/INITIALISATION/SJAP100/Bathymetry_SJAP100_NEW_range_0.5_7m_smooth.nc'];

outdir = [ddir conf '/' conf '-' tst '-OUT/A1/']

tmpfile =[ conf '-' tst '_1h_20160608_20160610_700068-09.nc'] % example file to copy from structure...
% 
time_origin   = '1950-01-01 00:00:00'  % must be consistent with iodef.xml for the run
                                       % (all runs should have same time_origin)
% ---- required period NOT used
%year =2015 ;
%mth = 2 ;
%day =1 ;
%dnum = 31;
% ----

% --- Parameters for search radius and buffers zone around the minimal
%     region that includes all observations from a group
% --- !!!!!! Need to be consistent with prepare_xml_files_${conf}.m
if      strcmp(conf,'BoF180')
  dx=0.05 ; % search radius in deg 
%  buffer=5; % # of extra grid points on each side
elseif  strcmp(conf,'SJAP100')
  dx=0.0025 ; % search radius in deg 
%  buffer=1%5; % # of extra grid points on each side    % JPP change to 1
elseif  strcmp(conf,'BoFSS036')
  dx=0.05 ; % search radius in deg 
%  buffer=2; % # of extra grid points on each side
end
%%%%%%%%%%%%%%%%%%%  end of input %%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [ 2 ] Load list of observation points
display('[ 2 ] Load list of observation points')
% Sample of input file:
fileID = fopen(fileObs);
summary= textscan(fileID,formatread,'headerlines',HEADER,'delimiter',DELIM);
fclose(fileID);
Iuse1=find(summary{1,5}==1);
Iuse2=find(summary{1,5}==2);
Iuse3=find(summary{1,5}==3);
% Get only the flags 1, 2 and 3
lat1=summary{1,2}(Iuse1); lon1=summary{1,3}(Iuse1);  date1=summary{1,4}(Iuse1); code1=summary{1,1}(Iuse1);
[nn,~]=size(lat1);
%lat2=summary{1,2}(Iuse2); lon2=summary{1,3}(Iuse2);  date2=summary{1,4}(Iuse2); code2=summary{1,1}(Iuse2);
%lat3=summary{1,2}(Iuse3); lon3=summary{1,3}(Iuse3);  date3=summary{1,4}(Iuse3);
% clear summary

%%%%%%%%%%%%%%%%%%%%%%%%%%
% load coordinates of A1 ZONE
elon = ncread(coord,'nav_lon') ; %%longitude
elat = ncread(coord,'nav_lat') ; %%latitude
var  = ncread(coord,'zos')     ; %%SSH
elon(elon==0)=nan;
elat(elat==0)=nan;
var (var==0 )=NaN;
[NX,NY]=size(elon);

% load bathymetry of BoF180 
%nlon = ncread(bathy,'nav_lon') ;
%nlat = ncread(bathy,'nav_lat') ;
%bby = ncread(bathy,'Bathymetry') ;
%bby = ncread(bathy,'Bathymetry') ;
%bby(bby==0) = nan;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [ 3 ] Find regions and specific locations
%display('[ 3 ] Find regions and specific locations')
%display('[ 3-1 ] Priority #1 area')
%[imin1,imax1,jmin1,jmax1]=f_define_area(lon1,lat1,nlon,nlat,bby,dx,buffer);
%nbi1=imax1-imin1+1;  nbj1=jmax1-jmin1+1;
%dep=bby(imin1:imax1,jmin1:jmax1);
% JPP-- replace dep by a simple mask where land = NaN and ocean ~=NaN
dep=squeeze(var(:,:,1));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Find ZONE ONE location
idx=[];
idx=f_location(lon1,lat1,elon,elat,dep,dx) ;
%save([outdir 'indices_SJAP.txt'],'idx','Observation') 

%JPP-- Add check to verify the observation is within the subdomain 
%      (IMPORTANT FOR SJAP100 and for future changes to obs list...)
valid=find(~isnan(idx(:,2)) & ~isnan(idx(:,3)) & idx(:,2)>2  & idx(:,3)>2 & idx(:,2)<NX-1  & idx(:,3)<NY-1 );
iobs=idx(valid,2);
jobs=idx(valid,3);
cobs=code1(valid);
[nbobs,~]=size(valid);
toto=idx(valid,:);
idx=toto;
code1=cobs;
%JPP-- 
%save('indices_SJAP100.txt','iobs','jobs','cobs','-ascii', '-double', '-tabs')

fileID = fopen('indices_SJAP100.txt','w');
formatwrite='%s %s %s \n'
for oo=1:nbobs
  tox=num2str(iobs(oo))
  toy=num2str(jobs(oo))
  cod=num2str(code1(oo))
  fprintf(fileID,formatwrite,tox,toy,cod);
end 
fclose(fileID);
pause
% check
%figure
%set(gca,'fontsize',14)
%pcolor(elon,elat,dep)
%shading flat
%hold on
%plot(lon1,lat1,'m*')
%hold on
%plot(idx(:,5),idx(:,4),'mo')

%figure
%set(gca,'fontsize',14)
%pcolor(dep')
%shading flat
%hold on
%plot(idx(:,2),idx(:,3),'mo')



%%%%%%%%%%%%%%%%%%
% read A1 Zone T, U, V data

[m n] =size(idx) 
ilist = size(tlist)  % total number of A1 zone files

for i=1:ilist(1)
tic
    infile=[ indir tlist(i,:)] 
   
    time_counter = ncread(infile,'time_counter') ;  
    time_counter_bounds=ncread(infile,'time_counter_bounds') ;
    
    time_centered = ncread(infile,'time_centered') ;  
    time_centered_bounds = ncread(infile,'time_centered_bounds') ;  

display('load T data')
    thetao = ncread(infile,'thetao') ;
    so = ncread(infile,'so') ;
    zos = ncread(infile,'zos') ;

display('load U data')
    infile=[indir  ulist(i,:)] 
    uo = ncread(infile,'uo') ;
    
display('load V data')
    infile=[indir  vlist(i,:)] 
    vo = ncread(infile,'vo') ;   
    
    % extract points lat lon
    tglon=[];tglat=[];tguo=[];tgvo=[];tgso=[];tgto=[];tgzos=[];
    for j=1:m
        j
        ofile = [outdir tlist(i,1:end-7) num2str(code1(j)) '-09.nc'] ;
        display([ num2str(j) '   ' ofile])
        tglon(:,:) = elon(idx(j,2)-1:idx(j,2)+1,idx(j,3)-1:idx(j,3)+1);
        tglat(:,:) = elat(idx(j,2)-1:idx(j,2)+1,idx(j,3)-1:idx(j,3)+1);
        tgvo = vo(idx(j,2)-1:idx(j,2)+1,idx(j,3)-1:idx(j,3)+1,:,:);
        tgvo(tgvo==0)=nan;
        
        tguo = uo(idx(j,2)-1:idx(j,2)+1,idx(j,3)-1:idx(j,3)+1,:,:);
        tguo(tguo==0)=nan;
        
        tgso = so(idx(j,2)-1:idx(j,2)+1,idx(j,3)-1:idx(j,3)+1,:,:);
        tgso(tgso==0)=nan;
        
        tgto = thetao(idx(j,2)-1:idx(j,2)+1,idx(j,3)-1:idx(j,3)+1,:,:);
        tgto(tgto==0)=nan;
        
        
        tgzos = zos(idx(j,2)-1:idx(j,2)+1,idx(j,3)-1:idx(j,3)+1,:);
        tgzos(tgzos==0)=nan;
        
        %write files
%        ofile = [outdir tlist(i,1:end-7) num2str(code1(j)) '-09.nc'] ;
        copyfile([indir tmpfile],[ofile])
        ncwrite(ofile,'nav_lon',tglon)
        ncwrite(ofile,'nav_lat',tglat)
        ncwrite(ofile,'vo',tgvo)
        ncwrite(ofile,'uo',tguo)
        ncwrite(ofile,'thetao',tgto) 
        ncwrite(ofile,'so',tgso)
        ncwrite(ofile,'zos',tgzos)
        ncwrite(ofile,'time_counter',time_counter)        
        ncwrite(ofile,'time_centered',time_centered)
        ncwrite(ofile,'time_centered_bounds',time_centered_bounds)
        ncwrite(ofile,'time_counter_bounds',time_counter_bounds)
    end     
    clear thetao so zos uo vo 
    display([' Time required to process ' infile ])
toc
end


timeend=clock
display(' TOTAL TIME FOR EXTRACTION')
display([datestr(timebeg)])
display([datestr(timeend)])
