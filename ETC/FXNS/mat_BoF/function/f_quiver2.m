% https://www.mathworks.com/matlabcentral/newsreader/view_thread/263647
% overlay 2 velocity plots
% checking
% f_quiver2(alon,alat,muu',mvv',alon,alat,muu',mvv',0.5)

function [ ]=f_quiver2(X1,Y1,U1,V1,X2,Y2,U2,V2,qscale)
%qscale = 0.1 ; % scaling factor for all vectors
h1 = quiver(X1,Y1,U1,V1,0,'k','linewidth',2) ; % the '0' turns off auto-scaling
hold on
h2 = quiver(X2,Y2,U2,V2,0,'m','linewidth',2) ;
hU = get(h1,'UData') ;
hV = get(h1,'VData') ;
set(h1,'UData',qscale*hU,'VData',qscale*hV)
hU = get(h2,'UData') ;
hV = get(h2,'VData') ;
set(h2,'UData',qscale*hU,'VData',qscale*hV)