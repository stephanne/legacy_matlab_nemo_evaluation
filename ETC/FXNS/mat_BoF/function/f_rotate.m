

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rotate east-north to cross-along transect
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%lon, lat are observed locations along transect
%x,y is u/v velocity
function [xx yy]=f_rotate(lon,lat,x,y)

% compute angle 
dy = lat(2)-lat(1) ;
dx = lon(2)-lon(1) ;
if dx*dy<0 % domain 2 & 4
angle = 90+ atan(diff(lat)./diff(lon))*360/pi/2
end

if dx*dy>0 % domain 1 &3
angle = atan(diff(lat)./diff(lon))*360/pi/2 -90
end


%rotate
xx=x*cosd(angle) + y*sind(angle) ;
yy=y*cosd(angle) - x*sind(angle) ;




