

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute spectra
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. replace nan with time mean
%2. remove time mean
%3. dt is sampling frequency, i.e. 1 day or 1 km
%% [pw f]=f_spectra(uu,dt) 
%% f is frequency, unit in cycle per day or km
%% pw is power spectra
function [pww f]=f_spectra(uu,dt)
%% replace nan with means
ii = find(isnan(uu)==1) ;
uu(ii) = nanmean(uu);
%% remove means
uu = uu-mean(uu) ;
[pw wx] = pwelch(uu,[],[],[]) ;
%%!!!! check variance !!!
df = mean(diff(wx));
%[nanvar(uu) sum(pw)*df] ;       

%change unit
f=wx/2/dt/pi ; %% unit in cycle per hour or km
pww = pw*(2*dt*pi) ; %% unit in (??)^2/(cph)

