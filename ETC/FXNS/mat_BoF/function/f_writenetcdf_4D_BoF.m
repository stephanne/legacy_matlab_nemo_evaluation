%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modified by Li Zhai for NEMO3.6 and Bof036
% Date: May 2017
%
%
% Previously modified by Shiliang Shan & .....
% Original Author:  JP Paquin - Jun2013 : Adapt for netcdf 3.6.2

function[isok]=f_writenetcdf_4D_BoF(namefile,varNC,...
                             lon,lat,mdepth,time_counter,data)
isok=0;
[NX,NY,NZ,NT]=size(data);

% - Information for netCDF files 
[aut,~]=evalc('system(''whoami'')');

ncout=netcdf.create([namefile],'NC_write');
% DEFINE DIMENSIONS AND ATTRIBUTES
dimidx = netcdf.defDim(ncout,'x',NX);
dimidy = netcdf.defDim(ncout,'y',NY); 
if NZ>1
dimidz = netcdf.defDim(ncout,'z',NZ);
end
dimidt = netcdf.defDim(ncout,'time_counter',netcdf.getConstant('NC_UNLIMITED'));

% Global attributes
globatt=netcdf.getConstant('NC_GLOBAL');
netcdf.putAtt(ncout,globatt,'Author',aut)
netcdf.putAtt(ncout,globatt,'Date',date)

idnavlon = netcdf.defVar(ncout,'nav_lon'     ,'float' ,[dimidx,dimidy]);
netcdf.putAtt(ncout,idnavlon,'units','degrees_east');
netcdf.putAtt(ncout,idnavlon,'valid_min', min(lon(:)) );
netcdf.putAtt(ncout,idnavlon,'valid_max', max(lon(:)) );
netcdf.putAtt(ncout,idnavlon,'long_name','Longitude at t-point');
netcdf.putAtt(ncout,idnavlon,'units','degrees_east');

idnavlat = netcdf.defVar(ncout,'nav_lat'     ,'float' ,[dimidx,dimidy]);
netcdf.putAtt(ncout,idnavlat,'units','degrees_north');
netcdf.putAtt(ncout,idnavlat,'valid_min', min(lat(:)) );
netcdf.putAtt(ncout,idnavlat,'valid_max', max(lat(:)) );
netcdf.putAtt(ncout,idnavlat,'long_name','Latitude at t-point');
netcdf.putAtt(ncout,idnavlat,'units','degrees_north');

% define variables and their attributes
if NZ>1
iddepth = netcdf.defVar(ncout, 'deptht', 'float', dimidz);
netcdf.putAtt(ncout, iddepth, 'units', 'model_levels');
netcdf.putAtt(ncout, iddepth, 'valid_min', min(mdepth) );
netcdf.putAtt(ncout, iddepth, 'valid_max', max(mdepth) );
netcdf.putAtt(ncout, iddepth, 'long_name', 'Model levels');
end
        
idtime   = netcdf.defVar(ncout,'time_counter','double', dimidt );
netcdf.putAtt(ncout,idtime,'units','seconds since 1950-01-01 00:00:00');
netcdf.putAtt(ncout,idtime,'calendar','gregorian');
netcdf.putAtt(ncout,idtime,'title','Time');
netcdf.putAtt(ncout,idtime,'long_name','Time axis');
netcdf.putAtt(ncout,idtime,'time_origin','1950-01-01 00:00:00');

switch varNC
case {'thetao'}
  idtemp = netcdf.defVar(ncout,'thetao'    ,'float' ,[dimidx,dimidy,dimidz,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','deg_C');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','temperature');
    netcdf.putAtt(ncout,idtemp,'standard_name','sea_water_potential_temperature');
    
case {'so'}
  idtemp = netcdf.defVar(ncout,'so'    ,'float' ,[dimidx,dimidy,dimidz,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','PSU');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','salinity');  
    netcdf.putAtt(ncout,idtemp,'standard_name','sea_water_practical_salinity');  
    
case {'zos'}
  idtemp = netcdf.defVar(ncout,'zos'    ,'float' ,[dimidx,dimidy,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Sea surface height');
    netcdf.putAtt(ncout,idtemp,'standard_name','sea_surface_height_above_geoid');
    
case {'runoff'}
  idtemp = netcdf.defVar(ncout,'runoff'    ,'float' ,[dimidx,dimidy,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','kg/s/m^2');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','river/melt runoffs');
    netcdf.putAtt(ncout,idtemp,'standard_name','river/melt runoffs');
    
case {'runofftemp'}
  idtemp = netcdf.defVar(ncout,'runofftemp'    ,'float' ,[dimidx,dimidy,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','degree_C');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','river/melt runoff temperature');
    netcdf.putAtt(ncout,idtemp,'standard_name','river/melt temperature');
    
case {'vozotrtx'} % U depth mean
  idtemp = netcdf.defVar(ncout,'vozotrtx'    ,'float' ,[dimidx,dimidy,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m/s');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Zonal depth mean velocity');
case {'vometrty'} % V depth mean
  idtemp = netcdf.defVar(ncout,'vometrty'    ,'float' ,[dimidx,dimidy,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m/s');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Zonal depth mean velocity');
case {'vozocrtx'} % U
  idtemp = netcdf.defVar(ncout,'vozocrtx'    ,'float' ,[dimidx,dimidy,dimidz,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m s-1');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Zonal velocity');
case {'vomecrty'} % V
  idtemp = netcdf.defVar(ncout,'vomecrty'    ,'float' ,[dimidx,dimidy,dimidz,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m s-1');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(data(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(data(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Meridional velocity');
end
    
netcdf.endDef(ncout);

% PUT VARIABLES
netcdf.putVar(ncout,idnavlon,lon)
netcdf.putVar(ncout,idnavlat,lat)
if NZ>1
netcdf.putVar(ncout,iddepth,mdepth)
end
netcdf.putVar(ncout,idtime,0,NT,time_counter)
netcdf.putVar(ncout,idtemp,data)
netcdf.close(ncout)

isok=1;
end
