%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modified by Li Zhai for NEMO3.6 and Bof036
% Date: May 2017
%
% NOTE:  North and East OBCs for NEMO3.6 must be inverted to
%        have index 1 at the outer layer increasing inward. DONE!!
%
% Previously modified by Shiliang Shan & .....
% Original Author:  JP Paquin - Jun2013 : Adapt for netcdf 3.6.2

function[isok]=f_writenetcdfbdy4D(namefile,varNC,...
                             lon_obc,lat_obc,mdepth,time_counter,fld_obc,obcname,nobc)
isok=0;
[NX,NY,NZ,NT]=size(fld_obc);
nxbT = NX*NY;

if NX == nobc
    lon_obc = permute(lon_obc,[2,1,3,4]) ;
    lat_obc = permute(lat_obc,[2,1,3,4]) ;
    fld_obc = permute(fld_obc,[2,1,3,4]) ;   
end

% - Information for netCDF files 
[aut,~]=evalc('system(''whoami'')');

switch obcname
    
    case {'east'}
        
        % flip matrix 
        lon_obc=flip(lon_obc,2);
        lat_obc=flip(lat_obc,2);     
        fld_obc=flip(fld_obc,2);       
    case {'north'}
        
        % flip coordinates
        lon_obc=flip(lon_obc,2);
        lat_obc=flip(lat_obc,2);        
        fld_obc=flip(fld_obc,2);       
                
end

        % reshape variable
        bdylon = reshape(lon_obc, 1, nxbT);
        bdylat = reshape(lat_obc, 1, nxbT);
        bdyvar = reshape(fld_obc, 1, nxbT, NZ, NT);


ncout=netcdf.create([namefile],'NC_write');
% DEFINE DIMENSIONS AND ATTRIBUTES
dimidx = netcdf.defDim(ncout,'x',nxbT);
dimidy = netcdf.defDim(ncout,'y',1); 
if NZ>1
dimidz = netcdf.defDim(ncout,'z',NZ);
end
dimidt = netcdf.defDim(ncout,'time_counter',netcdf.getConstant('NC_UNLIMITED'));

% Global attributes
globatt=netcdf.getConstant('NC_GLOBAL');
netcdf.putAtt(ncout,globatt,'Description','boundary conditions')
netcdf.putAtt(ncout,globatt,'Author',aut)
netcdf.putAtt(ncout,globatt,'Date',date)
netcdf.putAtt(ncout,globatt,'Convention','GDT 1.2')
netcdf.putAtt(ncout, globatt, 'NEMO_version', 'v3.6')
netcdf.putAtt(ncout, globatt, 'Boundary', obcname)
% netcdf.putAtt(ncout, globatt, 'nbdyind', num2str(nbdyind))
% netcdf.putAtt(ncout, globatt, 'nbdybeg', num2str(nbdybeg))
% netcdf.putAtt(ncout, globatt, 'nbdyend', num2str(nbdyend))

idnavlon = netcdf.defVar(ncout,'nav_lon'     ,'float' ,[dimidx,dimidy]);
netcdf.putAtt(ncout,idnavlon,'units','degrees_east');
netcdf.putAtt(ncout,idnavlon,'valid_min', min(bdylon) );
netcdf.putAtt(ncout,idnavlon,'valid_max', max(bdylon) );
netcdf.putAtt(ncout,idnavlon,'long_name','Longitude at t-point');

idnavlat = netcdf.defVar(ncout,'nav_lat'     ,'float' ,[dimidx,dimidy]);
netcdf.putAtt(ncout,idnavlat,'units','degrees_north');
netcdf.putAtt(ncout,idnavlat,'valid_min', min(bdylat) );
netcdf.putAtt(ncout,idnavlat,'valid_max', max(bdylat) );
netcdf.putAtt(ncout,idnavlat,'long_name','Latitude at t-point');

% define variables and their attributes
if NZ>1
iddepth = netcdf.defVar(ncout, 'deptht', 'float', dimidz);
netcdf.putAtt(ncout, iddepth, 'units', 'model_levels');
netcdf.putAtt(ncout, iddepth, 'valid_min', min(mdepth) );
netcdf.putAtt(ncout, iddepth, 'valid_max', max(mdepth) );
netcdf.putAtt(ncout, iddepth, 'long_name', 'Model levels');
end
        
idtime   = netcdf.defVar(ncout,'time_counter','double', dimidt );
netcdf.putAtt(ncout,idtime,'units','seconds since 1950-01-01 00:00:00');
netcdf.putAtt(ncout,idtime,'calendar','gregorian');
netcdf.putAtt(ncout,idtime,'title','Time');
netcdf.putAtt(ncout,idtime,'long_name','Time axis');
netcdf.putAtt(ncout,idtime,'time_origin','1950-JAN-01 00:00:00');

switch varNC
case {'votemper'}
  idtemp = netcdf.defVar(ncout,'votemper'    ,'float' ,[dimidx,dimidy,dimidz,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','deg_C');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','votemper');
case {'vosaline'}
  idtemp = netcdf.defVar(ncout,'vosaline'    ,'float' ,[dimidx,dimidy,dimidz,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','PSU');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','vosaline');  
case {'sossheig'}
  idtemp = netcdf.defVar(ncout,'sossheig'    ,'float' ,[dimidx,dimidy,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Sea surface height');
case {'vozotrtx'} % U depth mean
  idtemp = netcdf.defVar(ncout,'vozotrtx'    ,'float' ,[dimidx,dimidy,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m/s');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Zonal depth mean velocity');
case {'vometrty'} % V depth mean
  idtemp = netcdf.defVar(ncout,'vometrty'    ,'float' ,[dimidx,dimidy,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m/s');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Zonal depth mean velocity');
case {'vozocrtx'} % U
  idtemp = netcdf.defVar(ncout,'vozocrtx'    ,'float' ,[dimidx,dimidy,dimidz,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m s-1');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Zonal velocity');
case {'vomecrty'} % V
  idtemp = netcdf.defVar(ncout,'vomecrty'    ,'float' ,[dimidx,dimidy,dimidz,dimidt]);
    netcdf.putAtt(ncout,idtemp,'units','m s-1');
    netcdf.putAtt(ncout,idtemp,'valid_min',min(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'valid_max',max(bdyvar(:)));
    netcdf.putAtt(ncout,idtemp,'long_name','Meridional velocity');
end
    
netcdf.endDef(ncout);

% PUT VARIABLES
netcdf.putVar(ncout,idnavlon,bdylon)
netcdf.putVar(ncout,idnavlat,bdylat)
if NZ>1
netcdf.putVar(ncout,iddepth,mdepth)
end
netcdf.putVar(ncout,idtime,0,NT,time_counter)

display('       WARNING NaNs ARE REPLACED BY 0 !!!')
bdyvar(isnan(bdyvar))=0;

netcdf.putVar(ncout,idtemp,bdyvar)
netcdf.close(ncout)

isok=1;
end
