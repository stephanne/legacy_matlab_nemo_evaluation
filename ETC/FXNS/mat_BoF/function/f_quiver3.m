% https://www.mathworks.com/matlabcentral/newsreader/view_thread/263647
% overlay 2 velocity plots
% checking
% f_quiver2(alon,alat,muu',mvv',alon,alat,muu',mvv',0.5)
% splot ==0 draw vectors on the same plot
% splot ==1 draw vectors on the seperate plot

% Zhai
function [ ]=f_quiver2(X1,Y1,U1,V1,X2,Y2,U2,V2,X3,Y3,U3,V3,qscale,splot)
%qscale = 0.1 ; % scaling factor for all vectors
if splot==0
h1 = quiver(X1,Y1,U1,V1,0,'k','linewidth',1) ; % the '0' turns off auto-scaling
hold on
h2 = quiver(X2,Y2,U2,V2,0,'m','linewidth',2) ;
h3 = quiver(X3,Y3,U3,V3,0,'g','linewidth',2) ;
else splot==1
    subplot(311)
    set(gca,'fontsize',14)
    h1 = quiver(X1,Y1,U1,V1,0,'k','linewidth',1) ; % the '0' turns off auto-scaling
    subplot(312)
    set(gca,'fontsize',14)
    h2 = quiver(X2,Y2,U2,V2,0,'m','linewidth',2) ;
    subplot(313)
    set(gca,'fontsize',14)
    h3 = quiver(X3,Y3,U3,V3,0,'g','linewidth',2) ;
end

hU = get(h1,'UData') ;
hV = get(h1,'VData') ;
set(h1,'UData',qscale*hU,'VData',qscale*hV)
hU = get(h2,'UData') ;
hV = get(h2,'VData') ;
set(h2,'UData',qscale*hU,'VData',qscale*hV)
hU = get(h3,'UData') ;
hV = get(h3,'VData') ;
set(h3,'UData',qscale*hU,'VData',qscale*hV)