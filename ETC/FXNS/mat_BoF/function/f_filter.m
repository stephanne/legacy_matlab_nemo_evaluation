% low-pass filter
% dt is sampling frequency
% T is cut-off period
% tmp is time series
% oder = 3 ; %order of filter defort is 3
function [feta] = f_filter(dt,T,tmp,oder)
ii=find(isnan(tmp)==1); 
jj=find(isnan(tmp)==0); 
tmp(ii) = mean(tmp(jj));
if isempty(oder) ==1
    doder =3;
else
    doder =oder;
end

[b,a] = butter(doder,2*dt/T) ;  % !!!!!
feta = filtfilt(double(b),double(a),tmp) ;
feta(ii)=nan;
