%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% opareadj.m - Read all the 2-D, 3-D and 4-D NEMO output variables         %
%                                                                          %
%  This programe is mainly focused on simplify the reading process of      %
%  NEMO output netcdf file for Matlab users.                               %
%  NOTE, the assumed indexing for 2D data is data(y,x), for 3D data is     %
%  data(t,y,x) and for 4D data is data(t,z,y,x).                           %
%                                                                          %
%  Modified by li Zhai for Matlab version R2012a
% Author: JI LEI windflowerplus@gmail.com                                  %
% Originate Author: Zeliang Wang                                           %
% Date:   25 Jun 2010                                                      %
%                                                                          %
% Modified: 9 Dec 2010 by JI LEI to fix the bug of trouble reading 2D/3D   %                                                                          %
%           regional data.                                                 %
%                                                                          %
%                                                                          %
% Input:                                                                   %
% dirname - location of the files                                          %
% grid    - which grid file to read (grid_T, icemod, etc)                  %
% var     - name of variable (votemper, iicethic, etc)                     %
% t       - time snapshots to read (ignored for 1d variables)              %
% z       - vertical levels to read (ignored for 2d variables)             %
% x       - x range to extract                                             %
% y       - y range to extract                                             %
%                                                                          %
% Output:                                                                  %
% array   - the data indexed t,z,y,x                                       %
% T0      - only when reading 1D variable time_counter                     %
%                                                                          %                         
% Usage:                                                                   %
% s=opareadj('data/Runsample','grid_T','vosaline')                         %
% s=opareadj('data/Runsample','grid_T','vosaline',1:12)                    %
% s=opareadj('data/Runsample','grid_T','vosaline',':',1:24,200:300,1:50)   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [array T0] = opareadjzhai(dirname, grid, var, t, z, y, x)

array=[]; T0=[];
if ~exist ('t','var');t=':';end;
if ~exist ('z','var');z=':';end;
if ~exist ('x','var');x=':';end;
if ~exist ('y','var');y=':';end;

% record cwd, change to directory of files
cdir=pwd;
cd(dirname);

% look for netcdf files
A=dir(['*' grid '.nc']);
B=dir(['*' grid '_*.nc'])
C=dir(grid);

% if length(A) == 1 || length(C) == 1
%     % Case 1: Found a single merged file
%     if length(A)==1
%         fname=A(1).name;
%     else
%         fname=C(1).name;
%     end
%     % open the file
%     nc=netcdf(fname,'nowrite');
% 
%     % determine the dimensions of the requested variable
%     ndim=numel(dim(nc{var}));
% 
%     % load the data
%     if ndim==4,
%         array=nc{var}(t,z,y,x);
%     elseif ndim==3
%         array=nc{var}(t,y,x);
%     elseif ndim==2
%         array=nc{var}(y,x);
%     end
% 
%     % special case: time_counter
%     if isequal('time_counter',var)
%         array = nc{var}(:);
%         T0=nc{var}.time_origin(:);
%         % reorder the string sso datenum can read it
%         T0=[T0(11:12) T0(6:10) T0(2:5) T0(13:21)];
%     end
% 
%     % close file
%     close(nc);
% 
% elseif length(B) > 1
    % Case 2: Found multi-processor files

    % open the first file
   % nc=netcdf(B(1).name,'nowrite');
   B(1).name
    nc=netcdf.open(B(1).name,'nowrite'); % Zhai
  %  ncdisp(B(1).name) ;

    % get the global dimensions
[dimname, dimlen] = netcdf.inqDim(nc,0) ;
%     NX=nc.DOMAIN_size_global(1);
%     NY=nc.DOMAIN_size_global(2);

%DOMAIN_size_global = ncreadatt(B(1).name,'/','DOMAIN_size_global') ;
DOMAIN_size_global = netcdf.getAtt(nc,netcdf.getConstant('NC_GLOBAL'),'DOMAIN_size_global') ;
NX = DOMAIN_size_global(1) ;
NY = DOMAIN_size_global(2) ;

% determine the dimensions of the requested variable
%   ndim=numel(dim(nc{var}));
% Get variable ID of the first variable, given its name.
varid = netcdf.inqVarID(nc,var);
% Get information about first variable in the file.
[varname, xtype, dimids, atts] = netcdf.inqVar(nc,varid);
ndim= numel(dimids) 

    % special case: time_counter
    if isequal('time_counter',var)
%         array = nc{var}(:);
%         T0=nc{var}.time_origin(:);
 % Get ID of variable, given its name.
varid = netcdf.inqVarID(nc,var);
% Get value of attribute.
T0 = netcdf.getAtt(nc,varid,'time_origin');
array = netcdf.getVar(nc,varid);   
        
        % reorder the string sso datenum reads it
% time_origin   = ' 2003-JAN-05 00:00:00'
% T0=[T0(11:12) T0(6:10) T0(2:5) T0(13:21)];
    end
    
    % Zhai 
    if isequal('depthu',var)
varid = netcdf.inqVarID(nc,var);
array = netcdf.getVar(nc,varid);   
    end
    
    % close first file
    netcdf.close(nc);

    if ndim > 1
        % prepare output array
        nt=length(t);
        if ndim==4
            nz=length(z);
 %           array_h=zeros(nt,nz,NY,NX);
            array_h=zeros(NX,NY,nz,nt);
           
        elseif ndim==3
 %           array_h=zeros(nt,NY,NX);
            array_h=zeros(NX,NY,nt);
           
        elseif ndim==2
            array_h=zeros(NX,NY);
        end

        % loop over processors
        for p=1:length(B)
    %        nc=netcdf(B(p).name,'nowrite');
nc=netcdf.open(B(p).name,'nowrite'); % Zhai
DOMAIN_position_first = netcdf.getAtt(nc,netcdf.getConstant('NC_GLOBAL'),'DOMAIN_position_first'); 
DOMAIN_position_last  =  netcdf.getAtt(nc,netcdf.getConstant('NC_GLOBAL'),'DOMAIN_position_last') ;

            BB=DOMAIN_position_first ;
            EE=DOMAIN_position_last ;

            Ib=BB(1); Jb=BB(2);
            Ie=EE(1); Je=EE(2);

            I=Ie-Ib+1;
            J=Je-Jb+1;
            if ndim==4
%                data = nc{var}(t,z,1:J,1:I);
  data = netcdf.getVar(nc,varid,[0 0 z(1)-1 t(1)-1],[I J nz nt]);   
%              array_h(:,:,Jb:Je,Ib:Ie)=reshape(data,[nt nz J I]);
              array_h(Ib:Ie,Jb:Je,:,:)=data;

                elseif ndim==3
%                 data = nc{var}(t,1:J,1:I);
%                 array_h(:,Jb:Je,Ib:Ie)=reshape(data,[nt J I]);
              data = netcdf.getVar(nc,varid,[0 0 t(1)-1],[I J nt]);   
              array_h(Ib:Ie,Jb:Je,:)=reshape(data,[I J nt]);

                
            elseif ndim==2
%                array_h(Jb:Je,Ib:Ie)= nc{var}(1:J,1:I);
                array_h(Ib:Ie,Jb:Je)= nc{var}(1:I,1:J);
                
                
            end

            netcdf.close(nc);

        end
        if ndim==4
            array=array_h(x,y,:,:);
        elseif ndim==3
            array=array_h(x,y,:);
        elseif ndim==2
            array=array_h(x,y);
        end
    end
%end

% return to original directory
cd(cdir);

% remove singleton dimensions
array=squeeze(array);
end

