%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% New Usage: read abort or mask fields from NEMO3.6. Zhai                                                                 %
% s=f_readopa('data/Runsample','abort','vosaline',1,:,:,1:50)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  opareadj.m - Read all the 2-D, 3-D and 4-D NEMO output variables        %                                                                        %
%  This programe is mainly focused on simplify the reading process of      %
%  NEMO output netcdf file for Matlab users.                               %
%  NOTE, the assumed indexing for 2D data is data(y,x), for 3D data is     %
%  data(t,y,x) and for 4D data is data(t,z,y,x).                           %
%                                                                          %
%  Modified by li Zhai for Matlab version R2012a
%  Author: JI LEI windflowerplus@gmail.com                                 %
%  Originate Author: Zeliang Wang                                          %
%  Date:   25 Jun 2010                                                     %
%                                                                          %
%  Modified: 9 Dec 2010 by JI LEI to fix the bug of trouble reading 2D/3D  %                                                                          %
%           regional data.                                                 %
%                                                                          %
%                                                                          %
% Input:                                                                   %
% dirname - location of the files                                          %
% grid    - which grid file to read (grid_T, icemod, etc)                  %
% var     - name of variable (votemper, iicethic, etc)                     %
% t       - time snapshots to read (ignored for 1d variables)              %
% z       - vertical levels to read (ignored for 2d variables)             %
% x       - x range to extract                                             %
% y       - y range to extract                                             %
%                                                                          %
% Output:                                                                  %
% array   - the data indexed t,z,y,x                                       %
% T0      - only when reading 1D variable time_counter                     %
%                                                                          %                         
% Old Usage:                                                               %
% s=opareadj('data/Runsample','grid_T','vosaline')                         %
% s=opareadj('data/Runsample','grid_T','vosaline',1:12)                    %
% s=opareadj('data/Runsample','grid_T','vosaline',':',1:24,200:300,1:50)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [array T0] = f_readopa(dirname, grid, var, t, z, y, x)

array=[]; T0=[];
if ~exist ('t','var');t=':';end;
if ~exist ('z','var');z=':';end;
if ~exist ('x','var');x=':';end;
if ~exist ('y','var');y=':';end;

% record cwd, change to directory of files
cdir=pwd;
cd(dirname);

% look for netcdf files
A=dir(['*' grid '.nc']) ;
B=dir(['*' grid '_*.nc']) ;
C=dir(grid);

nc=netcdf.open(B(1).name,'nowrite'); % Zhai

% get the global dimensions
DOMAIN_size_global = netcdf.getAtt(nc,netcdf.getConstant('NC_GLOBAL'),'DOMAIN_size_global') ;
NX = DOMAIN_size_global(1) ;
NY = DOMAIN_size_global(2) ;

% Get variable ID of the first variable, given its name.
varid = netcdf.inqVarID(nc,var);
% Get information about first variable in the file.
[varname, xtype, dimids, atts] = netcdf.inqVar(nc,varid);
ndim = numel(dimids) ;

    
% close first file
netcdf.close(nc);

    if ndim > 1
        % prepare output array
        nt=length(t);
        if ndim==4
            nz=length(z);
            array_h=zeros(NX,NY,nz,nt);         
        elseif ndim==3
            array_h=zeros(NX,NY,nt);
        elseif ndim==2
            array_h=zeros(NX,NY);
        end

        % loop over processors
        for p=1:length(B)
            nc=netcdf.open(B(p).name,'nowrite'); % Zhai
            DOMAIN_position_first = netcdf.getAtt(nc,netcdf.getConstant('NC_GLOBAL'),'DOMAIN_position_first'); 
            DOMAIN_position_last  =  netcdf.getAtt(nc,netcdf.getConstant('NC_GLOBAL'),'DOMAIN_position_last') ;

            BB=DOMAIN_position_first ;
            EE=DOMAIN_position_last ;

            Ib=BB(1); Jb=BB(2);
            Ie=EE(1); Je=EE(2);

            I=Ie-Ib+1;
            J=Je-Jb+1;
            if ndim==4
                data = netcdf.getVar(nc,varid,[0 0 z(1)-1 t(1)-1],[I J nz nt]);   
                array_h(Ib:Ie,Jb:Je,:,:)=data;
            elseif ndim==3
                data = netcdf.getVar(nc,varid,[0 0 t(1)-1],[I J nt]);  
                array_h(Ib:Ie,Jb:Je,:)=reshape(data,[I J nt]);
            elseif ndim==2
                array_h(Ib:Ie,Jb:Je)= nc{var}(1:I,1:J);            
            end
            netcdf.close(nc);
        end
        
        if ndim==4
            array=array_h(x,y,:,:);
        elseif ndim==3
            array=array_h(x,y,:);
        elseif ndim==2
            array=array_h(x,y);
        end
    end

% return to original directory
cd(cdir);

% remove singleton dimensions
array=squeeze(array);
end

