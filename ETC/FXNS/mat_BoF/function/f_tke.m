%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute total kinetic energy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [tke] = f_tke(uu,vv,tmask,e1t,e2t,dz)
% tke is  total kinetic energy, unit is cm2/s2

dt =0;
for k=1:length(dz)
      tmp = squeeze(uu(:,:,k).^2+vv(:,:,k).^2)/2.*e1t.*e2t*dz(k) ;
      dt = dt + nansum( tmp(:)) ;
end


vol=0;
for k=1:length(dz)
      vol = vol+ sum(sum(e1t'.*e2t'*dz(k).*squeeze(double(tmask(:,:,k)))'));
end

tke = dt/vol*10^4 ;%unit cm2/s2

