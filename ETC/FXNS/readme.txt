Folder for all bash, Matlab, and python functions needed to run the scripts. 
Ensures that package is self-contained -- does not rely on user to have functions
in their path / run folder.
