function [rmse] = f_stats_rmse( mSld, oSld )

%  Root mean square error
%RmseSl = rms(mSld - oSld);
% Or:
% RmseSl = sqrt(sum((mSld - oSld).^2)/length(mSld));
rmse = sqrt(nansum((mSld - oSld).^2)/length(mSld));

end
