function [nemofvcom, susan, gamma] = f_stats_skill( mSld, oSld )


% Skill parameter
%Skill = 1 - (sum((abs(mSld - oSld)).^2) / (sum((abs(mSld - mean(mSld)) + abs(oSld - mean(oSld))).^2)));
nemofvcom = 1 - (nansum((abs(mSld - oSld)).^2) / (nansum((abs(mSld - nanmean(mSld)) + abs(oSld - nanmean(oSld))).^2)));
susan     = 1 - (nansum((abs(mSld - oSld)).^2) / (nansum((abs(mSld - nanmean(oSld)) + abs(oSld - nanmean(oSld))).^2)));
gamma     =     (nansum((abs(mSld - oSld)).^2) / (nansum((abs(mSld - nanmean(oSld)) + abs(oSld - nanmean(oSld))).^2)));
%Skill = 1 - (nansum((abs(mSld - oSld)).^2) / (nansum((abs(mSld - nanmean(oSld)) + abs(oSld - nanmean(oSld))).^2)));

end
