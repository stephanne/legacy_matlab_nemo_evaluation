function [MeanSl, StdSl, RmseSl, RelAvErr, CorrSl, SkewSl, Skill] = ...
    f_residual_stats( mSld, oSld )

%  RESIDUAL_STATS
%  A function to take a times series of detided (residual) model
%  predictions (mSld) and corresponding observations (oSld), and
%  calculate a series of descriptive statistics to summarize the
%  model performance
%  Simon Higginson, 12 September 2017
% ST: Added support for NaNs being present

%  Mean
%MeanSl = mean(mSld) - mean(oSld);
MeanSl = nanmean(mSld) - nanmean(oSld);

%  Standard deviation
StdSl = nanstd(mSld - oSld);
% Or:
% wk = mSld - oSld;
% StdSl = sqrt(sum((wk - mean(wk)).^2)/length(mSld));

%  Root mean square error
%RmseSl = rms(mSld - oSld);
% Or:
% RmseSl = sqrt(sum((mSld - oSld).^2)/length(mSld));
RmseSl = sqrt(nansum((mSld - oSld).^2)/length(mSld));

%  Relative average error
%RelAvErr = 100* sum((mSld - oSld).^2) / sum((abs(mSld - mean(oSld))).^2 + (abs(oSld - mean(oSld))).^2);
RelAvErr = 100* nansum((mSld - oSld).^2) / nansum((abs(mSld - nanmean(oSld))).^2 + (abs(oSld - nanmean(oSld))).^2);

%  Correlation
% CorrSl = corrcoef(mSld, oSld);
% CorrSl = CorrSl(1,2);
% Or:
%CorrSl = (sum((mSld - mean(mSld)) .* (oSld - mean(oSld)))) /  (sqrt(sum((mSld - mean(mSld)).^2)) .*  sqrt(sum((oSld - mean(oSld)).^2)));
CorrSl = (nansum((mSld - nanmean(mSld)) .* (oSld - nanmean(oSld)))) /  (sqrt(nansum((mSld - nanmean(mSld)).^2)) .*  sqrt(nansum((oSld - nanmean(oSld)).^2)));
%CorrSl = corrcoef(mSld, oSld);

% Skewness
SkewSl = skewness(mSld - mean(oSld));
SkewSl = skewness(mSld - nanmean(oSld));

% Skill parameter
%Skill = 1 - (sum((abs(mSld - oSld)).^2) / (sum((abs(mSld - mean(mSld)) + abs(oSld - mean(oSld))).^2)));
Skill = 1 - (nansum((abs(mSld - oSld)).^2) / (nansum((abs(mSld - nanmean(mSld)) + abs(oSld - nanmean(oSld))).^2)));
%Skill = 1 - (nansum((abs(mSld - oSld)).^2) / (nansum((abs(mSld - nanmean(oSld)) + abs(oSld - nanmean(oSld))).^2)));

end

