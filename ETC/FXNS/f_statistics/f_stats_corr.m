function [corr] = f_stats_corr( mSld, oSld )


%  Correlation
% CorrSl = corrcoef(mSld, oSld);
% CorrSl = CorrSl(1,2);
% Or:
%CorrSl = (sum((mSld - mean(mSld)) .* (oSld - mean(oSld)))) /  (sqrt(sum((mSld - mean(mSld)).^2)) .*  sqrt(sum((oSld - mean(oSld)).^2)));
corr = (nansum((mSld - nanmean(mSld)) .* (oSld - nanmean(oSld)))) /  (sqrt(nansum((mSld - nanmean(mSld)).^2)) .*  sqrt(nansum((oSld - nanmean(oSld)).^2)));
%CorrSl = corrcoef(mSld, oSld);

end
