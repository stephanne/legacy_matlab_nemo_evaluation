function [single] = f_stats_single(obs, mod)	

single.bias = f_stats_bias (obs, mod);
single.stddev = f_stats_stddev (obs, mod);
single.rmse = f_stats_rmse(obs, mod);
single.corr = f_stats_corr(obs, mod);
single.rae = f_stats_rae(obs, mod);
single.skew = f_stats_skew(obs, mod);
[single.oldskill, single.skill, single.gamma] = f_stats_skill (obs, mod);

end

