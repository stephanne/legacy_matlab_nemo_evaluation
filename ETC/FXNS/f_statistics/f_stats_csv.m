function f_stats_csv (fn, vals, meta)

% fn is the filename of the generated .csv file
% vals is a structure with individual statistics.  
% meta is a structure with the stations' metadata - used for header info 

[~,nstns] = size(vals);

fid = fopen (fn, 'w')
fprintf (fid, '# STATION, MOD_LON, MOD_LAT, BIAS, STDDEV, RMSE, CORR, SKILL \n')
for s = 1:nstns
	stn = meta(s).station;	lon = meta(s).mod_lon;	lat = meta(s).mod_lat;
	fprintf (fid, '%s, %.4f, %.4f, %.4f, %.4f, %.4f, %.4f, %.4f,\n', stn,lon,lat, vals(s).bias, vals(s).stddev, vals(s).rmse, vals(s).corr, vals(s).skill)
end
fclose(fid);

end


