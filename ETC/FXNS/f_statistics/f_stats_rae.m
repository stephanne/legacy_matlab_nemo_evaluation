function [rae] = f_stats_rae( mSld, oSld )


%  Relative average error
%RelAvErr = 100* sum((mSld - oSld).^2) / sum((abs(mSld - mean(oSld))).^2 + (abs(oSld - mean(oSld))).^2);
rae = 100* nansum((mSld - oSld).^2) / nansum((abs(mSld - nanmean(oSld))).^2 + (abs(oSld - nanmean(oSld))).^2);

end
