function [bias] = f_stats_bias( mSld, oSld )

bias = nanmean(mSld) - nanmean(oSld);

end
