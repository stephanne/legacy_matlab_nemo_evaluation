function [stddev] =  f_stats_stddev( mSld, oSld )


%  Standard deviation
stddev = nanstd(mSld - oSld);
% Or:
% wk = mSld - oSld;
% StdSl = sqrt(sum((wk - mean(wk)).^2)/length(mSld));

end
