function [skill_susan, skill_gamma] = f_skill_scores( mod, obs )

% Single function to calculate a variety of skill scores of various definitions.
% Compiled by S Taylor, early December 2018

% same skill score as used by Susan Allen
skill_susan = 1 - (nansum(    (mod - obs) .^2) / (nansum((abs(mod-nanmean(obs)) + abs(obs-nanmean(obs))).^2) )  );

% gamma skill score, used by Natacha
skill_gamma = nanvar(mod - obs) / nanvar (obs) ;

end

