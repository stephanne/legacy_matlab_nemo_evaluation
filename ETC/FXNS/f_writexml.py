
# Generate an XML file $filename with the data passed as an argument.  XML built to be used
# with GROUP_B scripts, not as NEMO input.  (That's done in a separate file, yet to be built.)

import numpy as np
#import math
#import collections 	# needed for named tuple output

def write_xml(filename, data, grp):

	# Data supplied is the nearest point on the grid, but the XML files uses the bottom left corner 
	# as the origin and then nx,ny to define the size of the box.

	# Further complicating matters, Python is 0 indexed while the scripts that use the XML file are
	# Matlab scripts, which is 1 indexed.  (of course)  
	# use interim variables for clarity.

	# Currently, this takes the values in data.grp to a) be numerical and b) correspond with the line
	# of the same number in groups.csv (or whatever group definition file is specified).  This is not
	# enormously robust - should have an interim step that matches 
	# wait you have all the info for that, just put it in now

	# Groups have only one flag each, which is assumed to be numerical and unique.  

	npts = len(data)

	fid = open (filename, 'w')
	fid.write ('<d>\n')
	for n in range (npts):
		d = data[n]

		idx = np.nan
		for g in range(len (grp)):
			if (int(d.grp) == int(grp[g].flags)):
				idx = g

		# Throw an error if a point doesn't match one of the specified groups
		if (np.isnan(idx)):
			print "Point ", d.id, " with group flag ", d.grp, " does not match any groups in ", grp 
			exit()
			
		

		# Get the range around the central point you want to output:
		nx = int(grp[idx].nx); ny = int(grp[idx].ny)

		# Adjust the indices from central point to bottom left corner
		bl_x = int(d.ix) + 1 - nx; bl_y = int(d.iy) + 1 - ny

		# And set nx, ny to be the size of the patch.
		nx = 2*nx + 1; ny = 2*ny + 1

		# Write to XML file.
		fid.write ('\t<domain> <id>%s</id> <ibegin>%i</ibegin> <jbegin>%i</jbegin> <ni>%i</ni> <nj>%i</nj> </domain>\n' % (d.id, bl_x, bl_y, nx,ny))
	fid.write ('</d>\n')
	fid.close()



