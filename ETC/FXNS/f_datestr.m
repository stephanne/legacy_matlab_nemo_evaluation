function dae = f_datestring ( nsec )
%Convert number of seconds since 1950-01-01 into a legible string

dae = datestr( nsec /86400 + datenum('19500101', 'yyyymmdd'));

end

