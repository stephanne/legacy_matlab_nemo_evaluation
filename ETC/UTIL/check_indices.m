% Script to extract the 3x3 (or other small sections) of the 100 m domain
% Input domain is expected to be a single day (so 48 slices for half hourly
% output), and one file for T/U/V grids.  Stations are read in through a 
% structured xml file rather than searching for each location in the grid
% variables. XML file needs editing from the model input file - put each attribute
% between tags.  (this is best done with liberal use of sed before this is
% run.)  To run more quickly, set up this script to loop over a subset of 
% the total days required rather than looping over stations - this minimizes
% the time spent opening and closing files.  All attributes from source files
% are transferred over to the patches as well as the data itself.

% History: 	written Nov. 13th, 2017 by Stephanne Taylor
%		Jan 2 2018, S Taylor: unhardwired variables

clear
addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts/mat_BoF/function
addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts
% put another addpath here if the xml2struct file isn't in the same directory

% INPUT INFO

% Directory and file root
station_file='/home/stt001/polished_scripts/EXR/ctd_adcp_etc_SJAP100_harbour.xml';


% Read the XML file in here.
data = xml2struct(station_file);
[junk nstations] = size (data.d.domain);
% So now all the stations can be indexed via: stn=d.domain{X}.id   No idea why the first thing's necessary but it didn't work without it
% parameters are id (station number), ibegin, jbegin (bottom left x and y coords), ni nj (size in x and y)


ioff = 1 -1;	joff = 120 - 1;

	for s=1:nstations

		% Put station data into variables for conciseness.
		stn = data.d.domain{s}.id.Text;
		ib  = str2num(data.d.domain{s}.ibegin.Text) - ioff;	 	ie = str2num(data.d.domain{s}.ibegin.Text) + str2num(data.d.domain{s}.ni.Text) - ioff - 1;
		jb  = str2num(data.d.domain{s}.jbegin.Text) - joff;		je = str2num(data.d.domain{s}.jbegin.Text) + str2num(data.d.domain{s}.nj.Text) - joff - 1;
		ni  = str2num(data.d.domain{s}.ni.Text); 	nj = str2num(data.d.domain{s}.nj.Text);

		a = [stn '  ' num2str(ib) ' ' num2str(ie) ' ' num2str(jb) ' ' num2str(je)]
		
	end % station loop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%









