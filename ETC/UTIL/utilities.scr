#!/bin/bash

# Chunks of developed scripts that can be used as independent functions.  In theory ST should then replace those chunks in the relevant
# scripts with calls to these functions, but leave that till later.

# UTILITY 1: Check if a variable has the right number of time slices, given how many days are in it and how many slices per day


check_dimension_size () 
{
	# Takes five arguments - file to be checked, dimension to be checked, number of days expected, slices per day
	local __inputfile=$1	# file to be checked
	local __dimension=$2	# dimension to be checked
	local __ndays=$3	# number of days expected in the file
	local __slices=$4	# number of slices per day
	
	local __size=`${3}*${4}`
	#$5			# returned variable - 0 if okay, number of slices found if not

	local __tmp=`ncks -C -m -v ${__dimension} ${__inputfile}  | grep "${__dimension}, size =" | cut -d' ' -f 7`
	
	if [${__tmp} -eq ${__size} ]; then # expected number of slices are found in the file
		$5=0
	else
		$5=${__tmp}
	fi

 }



# UTILITY 2:  Generate a 0-1 land-sea mask from an arbitrary variable. (rather than land=NaN)

make_land_mask ()
{
	# Takes three arguments: file to read in, variable to use, output file
	# First clip the variables into the mask file
	ncks -h -O -v nav_lon,nav_lat,time_counter,$2 $1 -o $3

	# Then convert the missing values to zeros
	touch __command.ncap2 ; rm __command.ncap2
	echo 'lsmk='$CV_IN'*0+1 ;'       >  __command.ncap2
	echo 'where('${CV_IN}' != 0) '   >> __command.ncap2
	echo 'lsmk=1.0;'                 >> __command.ncap2
	echo 'elsewhere'                 >> __command.ncap2
	echo 'lsmk=0.0;'                 >> __command.ncap2
	ncap2   -h -O -S __command.ncap2         ${3} -o ${3}
	ncatted -h -O -a _FillValue,lsmk,o,f,0.0 ${3} -o ${3}
	ncatted -h -O -a _FillValue,lsmk,d,f,0.0 ${3} -o ${3}
	ncks    -h -O -x -v ${CV_IN}             ${3} -o ${3}
	rm __command.ncap2


}
