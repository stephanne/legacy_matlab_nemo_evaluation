% Read tidal constituent list quickly
clear all

in_filename  = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TIDE_TESTING/TG/tidegauge_NEP36_test1_TG_all.mat'
out_filename = 'NEP36_75levels_noSN_M2.txt'
nstns = 15;

load (in_filename);
% Constituents:
% Q1 = 1, O1 = 2,  P1 = 3,  K1 = 4,  N2 = 5,  M2 = 6, S2 = 7, K2 = 8
con = 6;

ao = zeros(nstns,1);    am = zeros(nstns,1);
po = zeros(nstns,1);    pm = zeros(nstns,1);
stn= zeros(nstns,1);

for n=1:nstns
    % Amplitudes
    ao(n)=all_stations_obs(n).tidal_cons.tidecon(con,1);
    am(n)=all_stations_mod(n).tidal_cons.tidecon(con,1);
    
    % Phases
    po(n)=all_stations_obs(n).tidal_cons.tidecon(con,3);
    pm(n)=all_stations_mod(n).tidal_cons.tidecon(con,3);
    
    % Station
    stn(n) = str2num(all_stations_obs(n).station);
end

% Write a .csv file

fid = fopen(out_filename, 'w');
fprintf(fid,'#%s, %s, %s, %s, %s\n','Station','Amp Obs','Amp Mod','Phase Obs','Phase Mod');
for n=1:nstns
    fprintf(fid,'%i, %f, %f, %f, %f\n',stn(n,1),ao(n,1),am(n,1),po(n,1),pm(n,1));
end
fclose(fid)
