#!/bin/bash

# Rename toce and soce variable to harmonize the root string.


infile='/space/hall0/work/dfo/odis/stt001/SJAP100/PHASE1B/monthly/SJAP100-P1bP2P3_180ts_20150426_20160623_20150901_20150930'
outfile='/space/hall0/work/dfo/odis/stt001/SJAP100/PHASE1B/monthly/SJAP100-P1bP2_180ts_20150426_20160623_20150901_20150930'

stations='./STN/stations_ctd_adcp_etc_SJAP100.txt'
#stations='./STN/test_stations.txt'

yr='2016'
mth='06' # 02 03 04 05 06'


while IFS='' read -r line || [[ -n "$line" ]]; do
        # $line is the station number

	mv ${infile}_${line}-09.nc ${outfile}_${line}-09.nc

done < $stations

