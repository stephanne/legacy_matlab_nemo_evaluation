% Script to extract the 3x3 (or other small sections) of the 100 m domain
% Input domain is expected to be a single day (so 48 slices for half hourly
% output), and one file for T/U/V grids.  Stations are read in through a 
% structured xml file rather than searching for each location in the grid
% variables. XML file needs editing from the model input file - put each attribute
% between tags.  (this is best done with liberal use of sed before this is
% run.)  To run more quickly, set up this script to loop over a subset of 
% the total days required rather than looping over stations - this minimizes
% the time spent opening and closing files.  All attributes from source files
% are transferred over to the patches as well as the data itself.

% History: 	written Nov. 13th, 2017 by Stephanne Taylor
%		Jan 2 2018, S Taylor: unhardwired variables

clear
addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts/mat_BoF/function
addpath /fs/vnas_Hdfo/odis/stt001/matlab_scripts
% put another addpath here if the xml2struct file isn't in the same directory

% INPUT INFO

% Directory and file root
in_root  = '/space/hall0/work/dfo/odis/stt001/INITIALISATION_PHASE2/SJAP100/Bathymetry_SJAP100_PHASE2_180_for10pts_LinInt_16_AGRIF.nc'
out_root = '/space/hall0/work/dfo/odis/stt001/SJAP100/SJAP100-P2R1/SJAP100-P2R1-OUT/BAT/Bathymetry_SJAP100_PHASE2_180_for10pts_LinInt_16_AGRIF'

station_file='/home/stt001/polished_scripts/STN/ctd_adcp_etc_SJAP100.xml';	delimiter='09';


% Read the XML file in here.
data = xml2struct(station_file);
[~,nstations] = size (data.d.domain);
% So now all the stations can be indexed via: stn=d.domain{X}.id   No idea why the first thing's necessary but it didn't work without it
% parameters are id (station number), ibegin, jbegin (bottom left x and y coords), ni nj (size in x and y)

% Bathymetry does not change with time.



	% Open the file, grab the guts, including all the weird glands no-one cares about, and find a better metaphor.

	% One day per file
	t_file='/space/hall0/work/dfo/odis/stt001/INITIALISATION_PHASE2/SJAP100/Bathymetry_SJAP100_PHASE2_180_for10pts_LinInt_16_AGRIF.nc'

	% Read in the schema
	t_scheme = ncinfo(t_file);%	u_scheme = ncinfo(u_file);	v_scheme = ncinfo(v_file);

	% Get the data
	ncid = netcdf.open (t_file, 'NC_NOWRITE');
	
	latid = netcdf.inqVarID(ncid, 'nav_lat') + 1;	nav_lat = netcdf.getVar(ncid,latid-1);
	lonid = netcdf.inqVarID(ncid, 'nav_lon') + 1;	nav_lon = netcdf.getVar(ncid,lonid-1);
	batid = netcdf.inqVarID(ncid, 'Bathymetry') + 1;	bathy   = netcdf.getVar(ncid,batid-1);
	lvlid = netcdf.inqVarID(ncid, 'Bathy_level') + 1;	level   = netcdf.getVar(ncid,lvlid-1);
	netcdf.close(ncid)




	% Loop over stations, use the indexes to get the patch, then write the file with all those bits (from each of the three original files) in it. 
	% Use the schema to  get all the attributes etc.

	for s=1:nstations

		% Put station data into variables for conciseness.
		stn = data.d.domain{s}.id.Text;
		ib  = str2num(data.d.domain{s}.ibegin.Text); 	ie = str2num(data.d.domain{s}.ibegin.Text) + str2num(data.d.domain{s}.ni.Text) - 1;
		jb  = str2num(data.d.domain{s}.jbegin.Text);	je = str2num(data.d.domain{s}.jbegin.Text) + str2num(data.d.domain{s}.nj.Text) - 1;
		ni  = str2num(data.d.domain{s}.ni.Text); 	nj = str2num(data.d.domain{s}.nj.Text);

		% Patch!  Check if you need to NaN out the land again.

		patch_bat = bathy(ib:ie, jb:je);
		patch_lvl = level(ib:ie, jb:je);


		% lat/lon always present - can leave hardwired.  Always variables 1 and 2, can leave that hardwired too (at least initially).
                patch_lat = nav_lat (ib:ie, jb:je);	 patch_lon = nav_lon (ib:ie, jb:je);

		% Write the file. The schema is overkill at this point.
		%ncwrite (outfile, 'nav_lat', patch_lat);
		%ncwrite (outfile, 'nav_lon', patch_lon);
		%ncwrite (outfile, 'Bathymetry', patch_bat);


		% Adjust the schema

		% Alter the dimensions

		ni
		nj
		latid
		lonid
		batid
		lvlid
		t_scheme.Dimensions(1).Length=ni; t_scheme.Dimensions(2).Length=nj;


		% T variables
		% nav_lat / nav_lon
		t_scheme.Variables(latid).Dimensions(1).Length=ni;  t_scheme.Variables(latid).Dimensions(2).Length=nj;	
		t_scheme.Variables(lonid).Dimensions(1).Length=ni;  t_scheme.Variables(lonid).Dimensions(2).Length=nj;	
		t_scheme.Variables(latid).Size=[ni nj];	t_scheme.Variables(latid).ChunkSize=[ni nj];
		t_scheme.Variables(lonid).Size=[ni nj];	t_scheme.Variables(lonid).ChunkSize=[ni nj];

		% Requested
		t_scheme.Variables(batid).Dimensions(1).Length = ni; t_scheme.Variables(batid).Dimensions(2).Length = nj;
		t_scheme.Variables(lvlid).Dimensions(1).Length = ni; t_scheme.Variables(lvlid).Dimensions(2).Length = nj;
		t_scheme.Variables(batid).Size = [ni nj]; t_scheme.Variables(lvlid).Size = [ni nj];
		t_scheme.Variables(batid).ChunkSize = [ni nj]; t_scheme.Variables(lvlid).ChunkSize = [ni nj]; 
%
%
%		% Write the file
%		outfile = [out_root '_' stn '_' jour1 '-' jour2 '.nc']  
%
		outfile = [out_root '_' stn '.nc']  
		
		t_scheme.Variables(latid)
		t_scheme.Variables(lonid)
		t_scheme.Variables(batid)
		t_scheme.Variables(lvlid)

		ncwriteschema (outfile, t_scheme);

		ncwrite (outfile, 'nav_lat', patch_lat);
		ncwrite (outfile, 'nav_lon', patch_lon);
		ncwrite (outfile, 'Bathymetry', patch_bat);
		ncwrite (outfile, 'Bathy_level', patch_lvl);

		display (['Station ' s ' complete.']) 
	end % station loop








