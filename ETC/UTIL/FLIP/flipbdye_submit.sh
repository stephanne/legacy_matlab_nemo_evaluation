#!/bin/bash
# jobsub -c gpsc1.science.gc.ca --cwd RUN_PAR.sh
#
# VAR_SCRIPT.job
#
#$ -j y
#$ -o out_flipbdye.out	# Output filename
#$ -e err_flipbdye.err
#$ -N flipbdye		# Job name

#$ -pe dev 1		# Number of nodes
#$ -l res_cpus=1	# Number of cores per node
#$ -l res_mem=20000	# Memory per node ( I think it is per node not per core)   

#$ -l res_tmpfs=1000	# Not 100% told to set this by support

#$ -l res_image=dfo_all_default_ubuntu-14.04-amd64_latest	# Should be the same as the OS image that you compiled with
#$ -l h_rt=12:00:00	#Wall runtime

cd /home/stt001/polished_scripts/UPDATED_BDY

set -axe

# environment setup (from .profile)
#. /fs/ssm/main/env/ssmuse-boot.sh
export ORDENV_GROUP_PROFILE=eccc/cmc/1.0
export ORDENV_SITE_PROFILE=20161026
export ORDENV_COMM_PROFILE=eccc/20161103

. ssmuse-sh -x main/env/20160418
. ssmuse-sh -x /fs/ssm/main/base/20160321
. ssmuse-sh -d main/opt/matlab/matlab-R2016a  # ST addition
. ssmuse-sh -x /fs/ssm/comm/dfo/exp/nco/4.5.5/gcc  # for proper dimension flipping # DO NOT USE IMPRUDENTLY - WEIRD BEHAVIOUR IN NCKS

. ordenv-load


ulimit -s unlimited  # increase memory
ulimit -t 216000


ksh /home/stt001/polished_scripts/UPDATED_BDY/flipbdye_script.sh

#cd /space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2o/BoF180-P2T2o-OUT
#ncpdq -O -a time_counter,deptht,y,'-x' ./DIMG/BoF180-P2T2o_60ts_20150401-20160613_BDYE-SJAP100-grid-T_20150401-20150410.nc ./BDY/flipfirst/BoF180-P2T2o_60ts_20150401-20160613_BDYE-SJAP100-grid-T_20150401-20150410.nc
#ncpdq -O -a time_counter,deptht,y,'-x' ./DIMG/BoF180-P2T2o_60ts_20150401-20160613_BDYE-SJAP100-grid-T_20150411-20150420.nc ./BDY/flipfirst/BoF180-P2T2o_60ts_20150401-20160613_BDYE-SJAP100-grid-T_20150411-20150420.nc
#ncpdq -O -a time_counter,deptht,y,'-x' ./DIMG/BoF180-P2T2o_60ts_20150401-20160613_BDYE-SJAP100-grid-T_20150421-20150430.nc ./BDY/flipfirst/BoF180-P2T2o_60ts_20150401-20160613_BDYE-SJAP100-grid-T_20150421-20150430.nc

#cd /space/hall0/work/dfo/odis/stt001/WORK_BDY/BoF180/flipfirst/BDYE
#ncpdq -O -a time,y,'-x' tmp_unflipped_coord_BDYE.nc tmp_coord_BDYE.nc
#ncpdq -O -a time,y,'-x' tmp_unflipped_coord_SOURCE_BDYE.nc tmp_coord_SOURCE_BDYE.nc
#ncpdq -O -a t,z,y,'-x' tmp_unflipped_mesh_BDYE.nc tmp_mesh_BDYE.nc

