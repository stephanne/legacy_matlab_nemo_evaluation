#!/bin/bash

#Secondary script to flip the BDYE y dimension using the experimental NCO package (which has errors / issues in ncks)


dir='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/BDY'
fileroot='BoF180-P2T2_SJAP100_30ts'

yrs='2016'
#mths='01 02 03 04 05 06'
mths='06' #'10 11 12'
vars3d='thetao so uo vo'
vars2d='zos'

#mkdir ${dir}/FLIPPED
#mkdir ${dir}/UNFLIPPED

for yy in ${yrs}; do
	for mm in ${mths}; do
		for var in ${vars3d}; do

			filename=${fileroot}_BDYE_${var}_y${yy}m${mm}.nc

			mv ${dir}/${filename} ${dir}/UNFLIPPED

			ncpdq -O -a time_counter,z,'-y',x ${dir}/UNFLIPPED/${filename} ${dir}/FLIPPED/${filename}
			#ncpdq -O -a time_counter,z,y,'-x' ${dir}/UNFLIPPED/${filename} ${dir}/FLIPX/${filename}

			# original
			#ncpdq -O -a time_counter,z,'-y',x ${WORK}/${vv}_${SOURCE}-${DEST}_${BDY}.nc ${DIROUT}/${SOURCE}-${RUNID}_${DEST}_${INTERVAL}_${BDY}_${vv}_y${yy}m${mm}.nc

		done

		for var in ${vars2d}; do

			filename=${fileroot}_BDYE_${var}_y${yy}m${mm}.nc

			mv ${dir}/${filename} ${dir}/UNFLIPPED

			ncpdq -O -a time_counter,'-y',x ${dir}/UNFLIPPED/${filename} ${dir}/FLIPPED/${filename}

		done


		
	done
done


#BoF180-P1cT1_SJAP100_60ts_BDYS_vo_y2015m04.nc

