

# Loop through a month

mm='09'; ld='30'
dd='01'
while [ ${mm}${dd} -le ${mm}${ld} ] ; do
	echo $dd
dchar=$dd
typeset -RZ2 dchar

	jour='2015'${mm}${dchar}
	filename='/space/hall0/work/dfo/odis/stt001/SJAP100/SJAP100-P2R1/DIRRUN_SJAP100-P2R1/SJAP100-P2R1_180ts_20150411-20160613_grid-T_'${jour}-${jour}.nc
	outfile='/space/hall0/work/dfo/odis/stt001/SJAP100/SJAP100-P2R1/SJAP100-P2R1-OUT/EXT/SJAP100-P2R1_180ts_20150411-20160613_EXT_'${jour}-${jour}.nc

	ncks -F -d x,366,368 -d y,459,461 ${filename} ${outfile}

	dd=$((${dd} + 1))
done
