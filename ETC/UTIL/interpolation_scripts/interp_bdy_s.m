% INTERPOLATION IN TIME
% S Taylor, stephanne.taylor@dfo-mpo.gc.ca
% Last structural change: May 1, 2018

% PURPOSE : draft script for BDY generation with instantaneous data at 1X frequency required for output.
% INPUT   : unprocessed model output
% OUTPUT  : raw data interpolated to the middle of the time interval required for BDY generation.  
% REQUIRES: YOU MUST HAVE COPIED THE RAW FILES TO THE OUTPUT DIRECTORY BEFORE RUNNING THIS SCRIPT.
% CONTEXT : if needed, run before any BDY script is run.  alternatively, (convert to python?,) develop as a subscript of BDY
%          ideally you don't need this - having output at 2x required frequency is better (though more resource intensive)

% I've left this as it was when I found it, which is to say, it does the temporal interpolation AFTER the spatial interpolation.
% I already had the un-adjusted BDY files and was trying to figure out why I was getting weird results, so I altered what I had
% rather than treating this as a pre-processor for the BDY script.  It shouldn't take a lot of changes to make it more robust -
% it needs a loop over files of some sort, may as well be consistent with the original, straight-out-of-the-model files rather 
% than the BDY files.  It may well be more effort than it's worth though, so I'll leave it until I need it.



%---------------------------------------------------------------------------------------------------------------
%--- USER SPECIFIED INFORMATION --------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------------------

% Input data:
indir = '/space/hall0/work/dfo/odis/stt001/BoFSS036/BoFSS036-P1cT2-OUT/BDY/actualmesh_instant'	% directory with source data
fileroot = 'BoFSS036-P1cT2_BoF180_10ts_BDYS'; daystr = 'y2016m06'				% input file strings

% Output data:
outdir = './interp'

% Variables to adjust
list_3d_vars={'thetao','so','uo','vo'}; list_2d_vars={'zos'}

% Size of the BDY strip: current values are the fully processed strip size, when updating this should be the size of the source domain files.
%nx = 49; ny = 10; nz = 50; %nt = 1440;
nx = 323; ny = 10; nz = 50; nt = 1440;



%---------------------------------------------------------------------------------------------------------------
%--- LOOP OVER VARIABLES ---------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------------------

for varstr = list_3d_vars
	var = varstr{1}; varstr

	filename = ([indir '/' fileroot '_' var '_' daystr '.nc'])
	data = double(ncread(filename, var));
	[nx ny nz nt] = size (data);

	% points at which data exists
	time_instant = linspace (2,nt*2,nt);
	% points at which you need to inter/extrapolate it to
	time_counter = time_instant - 1;

	data_interpolated = zeros(nx,ny,nz,nt);
	%data_interpolated = zeros(nx,ny,nt);

	for z=1:nz 

	for x=1:nx
	for y=1:ny

		a = squeeze(data(x,y,z,:));
		%a = squeeze(data(x,y,:));

		if (isfinite(data(x,y,z,1)))
		%if (isfinite(data(x,y,1)))
			b = interp1 (time_instant, a, time_counter, 'spline', 'extrap');
			data_interpolated(x,y,z,:) = b;
			%data_interpolated(x,y,:) = b;
		else
			data_interpolated(x,y,z,:) = NaN;
			%data_interpolated(x,y,:) = NaN;
		end
	
	end
	end
	end


	% Overwrite the data in a(nother) copy of the original, raw file.
	filename =[outdir '/' fileroot '_' var '_' daystr '.nc']
	ncwrite (filename, var, data_interpolated);
	ncwriteatt(filename, '/','modifications',['Data interpolated from time_instant to time_counter and is now aligned with time_counter. Interpolation by Steph Taylor at ' datestr(now)]')


end


% And the 2d variables
for varstr = list_2d_vars
	var = varstr{1}; varstr
	filename = ([indir '/' fileroot '_' var '_' daystr '.nc'])
	data = double(ncread(filename, var));
	[nx ny nt] = size (data);

	% points at which data exists
	time_instant = linspace (2,nt*2,nt);
	% points at which you need to inter/extrapolate it to
	time_counter = time_instant - 1;

	%data_interpolated = zeros(nx,ny,nz,nt);
	data_interpolated = zeros(nx,ny,nt);


	for x=1:nx
	for y=1:ny

		%a = squeeze(data(x,y,z,:));
		a = squeeze(data(x,y,:));

		%if (isfinite(data(x,y,z,1)))
		if (isfinite(data(x,y,1)))
			b = interp1 (time_instant, a, time_counter, 'spline', 'extrap');
			%data_interpolated(x,y,z,:) = b;
			data_interpolated(x,y,:) = b;
		else
			%data_interpolated(x,y,z,:) = NaN;
			data_interpolated(x,y,:) = NaN;
		end
	
	end
	end


	% Overwrite the data in a(nother) copy of the original, raw file.
	filename =[outdir '/' fileroot '_' var '_' daystr '.nc']
	ncwrite (filename, var, data_interpolated);
	ncwriteatt(filename, '/','modifications',['Data interpolated from time_instant to time_counter and is now aligned with time_counter. Interpolation by Steph Taylor at ' datestr(now)]')

end
