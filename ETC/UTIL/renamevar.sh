#!/bin/bash

# Rename toce and soce variables to thetao and so.

source /fs/vnas_Hdfo/odis/stt001/polished_scripts/functions.sh

fileroot='/space/hall0/work/dfo/odis/stt001/SJAP100/SJAP100-P2R1/SJAP100-P2R1-OUT/EXR/SJAP100-P2R1_180ts_20150928-20160613'
stations='./STN/stations_ctd_adcp_etc_SJAP100.txt'

yr='2016'
mth='06' # 02 03 04 05 06'


while IFS='' read -r line || [[ -n "$line" ]]; do
        # $line is the station number

	for mm in ${mth}; do
		get_nb_days $yr $mm
		#for ((dd=1;dd<=${nb};dd++)); do
		for ((dd=1;dd<=12;dd++)); do
			#echo $dd
			dchar=${dd}
			typeset -RZ2 dchar
			day=${yr}${mm}${dchar}
			filename=${fileroot}_${line}_${day}-${day}.nc
			#filename=${fileroot}_${day}-${day}.nc
			echo ${filename}
			ncrename -v toce,thetao -v soce,so $filename
		done
	done



done < $stations

