
# Dictionary linking the observation filenames with a mooring short code used in iodef file.
# no modules needed - only core python functionality needed.
# keys are unique, but values are not necessarily - note there are often multiple observation
# files for a single mooring location.

# MOORED CTD DICTIONARY

dictionary = {}

dictionary ['A1_CTD_2015-08_2015-08_175m'] = 'A1aa'
dictionary ['A1_CTD_2015-08_2015-08_300m'] = 'A1ab'
dictionary ['A1_CTD_2015-08_2016-07_35m']  = 'A1ba'
dictionary ['A1_CTD_2015-08_2016-07_100m'] = 'A1bb'
dictionary ['A1_CTD_2015-08_2016-07_175m'] = 'A1bc'
dictionary ['A1_CTD_2015-08_2016-07_500m'] = 'A1bd'
dictionary ['A1_CTD_2016-07_2017-04_25m']  = 'A1ca'
dictionary ['A1_CTD_2016-07_2017-04_90m']  = 'A1cb'
dictionary ['A1_CTD_2016-07_2017-04_165m'] = 'A1cc'
dictionary ['A1_CTD_2016-07_2017-04_300m'] = 'A1cd'
dictionary ['A1_CTD_2016-07_2017-04_490m'] = 'A1ce'

dictionary ['AS04_CTD_2015-09_2015-09_112m'] = 'AS04'
dictionary ['BACAX_CTD_2015-09_2015-09_982m'] = 'BACAX'
dictionary ['BACCH_CTD_2016-05_2016-05_369m'] = 'BACCH'
dictionary ['BACME_CTD_2016-06_2016-06_896m'] = 'BACME'
dictionary ['BACWL_CTD_2016-05_2016-05_864m'] = 'BACWL'
dictionary ['BDYPM_CTD_2015-09_2015-09_229m'] = 'BDYPM'
dictionary ['BP1_CTD_2016-07_2017-07_100m']   = 'BP1'

dictionary ['CAM1_CTD_2015-07_2016-07_15m']   = 'CAM1a'
dictionary ['CAM1_CTD_2015-07_2016-07_40m']   = 'CAM1b'
dictionary ['CAM1_CTD_2015-07_2016-07_150m']  = 'CAM1c'
dictionary ['CAM1_CTD_2015-07_2016-07_416m']  = 'CAM1d'

dictionary ['CAM2_CTD_2015-07_2016-07_40m']   = 'CAM2a'
dictionary ['CAM2_CTD_2015-07_2016-07_150m']  = 'CAM2b'
dictionary ['CAM2_CTD_2015-07_2016-07_226m']  = 'CAM2c'

dictionary ['CMP1_CTD_2015-07_2016-01_15m']    = 'CMP1aa'
dictionary ['CMP1_CTD_2015-07_2016-01_40m']    = 'CMP1ab'
dictionary ['CMP1_CTD_2015-07_2016-01_150m']   = 'CMP1ac'
dictionary ['CMP1_CTD_2015-07_2016-01_355m']   = 'CMP1ad'
dictionary ['CMP1_CTD_2016-01_2016-05_150m']   = 'CMP1ba'
dictionary ['CMP1_CTD_2016-01_2016-05_355m']   = 'CMP1bb'

dictionary ['CRIP.C2_CTD_2017-01_2017-01_8m']  = 'CRIPC2'
dictionary ['DIIP.C2_CTD_2016-08_2016-08_28m'] = 'DIIPC2'

dictionary ['Dev1_CTD_2015-07_2016-05_15m']    = 'DEV1a'
dictionary ['Dev1_CTD_2015-07_2016-05_100m']   = 'DEV1b'
dictionary ['Dev1_CTD_2015-07_2016-05_110m']   = 'DEV1c'
dictionary ['Dev1_CTD_2015-07_2016-05_152m']   = 'DEV1d'

dictionary ['E01_CTD_2015-08_2016-07_35m']     = 'E01aa'
dictionary ['E01_CTD_2015-08_2016-07_75m']     = 'E01ab'
dictionary ['E01_CTD_2015-08_2016-07_90m']     = 'E01ac'
dictionary ['E01_CTD_2016-07_2017-07_35m']     = 'E01ba'
dictionary ['E01_CTD_2016-07_2017-07_75m']     = 'E01bb'
dictionary ['E01_CTD_2016-07_2017-07_92m']     = 'E01bc'

dictionary ['EF04_CTD_2015-08_2016-07_35m']    = 'EF04aa'
dictionary ['EF04_CTD_2015-08_2016-07_75m']    = 'EF04ab'
dictionary ['EF04_CTD_2015-08_2016-07_102m']   = 'EF04ac'
dictionary ['EF04_CTD_2016-07_2017-07_35m']    = 'EF04ba'
dictionary ['EF04_CTD_2016-07_2017-07_75m']    = 'EF04bb'
dictionary ['EF04_CTD_2016-07_2017-07_100m']   = 'EF04bc'

dictionary ['ES1_CTD_2014-06_2015-07_15m']     = 'ES1a'
dictionary ['ES1_CTD_2014-07_2015-07_40m']     = 'ES1b'
dictionary ['ES1_CTD_2014-07_2015-07_170m']    = 'ES1c'

dictionary ['FGPD_CTD_2015-09_2015-09_95m']    = 'FGPD'
dictionary ['FGPPN_CTD_2015-09_2015-09_25m']   = 'FGPPN'

#dictionary ['FOC1_CTD_2013-07_2014-07_16m']    = 'FOC1aa'
#dictionary ['FOC1_CTD_2013-07_2014-07_52m']    = 'FOC1ab'
#dictionary ['FOC1_CTD_2013-07_2014-07_151m']   = 'FOC1ac'
#dictionary ['FOC1_CTD_2013-07_2014-07_315m']   = 'FOC1ad'
#dictionary ['FOC1_CTD_2013-07_2014-07_357m']   = 'FOC1ae'

dictionary ['FOC1_CTD_2014-07_2015-07_15m']    = 'FOC1ba'
dictionary ['FOC1_CTD_2014-07_2015-07_38m']    = 'FOC1bb'
dictionary ['FOC1_CTD_2014-07_2015-07_51m']    = 'FOC1bc'
dictionary ['FOC1_CTD_2014-07_2015-07_150m']   = 'FOC1bd'
dictionary ['FOC1_CTD_2014-07_2015-07_300m']   = 'FOC1be'
dictionary ['FOC1_CTD_2014-07_2015-07_357m']   = 'FOC1bf'

dictionary ['FOC1_CTD_2015-07_2016-07_15m']    = 'FOC1ca'
dictionary ['FOC1_CTD_2015-07_2016-07_40m']    = 'FOC1cb'
dictionary ['FOC1_CTD_2015-07_2016-07_51m']    = 'FOC1cc'
dictionary ['FOC1_CTD_2015-07_2016-07_150m']   = 'FOC1cd'
dictionary ['FOC1_CTD_2015-07_2016-07_250m']   = 'FOC1ce'
dictionary ['FOC1_CTD_2015-07_2016-07_300m']   = 'FOC1cf'
dictionary ['FOC1_CTD_2015-07_2016-07_350m']   = 'FOC1cg'

#dictionary ['HEC1_CTD_2013-07_2014-06_46m']    = 'HEC1aa'
#dictionary ['HEC1_CTD_2013-07_2014-06_122m']   = 'HEC1ab'

#dictionary ['HEC1_CTD_2014-07_2015-07_15m']    = 'HEC1ba'
#dictionary ['HEC1_CTD_2014-06_2015-07_40m']    = 'HEC1bb'
#dictionary ['HEC1_CTD_2014-06_2015-07_51m']    = 'HEC1bc'
#dictionary ['HEC1_CTD_2014-06_2014-11_124m']   = 'HEC1bd'

dictionary ['HEC1_CTD_2015-07_2016-07_15m']    = 'HEC1ca'
dictionary ['HEC1_CTD_2015-07_2016-07_39m']    = 'HEC1cb'
dictionary ['HEC1_CTD_2015-07_2016-07_126m']   = 'HEC1cc'

dictionary ['JF2C_CTD_2015-09_2015-09_182m']   = 'JF2C'

#dictionary ['KSK1_CTD_2013-07_2014-07_17m']    = 'KSK1aa'
#dictionary ['KSK1_CTD_2013-07_2014-07_322m']   = 'KSK1ab'

#dictionary ['KSK1_CTD_2014-07_2015-07_15m']    = 'KSK1ba'
#dictionary ['KSK1_CTD_2014-07_2015-07_40m']    = 'KSK1bb'
#dictionary ['KSK1_CTD_2014-07_2015-07_150m']   = 'KSK1bc'
#dictionary ['KSK1_CTD_2014-07_2015-07_359m']   = 'KSK1bd'

dictionary ['KSK1_CTD_2015-07_2016-05_15m']    = 'KSK1ca'
dictionary ['KSK1_CTD_2015-07_2016-05_40m']    = 'KSK1cb'
dictionary ['KSK1_CTD_2015-07_2016-05_150m']   = 'KSK1cc'
dictionary ['KSK1_CTD_2015-07_2016-05_361m']   = 'KSK1cd'

dictionary ['KVIP.C1_CTD_2017-04_2017-04_40m'] = 'KVIPC1'
dictionary ['LSBBL_CTD_2015-09_2015-09_147m']  = 'LSBBL'
dictionary ['MAC_CTD_2015-09_2015-09_55m']     = 'MAC'
dictionary ['NC27_CTD_2015-09_2015-09_2654m']  = 'NC27'
dictionary ['NC89_CTD_2015-09_2015-09_1259m']  = 'NC89'

#dictionary ['OC1_CTD_2014-07_2015-07_15m']     = 'OC1a'
#dictionary ['OC1_CTD_2014-07_2015-07_40m']     = 'OC1b'
#dictionary ['OC1_CTD_2014-07_2015-07_150m']    = 'OC1c'
#dictionary ['OC1_CTD_2014-07_2015-07_280m']    = 'OC1d'
#dictionary ['PC1_CTD_2014-07_2015-07_150m']    = 'PC1'

dictionary ['RCNE1_CTD_2016-05_2016-05_2153m'] = 'RCNE1'
dictionary ['RCNE2_CTD_2016-05_2016-05_2106m'] = 'RCNE2'
dictionary ['RCNE3_CTD_2016-05_2016-05_2030m'] = 'RCNE3'
dictionary ['RCNE4_CTD_2016-05_2016-05_1956m'] = 'RCNE4'
dictionary ['RCNW1_CTD_2016-05_2016-05_2140m'] = 'RCNW1'
dictionary ['RCNW3_CTD_2016-05_2016-05_2018m'] = 'RCNW3'
dictionary ['RCNW4_CTD_2016-05_2016-05_1943m'] = 'RCNW4'

dictionary ['RCSE1_CTD_2016-06_2016-06_2221m'] = 'RCSE1'
dictionary ['RCSE2_CTD_2016-06_2016-06_2175m'] = 'RCSE2'
dictionary ['RCSE3_CTD_2016-06_2016-06_2100m'] = 'RCSE3'
dictionary ['RCSE4_CTD_2016-06_2016-06_2027m'] = 'RCSE4'
dictionary ['RCSW1_CTD_2016-06_2016-06_2171m'] = 'RCSW1'
dictionary ['RCSW3_CTD_2016-06_2016-06_2049m'] = 'RCSW3'
dictionary ['RCSW4_CTD_2016-06_2016-06_1974m'] = 'RCSW4'

dictionary ['SCHDW_CTD_2015-09_2015-09_300m']  = 'SCHDW'
dictionary ['SCVIP_CTD_2015-09_2015-09_300m']  = 'SCVIP'
dictionary ['TOF1_CTD_2015-08_2016-07_32m']    = 'TOF1'
dictionary ['USDDL_CTD_2015-09_2015-09_114m']  = 'USDDL'
dictionary ['Zuc1_CTD_2015-08_2016-07_40m']    = 'ZUC1a'
dictionary ['Zuc1_CTD_2015-08_2016-07_183m']   = 'ZUC1b'

