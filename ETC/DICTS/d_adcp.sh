#!/bin/ksh

# Declare the dictionary linking the strings in the observation files and the short codes associated with them.

#declare -A ADCPs	# BASH only
typeset -A ADCPs	# KSH?

#animals=( ["moo"]="cow" ["woof"]="dog")

#ADCPs=( ["A1a"]="A1_ADCP_2015-08_2016-07_493m")
ADCPs=( ["A1aa"]="A1-ST_ADCP_2015-08_2016-01_493m")
ADCPs+=(["A1ab"]="A1-ST_ADCP_2016-01_2016-07_493m")
ADCPs+=(["A1b"]="A1_ADCP_2016-07_2017-05_480m")
#ADCPs+=(["BP1a"]="BP1_ADCP_2015-07_2016-07_94m")
ADCPs+=(["BP1aa"]="BP1-ST_ADCP_2015-07_2016-XX_94m")
ADCPs+=(["BP1ab"]="BP1-ST_ADCP_2016-XX_2016-07_94m")
ADCPs+=(["BP1b"]="BP1_ADCP_2016-07_2017-07_99m")
ADCPs+=(["EO1a"]="E01_ADCP_2015-08_2016-07_91m")
ADCPs+=(["EO1b"]="E01_ADCP_2016-07_2017-07_91m")
ADCPs+=(["HEC1a"]="HEC1_ADCP_2015-07_2016-07_39m")   # moorings with multiple instruments need to be denoted a / b / c etc.  surface downwards.
ADCPs+=(["HEC1b"]="HEC1_ADCP_2015-07_2016-07_125m")  

ADCPs+=(["BARK1a"]="BarkleyCanyon_ADCP_2016-06_2016-10_885m.nc")
ADCPs+=(["BARK1b"]="BarkleyCanyon_ADCP_2016-06_2017-06_892m.nc")
ADCPs+=(["BARK1c"]="BarkleyCanyon_ADCP_2016-06_2017-06_968m.nc")
ADCPs+=(["BARK1d"]="BarkleyCanyon_ADCP_2016-06_2017-06_981m.nc")
ADCPs+=(["BARK1e"]="BarkleyCanyon_ADCP_2016-10_2017-10_885m.nc")
ADCPs+=(["BOPA1"]="BoundaryPass_ADCP_2016-10_2017-04_222m.nc")
#ADCPs+=([""]="CAM1_ADCP_2015-07_2016-07_420m.nc")	# no obvious mooring output
#ADCPs+=([""]="CAM1_ADCP_2015-07_2016-07_45m.nc")
ADCPs+=(["CAM2a"]="CAM2_ADCP_2015-07_2016-01_30m.nc")
ADCPs+=(["CAM2b"]="CAM2_ADCP_2015-07_2016-07_227m.nc")
ADCPs+=(["CMP1a"]="CMP1_ADCP_2015-07_2016-01_40m.nc")
ADCPs+=(["CMP1b"]="CMP1_ADCP_2015-07_2016-05_355m.nc")
ADCPs+=(["DEV1a"]="DEV1_ADCP_2015-07_2016-04_102m.nc")
ADCPs+=(["DEV1b"]="DEV1_ADCP_2015-07_2016-05_112m.nc")
#ADCPs+=([""]="FOC1_ADCP_2015-07_2016-07_37m.nc")	# no obvious mooring output
#ADCPs+=([""]="FOC1_ADCP_2015-07_2016-07_356m.nc")
ADCPs+=(["FGPD"]="FolgerPassage_ADCP_2015-09_2016-03_22m.nc")
ADCPs+=(["JFC2"]="JuandeFucaStrait_ADCP_2015-09_2016-05_182m.nc")
ADCPs+=(["AS04"]="JuandeFucaStrait_ADCP_2016-04_2016-10_110m.nc")	# different location than first Juan de Fuca mooring - ~ 1 degree further west than first listed mooring
ADCPs+=(["MAC"]="JuandeFucaStrait_ADCP_2016-10_2017-04_55m.nc")
ADCPs+=(["KSK1a"]="KSK1_ADCP_2015-07_2016-05_41m.nc")
ADCPs+=(["KSK1b"]="KSK1_ADCP_2015-07_2016-05_359m.nc")

#dictionary ['CYP1_ADCP_2016-07_2017-10_64m'] =
#dictionary ['ClayoquotSlope_ADCP_2017-06_2018-05_1242m'] =
#dictionary ['ESP1_ADCP_2016-07_2017-07_52m'] =
ADCPs+=(["FOP1"]="FolgerPassage_ADCP_2015-09_2016-03_22m")
ADCPs+=(["HAK1a"]="HAK1_ADCP_2016-07_2017-07_42m")
ADCPs+=(["HAK1b"]="HAK1_ADCP_2016-07_2017-07_137m")
#dictionary ['HSC1_ADCP_2016-07_2016-12_49m'] =
#dictionary ['MUC1_ADCP_2016-07_2017-07_41m'] =
#dictionary ['MUC1_ADCP_2016-07_2017-07_277m'] =
ADCPs+=(["SCO1a"]="SCOTT1_ADCP_2016-07_2017-07_41m")
ADCPs+=(["SCO1b"]="SCOTT1_ADCP_2016-07_2017-07_102m")
ADCPs+=(["SCO2a"]="SCOTT2_ADCP_2016-07_2017-07_40m")
ADCPs+=(["SCO2b"]="SCOTT2_ADCP_2016-07_2017-07_275m")
ADCPs+=(["SOGN1aa"]="SOGN2_ADCP_2015-10_2016-07_33m")
ADCPs+=(["SOGN1ab"]="SOGN2_ADCP_2015-10_2016-07_314m")
ADCPs+=(["SOGN1b"]="SOGN2_ADCP_2016-07_2017-04_39m")
ADCPs+=(["SOGN1c"]="SOGN2_ADCP_2016-07_2017-07_320m")
#ADCPs+=(["SOGNC"]="SOGNC_ADCP_2015-10_2016-06_174m")
ADCPs+=(["SRC1a"]="SRC1_ADCP_2016-07_2017-07_54m")
ADCPs+=(["SRC1b"]="SRC1_ADCP_2016-07_2017-07_203m")






#echo ${ADCPs['A1']}
#echo ${!ADCPs[@]}
#echo ${!ADCPs["A1_ADCP_2015-08_2016-07_493m"]}









#for eachkey in "${!ADCPs[@]}"; do echo "${eachkey}"; done	# A1, BP1, etc

#echo

#for eachvalue in "${ADCPs[@]}"; do echo "${eachvalue}"; done	# observation file names

