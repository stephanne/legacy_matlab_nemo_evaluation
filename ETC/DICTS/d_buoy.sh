#!/bin/ksh

# Declare the dictionary linking the strings in the observation files and the short codes associated with them.

#declare -A ADCPs	# BASH only
typeset -A BUOYs	# KSH?

#animals=( ["moo"]="cow" ["woof"]="dog")

BUOYs=( ["46005"]="46005_SST_2015-09_2018-05")
BUOYs+=(["46041"]="46041_SST_2015-09_2018-05")
BUOYs+=(["46084"]="46084_SST_2015-09_2016-12")
BUOYs+=(["46087"]="46087_SST_2015-09_2018-05")
BUOYs+=(["46089"]="46089_SST_2015-09_2018-05")
BUOYs+=(["46096"]="46096_SST_2016-09_2017-10")
BUOYs+=(["46119"]="46119_SST_2016-04_2016-05")
BUOYs+=(["46127"]="46127_SST_2016-09_2016-10")
BUOYs+=(["46211"]="46211_SST_2015-09_2018-05")
BUOYs+=(["46243"]="46243_SST_2015-09_2018-05")
BUOYs+=(["46248"]="46248_SST_2015-09_2018-05")
BUOYs+=(["CDIXON"]="Central_Dixon_Entrance_SST_2015-09_2018-08")
BUOYs+=(["EDELWD"]="East_Dellwood_SST_2015-09_2018-08")
BUOYs+=(["ELFNCV"]="Elfin_Cove_SST_2015-09_2018-07")
BUOYs+=(["FRIDAY"]="Friday_Harbour_SST_2015-09_2018-07")
BUOYs+=(["GRBLDI"]="Garibaldi_SST_2015-09_2018-07")
BUOYs+=(["HALIBT"]="Halibut_Bank_SST_2015-09_2018-08")
BUOYs+=(["JUNEAU"]="Juneau_SST_2015-09_2018-07")
BUOYs+=(["KTCHKN"]="Ketchikan_SST_2015-09_2018-07")
BUOYs+=(["PEROUS"]="La_Perouse_Bank_SST_2015-09_2018-07")
BUOYs+=(["LAPUSH"]="La_Push_SST_2015-09_2018-07")
BUOYs+=(["MNOMAD"]="Middle_Nomad_SST_2015-09_2018-08")
BUOYs+=(["NNAKWA"]="Nanakwa_Shoal_SST_2015-09_2018-08")
BUOYs+=(["NEAHBY"]="Neah_Bay_SST_2015-09_2018-07")
BUOYs+=(["NHECAT"]="North_Hecate_Strait_SST_2015-09_2018-08")
BUOYs+=(["NNOMAD"]="North_Nomad_SST_2015-09_2018-08")
BUOYs+=(["PTALEX"]="Port_Alexander_SST_2015-09_2018-07")
BUOYs+=(["PTANGL"]="Port_Angeles_SST_2015-09_2018-07")
BUOYs+=(["PTTWNS"]="Port_Townsend_SST_2015-09_2018-07")
BUOYs+=(["SENTRY"]="Sentry_Shoal_SST_2015-09_2018-08")
BUOYs+=(["SBEACH"]="South_Beach_SST_2015-09_2018-07")
BUOYs+=(["SBROOK"]="South_Brooks_SST_2015-09_2018-05")
BUOYs+=(["SHECAT"]="South_Hecate_Strait_SST_2015-09_2018-06")
BUOYs+=(["SMRSBY"]="South_Moresby_SST_2015-09_2018-08")
BUOYs+=(["SNOMAD"]="South_Nomad_SST_2015-09_2018-08")
BUOYs+=(["TOKEPT"]="Toke_Point_SST_2015-09_2018-07")
BUOYs+=(["WDIXON"]="West_Dixon_Entrance_SST_2015-09_2018-08")
BUOYs+=(["WMRSBY"]="West_Moresby_SST_2015-09_2018-08")
BUOYs+=(["WSEAOT"]="West_Sea_Otter_SST_2015-09_2018-08")
BUOYs+=(["WSTPRT"]="Westport_SST_2015-09_2018-07")
BUOYs+=(["NEAW1"]="neaw1_SST_2015-09_2018-05")
BUOYs+=(["PLXA2"]="plxa2_SST_2015-09_2018-05")
BUOYs+=(["PTAW1"]="ptaw1_SST_2015-09_2018-05")
BUOYs+=(["TLBO3"]="tlbo3_SST_2015-09_2018-05")
BUOYs+=(["WPTW1"]="wptw1_SST_2015-09_2018-05")

#for eachkey in "${!BUOYs[@]}"; do echo "${eachkey}"; done	# A1, BP1, etc

#echo

#for eachvalue in "${ADCPs[@]}"; do echo "${eachvalue}"; done	# observation file names

