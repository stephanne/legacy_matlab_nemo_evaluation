
# Dictionary linking the observation filenames with a mooring short code used in iodef file.
# no modules needed - only core python functionality needed.
# keys are unique, but values are not necessarily - note there are often multiple observation
# files for a single mooring location.

# ADCP DICTIONARY

dictionary = {}

#dictionary ['A1_ADCP_2015-08_2016-07_493m'] = 'A1a'
dictionary ['A1-ST_ADCP_2015-08_2016-01_493m'] = 'A1aa'
dictionary ['A1-ST_ADCP_2016-01_2016-07_493m'] = 'A1ab'
dictionary ['A1_ADCP_2016-07_2017-05_480m'] = 'A1b'
#dictionary ['BP1_ADCP_2015-07_2016-07_94m'] = 'BP1a'
dictionary ['BP1-ST_ADCP_2015-07_2016-XX_94m'] = 'BP1aa'
dictionary ['BP1-ST_ADCP_2016-XX_2016-07_94m'] = 'BP1ab'
dictionary ['BP1_ADCP_2016-07_2017-07_99m'] = 'BP1b'
dictionary ['E01_ADCP_2015-08_2016-07_91m'] = 'EO1a'
dictionary ['E01_ADCP_2016-07_2017-07_91m'] = 'EO1b'
dictionary ['HEC1_ADCP_2015-07_2016-07_39m'] = 'HEC1a'
dictionary ['HEC1_ADCP_2015-07_2016-07_125m'] = 'HEC1b'

dictionary ['BarkleyCanyon_ADCP_2016-06_2016-10_885m'] = 'BARK1a'
dictionary ['BarkleyCanyon_ADCP_2016-06_2017-06_892m'] = 'BARK1b'
dictionary ['BarkleyCanyon_ADCP_2016-06_2017-06_968m'] = 'BARK1c'
dictionary ['BarkleyCanyon_ADCP_2016-06_2017-06_981m'] = 'BARK1d'
dictionary ['BarkleyCanyon_ADCP_2016-10_2017-10_885m'] = 'BARK1e'

dictionary ['BoundaryPass_ADCP_2016-10_2017-04_222m'] = 'BOPA1'		# BDYPM is off by one - had duplicate already, adjusted one

dictionary ['CAM1_ADCP_2015-07_2016-07_45m'] = 'CAM1a'
dictionary ['CAM1_ADCP_2015-07_2016-07_420m'] = 'CAM1b'
dictionary ['CAM2_ADCP_2015-07_2016-01_30m'] = 'CAM2a'
dictionary ['CAM2_ADCP_2015-07_2016-07_227m'] = 'CAM2b'
dictionary ['CMP1_ADCP_2015-07_2016-01_40m'] = 'CMP1a'
dictionary ['CMP1_ADCP_2015-07_2016-05_355m'] = 'CMP1b'
dictionary ['DEV1_ADCP_2015-07_2016-04_102m'] = 'DEV1a'
dictionary ['DEV1_ADCP_2015-07_2016-05_112m'] = 'DEV1b'

dictionary ['FOC1_ADCP_2015-07_2016-07_37m'] = 'FOC1a'
dictionary ['FOC1_ADCP_2015-07_2016-07_356m'] = 'FOC1b'

dictionary ['FolgerPassage_ADCP_2015-09_2016-03_22m'] = 'FGPD'
dictionary ['JuandeFucaStrait_ADCP_2015-09_2016-05_182m'] = 'JFC2'
dictionary ['JuandeFucaStrait_ADCP_2016-04_2016-10_110m'] = 'AS04'	# originally JFC2b
dictionary ['JuandeFucaStrait_ADCP_2016-10_2017-04_55m'] = 'MAC'	# originally JFC2c
dictionary ['KSK1_ADCP_2015-07_2016-05_41m'] = 'KSK1a'
dictionary ['KSK1_ADCP_2015-07_2016-05_359m'] = 'KSK1b'

dictionary ['CYP1_ADCP_2016-07_2017-10_64m'] = 'CYP1'
dictionary ['ClayoquotSlope_ADCP_2017-06_2018-05_1242m'] = 'CQS1'
dictionary ['ESP1_ADCP_2016-07_2017-07_52m'] = 'ESP1'

dictionary ['FolgerPassage_ADCP_2015-09_2016-03_22m'] = 'FOP1'
dictionary ['HAK1_ADCP_2016-07_2017-07_42m'] = 'HAK1a'
dictionary ['HAK1_ADCP_2016-07_2017-07_137m'] = 'HAK1b'
dictionary ['HSC1_ADCP_2016-07_2016-12_49m'] = 'HSC1'
dictionary ['MUC1_ADCP_2016-07_2017-07_41m'] = 'MUC1a'
dictionary ['MUC1_ADCP_2016-07_2017-07_277m'] = 'MUC1b'

dictionary ['SCOTT1_ADCP_2016-07_2017-07_41m'] = 'SCO1a'
dictionary ['SCOTT1_ADCP_2016-07_2017-07_102m'] = 'SCO1b'
dictionary ['SCOTT2_ADCP_2016-07_2017-07_40m'] = 'SCO2a'
dictionary ['SCOTT2_ADCP_2016-07_2017-07_275m'] = 'SCO2b'
dictionary ['SOGN2_ADCP_2015-10_2016-07_33m'] = 'SOGN1aa'
dictionary ['SOGN2_ADCP_2015-10_2016-07_314m'] = 'SOGN1ab'
dictionary ['SOGN2_ADCP_2016-07_2017-04_39m'] = 'SOGN1b'
dictionary ['SOGN2_ADCP_2016-07_2017-07_320m'] = 'SOGN1c'
dictionary ['SOGNC_ADCP_2015-10_2016-06_174m'] = 'SOGNC'
dictionary ['SRC1_ADCP_2016-07_2017-07_54m'] = 'SRC1a'
dictionary ['SRC1_ADCP_2016-07_2017-07_203m'] = 'SRC1b'


#dictionary ['CAM2_ADCP_2015-07_2016-01_30m'] = 'X-CAM'	# not currently output
#dictionary ['A1_ADCP_2015-08_2016-07_493m'] = 'A1'
#dictionary ['BP1_ADCP_2015-07_2016-07_94m'] = 'BP1'

