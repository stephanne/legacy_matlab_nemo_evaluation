
# Dictionary linking the observation filenames with a mooring short code used in iodef file.
# no modules needed - only core python functionality needed.
# keys are unique, but values are not necessarily - note there are often multiple observation
# files for a single mooring location.

# ADCP DICTIONARY

dictionary = {}

dictionary ['./ADCP/BoundaryPass_ADCP_2016-10_2017-04_222m'] = 'ST-BDYPASS'
dictionary ['./ADCP/ClayoquotSlope_ADCP_2017-06_2018-05_1242m'] = 'ST-CLAYOQUOT'
dictionary ['./ADCP/Endeavour_ADCP_2015-09_2015-12_2199m'] = 'ST-ENDEAVOUR1'
dictionary ['./ADCP/Endeavour_ADCP_2017-06_2018-05_2191m'] = 'ST-ENDEAVOUR2'
dictionary ['./ADCP/CAM1_ADCP_2015-07_2016-07_45m'] = 'ST-CAM1'
dictionary ['./ADCP/FOC1_ADCP_2015-07_2016-07_37m'] = 'ST-FOC1'
dictionary ['./ADCP/MUC1_ADCP_2016-07_2017-07_41m'] = 'ST-MUC1'
dictionary ['./ADCP/CYP1_ADCP_2016-07_2017-10_64m'] = 'ST-CYP1'
dictionary ['./ADCP/ESP1_ADCP_2016-07_2017-07_52m'] = 'ST-ESP1'
dictionary ['./ADCP/HSC1_ADCP_2016-07_2016-12_49m'] = 'ST-HSC1'
dictionary ['./ADCP/JuandeFucaStrait_ADCP_2015-09_2015-10_111m'] = 'ST-JF2CA'
dictionary ['./ADCP/JuandeFucaStrait_ADCP_2015-09_2016-05_182m'] = 'ST-JF2CB'
dictionary ['./ADCP/JuandeFucaStrait_ADCP_2016-04_2016-10_110m'] = 'ST-JF2CC'
dictionary ['./ADCP/JuandeFucaStrait_ADCP_2016-10_2017-04_55m'] = 'ST-JF2CD'
dictionary ['./ADCP/JuandeFucaStrait_ADCP_2017-04_2017-11_114m'] = 'ST-JF2CE'
dictionary ['./ADCP/JuandeFucaStrait_ADCP_2017-04_2017-11_57m'] = 'ST-JF2CF'
dictionary ['./MCTD/JF2C_CTD_2015-09_2015-09_182m'] = 'ST-JF2CG'
dictionary ['./ADCP/SOGN2_ADCP_2015-10_2016-07_314m'] = 'ST-SOGN2A'
dictionary ['./ADCP/SOGN2_ADCP_2016-07_2017-04_39m'] = 'ST-SOGN2B'
dictionary ['./ADCP/SOGN2_ADCP_2016-07_2017-07_320m'] = 'ST-SOGN2C'
dictionary ['./ADCP/SOGNC_ADCP_2015-10_2016-06_174m'] = 'ST-SOGNC'
dictionary ['./MCTD/BDYPM_CTD_2015-09_2015-09_229m'] = 'ST-BDYPM'
dictionary ['./MCTD/RCNE1_CTD_2016-05_2016-05_2153m'] = 'ST-RCNE1'
dictionary ['./MCTD/RCNE2_CTD_2016-05_2016-05_2106m'] = 'ST-RCNE2'
dictionary ['./MCTD/RCNE3_CTD_2016-05_2016-05_2030m'] = 'ST-RCNE3'
dictionary ['./MCTD/RCNE4_CTD_2016-05_2016-05_1956m'] = 'ST-RCNE4'
dictionary ['./MCTD/RCNW1_CTD_2016-05_2016-05_2140m'] = 'ST-RCNW1'
dictionary ['./MCTD/RCNW3_CTD_2016-05_2016-05_2018m'] = 'ST-RCNW3'
dictionary ['./MCTD/RCNW4_CTD_2016-05_2016-05_1943m'] = 'ST-RCNW4'
dictionary ['./MCTD/RCSE1_CTD_2016-06_2016-06_2221m'] = 'ST-RCSE1'
dictionary ['./MCTD/RCSE2_CTD_2016-06_2016-06_2175m'] = 'ST-RCSE2'
dictionary ['./MCTD/RCSE3_CTD_2016-06_2016-06_2100m'] = 'ST-RCSE3'
dictionary ['./MCTD/RCSE4_CTD_2016-06_2016-06_2027m'] = 'ST-RCSE4'
dictionary ['./MCTD/RCSW1_CTD_2016-06_2016-06_2171m'] = 'ST-RCSW1'
dictionary ['./MCTD/RCSW3_CTD_2016-06_2016-06_2049m'] = 'ST-RCSW3'
dictionary ['./MCTD/RCSW4_CTD_2016-06_2016-06_1974m'] = 'ST-RCSW4'
#dictionary ['./MCTD/'] = 'ST-'


