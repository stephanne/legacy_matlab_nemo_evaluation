#!/bin/ksh

# Declare the dictionary linking the strings in the observation files and the short codes associated with them.

#declare -A CMs	# BASH only
typeset -A CMs	# KSH?

#animals=( ["moo"]="cow" ["woof"]="dog")

CMs=( ["A1aa"]="A1_CM_2015-08_2016-07_35m")
CMs+=(["A1ab"]="A1_CM_2015-08_2016-07_100m")
CMs+=(["A1ac"]="A1_CM_2015-08_2016-07_300m")
CMs+=(["A1ba"]="A1_CM_2016-07_2017-04_25m")
CMs+=(["A1bb"]="A1_CM_2016-07_2017-04_90m")
CMs+=(["A1bc"]="A1_CM_2016-07_2017-04_300m")
CMs+=(["BACCH"]="BACCH_CM_2016-05_2016-05_369m")
CMs+=(["CAM1"]="CAM1_CM_2015-07_2016-07_150m")
CMs+=(["CAM2"]="CAM2_CM_2015-07_2016-07_150m")
CMs+=(["CMP1a"]="CMP1_CM_2015-07_2016-01_250m")
CMs+=(["CMP1b"]="CMP1_CM_2016-01_2016-05_250m")
CMs+=(["DEV1"]="DEV1_CM_2015-07_2016-05_152m")
CMs+=(["EO1a"]="E01_CM_2015-08_2016-05_35m")
CMs+=(["EO1b"]="E01_CM_2016-07_2017-04_35m")
CMs+=(["ENWF1"]="ENWF_CM_2016-06_2016-06_2360m")
CMs+=(["FGPD"]="FGPPN_CM_2015-09_2015-09_25m")
#CMs+=(["FOC1a"]="FOC1_CM_2015-07_2016-06_150m")
#CMs+=(["FOC1b"]="FOC1_CM_2015-07_2016-06_250m")
CMs+=(["JFC2"]="JF2C_CM_2016-05_2016-05_182m")
CMs+=(["KSK1"]="KSK1_CM_2015-07_2016-05_150m")
CMs+=(["S46087"]="NDBC_46087_CM_2015-09_2018-05")
CMs+=(["S46088"]="NDBC_46088_CM_2015-09_2018-05")
CMs+=(["RCNE1"]="RCNE1_CM_2016-05_2016-05_2153m")
CMs+=(["RCNE1"]="RCNE2_CM_2016-05_2016-05_2106m")
CMs+=(["RCNE1"]="RCNE4_CM_2016-05_2016-05_1956m")
CMs+=(["RCNW1"]="RCNW1_CM_2016-05_2016-05_2140m")
CMs+=(["RCSE1"]="RCSE1_CM_2016-06_2016-06_2221m")
CMs+=(["RCSE1"]="RCSE2_CM_2016-06_2016-06_2175m")
CMs+=(["RCSE1"]="RCSE3_CM_2016-06_2016-06_2100m")
#CMs+=([""]="")


