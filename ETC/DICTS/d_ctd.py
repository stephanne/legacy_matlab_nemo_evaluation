
# Dictionary linking the observation filenames with a mooring short code used in iodef file.
# no modules needed - only core python functionality needed.
# keys are unique, but values are not necessarily - note there are often multiple observation
# files for a single mooring location.

# CTD DICTIONARY

# The casts are far too voluminous - save this for the moored ones.

dictionary = {}
#dictionary ['HEC1_ADCP_2015-07_2016-07_125m'] = 'HEC1'



