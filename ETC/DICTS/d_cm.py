
# Dictionary linking the observation filenames with a mooring short code used in iodef file.
# no modules needed - only core python functionality needed.
# keys are unique, but values are not necessarily - note there are often multiple observation
# files for a single mooring location.

# CURRENT METRE DICTIONARY

dictionary = {}

dictionary ['A1_CM_2015-08_2016-07_35m'] =      'A1aa'
dictionary ['A1_CM_2015-08_2016-07_100m'] =     'A1ab'
dictionary ['A1_CM_2015-08_2016-07_300m'] =     'A1ac'
dictionary ['A1_CM_2016-07_2017-04_25m'] =      'A1ba'
dictionary ['A1_CM_2016-07_2017-04_90m'] =      'A1bb'
dictionary ['A1_CM_2016-07_2017-04_300m'] =     'A1bc'
dictionary ['BACCH_CM_2016-05_2016-05_369m'] =  'BACCH'
dictionary ['CAM1_CM_2015-07_2016-07_150m'] =   'CAM1'
dictionary ['CAM2_CM_2015-07_2016-07_150m'] =   'CAM2'
dictionary ['CMP1_CM_2015-07_2016-01_250m'] =   'CMP1a'
dictionary ['CMP1_CM_2016-01_2016-05_250m'] =   'CMP1b'
dictionary ['DEV1_CM_2015-07_2016-05_152m'] =   'DEV1'
dictionary ['E01_CM_2015-08_2016-05_35m'] =     'EO1a'
dictionary ['E01_CM_2016-07_2017-04_35m'] =     'EO1b'
dictionary ['ENWF_CM_2016-06_2016-06_2360m'] =  'ENWF1'
dictionary ['FGPPN_CM_2015-09_2015-09_25m'] =   'FGPD'
dictionary ['FOC1_CM_2015-07_2016-06_150m'] =   'FOC1a'
dictionary ['FOC1_CM_2015-07_2016-06_250m'] =   'FOC1b'
dictionary ['JF2C_CM_2016-05_2016-05_182m'] =   'JFC2'
dictionary ['KSK1_CM_2015-07_2016-05_150m'] =   'KSK1'
dictionary ['NDBC_46087_CM_2015-09_2018-05'] =  'S46087'
dictionary ['NDBC_46088_CM_2015-09_2018-05'] =  'S46088'
dictionary ['RCNE1_CM_2016-05_2016-05_2153m'] = 'RCNE1'
dictionary ['RCNE2_CM_2016-05_2016-05_2106m'] = 'RCNE1'
dictionary ['RCNE4_CM_2016-05_2016-05_1956m'] = 'RCNE1'
dictionary ['RCNW1_CM_2016-05_2016-05_2140m'] = 'RCNW1'
dictionary ['RCSE1_CM_2016-06_2016-06_2221m'] = 'RCSE1'
dictionary ['RCSE2_CM_2016-06_2016-06_2175m'] = 'RCSE1'
dictionary ['RCSE3_CM_2016-06_2016-06_2100m'] = 'RCSE1' 

