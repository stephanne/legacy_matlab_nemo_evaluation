
# Dictionary linking the observation filenames with a short code.
# no modules needed - only core python functionality needed.
# keys are unique, but values are not necessary, although there is plenty enough space between tide gauges that no two the same. 

# TIDE GAUGE DICTIONARY

dictionary = {}
dictionary ['Bamfield']             = 'TG-BAMFLD-8545'	
dictionary ['Bella-Bella']          = 'TG-BELLA2-8976'
dictionary ['Bonilla-Island']       = 'TG-BONILA-9227'
dictionary ['Campbell-River']       = 'TG-CAMRIV-8074'
dictionary ['Cherry-Point']         = 'TG-CHERRY-9449424'
dictionary ['Friday-Harbour']       = 'TG-FRIDAY-9449880'
dictionary ['Hartley-Bay']          = 'TG-HARTLY-9130'
dictionary ['Henslung-Cove']        = 'TG-HENSCV-9958'
dictionary ['Kitimat']              = 'TG-KITIMT-9140'
dictionary ['La-Push']              = 'TG-LAPUSH-9442396'
dictionary ['Nanaimo']              = 'TG-NANAIM-7917'
dictionary ['Neah-Bay']             = 'TG-NEAHBY-9443090'
dictionary ['New-Westminster']      = 'TG-NWSTMN-7654'
dictionary ['Patricia-Bay']         = 'TG-PATBAY-7277'
dictionary ['Point-Atkinson']       = 'TG-PNTATK-7795'
dictionary ['Port-Angeles']         = 'TG-PTANGL-9444090'
dictionary ['Port-Townsend']        = 'TG-PTTWNS-9444900'
dictionary ['Port-Hardy']           = 'TG-PTHRDY-8408'
dictionary ['Prince-Rupert']        = 'TG-RUPERT-9354'
dictionary ['Queen-Charlotte-City'] = 'TG-QCHARL-9850'
dictionary ['Rose-Harbour']         = 'TG-ROSEHB-9713'
dictionary ['Tofino']               = 'TG-TOFINO-8615'
dictionary ['Vancouver-Harbour']    = 'TG-VANCVR-7735'
dictionary ['Victoria-Harbour']     = 'TG-VICTRA-7120'
dictionary ['Winter-Harbour']       = 'TG-WINTER-8735'

dictionary ['Seattle']              = 'TG-SEATTL-9441730'
dictionary ['Tacoma']               = 'TG-TACOMA-9446484'
dictionary ['Westport']             = 'TG-WSTPRT-9441102'
dictionary ['Toke-Point']           = 'TG-TOKEPT-9440910'
dictionary ['Cape-Disappointment']  = 'TG-DISAPT-9440581'
dictionary ['Garibaldi']            = 'TG-GRBLDI-9437540'

dictionary ['Ketchikan']            = 'TG-KTCHKN-9450460'
dictionary ['Port-Alexander']       = 'TG-PTALEX-9451054'
dictionary ['Sitka']                = 'TG-SITKA-9451600'
dictionary ['Juneau']               = 'TG-JUNEAU-9452210'
dictionary ['Elfin-Cove']           = 'TG-ELFNCV-9452634'
