
# Dictionary linking the observation filenames with a mooring short code used in iodef file.
# no modules needed - only core python functionality needed.
# keys are unique, but values are not necessarily - note there are often multiple observation
# files for a single mooring location.

# SST BUOY DICTIONARY

dictionary = {}

dictionary ['46005_SST_2015-09_2018-05'] = 'BUOY-46005'
dictionary ['46041_SST_2015-09_2018-05'] = 'BUOY-46041'
dictionary ['46084_SST_2015-09_2016-12'] = 'BUOY-46084'
dictionary ['46087_SST_2015-09_2018-05'] = 'BUOY-46087'
dictionary ['46089_SST_2015-09_2018-05'] = 'BUOY-46089'
dictionary ['46096_SST_2016-09_2017-10'] = 'BUOY-46096'
dictionary ['46119_SST_2016-04_2016-05'] = 'BUOY-46119'
dictionary ['46127_SST_2016-09_2016-10'] = 'BUOY-46127'
dictionary ['46211_SST_2015-09_2018-05'] = 'BUOY-46211'
dictionary ['46243_SST_2015-09_2018-05'] = 'BUOY-46243'
dictionary ['46248_SST_2015-09_2018-05'] = 'BUOY-46248'
dictionary ['Central_Dixon_Entrance_SST_2015-09_2018-08'] = 'BUOY-CDIXON'
dictionary ['East_Dellwood_SST_2015-09_2018-08'] = 'BUOY-EDELWD'
dictionary ['Elfin_Cove_SST_2015-09_2018-07'] = 'BUOY-ELFNCV'
dictionary ['Friday_Harbour_SST_2015-09_2018-07'] = 'BUOY-FRIDAY'
dictionary ['Garibaldi_SST_2015-09_2018-07'] = 'BUOY-GRBLDI'
dictionary ['Halibut_Bank_SST_2015-09_2018-08'] = 'BUOY-HALIBT'
dictionary ['Juneau_SST_2015-09_2018-07'] = 'BUOY-JUNEAU'
dictionary ['Ketchikan_SST_2015-09_2018-07'] = 'BUOY-KTCHKN'
dictionary ['La_Perouse_Bank_SST_2015-09_2018-07'] = 'BUOY-PEROUS'
dictionary ['La_Push_SST_2015-09_2018-07'] = 'BUOY-LAPUSH'
dictionary ['Middle_Nomad_SST_2015-09_2018-08'] = 'BUOY-MNOMAD'
dictionary ['Nanakwa_Shoal_SST_2015-09_2018-08'] = 'BUOY-NNAKWA'
dictionary ['Neah_Bay_SST_2015-09_2018-07'] = 'BUOY-NEAHBY'
dictionary ['North_Hecate_Strait_SST_2015-09_2018-08'] = 'BUOY-NHECAT'
dictionary ['North_Nomad_SST_2015-09_2018-08'] = 'BUOY-NNOMAD'
dictionary ['Port_Alexander_SST_2015-09_2018-07'] = 'BUOY-PTALEX'
dictionary ['Port_Angeles_SST_2015-09_2018-07'] = 'BUOY-PTANGL'
dictionary ['Port_Townsend_SST_2015-09_2018-07'] = 'BUOY-PTTWNS'
dictionary ['Sentry_Shoal_SST_2015-09_2018-08'] = 'BUOY-SENTRY'
dictionary ['South_Beach_SST_2015-09_2018-07'] = 'BUOY-SBEACH'
dictionary ['South_Brooks_SST_2015-09_2018-05'] = 'BUOY-SBROOK'
dictionary ['South_Hecate_Strait_SST_2015-09_2018-06'] = 'BUOY-SHECAT'
dictionary ['South_Moresby_SST_2015-09_2018-08'] = 'BUOY-SMRSBY'
dictionary ['South_Nomad_SST_2015-09_2018-08'] = 'BUOY-SNOMAD'
dictionary ['Toke_Point_SST_2015-09_2018-07'] = 'BUOY-TOKEPT'
dictionary ['West_Dixon_Entrance_SST_2015-09_2018-08'] = 'BUOY-WDIXON'
dictionary ['West_Moresby_SST_2015-09_2018-08'] = 'BUOY-WMRSBY'
dictionary ['West_Sea_Otter_SST_2015-09_2018-08'] = 'BUOY-WSEAOT'
dictionary ['Westport_SST_2015-09_2018-07'] = 'BUOY-WSTPRT'
dictionary ['neaw1_SST_2015-09_2018-05'] = 'BUOY-NEAW1'
dictionary ['plxa2_SST_2015-09_2018-05'] = 'BUOY-PLXA2'
dictionary ['ptaw1_SST_2015-09_2018-05'] = 'BUOY-PTAW1'
dictionary ['tlbo3_SST_2015-09_2018-05'] = 'BUOY-TLBO3'
dictionary ['wptw1_SST_2015-09_2018-05'] = 'BUOY-WPTW1'

