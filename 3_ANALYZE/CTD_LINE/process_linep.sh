#!/bin/bash

# PROCESS LINE P CTD DATA
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: October 12, 2018.

# USED WITH: process_ctd.sh
# REQUIRES : MATLAB 2013+, GSW matlab library
# PURPOSE  : Reads in a line of data from a cruise, makes a nice plot along the line.  CTDs already processed, in a .mat file.

# INPUT : Line P CTD data organized by cruise, daily 3D T files, local bathymetry. 

# OUTPUT: 

set -a; source ../../config.ksh; set +a

CONFIG='NEP36';       RUN_ID='DI02d'

# INPUT FILES 
# (CTD processed already, in a .mat file in the order to be plotted.  Plotting done by lat/lon but to avoid weird contours need it ordered.)
FILE_OBS_DATA='/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/LINE_P/NEP36-DI02d_1d_20160208-20160219_CTD_summary.mat'
FILE_MOD_DATA='/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/LINE_P/NEP36-DI02d_1d_20160208-20160219_CTD_summary.mat'

# OUTPUT FILES
DIR_OUTPUT=${REF_DIR_UMBRELLA}'/TESTING_LINE';	ROOT_OUTPUT='NEP36-DI02d_1d_20160208-20160219'

# PARAMETERS
FILE_CONFIG='config_ctdline'

# Metadata
NOTE='winter_cruise'			# string for any relevant information
USER=`whoami`				# attribution string


# Template files
TEMPLATE_SCRIPT_LINEP=${PKG_LOCATION}/${DIR_LIB}'/template_linep.m'
TEMPLATE_SUBMIT=${PKG_LOCATION}/${DIR_LIB}/${REF_FILE_JOBSUB}

# Local script roots
SCRIPT_ROOT=${PKG_LOCATION}'/3_ANALYZE/CTD_LINE/scr_201602_'${CONFIG}_${RUN_ID}


#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [ 0 ] HOUSEKEEPING

if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p ${DIR_OUTPUT}; fi

SED_FILE_OBS_DATA=$(printf "%s\n" "$FILE_OBS_DATA" | sed 's/[\&/]/\\&/g')
SED_FILE_MOD_DATA=$(printf "%s\n" "$FILE_MOD_DATA" | sed 's/[\&/]/\\&/g')

SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g')
SED_ROOT_OUTPUT=$(printf "%s\n" "$ROOT_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_FILE_CONFIG=$(printf "%s\n" "$FILE_CONFIG" | sed 's/[\&/]/\\&/g')

SED_REF_FILE_BATHY=$(printf "%s\n" "$REF_FILE_BATHY" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_MASK=$(printf "%s\n" "$REF_FILE_MASK" | sed 's/[\&/]/\\&/g')
SED_REF_PATH_FXNS=$(printf "%s\n" "$REF_PATH_FXNS" | sed 's/[\&/]/\\&/g')

START_YEAR=${DATE_START:0:4};	START_MONTH=${DATE_START:4:2};	START_DAY=${DATE_START:6:2}
FINAL_YEAR=${DATE_FINAL:0:4};	FINAL_MONTH=${DATE_FINAL:4:2};	FINAL_DAY=${DATE_FINAL:6:2}

#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')



# Copy and swap in the template file


SCRIPT=$SCRIPT_ROOT'_LINE.m'
cp $TEMPLATE_SCRIPT_LINEP $SCRIPT

sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT
sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT

sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT

sed -i "s/VAR_FILE_OBS_DATA/$SED_FILE_OBS_DATA/g" $SCRIPT
sed -i "s/VAR_FILE_MOD_DATA/$SED_FILE_MOD_DATA/g" $SCRIPT

sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_OUTPUT/g" $SCRIPT

sed -i "s/VAR_FILE_CONFIG/$SED_FILE_CONFIG/g" $SCRIPT




