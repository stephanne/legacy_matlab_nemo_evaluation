
% Parameter values for near-surface Line P plot:

nstns = 27;	% specify explicitly?  may have multiple casts at a specific station.  

% Winter Cruise
%cruise = '2016-02';
%nc = 8;        % number of colours / contours
%mint = 5.5;       maxt = 10;      difft = 2;      nct = 9;
%mins = 32;      maxs = 34.5;      diffs = 1;      ncs = 10;
%minr = 1024.5;    maxr = 1028;    diffr = 1;      ncr = 14;

% Summer Cruise
cruise = '2016-06';
nc = 8;        % number of colours / contours
mint = 5;       maxt = 12;      difft = 2;      nct = 14;
mins = 31;      maxs = 34;      diffs = 1;      ncs = 12;
minr = 1023;    maxr = 1028;    diffr = 1;      ncr = 10;

% Summer Cruise
%cruise = '2016-08';
%nc = 8;        % number of colours / contours
%mint = 6;       maxt = 18;      difft = 2;      nct = 12;
%mins = 31.75;      maxs = 34.5;      diffs = 1;      ncs = 11;
%minr = 1023;    maxr = 1028;    diffr = 1;      ncr = 10;

% Contour variables
ct_con      = linspace(mint, maxt, nct+1);
ct_con_diff = linspace(-difft, difft, 2*(nc+1));

sa_con      = linspace(mins, maxs, ncs+1);
sa_con_diff = linspace(-diffs, diffs, 2*(nc+1));

rho_con      = linspace(minr, maxr, ncs+1);
rho_con_diff = linspace(-diffr, diffr, 2*(nc+1));


% 
bathy_file = '/space/hall0/sitestore/dfo/odis/stt001/PACKAGE_MERGING/ETC/REF/Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc';
bathy = double(ncread(bathy_file, 'Bathymetry'));
navlat   = double(ncread(bathy_file, 'nav_lat'));
navlon   = double(ncread(bathy_file, 'nav_lon'));

xl = -143.0;	xr = -126.0;
yb =    0.0;	yt =  200.0;
tk =  0.025;

