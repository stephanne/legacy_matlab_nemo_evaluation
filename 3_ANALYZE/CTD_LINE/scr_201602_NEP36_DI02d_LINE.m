
% Reads in observation and model data for Line P stations (one cruise at a time), constructs nice plots with full range of information available.
% Station 1 is on the right, station 35 on the left - matches how it looks on the map.

clear all

p = genpath ('/space/hall0/sitestore/dfo/odis/stt001/PACKAGE_MERGING/ETC/FXNS'); addpath(p); warning ('off', 'all');


% INPUT INFO
file_obs_data = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/LINE_P/NEP36-DI02d_1d_20160208-20160219_CTD_summary.mat';
file_mod_data = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/LINE_P/NEP36-DI02d_1d_20160208-20160219_CTD_summary.mat';
bathy_file = '/space/hall0/sitestore/dfo/odis/stt001/PACKAGE_MERGING/ETC/REF/Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc';

runid = 'DI02d';


% OUTPUT INFO
dir_output = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TESTING_LINE';		root_output = 'NEP36-DI02d_1d_20160208-20160219';


% PARAMETER FILE
config_ctdline;



%------------------------------------------------------------------------------------------------------------------
%--- END OF USER-SUPPLIED INFO ------------------------------------------------------------------------------------
%------------------------------------------------------------------------------------------------------------------

% [ 0 ] HOUSEKEEPING

% Initialize arrays to store sorted data
ncasts = zeros (nstns,1);
ct_mod = zeros(nstns,75); 	sa_mod = zeros(nstns,75); 	rho_mod = zeros(nstns,75);
ct_obs = zeros(nstns,75); 	sa_obs = zeros(nstns,75); 	rho_obs = zeros(nstns,75);

obs_lon = zeros(nstns,75);		obs_lat = zeros(nstns,75);
mod_lon = zeros(nstns,75);		mod_lat = zeros(nstns,75);
obs_depths = zeros(nstns,75);		mod_depths = zeros(nstns,75);



% [ 1 ] READ IN DATA

% Perhaps instead of sorting etc, below, take it in order of what's in the summary file?
% That way can specify order via filelist input when first processing CTDs.
% Unclear how best to handle multiple casts at a station (which ordinarily would average)
%load ([dir_data '/' root_data '_CTD_summary']);	% contains summary_mod, _obs, _meta
load (file_mod_data);	% contains summary_mod, _obs, _meta

[~, nprofiles ] = size(summary_meta);

% Go profile by profile and sort data into spatial order
for n=1:nprofiles

	% Parse the station number (chop P, check for letter, chop to dash)
	station_str = summary_meta(n).station;
	loc = strfind (station_str, '-');
	stn = station_str(2:(loc(1)-1)  );
	if (isletter(stn(1)) );	stn = stn(2:end); end	% some indicated by PA rather than just P


	% stn is then the numerical station code for the profile
	% note that stn 26 is the end and 35 is between 26 and 25.  (for some baffling and obscure reason)
	cast_ct_mod = summary_mod(n).ct;	cast_sa_mod = summary_mod(n).sa;	cast_rho_mod = summary_mod(n).rho;	cast_depths_mod = summary_mod(n).depths;
	cast_ct_obs = summary_obs(n).ct;	cast_sa_obs = summary_obs(n).sa;	cast_rho_obs = summary_obs(n).rho;	cast_depths_obs = summary_obs(n).depths;


	if     (str2num(stn) == 35);	ind = 26;
	elseif (str2num(stn) == 26);	ind = 27;
	elseif (str2num(stn) <= 25);	ind = str2num(stn);
	else
		display  (['Unrecognized station id: ' stn ])
		stop;
	end

	ct_mod  (ind,:) = cast_ct_mod (:,1); sa_mod  (ind,:) = cast_sa_mod (:,1); rho_mod (ind,:) = cast_rho_mod(:,1);

	% Obs stations may have > 1 per day - ensure that only one day is included in the average by filtering directory contents by hand
	ct_obs  (ind,:) = ct_obs (ind,:) + cast_ct_obs'; 
	sa_obs  (ind,:) = sa_obs (ind,:) + cast_sa_obs'; 
	rho_obs (ind,:) = rho_obs(ind,:) + cast_rho_obs';
	ncasts  (ind)   = ncasts(ind) + 1;


	% spatial info
	obs_lon (ind,:) = summary_meta(n).obs_lon;	obs_lat(ind,:) = summary_meta(n).obs_lat;
	mod_lon (ind,:) = summary_meta(n).mod_lon;	mod_lat(ind,:) = summary_meta(n).mod_lat;

	obs_depths (ind,:) = cast_depths_obs;		mod_depths (ind,:) = cast_depths_mod;

end

% Average the observations by number of casts in each location, NaN anything that doesn't have observations
for s=1:nstns
	if (ncasts(ind) > 0)
		ct_obs  (s,:) = ct_obs (s,:) / ncasts(s);
		sa_obs  (s,:) = sa_obs (s,:) / ncasts(s);	
		rho_obs (s,:) = rho_obs(s,:) / ncasts(s);
	else
		ct_obs  (s,:) = NaN;
		sa_obs  (s,:) = NaN;
		rho_obs (s,:) = NaN;
	end
end


% NaN out anything that's set to zero
ct_obs (ct_obs==0) = NaN; ct_mod (ct_mod==0) = NaN;
sa_obs (sa_obs==0) = NaN; sa_mod (sa_mod==0) = NaN;
rho_obs (rho_obs==0) = NaN; rho_mod (rho_mod==0) = NaN;

% Get the differences
ct_diff = ct_mod - ct_obs;
sa_diff = sa_mod - sa_obs;
rho_diff = rho_mod - rho_obs;


% So now have daily average profiles for stations sorted into appropriate bins. Observation data is already interpolated to model grid depths

% [ 2 ] Plot! That! Data!!!

fig = figure;
subplot (4,3,[1,3]); hold on
pcolor (navlon,navlat,bathy);	shading flat; colorbar		% bathymetry
colormap(gca,cmocean('-deep'))
scatter(obs_lon(:,1),obs_lat(:,1), 50, 'm',  'filled')	% station locations (label them somehow)
set(gca, 'TickLength', [0.025 0.025]); set (gca, 'Linewidth', 1.5, 'FontSize', 16);
xlim ([xl xr]);	ylim ([48.5 50]);

% Temperature
subplot (4,3,4);
ints = linspace(mint, maxt, nct+1);
contourf  (mod_lon, mod_depths, ct_mod, ints); %shading flat; colorbar;
f_fmtaxes    (gca, [xl xr], [yb yt], '', 'Depth (m)', 0, 0, 16, tk, 0);
f_fmtcontour (gca, cmocean('thermo',nct), [mint maxt], -1);
tlabel = text (-149,100, 'Con. Temperature', 'HorizontalAlignment', 'center', 'Rotation', 90, 'FontSize', 22);
mlabel = text ((xr+xl)/2,-25, 'Model', 'HorizontalAlignment', 'center', 'FontSize', 22);

subplot (4,3,5);
contourf  (obs_lon, obs_depths, ct_obs, ct_con); 
f_fmtaxes    (gca, [xl xr], [yb yt], '', '', 0, 0, 16, tk, 0);
f_fmtcontour (gca, cmocean('thermo',nct), [mint maxt], -1);
olabel = text ((xr+xl)/2,-25, 'Observations', 'HorizontalAlignment', 'center', 'FontSize', 22);

subplot (4,3,6);
contourf  (mod_lon, mod_depths, ct_diff, ct_con_diff);  
f_fmtaxes    (gca, [xl xr], [yb yt], '', '', 0, 0, 16, tk, 0);
f_fmtcontour (gca,cmocean('balance',nc), [-difft difft], -1)
dlabel = text ((xr+xl)/2,-25, 'Model - Obs', 'HorizontalAlignment', 'center', 'FontSize', 22);


% Salinity

subplot (4,3,7);
contourf  (mod_lon, mod_depths, sa_mod, sa_con);  
f_fmtaxes    (gca, [xl xr], [yb yt], '', 'Depth (m)', 0, 0, 16, tk, 0);
f_fmtcontour (gca,cmocean('haline', ncs), [mins maxs], -1);
slabel = text (-149,100, 'Abs. Salinity', 'HorizontalAlignment', 'center', 'Rotation', 90, 'FontSize', 22);

subplot (4,3,8);
contourf  (obs_lon, obs_depths, sa_obs, sa_con); 
f_fmtaxes    (gca, [xl xr], [yb yt], '', '', 0, 0, 16, tk, 0);
f_fmtcontour(gca,cmocean('haline',ncs), [mins maxs], -1)

subplot (4,3,9);
contourf  (mod_lon, mod_depths, sa_diff, sa_con_diff); 
f_fmtaxes    (gca, [xl xr], [yb yt], '', '', 0, 0, 16, tk, 0);
f_fmtcontour(gca,cmocean('balance', nc), [-diffs diffs], -1);


% Density
subplot (4,3,10);
contourf  (mod_lon, mod_depths, rho_mod, rho_con);
f_fmtaxes    (gca, [xl xr], [yb yt], 'Longitude', 'Depth (m)', 0, 0, 16, tk, 0);
f_fmtcontour(gca,cmocean('dense', ncr), [minr maxr], -1);
rlabel = text (-149,100, 'In Situ Density', 'HorizontalAlignment', 'center', 'Rotation', 90, 'FontSize', 22);

subplot (4,3,11);
contourf  (obs_lon, obs_depths, rho_obs, rho_con); 
f_fmtaxes    (gca, [xl xr], [yb yt], 'Longitude', '', 0, 0, 16, tk, 0);
f_fmtcontour(gca,cmocean('dense',ncr), [minr maxr], -1)

subplot (4,3,12);
contourf  (mod_lon, mod_depths, rho_diff, rho_con_diff);
f_fmtaxes    (gca, [xl xr], [yb yt], 'Longitude', '', 0, 0, 16, tk, 0);
f_fmtcontour(gca,cmocean('balance',nc), [-diffr diffr], -1)


%annotation('textbox', [0 0.875 1 0.1], 'String', ['Line P  Run ID: ' runid '  Cruise: ' cruise '   Near Surface'  ], 'EdgeColor', 'none', 'HorizontalAlignment', 'center', 'FontSize', 28)
f_titleannot(['Line P  Run ID: ' runid '  Cruise: ' cruise '   Near Surface'  ], 28);




% Output the figure

pngname = [dir_output, '/', root_output '_LineP_', cruise,  ];
f_figprint (gcf, pngname, 5600, 4200, 300);
close(fig);

