% PLOTTING AND STATISTICS FOR THINGS WITH A TIME SERIES
% S Taylor
% Last structural changes: September 20, 2018

% USED WITH: process_tidegauge.sh, process_adcp.sh
% REQUIRES : Matlab 2013+
% PURPOSE  : Reads in .mat file with time series info of some sort (tide gauge, adcp, etc). Plots them 
%            nicely, calculates a frequency spectra (currently no filters applied) and does some basic 
%            stats on the series.

% INPUT    : List of tide gauge stations, .mat files generated with same list (order is important)

% OUTPUT   : Set of plots, .csv with stats etc


%  ------------------------------------------------------------------------------------------------------
%  --- INPUT INFO ---------------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------

clear all

p = genpath ('/space/hall0/sitestore/dfo/odis/stt001/PACKAGE_MERGING/ETC/FXNS');
addpath(p);
warning('off', 'all');


% INPUT INFO
input_summary    = ['/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/BUOY' '/' 'BUOY_NEP36-DI02d_BUOY' '_summary'];
input_directory  = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/BUOY';  input_fileroot   = 'BUOY_NEP36-DI02d_BUOY';
output_directory = '/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/SST'; output_fileroot  = 'SST_NEP36-DI02d';

bathy_file = '/space/hall0/sitestore/dfo/odis/stt001/PACKAGE_MERGING/ETC/REF/Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc';

runid = 'DI02d';


% CONFIGURATION
type_analysis    = 'SST';
str_start = '20160101';	str_final = '20161231';
%config_reference = 'config_plot_reference';
%config_specific  = 'config_plot_series';
config_plot_reference;
config_plot_series;



%  ------------------------------------------------------------------------------------------------------
%  --- END INPUT INFO -----------------------------------------------------------------------------------
%  ------------------------------------------------------------------------------------------------------


% [ 0 ] LOAD THE DATA

load (input_summary);		% contains summary_obs, summary_mod, summary_stats, summary_meta.  

% Coastline / land data
%bathy = double(ncread(bathy_file, 'Bathymetry'));
%lat   = double(ncread(bathy_file, 'nav_lat'));
%lon   = double(ncread(bathy_file, 'nav_lon'));
%land = bathy; land(land>0)=NaN; land(land==0)=1;


[n1,n2] = size (summary_mod);	nstns = max(n1,n2);


% error check to makre sure all plot parameters are present in config files?


% Use summary.meta to get the individual files with the full processed info
for s = 1:nstns

	station = summary_meta(1,s).station;

	src_filename = [input_directory, '/', input_fileroot, '_', station, '_', str_start, '-', str_final, '.mat'];
	load (src_filename);
	
	out_filename = [output_directory, '/' output_fileroot '_' station];


	% Location info for the data
	olon = meta.obs_lon;	olat = meta.obs_lat;
	mlon = meta.mod_lon;	mlat = meta.mod_lat;

	if (exist('nlevels','var') == 0)	% not all type of input data  has multiple levels; put an override in the config_specific for types that don't.
		nlevels = count(meta.levels);
		for l = 1:nlevels
			depth_string{l,:} = ['_' num2str(meta.levels(l))]
		end
	end


	% Instrument switch for different quantities to plot
	switch type_analysis 
		case 'WATER_LEVEL'
			obs_time = observations.time_reg(:);	mod_time = model.time(:);
			obs_1(1,:) = observations.residual(:);	mod_1(1,:) = model.residual(:);
			obs_2 = NaN*obs_time;			mod_2 = NaN*mod_time;
			obs_3 = NaN*obs_time;			mod_3 = NaN*mod_time;
			

		case 'CURRENT'
			obs_time = observations.time_reg(:);	mod_time = model.time(:);
			for l=1:nlevels
				obs_1(l,:) = real(observations(l).residual(:));	mod_1(l,:) = real(model(l).residual(:));
				obs_2(l,:) = imag(observations(l).residual(:));	mod_2(l,:) = imag(model(l).residual(:));
				obs_3(l,:) = obs_1(l,:).*obs_1(l,:) + obs_2(l,:).*obs_2(l,:);
				mod_3(l,:) = mod_1(l,:).*mod_1(l,:) + mod_2(l,:).*mod_2(l,:);
			end

		case 'MCTD'
			obs_time = observations.time_reg(:);	mod_time = model.time(:);
			obs_1(1,:) = observations.ct(:);	mod_1(1,:) = model.ct(:);
			obs_2(1,:) = observations.sa(:);	mod_2(1,:) = model.sa(:);
			obs_3(1,:) = observations.rho(:);	mod_3(1,:) = model.rho(:);
	
		case 'SST'
			obs_time = observations.time(:);	mod_time = model.time(:);
			obs_1(1,:) = observations.t(:);		mod_1(1,:) = model.t(:);
			obs_2 = NaN*obs_time;			mod_2 = NaN*mod_time;
			obs_3 = NaN*obs_time;			mod_3 = NaN*mod_time;

		case default
			display (['Unrecognized instrument: ' instrument ])
			stop
	end
		



	% [ 1 ]  PREPARE THE TIME SERIES.  Prepare isn't quite r ight - should have regularized / windowed / whatever the time series already before you get here.
	% also you're going to read in all the statistics from the individual file - no sense in duplicating things.


	for l=1:nlevels

		% Calculate how much of the observations are present (ie, how much missing data is there?)
		[npts,~] = size (obs_1);	[nnans,~] = size (find( (isnan(obs_1) == 1) ));	pcmissing = 100.0*nnans / npts;

		% Frequency spectra of panel 3 or 1 (if 3 is just NaNs)
	
		a = [1./240, 1./24, 1./12, 1./6, 1./3];
		TL = {'240', '24', '12', '6', '3'};

		if (plot_spectra == 1)
			if (max(max(obs_3)) == NaN)	% use the first panel
				win = round(length(obs_1)/8);	[fo,po] = spec1(win,obs_1);
				win = round(length(mod_1)/8);	[fm,pm] = spec1(win,mod_1);
			else
				win = round(length(obs_3)/8);	[fo,po] = spec1(win,obs_3);
				win = round(length(mod_3)/8);	[fm,pm] = spec1(win,mod_3);
			end

		else
			fo = NaN*ones(length(obs_1)/8); po = NaN*ones(length(obs_1)/8);
			fm = NaN*ones(length(mod_1)/8); pm = NaN*ones(length(mod_1)/8);
		end
		

		% [ 2 ] PLOT!!!  Unhardwire as much of this as possible with a config file


		fig = figure;

		% First the maps
		subplot (spy,spx,spm1)
	        pcolor (lon,lat,land); shading flat; colormap(clr_land);   hold on;        % coastline
		xlim (map_xlim);	ylim (map_ylim);

		[xm ym xd yd] = f_mapzoom(olon,olat,2);
		h0 = scatter(xd,yd, sz_scatter/4, clr_zoom, '.');
	        h1 = scatter (olon,olat, sz_scatter, clr_obs, 'filled');
	        h2 = scatter (mlon,mlat, sz_scatter, clr_mod, 'filled');

		if (inc_legend == 1);	legend ([h1 h2], leg_1, leg_2, 'Location', leg_locn);	end
		curax = gca;		f_fmtaxes (curax, '', '', 0, 0, sz_font, tk, -0.04)
	        %set(gca, 'TickLength', [0.025 0.025], 'layer','top', 'Linewidth', 1.5, 'FontSize', sz);
		%pos = get(gca, 'Position');	pos(1) = pos(1) - 0.04;	set(gca, 'Position', pos)

		subplot (spy,spx,spm2)
        	pcolor (lon,lat,land); shading flat; colormap(clr_land);   hold on;        % coastline
	        h1 = scatter (olon,olat, sz_scatter, clr_obs, 'filled');
	        h2 = scatter (mlon,mlat, sz_scatter, clr_mod, 'filled');
		curax = gca;		f_fmtaxes (curax, '', '', 0, 0, sz_font, tk, -0.04)
        	xlim (xm); ylim (ym);
	        %set(gca, 'TickLength', [0.025 0.025], 'layer','top', 'Linewidth', 1.5, 'FontSize', sz);
		%pos = get(gca, 'Position');	pos(1) = pos(1) - 0.04;	set(gca, 'Position', pos)



	
		% Now thetime series

		subplot (spy,spx, spp1)
		plot (obs_time, obs_1(l,:), 'Color', clr_obs, 'Linewidth', lw); hold on
		plot (mod_time, mod_1(l,:), 'Color', clr_mod, 'Linewidth', lw);
		curax = gca;		f_fmtaxes (curax, xax{1,1}, yax{1,1}, 1, 1, sz_font, tk, +0.04)	% need to adjust datetime format automatically?

		%datetick ('x', 'mmm'); axis tight; ylabel (yax{1,1}); xlabel (xax{1,1});
		%set (gca, 'Linewidth', 2, 'FontSize', 16, 'TickLength', [0.025 0.025]);
		%pos = get(gca, 'Position');	pos(1) = pos(1) + 0.04;	set(gca, 'Position', pos)
		% %pos = get(gca, 'Position');	pos(3:4) = 0.8*pos(3:4);	set(gca, 'Position', pos)
		%grid on;

		if (npanels == 4)
			subplot (spy,spx,spp2)
			plot (obs_time, obs_2(l,:), 'Color', clr_obs, 'Linewidth', lw); hold on
			plot (mod_time, mod_2(l,:), 'Color', clr_mod, 'Linewidth', lw);
			curax = gca;		f_fmtaxes (curax, xax{1,2}, yax{1,2}, 1, 1, sz_font, tk, +0.04);
			%datetick ('x', 'mmm'); axis tight; ylabel (yax{1,2}); xlabel (xax{1,2});
			%set (gca, 'Linewidth', 2, 'FontSize', 16, 'TickLength', [0.025 0.025]);
			%pos = get(gca, 'Position');	pos(1) = pos(1) + 0.04;	set(gca, 'Position', pos)

			subplot (spy,spx,spp3)
			plot (obs_time, obs_3(l,:), 'Color', clr_obs, 'Linewidth', lw); hold on
			plot (mod_time, mod_3(l,:), 'Color', clr_mod, 'Linewidth', lw);
			curax = gca;		f_fmtaxes (curax, xax{1,3}, yax{1,3}, 1, 1, sz_font, tk, +0.04);
			%datetick ('x', 'mmm'); axis tight; ylabel (yax{1,3}); xlabel (xax{1,3});
			%set (gca, 'Linewidth', 2, 'FontSize', 16, 'TickLength', [0.025 0.025]);
			%pos = get(gca, 'Position');	pos(1) = pos(1) + 0.04;	set(gca, 'Position', pos)
		end

	subplot (spy,spx,spp4)
	plot(log(fo), po, 'Color', clr_obs, 'LineWidth', lw); hold on;		% hacky semilog plot so can have only specified ticks
	plot(log(fm), pm, 'Color', clr_mod, 'LineWidth', lw);
	curax = gca;		f_fmtaxes (curax, xax{1,4}, yax{1,4}, 0, 1, sz_font, tk, +0.04);
	set (gca, 'xtick', log(a), 'xticklabel', TL);
	%ylabel (yax{1,4}); xlabel (xax{1,4});
	%xlim([0 0.2]); %ylim ([0 8.0e-4]);
	%set (gca, 'Linewidth', 2, 'FontSize', 16, 'TickLength', [0.025 0.025]);
	%pos = get(gca, 'Position');	pos(1) = pos(1) + 0.04;	set(gca, 'Position', pos)
	%grid on;


	% Title and annotations with info about data / statistics
	%str_title = [station '   Run ID: ' runid '  ]
	%annotation('textbox', [0 0.9 1 0.1], 'String', str_title, 'EdgeColor','none', 'HorizontalAlignment','center', 'FontSize',20 )
	f_titleannot ([station '   Run ID: ' runid ], 20)
	


	% PRINT!


	if (exist('depth_string','var') == 1)	
		pngname = [out_filename depth_string '_res-spec.png'];
	else
		pngname = [out_filename '_res-spec.png'];
	end
	curfig = gcf;	%pngname = [out_filename depth_string '_res-spec.png']
	f_figprint (curfig, pngname, width, height, resolution)
	%set(gcf,'paperunits','inches'); set(gcf,'paperposition',[0 0 width height]./resolution);
	%print('-dpng', ['-r' num2str(resolution)], '-zbuffer', [out_filename depth_string '_res-spec']);
	%pause(3);	
	close(fig);






	% Clear some variables
	clear obs_resid  obs_resid_i


	end % level loop

	display (['[ ' num2str(s) ' ] Station ' station ' complete.']  )


end
