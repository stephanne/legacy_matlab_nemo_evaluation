
% Configuration info for plotting in general (not data - specific)

% Scatter plots
sz_scatter  = 50;	sz_font = 16;	sz_title = 20;

% Colours
clr_obs   = [0.2 0.2 0.2];
clr_mod   = [1 0 0];
clr_hc    = [1 0 1];
clr_riops = [0 1 1];

clr_wt    = [0 0.5 0.25];
clr_cmc   = [0 0.5 0.5];
clr_zoom  = [0 0.5 0.25];	% same as webtide?

clr_land  = [0.7 0.7 0.7];

% Linewidth
lw = 2.0;	% default linewidth
tk = 0.025;	% tick length


% Maps and zoom box
sz_zoom = 2.0;	% number of degrees +/- an observation that zoomed-in map map covers
map_xlim = [-142 -120];		map_ylim = [44 60];

% Land mask
bathy_file = '/space/hall0/sitestore/dfo/odis/stt001/PACKAGE_MERGING/ETC/REF/Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc';
bathy = double(ncread(bathy_file, 'Bathymetry'));
lat   = double(ncread(bathy_file, 'nav_lat'));
lon   = double(ncread(bathy_file, 'nav_lon'));

land = bathy; land(land>0)=NaN; land(land==0)=1;


% Figure printing parameters
resolution = 300;
inc_legend = 1;
