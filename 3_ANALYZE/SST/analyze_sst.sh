#!/bin/bash

# First attempt at a robust generation script for multiple source plots.
# This is necessarily clunky - don't put a ton of effort into making it sleek beofre translating to python. (May be able to use a .py file rather than .sh?)



set -a; source ../../config.ksh; set +a

CONFIG='NEP36';       RUN_ID='DI02d';	UMBRELLA=${REF_DIR_UMBRELLA}

# INPUT FILES

MODEL_SOURCES="{'CIOPS-W', 'HINDCAST', 'RIOPS'}"	# observations are included with each model source processing file - no need to specify explicitly.
REFERENCE_SOURCES="{'CMC-SST'}"		# things like WebTide, CMC_SST, etc  - not necessarily generated in quite the same way

# Only one option for each of the possible sources - can generalize later
SWITCH_CIOPS=1;		DIR_CIOPS=${UMBRELLA}'/BUOY';		ROOT_CIOPS='BUOY_NEP36-DI02d_BUOY';	# needs the trailing BUOY but chop it eventually
SWITCH_HINDCAST=1;	DIR_HINDCAST=${UMBRELLA}'/BUOY';	ROOT_HINDCAST='BUOY_NEP36-OPM224_BUOY';
SWITCH_RIOPS=0;		DIR_RIOPS=${UMBRELLA}'/BUOY';		ROOT_RIOPS='BUOY_RIOPS-rx021_BUOY';
SWITCH_CMCSST=0;	DIR_CMCSST=${UMBRELLA}'/BUOY';		ROOT_CMCSST='BUOY_CMCSST';


# OUTPUT FILES / WHAT NEEDS TO BE RUN?
DIR_OUTPUT=${UMBRELLA}'/SST';		ROOT_OUTPUT='SST_NEP36-DI02d'

SWITCH_STATISTICS=1;	ROOT_STATISTICS='SST_STATS_'$CONFIG'-'$RUN_ID;
SWITCH_SERIES=1;	ROOT_SERIES='SST_PLOT-SERIES_'$CONFIG'-'$RUN_ID;	FILE_CONFIG_SERIES='config_plot_series'
SWITCH_MAP=1;		ROOT_MAP='SST_PLOT-MAP_'$CONFIG'-'$RUN_ID;		FILE_CONFIG_MAP='config_plot_map'


# REFERENCE DATA
TYPE_ANALYSIS='SST'
FILE_CONFIG_REFERENCE='config_plot_reference';		# File with the relatively static parameters (colour codes, font sizes, etc etc)
#FILE_CONFIG='config_plot_sst';			# File with specifics of what variables to plot etc for a specific run of this script
DATE_START='20160101';  DATE_FINAL='20161231';	# Override window - makes it easy to chop a specific subsection of larger analysis

# ADDITIONAL METADATA
NOTE='surface_buoys'            # string for any relevant information
USER=`whoami`                           # attribution string


# TEMPLATE FILES
TEMPLATE_STATISTICS=${PKG_LOCATION}/${DIR_LIB}'/template_statistics.m'
TEMPLATE_PLOT_SERIES=${PKG_LOCATION}/${DIR_LIB}'/template_plot_series.m'
TEMPLATE_PLOT_MAP=${PKG_LOCATION}/${DIR_LIB}'/template_plot_map.m'

# LOCAL SCRIPT ROOTS
SCRIPT_ROOT=${PKG_LOCATION}'/3_ANALYZE/SST/scr_'${CONFIG}_${RUN_ID}
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [ 0 ] HOUSEKEEPING.  Prepare variables, ensure all directories present, etc

if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p ${DIR_OUTPUT}; fi

SED_DIR_CIOPS=$(printf "%s\n" "$DIR_CIOPS" | sed 's/[\&/]/\\&/g'); 		SED_ROOT_CIOPS=$(printf "%s\n" "$ROOT_CIOPS" | sed 's/[\&/]/\\&/g');
SED_DIR_RIOPS=$(printf "%s\n" "$DIR_RIOPS" | sed 's/[\&/]/\\&/g'); 		SED_ROOT_RIOPS=$(printf "%s\n" "$ROOT_RIOPS" | sed 's/[\&/]/\\&/g');
SED_DIR_HINDCAST=$(printf "%s\n" "$DIR_HINDCAST" | sed 's/[\&/]/\\&/g'); 	SED_ROOT_HINDCAST=$(printf "%s\n" "$ROOT_HINDCAST" | sed 's/[\&/]/\\&/g');
SED_DIR_CMCSST=$(printf "%s\n" "$DIR_CMCSST" | sed 's/[\&/]/\\&/g'); 		SED_ROOT_CMCSST=$(printf "%s\n" "$ROOT_CMCSST" | sed 's/[\&/]/\\&/g');
SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g'); 		SED_ROOT_OUTPUT=$(printf "%s\n" "$ROOT_OUTPUT" | sed 's/[\&/]/\\&/g');

SED_ROOT_STATISTICS=$(printf "%s\n" "$ROOT_STATISTICS" | sed 's/[\&/]/\\&/g')
SED_ROOT_SERIES=$(printf "%s\n" "$ROOT_SERIES" | sed 's/[\&/]/\\&/g')
SED_ROOT_MAP=$(printf "%s\n" "$ROOT_MAP" | sed 's/[\&/]/\\&/g')

SED_FILE_CONFIG_REFERENCE=$(printf "%s\n" "$FILE_CONFIG_REFERENCE" | sed 's/[\&/]/\\&/g')
SED_FILE_CONFIG_SERIES=$(printf "%s\n" "$FILE_CONFIG_SERIES" | sed 's/[\&/]/\\&/g')
SED_FILE_CONFIG_MAP=$(printf "%s\n" "$FILE_CONFIG_MAP" | sed 's/[\&/]/\\&/g')

SED_REF_PATH_FXNS=$(printf "%s\n" "$REF_PATH_FXNS" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_BATHY=$(printf "%s\n" "$REF_FILE_BATHY" | sed 's/[\&/]/\\&/g')



# [ 1 ] STATISTICS

if [[ ${SWITCH_STATISTICS} == 1 ]] ; then;

	SCRIPT=$SCRIPT_ROOT'_STATS.m'
	cp $TEMPLATE_STATISTICS $SCRIPT

	# Swap in the info 
	sed -i "s/VAR_PATH_FXNS/$SED_PATH_FXNS/g" $SCRIPT

	sed -i "s/VAR_FILE_CONFIG/$SED_FILE_CONFIG/g" $SCRIPT
	sed -i "s/VAR_FILE_REFERENCE/$SED_FILE_REFERENCE/g" $SCRIPT

	sed -i "s/VAR_DIR_CIOPS/$SED_DIR_CIOPS/g" $SCRIPT
	sed -i "s/VAR_DIR_RIOPS/$SED_DIR_RIOPS/g" $SCRIPT
	sed -i "s/VAR_DIR_HINDCAST/$SED_DIR_HINDCAST/g" $SCRIPT
	sed -i "s/VAR_DIR_REF/$SED_DIR_CMCSST/g" $SCRIPT
	sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT

	sed -i "s/VAR_ROOT_CIOPS/$SED_ROOT_CIOPS/g" $SCRIPT
	sed -i "s/VAR_ROOT_RIOPS/$SED_ROOT_RIOPS/g" $SCRIPT
	sed -i "s/VAR_ROOT_HINDCAST/$SED_ROOT_HINDCAST/g" $SCRIPT
	sed -i "s/VAR_ROOT_REF/$SED_ROOT_CMCSST/g" $SCRIPT
	sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_STATISTICS/g" $SCRIPT

	sed -i "s/VAR_SWITCH_CIOPS/$SWITCH_CIOPS/g" $SCRIPT
	sed -i "s/VAR_SWITCH_RIOPS/$SWITCH_RIOPS/g" $SCRIPT
	sed -i "s/VAR_SWITCH_HINDCAST/$SWITCH_HINDCAST/g" $SCRIPT
	sed -i "s/VAR_SWITCH_REF/$SWITCH_CMCSST/g" $SCRIPT

	sed -i "s/VAR_TYPE_ANALYSIS/$TYPE_ANALYSIS/g" $SCRIPT
	#sed -i "s/VAR_/$/g" $SCRIPT
fi


# [ 2 ] PLOTS

if [[ ${SWITCH_SERIES} == 1 ]]; then

	SCRIPT=$SCRIPT_ROOT'_SERIES.m'
	cp $TEMPLATE_PLOT_SERIES $SCRIPT

	sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT
	sed -i "s/VAR_DIR_INPUT/$SED_DIR_CIOPS/g" $SCRIPT
	sed -i "s/VAR_ROOT_INPUT/$SED_ROOT_CIOPS/g" $SCRIPT
	sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT

	sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT
	sed -i "s/VAR_FILE_CONFIG_REF/$SED_FILE_CONFIG_REFERENCE/g" $SCRIPT
	sed -i "s/VAR_FILE_CONFIG_SPECIFIC/$SED_FILE_CONFIG_SERIES/g" $SCRIPT
	sed -i "s/VAR_TYPE_ANALYSIS/$TYPE_ANALYSIS/g" $SCRIPT

	sed -i "s/VAR_DATE_START/$DATE_START/g" $SCRIPT
	sed -i "s/VAR_DATE_FINAL/$DATE_FINAL/g" $SCRIPT
fi


if [[ ${SWITCH_MAP} == 1 ]]; then

	SCRIPT=$SCRIPT_ROOT'_MAP.m'
	cp $TEMPLATE_PLOT_MAP $SCRIPT

	sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT
	sed -i "s/VAR_DIR_INPUT/$SED_DIR_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_ROOT_INPUT/$SED_ROOT_STATISTICS/g" $SCRIPT
	sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT

	sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT
	sed -i "s/VAR_FILE_CONFIG_REF/$SED_FILE_CONFIG_REFERENCE/g" $SCRIPT
	sed -i "s/VAR_FILE_CONFIG_SPECIFIC/$SED_FILE_CONFIG_MAP/g" $SCRIPT
	sed -i "s/VAR_TYPE_ANALYSIS/$TYPE_ANALYSIS/g" $SCRIPT

	sed -i "s/VAR_DATE_START/$DATE_START/g" $SCRIPT
	sed -i "s/VAR_DATE_FINAL/$DATE_FINAL/g" $SCRIPT
fi


