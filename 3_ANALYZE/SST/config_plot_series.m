
% Plotting variables specifically for SST plots.


npanels = 2;	str_sp = [211, 000, 000, 212];          % strings labelling the subplots
spx = 6;        spy = 2;
spm1 = [1,2];	spm2 = [7,8];	spp1 = [3,6];	spp2=0;	spp3=0;	spp4 = [9,12];
xax = {'Time', [],[], 'Period (h)'};
yax = {'In-situ Temp.', [],[], 'Power'};
width = 3200;	height = 2400; 

leg_1 = 'Obs.';
leg_2 = 'Model';
leg_locn = 'northeast';

nlevels = 1;

plot_spectra = 0;
