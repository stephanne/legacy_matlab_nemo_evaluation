
% Parameters for map output

nlevels=1;

nvars = 3;
varnames = {'bias', 'rmse', 'skill'}
vartitles = {'Mean Bias', 'RMSE', 'Skill'}

nclrs = [10,10,10];
clr_bar = {'balance', 'matter', 'matter'};
cax = {[-1 1], [0 1], [0.75,1]};
ctk = {[-1:0.2:1], [0:0.1:1.0], [0.75:0.025:1.0]};


inds_nan = [];
%inds_nan = [6,7,8,21,23,24,27,29,36,40];	% any stations that need to be nan'd out due to lack of data etc

xm = [-129 -122]; ym = [47.5 51];	% Salish Sea zoom
xm = [-140 -122]; ym = [44 60];		% Salish Sea zoom


width = 3200; height = 3200; resolution = 300;

land(land==1) = 0;

