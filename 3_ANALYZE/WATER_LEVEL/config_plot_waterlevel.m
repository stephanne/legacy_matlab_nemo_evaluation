
% Parameters for the water level constituent plots

TC = {'Q1', 'O1', 'P1', 'K1', 'N2', 'M2', 'S2', 'K2'};

switch_errbar_obs = 0;
switch_errbar_mod = 1;

nlines = 3;	% number of sources of constituent data you want to plot
clrs = [clr_obs; clr_mod; clr_wt];



