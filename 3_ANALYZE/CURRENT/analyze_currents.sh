#!/bin/bash

# PROCESS ADCP DATA
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: September 6, 2018.

# USED WITH: a bunch of things 
# REQUIRES : MATLAB 2013+, t_tide
# PURPOSE  : Calculates tidal constituents at a series of points.

# INPUT : Extracted subdomains (one per mooring), observational data (one file per mooring), and local bathymetry. Observation
#         data is a netcdf file, with a particular structure - see documentation for details.  Observations may need 
#         preprocessing.

# OUTPUT: .mat file with constituents as well as tidal, residual and mean water level for both model and observations.  Summary stats as well

set -a; source ../../config.ksh; set +a

CONFIG='NEP36';       RUN_ID='DI02d'

# INPUT INFO

DIR_OBS_ADCP=${REF_DIR_UMBRELLA}'/DATA/ADCP';	FILELIST_OBS_ADCP='filelist_adcp.txt'
DIR_OBS_CURM=${REF_DIR_UMBRELLA}'/DATA/CURM';	FILELIST_OBS_CURM='filelist_cm.txt'

DIR_MOD_ADCP=${REF_DIR_UMBRELLA}'/DATA/ADCP';	FILELIST_MOD_ADCP='filelist_adcp.txt'
DIR_MOD_CURM=${REF_DIR_UMBRELLA}'/DATA/CURM';	FILELIST_MOD_CURM='filelist_cm.txt'

DIR_BATHY=${REF_DIR_UMBRELLA}'/BATHY';			ROOT_BATHY='bathymetry_nep36'

# OUTPUT INFO

DIR_OUTPUT='';		ROOT_OUTPUT=''


# SWITCHES
SWITCH_ELLIPSES=1;	SWITCH_SERIES=1;

# Parameters
CONSTITUENTS="${REF_CONSTITUENTS}"
DATE_START='20160101';	DATE_FINAL='20161231'


# Metadata
NOTE='standard levels'		# string for any relevant information
USER=`whoami`				# attribution string


# Template files
TEMPLATE_SCRIPT_SERIES=${PKG_LOCATION}/${DIR_LIB}'/template_plot_series.m'
TEMPLATE_SCRIPT_ELLIPSES=${PKG_LOCATION}/${DIR_LIB}'/template_plot_ellipses.m'

# Local script roots
SCRIPT_ROOT=${PKG_LOCATION}'/2_ANALYSIS/ADCP/scr_dec4_dood_'${CONFIG}_${RUN_ID}

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [ 0 ] HOUSEKEEPING

if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p ${DIR_OUTPUT}; fi

SED_DIR_OBS_ADCP=$(printf "%s\n" "$DIR_OBS_ADCP" | sed 's/[\&/]/\\&/g')
SED_DIR_OBS_CURM=$(printf "%s\n" "$DIR_OBS_CURM" | sed 's/[\&/]/\\&/g')
SED_DIR_MOD_ADCP=$(printf "%s\n" "$DIR_MOD_ADCP" | sed 's/[\&/]/\\&/g')
SED_DIR_MOD_CURM=$(printf "%s\n" "$DIR_MOD_CURM" | sed 's/[\&/]/\\&/g')

SED_DIR_BATHY=$(printf "%s\n" "$DIR_BATHY" | sed 's/[\&/]/\\&/g')

SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_REF_FILE_BATHY=$(printf "%s\n" "$REF_FILE_BATHY" | sed 's/[\&/]/\\&/g')
SED_REF_PATH_FXNS=$(printf "%s\n" "$REF_PATH_FXNS" | sed 's/[\&/]/\\&/g')

SED_FILE_INTERIM=$(printf "%s\n" "$FILE_INTERIM" | sed 's/[\&/]/\\&/g')

#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')


NOTE=${NOTE// /_}

# [ 1 ] TIDAL ELLIPSES

if [[ ${SWITCH_ELLIPSES} == 1 ]] ; then

	# [ 4.1 ] Tidal ellipses

	SCRIPT=$SCRIPT_ROOT'_PELL.m'
	cp $TEMPLATE_SCRIPT_PLTELL $SCRIPT

	# Adjust the script files
	sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

	sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_ROOT_OUTPUT/${SED_ROOT_OUTPUT}_ADCP/g" $SCRIPT

	sed -i "s/VAR_TIDAL_CONSTITUENTS/$CONSTITUENTS/g" $SCRIPT
	sed -i "s/VAR_GAP/$REF_PLT_ELLGAP/g" $SCRIPT

	sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT
        sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT


fi

if [[ ${SWITCH_SERIES} == 1 ]]; then

	# [ 4.2 ] Total and residual time series, spectra, and basic statistics  (maybe split out the stats package?)

	SCRIPT=$SCRIPT_ROOT'_PSTS.m'
	cp $TEMPLATE_SCRIPT_PLTSTAT $SCRIPT

	# Adjust the script files
	sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

	sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_ROOT_OUTPUT/${SED_ROOT_OUTPUT}_ADCP/g" $SCRIPT
	sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT

	sed -i "s/VAR_TIDAL_CONSTITUENTS/$CONSTITUENTS/g" $SCRIPT
	sed -i "s/VAR_NHOURS/$REF_PLT_NHOURS/g" $SCRIPT

	sed -i "s/VAR_DATE_START/$DATE_START/g" $SCRIPT
	sed -i "s/VAR_DATE_FINAL/$DATE_FINAL/g" $SCRIPT

	sed -i "s/VAR_INTERVAL_OBS/$OBS_INTERP_INTERVAL/g" $SCRIPT
	sed -i "s/VAR_INTERVAL_MODEL/$MOD_INTERVAL/g" $SCRIPT

        sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT


fi


