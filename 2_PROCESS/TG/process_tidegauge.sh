#!/bin/bash

# PROCESS TIDE GAUGES
# One script from model output files to analysis.
# S Taylor, based on routine from Simon Higginson. stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: July 26, 2018.

# USED WITH: template_tidegauge.sh
# REQUIRES : MATLAB 2013+
# PURPOSE  : Calculates tidal constituents at a series of points.

# INPUT : Extracted subdomains (one per tide gauge), as well as tide gauge data itself. 
# OUTPUT: .mat file with constituents as well as tidal, residual and mean water level.

set -a; source ../../config.ksh; set +a

CONFIG=${REF_CONFIG};       RUN_ID='DI02c';	#${REF_RUN_ID}
SWITCH_SUBMIT=false 

# [ 0 ] PRELIMINARY STEPS
#             Extract the station data and compile a time series
#             Then ready to do tide gauge anaylsis
SWITCH_GETPOINTS=0;	SWITCH_EXTRACT=0;	SWITCH_TIDEGAUGE=1;
SWITCH_RMINTERIM=0		# if 0, delete the extracted and series files generated as a result of this.

DIR_UMBRELLA=${REF_DIR_UMBRELLA}

DIR_MOD_DATA=${DIR_UMBRELLA}/DATA/DI02c		# Model data
DIR_OBS_DATA=${DIR_UMBRELLA}/DATA/TG		# Observation (ie tidegauge) data
DIR_SERIES=${DIR_UMBRELLA}/SER;	
DIR_WORK=${DIR_UMBRELLA}/WORK_A_${RUN_ID}; 
DIR_COORD=${DIR_UMBRELLA}/COORD;
DIR_IB=${DIR_UMBRELLA}/IB;			ROOT_IB='TG_NOSASSA_SNR0_NEP36-OPM224_IB';

FILE_INTERIM=${DIR_WORK}'/interimfile_'${RUN_ID}'.nc'


# [ 1 ] TIDE GAUGE ANALYSIS

# Input files

FILE_TG_DATA_INFO='filelist_tidegauge.txt'		# List of filenames with tide gauge data (and some related info).  Used for TG script only
FILE_STATION_INFO='info_tidegauge_adjusted.csv'			# Configuration file generated with the station definition script


# Output files
DIR_OUTPUT=${DIR_UMBRELLA}'/TESTING_TG'; 	ROOT_OUTPUT='TG_SUSAN_'${CONFIG}-${RUN_ID}

# Window to concatenate
START_YEAR='2016';      START_MONTH='01';       START_DAY='01';
FINAL_YEAR='2016';      FINAL_MONTH='12';       FINAL_DAY='31';

# Variables to extract / include in time series
VARS_T2D='zos ssh_ib';		VARS_U2D='';		VARS_V2D=''
DELIMITER='grid_T_2D'		# string used to ensure only one set of files is grab to concatenate - not patchs from all three grids.
				# also used in the tide gauge script, so make sure it's the one with the ssh data in it.

# Parameters
DATE_START='20160101';	DATE_FINAL='20161231'			# not really necessary - can compile from window variables
INTERVAL_MIN=60;	INTERVAL_HR=1		INTERVAL_STR='1h'
SWITCH_USEFILENAME=0	# if 1, use the observation fileanems to label the extracted 3x3 patches
SWITCH_REMOVE_IB=1	# if 1, remove the inverse barometer from both model and observation time series
SWITCH_EXTERNAL_IB=1	# if 1, use an external file to remove the IB from observations (rather than the model IB) 
OBS_INTERP_INTERVAL='0.5';			MOD_INTERVAL='1'
RAYLEIGH=-1;		SIGNAL_TO_NOISE=0;	# parameters for tidal analysis
SWITCH_APPLY_FILTERS=1				# if 1, apply 3 butterworth filters to filter out diurnal, semi-diurnal, and M3-ish frequencies.
DICTIONARY='d_tidegauge'

# Variables
VAR_SSH='zos';	VAR_TIME='time_counter';	VAR_LAT='nav_lat';	VAR_IB='ssh_ib'
COORD_X=2;	COORD_Y=2;	# Matlab is 1-indexed, and 3x3 patchs are extracted


# Metadata
NOTE='IB correction made'				# string for any relevant information
USER=`whoami`				# attribution string


# Template files
TEMPLATE_SCRIPT_STN=${PKG_LOCATION}/${DIR_LIB}'/template_station_newest.py'
TEMPLATE_SCRIPT_EXT=${PKG_LOCATION}/${DIR_LIB}'/template_extract_ncks.sh'
TEMPLATE_SCRIPT_TG=${PKG_LOCATION}/${DIR_LIB}'/template_process_tidegauge.m'

TEMPLATE_SUBMIT=${PKG_LOCATION}/${DIR_LIB}/${REF_FILE_JOBSUB}

# Local script roots
SCRIPT_ROOT=${PKG_LOCATION}'/2_PROCESS/TG/scr_susan_'${CONFIG}_${RUN_ID}_R1_SNR0
SUBMIT_ROOT=${PKG_LOCATION}'/2_PROCESS/TG/sub_susan_'${CONFIG}_${RUN_ID}_R1_SNR0
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p ${DIR_OUTPUT}; fi
if [ ! -d $DIR_SERIES ] ; then; mkdir -p ${DIR_SERIES}; fi
if [ ! -d $DIR_WORK   ] ; then; mkdir -p ${DIR_WORK}; fi


# [ 0 ]: Prepare variables.

SED_DIR_SOURCE_DATA=$(printf "%s\n" "$DIR_MOD_DATA" | sed 's/[\&/]/\\&/g')

SED_FILE_INTERIM=$(printf "%s\n" "$FILE_INTERIM" | sed 's/[\&/]/\\&/g')

SED_DIR_OBS_DATA=$(printf "%s\n" "$DIR_OBS_DATA" | sed 's/[\&/]/\\&/g')
SED_DIR_MOD_DATA=$(printf "%s\n" "$DIR_MOD_DATA" | sed 's/[\&/]/\\&/g')
SED_DIR_COORD=$(printf "%s\n" "$DIR_COORD" | sed 's/[\&/]/\\&/g')
SED_DIR_SERIES=$(printf "%s\n" "$DIR_SERIES" | sed 's/[\&/]/\\&/g')
SED_DIR_WORK=$(printf "%s\n" "$DIR_WORK" | sed 's/[\&/]/\\&/g')

SED_DIR_IB=$(printf "%s\n" "$DIR_IB" | sed 's/[\&/]/\\&/g')
SED_ROOT_IB=$(printf "%s\n" "$ROOT_IB" | sed 's/[\&/]/\\&/g')

SED_REF_FILE_COORD=$(printf "%s\n" "$REF_FILE_COORD" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_BATHY=$(printf "%s\n" "$REF_FILE_BATHY" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_MASK=$(printf "%s\n" "$REF_FILE_MASK" | sed 's/[\&/]/\\&/g')
SED_REF_PATH_FXNS=$(printf "%s\n" "$REF_PATH_FXNS" | sed 's/[\&/]/\\&/g')

SED_FILE_STATION_INFO=$(printf "%s\n" "$FILE_STATION_INFO" | sed 's/[\&/]/\\&/g')
SED_FILE_TG_DATA_INFO=$(printf "%s\n" "$FILE_TG_DATA_INFO" | sed 's/[\&/]/\\&/g')

SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g')
SED_ROOT_OUTPUT=$(printf "%s\n" "$ROOT_OUTPUT" | sed 's/[\&/]/\\&/g')
#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')

# [ 1 ] Set up the submission file

SUBMIT=$SUBMIT_ROOT'_TG.job'
cp $TEMPLATE_SUBMIT $SUBMIT  

JOB_NAME=TG_${RUN_ID}

sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

echo 'cd ' `pwd` >> $SUBMIT



# [ 1 ] If requested, determine what points need to be extracted

if [[ ${SWITCH_GETPOINTS} == 1 ]]; then
        SCRIPT=$SCRIPT_ROOT'_STN.py'
        cp $TEMPLATE_SCRIPT_STN $SCRIPT

        # Swap in the info 
        sed -i "s/VAR_FILELIST/$SED_FILE_TG_DATA_INFO/g" $SCRIPT
        sed -i "s/VAR_DIR_OBS/$SED_DIR_OBS_DATA/g" $SCRIPT

        sed -i "s/VAR_FILE_COORD/$SED_REF_FILE_COORD/g" $SCRIPT
        sed -i "s/VAR_FILE_LMASK/$SED_REF_FILE_MASK/g" $SCRIPT
        sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT

        sed -i "s/VAR_SWITCH_USEFILENAME/$SWITCH_USEFILENAME/g" $SCRIPT
        sed -i "s/VAR_STR_INSTRUMENT/$INSTRUMENT/g" $SCRIPT

        sed -i "s/VAR_OUTPUT/$SED_FILE_STATION_INFO/g" $SCRIPT

        sed -i "s/VAR_DICTIONARY/$DICTIONARY/g" $SCRIPT

        sed -i "s/VAR_VAL_DX/$REF_DX/g" $SCRIPT

        python $SCRIPT
fi



# [ 1 ] EXTRACTIONs / CONCATENATION

if [[ ${SWITCH_EXTRACT} == 1 ]]; then

	SWITCH_SERIES=1

        SCRIPT=$SCRIPT_ROOT'_EXT.sh'
        cp $TEMPLATE_SCRIPT_EXT $SCRIPT

        # Swap in the variables into the extract / series script
        sed -i "s/VAR_CONFIG/$CONFIG/g" $SCRIPT
        sed -i "s/VAR_RUNID/$RUN_ID/g" $SCRIPT

        sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT;        sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT;      sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT
        sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT;        sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT;      sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT

        sed -i "s/VAR_STATION_LIST/$SED_FILE_STATION_INFO/g" $SCRIPT
        sed -i "s/VAR_SWITCH_USEFILENAME/$SWITCH_USEFILENAME/g" $SCRIPT

        sed -i "s/VAR_DIR_SOURCE/$SED_DIR_SOURCE_DATA/g" $SCRIPT
        sed -i "s/VAR_DIR_WORK/$SED_DIR_WORK/g" $SCRIPT
	sed -i "s/VAR_DIR_SERIES/$SED_DIR_SERIES/g" $SCRIPT

        sed -i "s/VAR_FILE_INTERIM/$SED_FILE_INTERIM/g" $SCRIPT

        sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

        sed -i "s/VAR_FILE_COORD/$SED_REF_FILE_COORD/g" $SCRIPT
        sed -i "s/VAR_DIR_COORD/$SED_DIR_COORD/g" $SCRIPT

        sed -i "s/VAR_GRID_T2D/$REF_GRID_T2D/g" $SCRIPT;        sed -i "s/VAR_GRID_U2D/$REF_GRID_U2D/g" $SCRIPT;        sed -i "s/VAR_GRID_V2D/$REF_GRID_V2D/g" $SCRIPT
        sed -i "s/VAR_GRID_T/$REF_GRID_T/g" $SCRIPT;            sed -i "s/VAR_GRID_U/$REF_GRID_U/g" $SCRIPT;            sed -i "s/VAR_GRID_V/$REF_GRID_V/g" $SCRIPT

        sed -i "s/VAR_VARS_T2D/$VARS_T2D/g" $SCRIPT;            sed -i "s/VAR_VARS_U2D/$VARS_U2D/g" $SCRIPT;            sed -i "s/VAR_VARS_V2D/$VARS_V2D/g" $SCRIPT
        sed -i "s/VAR_VARS_T/$VARS_T/g" $SCRIPT;                sed -i "s/VAR_VARS_U/$VARS_U/g" $SCRIPT;                sed -i "s/VAR_VARS_V/$VARS_V/g" $SCRIPT

        sed -i "s/VAR_END_OF_DAY/$REF_END_OF_DAY/g" $SCRIPT

        sed -i "s/VAR_SWITCH_SERIES/$SWITCH_SERIES/g" $SCRIPT
        sed -i "s/VAR_SWITCH_RMINTERIM/$SWITCH_RMINTERIM/g" $SCRIPT

        sed -i "s/VAR_DELIMITER/$DELIMITER/g" $SCRIPT
        sed -i "s/VAR_INTERVAL/$INTERVAL_STR/g" $SCRIPT
        sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT


        echo 'ksh '$SCRIPT >> $SUBMIT


fi






# [ 1 ]: List all the files that match in the input root, look for the specified dates, chop, split
# Copy and adjust each script file

if [[ ${SWITCH_TIDEGAUGE} == 1 ]]; then

	SCRIPT=$SCRIPT_ROOT'_TG.m'
	cp $TEMPLATE_SCRIPT_TG $SCRIPT


	# Adjust the script files
	sed -i "s/VAR_DIR_OBS_DATA/$SED_DIR_OBS_DATA/g" $SCRIPT
	sed -i "s/VAR_DIR_MOD_DATA/$SED_DIR_MOD_DATA/g" $SCRIPT
	sed -i "s/VAR_DIR_SERIES/$SED_DIR_SERIES/g" $SCRIPT
	sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_DIR_IB/$SED_DIR_IB/g" $SCRIPT
	sed -i "s/VAR_ROOT_IB/$SED_ROOT_IB/g" $SCRIPT

        sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT
	sed -i "s/VAR_CONFIG/$CONFIG/g" $SCRIPT
        sed -i "s/VAR_RUNID/$RUN_ID/g" $SCRIPT

	sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT
	sed -i "s/VAR_META_USER/$USER/g" $SCRIPT

	sed -i "s/VAR_FILE_INFO/$SED_FILE_STATION_INFO/g" $SCRIPT
	sed -i "s/VAR_CONSTITUENTS/$REF_CONSTITUENTS/g" $SCRIPT
	sed -i "s/VAR_DELIMITER/$DELIMITER/g" $SCRIPT

	sed -i "s/VAR_SWITCH_REMOVE_IB/$SWITCH_REMOVE_IB/g" $SCRIPT
	sed -i "s/VAR_SWITCH_EXTERNAL_IB/$SWITCH_EXTERNAL_IB/g" $SCRIPT

	sed -i "s/VAR_RAYLEIGH/$RAYLEIGH/g" $SCRIPT
	sed -i "s/VAR_SIGNAL_TO_NOISE/$SIGNAL_TO_NOISE/g" $SCRIPT
	sed -i "s/VAR_SWITCH_APPLY_FILTERS/$SWITCH_APPLY_FILTERS/g" $SCRIPT

	sed -i "s/VAR_DATE_START/$DATE_START/g" $SCRIPT
	sed -i "s/VAR_DATE_FINAL/$DATE_FINAL/g" $SCRIPT

	sed -i "s/VAR_INTERVAL_MIN/$INTERVAL_MIN/g" $SCRIPT
	sed -i "s/VAR_INTERVAL_HR/$INTERVAL_HR/g" $SCRIPT

	sed -i "s/VAR_COORD_X/$COORD_X/g" $SCRIPT
	sed -i "s/VAR_COORD_Y/$COORD_Y/g" $SCRIPT

	sed -i "s/VAR_VAR_SSH/$VAR_SSH/g" $SCRIPT
	sed -i "s/VAR_VAR_IB/$VAR_IB/g" $SCRIPT
	sed -i "s/VAR_VAR_TIME/$VAR_TIME/g" $SCRIPT
	sed -i "s/VAR_VAR_LAT/$VAR_LAT/g" $SCRIPT

	echo 'matlab < '${SCRIPT} >> $SUBMIT

fi







# [3] SET UP THE SUBMISSION FILES.



SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )
if [ $SWITCH_SUBMIT = 'true' ] ; then
	jobsub -c $CLUSTER $SUBMIT
fi

