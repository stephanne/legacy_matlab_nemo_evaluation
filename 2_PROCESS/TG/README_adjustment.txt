If regenerating the station file, two stations need to be manually adjusted after the file is built.
Both Patricia Bay and Henslung Cove get put on the wrong side of a peninsula: Patricia Bay get put
in the Salish Sea, and Henslung Cove does not see the open ocean directly as it does in real life 
(the channel in which it's placed is narrow and so closed in CIOPSW).
These are the two lines you should use:

Instead of: Patricia-Bay, TG-PATBAY-7277, -123.451500, 48.653600, 675, 288, 0.301343, 24.135830,
Use       : Patricia-Bay, TG-PATBAY-7277, -123.451500, 48.653600, 671, 288, 0.301343, 24.135830,

Instead of: Henslung-Cove, TG-HENSCV-9958, -133.002000, 54.191000, 279, 650, 3.642709, 25.000000,
Use       : Henslung-Cove, TG-HENSCV-9958, -133.002000, 54.191000, 276, 648, 3.642709, 25.000000,

