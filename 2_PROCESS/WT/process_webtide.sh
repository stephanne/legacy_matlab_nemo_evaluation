#!/bin/bash

# INTERPOLATE WEBTIDE DATA TO A DOMAIN
# S Taylor, based on script from JP Paquin. stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: July 10, 2018.

# USED WITH: template_webtide.sh
# REQUIRES : MATLAB 2013+, floodnan4_opa, opa_angle_2016
# PURPOSE  : Interpolates WebTide data to a different set of coordinates.

# INPUT : WebTide dataset, coordinate and bathymetry files for domain

# OUTPUT: .mat file for each constituent and all three variables (SSH, u ,v)
#         netcdf file with all constituents for each of the three variables.


CONFIG='NEP36';       RUN_ID='9l'
SWITCH_SUBMIT=false 

# Input files
DIR_WEBTIDE_DATA='~/sitestore/DATA/WebTide'
WEBTIDE_DATASET='ne_pac4'

DIR_DOMAIN_DATA='/space/hall0/sitestore/dfo/odis/jpp001/ECCC/NEP36/INITIALISATION_714x1020'
FILE_COORD='coordinates_NEP_714x1020.nc'
FILE_BATHY='Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc'


# Output files
DIR_OUTPUT='~/sitestore/PACKAGE_CIOPSW/3_ANALYZE/WT'
ROOT_OUTPUT='NEP36_9l_webtide_linear_noflood'
SWITCH_NETCDF=1;	SWITCH_MAT=1;	 HEADERLINES=3;		DELIMITER=' ';

# Parameters
CONSTITUENTS="'M2', 'N2', 'S2', 'O1', 'K1', 'K2', 'P1', 'Q1'"
SWITCH_TRANSPORT=0;

# Metadata
NOTE='initial test string'		# string for any relevant information
USER=`whoami`				# attribution string


# Template files
TEMPLATE_SCRIPT='/home/stt001/sitestore/PACKAGE_CIOPSW/3_ANALYZE/WT/template_webtide.m'
TEMPLATE_SUBMIT='/home/stt001/sitestore/PACKAGE_CIOPSW/3_ANALYZE/WT/template_submit.sh'

# Local script roots
SCRIPT_ROOT='/home/stt001/sitestore/PACKAGE_CIOPSW/3_ANALYZE/WT/scr_'${CONFIG}_${RUN_ID}
SUBMIT_ROOT='/home/stt001/sitestore/PACKAGE_CIOPSW/3_ANALYZE/WT/sub_'${CONFIG}_${RUN_ID}
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [0]: Prepare variables.

SED_DIR_WEBTIDE_DATA=$(printf "%s\n" "$DIR_WEBTIDE_DATA" | sed 's/[\&/]/\\&/g')
SED_DIR_DOMAIN_DATA=$(printf "%s\n" "$DIR_DOMAIN_DATA" | sed 's/[\&/]/\\&/g')

SED_FILE_COORD=$(printf "%s\n" "$FILE_COORD" | sed 's/[\&/]/\\&/g')
SED_FILE_BATHY=$(printf "%s\n" "$FILE_BATHY" | sed 's/[\&/]/\\&/g')

SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g')
SED_ROOT_OUTPUT=$(printf "%s\n" "$ROOT_OUTPUT" | sed 's/[\&/]/\\&/g')

CONSTITUENTS_STR=$(printf "%s\n" "$CONSTITUENTS" | sed "s/[\'&,]//g")
#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')

source /home/stt001/sitestore/PACKAGE_CIOPSW/LIB/library.scr


# [1]: List all the files that match in the input root, look for the specified dates, chop, split
# Copy and adjust each script file


SCRIPT=$SCRIPT_ROOT'_WT.m'
cp $TEMPLATE_SCRIPT $SCRIPT


# Adjust the script files
sed -i "s/VAR_DIR_WEBTIDE_DATA/$SED_DIR_WEBTIDE_DATA/g" $SCRIPT
sed -i "s/VAR_WT_DATASET/$WEBTIDE_DATASET/g" $SCRIPT

sed -i "s/VAR_DIR_DOMAIN/$SED_DIR_DOMAIN_DATA/g" $SCRIPT

sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_OUTPUT/g" $SCRIPT

sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT
sed -i "s/VAR_META_USER/$USER/g" $SCRIPT

sed -i "s/VAR_FILE_COORD/$SED_FILE_COORD/g" $SCRIPT
sed -i "s/VAR_FILE_BATHY/$SED_FILE_BATHY/g" $SCRIPT

sed -i "s/VAR_CONSTITUENTS_STR/$CONSTITUENTS_STR/g" $SCRIPT
sed -i "s/VAR_CONSTITUENTS/$CONSTITUENTS/g" $SCRIPT

sed -i "s/VAR_SWITCH_NETCDF/$SWITCH_NETCDF/g" $SCRIPT
sed -i "s/VAR_SWITCH_MAT/$SWITCH_MAT/g" $SCRIPT
sed -i "s/VAR_SWITCH_TRANSPORT/$SWITCH_TRANSPORT/g" $SCRIPT

sed -i "s/VAR_HEADERLINES/$HEADERLINES/g" $SCRIPT
sed -i "s/VAR_DELIMITER/$DELIMITER/g" $SCRIPT


# [3] SET UP THE SUBMISSION FILES.


SCRIPT_FL=$SCRIPT_ROOT'_WT.m'
SED_SCRIPT=$(printf "%s\n" "$SCRIPT" | sed 's/[\&/]/\\&/g')

SUBMIT=$SUBMIT_ROOT'_WT.job'
cp $TEMPLATE_SUBMIT $SUBMIT  

JOB_NAME=${RUN_ID}_WT

sed -i "s/VAR_SCRIPT/$SED_SCRIPT/g" $SUBMIT
sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

RUN_COMMAND='matlab < '$SED_SCRIPT
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT

SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )
if [ $SWITCH_SUBMIT = 'true' ] ; then
	jobsub -c $CLUSTER $SUBMIT
fi

