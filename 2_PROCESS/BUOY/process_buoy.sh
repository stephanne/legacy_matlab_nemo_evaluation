#!/bin/bash

# PROCESS SSST BUOY DATA
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: September 25, 2018.

# USED WITH: a bunch of things 
# REQUIRES : MATLAB 2013+, GSW library
# PURPOSE  : Compares sea surface temperature time series observations. 

# INPUT : Extracted subdomains (one per buoy), observational data (one file per buoy), and local bathymetry. Since surface only, 
#         no mooring 3x3 patchs available - just extract straight from the 2D hourly model field.

# OUTPUT: .mat file with extracted time series and observations put on a common time axis. Summary stats as well

#source ../../config.ksh
set -a; source ../../config.ksh; set +a

CONFIG=${REF_CONFIG};       RUN_ID='DI02d'
SWITCH_SUBMIT=false 

# Input files

# Observations
DIR_OBS_DATA=${REF_DIR_UMBRELLA}'/DATA/BUOY';	FILELIST_OBS='filelist_buoy.txt'

# Model data
DIR_MODEL_RAW=${REF_DIR_UMBRELLA}'/DATA/DI02d';		ROOT_MODEL='NEP36-DI02d_1h'
DIR_MODEL_SER=${REF_DIR_UMBRELLA}'/SER';		DELIMITER='grid_T_2D';	
DIR_WORK=${REF_DIR_UMBRELLA}'/WORK_'${RUN_ID};		FILE_INTERIM=${DIR_WORK}'/interimfile_'${RUN_ID}'.nc'
DIR_COORD=${REF_DIR_UMBRELLA}'/COORD';			ROOT_COORD='coordinates_nep36'

# Timing information
INTERVAL_STR='1h';	OBS_INTERP_INTERVAL='1';	MOD_INTERVAL='1'	# these should be in hours, can be partial

# What needs to be run?
SWITCH_STATION=1;	SWITCH_EXTRACT=0;	SWITCH_SERIES=0;	SWITCH_BUOY=1;

# Info file with obs file string, short code, latlon coords, etc 
FILE_STATION_INFO='info_buoy.csv';
DICTIONARY='d_buoy'		# Located in PACKAGE_LOCATION/ETC/DICTS.  Do not use the .py extension


# Output files
DIR_OUTPUT=${REF_DIR_UMBRELLA}'/BUOY';	ROOT_OUTPUT='BUOY_NEP36-DI02d'

# Parameters
CONSTITUENTS="${REF_CONSTITUENTS}"
DATE_START='20160101';	DATE_FINAL='20161231'
SWITCH_RMINTERIM=1;	# 1 if interim patch files are removed after use
SWITCH_USEFILENAME=0    # if 1, use the observation fileanems to label the extracted 3x3 patches
INSTRUMENT='SST_Buoy' # Only used if switch_filename = 1
SWITCH_SINGLE_FILE=0	# if 1, output a single giant file as well as all individual files


# Variables
VARS_MODT='tos';		VARS_MODS='sos';		VARS_OBST='Temp';	# Temperature variables
VARS_TIME='time_instant';	VARS_LAT='nav_lat';		VARS_LON='nav_lon';
VARS_T2D='tos';         	GRIDS=(${REF_GRID_T_2D} );	NGRIDS=1;		# Grid assignment
DEPTH_VAR='deptht'	# should be consistent with VEL_GRIDSET
#VARS_SSH='zos';	VARS_BATHY='Bathymetry'
COORD_X=2;	COORD_Y=2;		# Matlab is 1-indexed


# Metadata
NOTE='surface_buoys'		# string for any relevant information
USER=`whoami`				# attribution string


# Template files
TEMPLATE_SCRIPT_STN=${PKG_LOCATION}/${DIR_LIB}'/template_station_newest.py'
TEMPLATE_SCRIPT_EXT=${PKG_LOCATION}/${DIR_LIB}'/template_extract_ncks.sh'
TEMPLATE_SCRIPT_BUOY=${PKG_LOCATION}/${DIR_LIB}'/template_process_buoy.m'
TEMPLATE_SUBMIT=${PKG_LOCATION}/${DIR_LIB}/${REF_FILE_JOBSUB}

# Local script roots
SCRIPT_ROOT=${PKG_LOCATION}'/2_PROCESS/BUOY/scr_'${CONFIG}_${RUN_ID}
SUBMIT_ROOT=${PKG_LOCATION}'/2_PROCESS/BUOY/sub_'${CONFIG}_${RUN_ID}
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [ 0 ] HOUSEKEEPING

if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p ${DIR_OUTPUT}; fi
if [ ! -d $DIR_WORK ] ;   then; mkdir -p ${DIR_WORK}; fi
if [ ! -d $DIR_MODEL_SER ] ; then; mkdir -p ${DIR_MODEL_SER}; fi

SED_DIR_OBS_DATA=$(printf "%s\n" "$DIR_OBS_DATA" | sed 's/[\&/]/\\&/g')
SED_DIR_MODEL_RAW=$(printf "%s\n" "$DIR_MODEL_RAW" | sed 's/[\&/]/\\&/g')
SED_DIR_MODEL_SER=$(printf "%s\n" "$DIR_MODEL_SER" | sed 's/[\&/]/\\&/g')
SED_DIR_WORK=$(printf "%s\n" "$DIR_WORK" | sed 's/[\&/]/\\&/g')
SED_DIR_COORD=$(printf "%s\n" "$DIR_COORD" | sed 's/[\&/]/\\&/g')
SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_FILELIST_OBS=$(printf "%s\n" "$FILELIST_OBS" | sed 's/[\&/]/\\&/g')
SED_FILE_STATION_INFO=$(printf "%s\n" "$FILE_STATION_INFO" | sed 's/[\&/]/\\&/g')

SED_REF_FILE_COORD=$(printf "%s\n" "$REF_FILE_COORD" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_BATHY=$(printf "%s\n" "$REF_FILE_BATHY" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_MASK=$(printf "%s\n" "$REF_FILE_MASK" | sed 's/[\&/]/\\&/g')
SED_REF_PATH_FXNS=$(printf "%s\n" "$REF_PATH_FXNS" | sed 's/[\&/]/\\&/g')

SED_FILE_INTERIM=$(printf "%s\n" "$FILE_INTERIM" | sed 's/[\&/]/\\&/g')

SED_ROOT_OBS=$(printf "%s\n" "$ROOT_OBS" | sed 's/[\&/]/\\&/g')
SED_ROOT_MODEL=$(printf "%s\n" "$ROOT_MODEL" | sed 's/[\&/]/\\&/g')
SED_ROOT_COORD=$(printf "%s\n" "$ROOT_COORD" | sed 's/[\&/]/\\&/g')
SED_ROOT_OUTPUT=$(printf "%s\n" "$ROOT_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_FILE_INFO=$(printf "%s\n" "$FILE_INFO" | sed 's/[\&/]/\\&/g')

START_YEAR=${DATE_START:0:4};	START_MONTH=${DATE_START:4:2};	START_DAY=${DATE_START:6:2}
FINAL_YEAR=${DATE_FINAL:0:4};	FINAL_MONTH=${DATE_FINAL:4:2};	FINAL_DAY=${DATE_FINAL:6:2}

# End of day switch
SWITCH_END_OF_DAY=${REF_END_OF_DAY}

if [[ ${SWITCH_END_OF_DAY} == '00:00:00' ]]; then;
        datemin=${START_YEAR}'-'${START_MONTH}'-'${START_DAY}' 00:00:01';
        datemax=${FINAL_YEAR}'-'${FINAL_MONTH}'-'$((${FINAL_DAY} + 1))' 00:00:00'
else
        datemin=${START_YEAR}'-'${START_MONTH}'-'${START_DAY}' 00:00:00';
        datemax=${FINAL_YEAR}'-'${FINAL_MONTH}'-'${FINAL_DAY}' '${REF_END_OF_DAY}
fi


#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')


NOTE=${NOTE// /_}

# Prepare the submission script
SUBMIT=$SUBMIT_ROOT'_BUOY.job'
cp $TEMPLATE_SUBMIT $SUBMIT

JOB_NAME=BUOY_${RUN_ID}

sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

echo 'cd ' `pwd` >> $SUBMIT


# [ 0.5 ] FIND THE NEAREST POINTS ON THE GRID TO EXTRACT

if [[ ${SWITCH_STATION} == 1 ]]; then
	SCRIPT=$SCRIPT_ROOT'_STN.py'
	cp $TEMPLATE_SCRIPT_STN $SCRIPT

	# Swap in the info 
	sed -i "s/VAR_FILELIST/$SED_FILELIST_OBS/g" $SCRIPT
	sed -i "s/VAR_DIR_OBS/$SED_DIR_OBS_DATA/g" $SCRIPT

	sed -i "s/VAR_FILE_COORD/$SED_REF_FILE_COORD/g" $SCRIPT
	sed -i "s/VAR_FILE_LMASK/$SED_REF_FILE_MASK/g" $SCRIPT
	sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT

	sed -i "s/VAR_OUTPUT/$SED_FILE_STATION_INFO/g" $SCRIPT

        sed -i "s/VAR_SWITCH_USEFILENAME/$SWITCH_USEFILENAME/g" $SCRIPT
        sed -i "s/VAR_STR_INSTRUMENT/$INSTRUMENT/g" $SCRIPT

	sed -i "s/VAR_DICTIONARY/$DICTIONARY/g" $SCRIPT

	sed -i "s/VAR_VAL_DX/$REF_DX/g" $SCRIPT

        python $SCRIPT 
fi



# [ 1 ] EXTRACTIONs / CONCATENATION
#       If necessary, extract the model station data from whole-domain snapshots.  Likely not needed.

if [[ ${SWITCH_EXTRACT} == 1 ]]; then

        SWITCH_SERIES=1         # if you're extracting patches, you need to concatenate them

        SCRIPT=$SCRIPT_ROOT'_EXT.sh'
        cp $TEMPLATE_SCRIPT_EXT $SCRIPT

        # Swap in the variables into the extract / series script
        sed -i "s/VAR_CONFIG/$CONFIG/g" $SCRIPT
        sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT

        sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT;        sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT;      sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT
        sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT;        sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT;      sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT

        sed -i "s/VAR_STATION_LIST/$SED_FILE_STATION_INFO/g" $SCRIPT

        sed -i "s/VAR_DIR_SOURCE/$SED_DIR_MODEL_RAW/g" $SCRIPT
        sed -i "s/VAR_DIR_WORK/$SED_DIR_WORK/g" $SCRIPT
        sed -i "s/VAR_DIR_SERIES/$SED_DIR_MODEL_SER/g" $SCRIPT

        sed -i "s/VAR_FILE_INTERIM/$SED_FILE_INTERIM/g" $SCRIPT

        sed -i "s/VAR_SWITCH_USEFILENAME/$SWITCH_USEFILENAME/g" $SCRIPT

        sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

        sed -i "s/VAR_FILE_COORD/$SED_REF_FILE_COORD/g" $SCRIPT
        sed -i "s/VAR_DIR_COORD/$SED_DIR_COORD/g" $SCRIPT

        sed -i "s/VAR_GRID_T2D/$REF_GRID_T2D/g" $SCRIPT;        sed -i "s/VAR_GRID_U2D/$REF_GRID_U2D/g" $SCRIPT;        sed -i "s/VAR_GRID_V2D/$REF_GRID_V2D/g" $SCRIPT
        sed -i "s/VAR_GRID_T/$REF_GRID_T/g" $SCRIPT;            sed -i "s/VAR_GRID_U/$REF_GRID_U/g" $SCRIPT;            sed -i "s/VAR_GRID_V/$REF_GRID_V/g" $SCRIPT

        sed -i "s/VAR_VARS_T2D/$VARS_T2D/g" $SCRIPT;            sed -i "s/VAR_VARS_U2D/$VARS_U2D/g" $SCRIPT;            sed -i "s/VAR_VARS_V2D/$VARS_V2D/g" $SCRIPT
        sed -i "s/VAR_VARS_T/$VARS_T/g" $SCRIPT;                sed -i "s/VAR_VARS_U/$VARS_U/g" $SCRIPT;                sed -i "s/VAR_VARS_V/$VARS_V/g" $SCRIPT

        sed -i "s/VAR_END_OF_DAY/$REF_END_OF_DAY/g" $SCRIPT

        sed -i "s/VAR_SWITCH_SERIES/$SWITCH_SERIES/g" $SCRIPT
        sed -i "s/VAR_SWITCH_RMINTERIM/$SWITCH_RMINTERIM/g" $SCRIPT

        sed -i "s/VAR_DELIMITER/$DELIMITER/g" $SCRIPT
        sed -i "s/VAR_INTERVAL/$INTERVAL_STR/g" $SCRIPT
        sed -i "s/VAR_MOORING_INTERVAL/$MOORING_INTERVAL/g" $SCRIPT

        sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT


        echo 'ksh '$SCRIPT >> $SUBMIT


elif [[ ${SWITCH_SERIES} == 1 ]]; then

	# Nothing has been added to the submission script, so there's no need to put this in a separate file and run it there
	# If you add anything before this that does add to the submission script, you'll need to change this.	

	# If not extracting, need to concatenate the 3x3 station files and trim to match the observations.
	# To match behaviour of template_extract_ncks.sh, keep grids separate rather than combining into a single file.

	# Load the specified dictionary linking the model domain codes with observation files

	. ${PKG_LOCATION}/ETC/DICTS/${DICTIONARY}.sh

	#for eachkey in "${!ADCPs[@]}"; do echo "${eachkey}"; done       # A1, BP1, etc
	#for eachvalue in "${ADCPs[@]}"; do echo "${eachvalue}"; done    # observation file names

		# This is fine if you have hextracted slices that are just waiting to be collated.  Probably never needed, but leave as an option just in case.
		for key in "${!BUOYs[@]}"; do
			# This assumes that all the variables are in a single file - update as necessary

			echo ${key}
                	PATCHLIST=`ls -1 ${DIR_WORK}'/'*'_'${key}'_'*`
        	        ncrcat -O -h $PATCHLIST $FILE_INTERIM
			ncks -O -h -d time_counter,"$datemin","$datemax" ${FILE_INTERIM} ${DIR_MODEL_SER}/${ROOT_MODEL}_SER_BUOY-${key}_${DATE_START}-${DATE_FINAL}.nc
			ncatted -a  'note',global,a,c,${NOTE} ${DIR_MODEL_SER}/${ROOT_MODEL}_SER_BUOY-${key}_${DATE_START}-${DATE_FINAL}.nc
			rm ${FILE_INTERIM}
	
		done

fi



# [ 3 ] DATA ANALYSIS

if [[ ${SWITCH_BUOY} == 1 ]] ; then

	SCRIPT=$SCRIPT_ROOT'_BUOY.m'
	cp $TEMPLATE_SCRIPT_BUOY $SCRIPT

	# Adjust the script files

        sed -i "s/VAR_CTD_TYPE/$CTD_TYPE/g" $SCRIPT
        sed -i "s/VAR_SINGLE_FILE/$SWITCH_SINGLE_FILE/g" $SCRIPT

	sed -i "s/VAR_DIR_OBS/$SED_DIR_OBS_DATA/g" $SCRIPT

        sed -i "s/VAR_DIR_MODEL/$SED_DIR_MODEL_SER/g" $SCRIPT
        sed -i "s/VAR_ROOT_MODEL/$SED_ROOT_MODEL/g" $SCRIPT		# likely needs changing

        sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
        sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_OUTPUT/g" $SCRIPT

        sed -i "s/VAR_STATION_LIST/$SED_FILE_STATION_INFO/g" $SCRIPT

        sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

        sed -i "s/VAR_FILE_INFO/$SED_FILE_INFO/g" $SCRIPT
        sed -i "s/VAR_HEADER_LINES/$HEADER_LINES/g" $SCRIPT

        sed -i "s/VAR_DATE_START/$DATE_START/g" $SCRIPT
        sed -i "s/VAR_DATE_FINAL/$DATE_FINAL/g" $SCRIPT

        sed -i "s/VAR_COORD_X/$COORD_X/g" $SCRIPT
        sed -i "s/VAR_COORD_Y/$COORD_Y/g" $SCRIPT

        sed -i "s/VAR_MODVAR_TEMP/$VARS_MODT/g" $SCRIPT
        sed -i "s/VAR_MODVAR_SALT/$VARS_MODS/g" $SCRIPT

        sed -i "s/VAR_OBSVAR_TEMP/$VARS_OBST/g" $SCRIPT
        sed -i "s/VAR_VAR_TIME/$REF_VAR_TIME/g" $SCRIPT
        sed -i "s/VAR_VAR_LAT/$VARS_LAT/g" $SCRIPT

        sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT
        sed -i "s/VAR_META_USER/$USER/g" $SCRIPT
fi



# [ 5 ] SET UP THE SUBMISSION FILES.


SCRIPT_FL=$SCRIPT_ROOT'_BUOY.m'
SED_SCRIPT=$(printf "%s\n" "$SCRIPT" | sed 's/[\&/]/\\&/g')

SUBMIT=$SUBMIT_ROOT'_BUOY.job'
cp $TEMPLATE_SUBMIT $SUBMIT  

JOB_NAME=${RUN_ID}_BUOY

sed -i "s/VAR_SCRIPT/$SED_SCRIPT/g" $SUBMIT
sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

RUN_COMMAND='matlab < '$SED_SCRIPT
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT

SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )
if [ $SWITCH_SUBMIT = 'true' ] ; then
	jobsub -c $CLUSTER $SUBMIT
fi

