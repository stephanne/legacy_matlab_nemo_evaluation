#!/bin/bash

# PROCESS MOORED CTD DATA
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: September 24, 2018.

# USED WITH: a bunch of things 
# REQUIRES : MATLAB 2013+, GSW library
# PURPOSE  : Compares moored CTD time series observations.

# INPUT : Extracted subdomains (one per mooring), observational data (one file per mooring), and local bathymetry. Observation
#         data is a netcdf file, with a particular structure - see documentation for details.  Observations may need 
#         preprocessing.  Extremely similar to ADCP script

# OUTPUT: .mat file with constituents as well as tidal, residual and mean water level for both model and observations.  Summary stats as well

#source ../../config.ksh
set -a; source ../../config.ksh; set +a

CONFIG='NEP36';       RUN_ID='DI02d'
SWITCH_SUBMIT=false 

# Input files

# Observations
DIR_OBS_DATA=${REF_DIR_UMBRELLA}'/DATA/MCTD';	FILELIST_OBS='filelist_mctd.txt'

# Model data
DIR_MODEL_RAW=${REF_DIR_UMBRELLA}'/DATA/DI02d';		ROOT_MODEL='NEP36-DI02d_1h_20160101-20161231'
DIR_MODEL_SER=${REF_DIR_UMBRELLA}'/SER_MCTD';		DELIMITER='grid_T';	
DIR_WORK=${REF_DIR_UMBRELLA}'/WORK_'${RUN_ID};		FILE_INTERIM=${DIR_WORK}'/interimfile_'${RUN_ID}'.nc'
DIR_COORD=${REF_DIR_UMBRELLA}'/COORD';			ROOT_COORD='coordinates_nep36'

# Timing information
INTERVAL_STR='1d';		MOORING_INTERVAL='1h';	
OBS_INTERP_INTERVAL='0.5'	MOD_INTERVAL='1'	# these should be in hours, can be partial

# What needs to be run?
SWITCH_STATION=0;	SWITCH_EXTRACT=0;	SWITCH_SERIES=1;	SWITCH_MCTD=1;

# Info file with obs file string, short code, latlon coords, etc 
FILE_STATION_INFO='info_mctd.csv';	HEADER_LINES=1
DICTIONARY='d_mctd'		# Located in PACKAGE_LOCATION/ETC/DICTS.  Do not use the .py extension


# Output files
DIR_OUTPUT=${REF_DIR_UMBRELLA}'/TESTING_MCTD';	ROOT_OUTPUT='MCTD_NEP36-DI02d_20160101-20161231'

# Parameters
CONSTITUENTS="${REF_CONSTITUENTS}"
DATE_START='20160101';	DATE_FINAL='20161231'
SWITCH_ISMOORING=1;	# 1 if 3x3 mooring files are used (typically = 1)
SWITCH_RMINTERIM=1;	# 1 if interim patch files are removed after use
SWITCH_USEFILENAME=0    # if 1, use the observation fileanems to label the extracted 3x3 patches
INSTRUMENT='Moored_CTD' # Only used if switch_filename = 1
SWITCH_SINGLE_FILE=1	# if 1, output a single giant file as well as all individual files
CTD_TYPE='moored'	# lowercase, cast or moored





# Variables
VARS_T='thetao so';             # Model variables
VARS_MODT='thetao';     VARS_MODS='so';
VARS_OBST='Temp';       VARS_OBSS='Sal';                VARS_OBSP='Pres';       # Observation variables
VARS_TIME='time_instant';	VARS_LAT='nav_lat';	VARS_LON='nav_lon';
GRIDS=(${REF_GRID_T} );	NGRIDS=1;	
DEPTH_VAR='deptht'	# should be consistent with VEL_GRIDSET
#VARS_SSH='zos';	VARS_BATHY='Bathymetry'
COORD_X=2;	COORD_Y=2;		# Matlab is 1-indexed


# Metadata
NOTE='initial test string'		# string for any relevant information
USER=`whoami`				# attribution string


# Template files
TEMPLATE_SCRIPT_STN=${PKG_LOCATION}/${DIR_LIB}'/template_station_newest.py'
TEMPLATE_SCRIPT_EXT=${PKG_LOCATION}/${DIR_LIB}'/template_extract_ncks.sh'
TEMPLATE_SCRIPT_MCTD=${PKG_LOCATION}/${DIR_LIB}'/template_process_ctd.m'
TEMPLATE_SUBMIT=${PKG_LOCATION}/${DIR_LIB}/${REF_FILE_JOBSUB}

# Local script roots
SCRIPT_ROOT=${PKG_LOCATION}'/2_PROCESS/MCTD/scr_'${CONFIG}_${RUN_ID}
SUBMIT_ROOT=${PKG_LOCATION}'/2_PROCESS/MCTD/sub_'${CONFIG}_${RUN_ID}
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [ 0 ] HOUSEKEEPING

if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p ${DIR_OUTPUT}; fi
if [ ! -d $DIR_MODEL_SER ] ; then; mkdir -p ${DIR_MODEL_SER}; fi
if [ ! -d $DIR_WORK   ] ; then; mkdir -p ${DIR_WORK}; fi


if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p ${DIR_OUTPUT}; fi

SED_DIR_OBS_DATA=$(printf "%s\n" "$DIR_OBS_DATA" | sed 's/[\&/]/\\&/g')
SED_DIR_MODEL_RAW=$(printf "%s\n" "$DIR_MODEL_RAW" | sed 's/[\&/]/\\&/g')
SED_DIR_MODEL_SER=$(printf "%s\n" "$DIR_MODEL_SER" | sed 's/[\&/]/\\&/g')
SED_DIR_WORK=$(printf "%s\n" "$DIR_WORK" | sed 's/[\&/]/\\&/g')
SED_DIR_COORD=$(printf "%s\n" "$DIR_COORD" | sed 's/[\&/]/\\&/g')
SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_FILELIST_OBS=$(printf "%s\n" "$FILELIST_OBS" | sed 's/[\&/]/\\&/g')
SED_FILE_STATION_INFO=$(printf "%s\n" "$FILE_STATION_INFO" | sed 's/[\&/]/\\&/g')

SED_REF_FILE_COORD=$(printf "%s\n" "$REF_FILE_COORD" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_BATHY=$(printf "%s\n" "$REF_FILE_BATHY" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_MASK=$(printf "%s\n" "$REF_FILE_MASK" | sed 's/[\&/]/\\&/g')
SED_REF_PATH_FXNS=$(printf "%s\n" "$REF_PATH_FXNS" | sed 's/[\&/]/\\&/g')

SED_FILE_INTERIM=$(printf "%s\n" "$FILE_INTERIM" | sed 's/[\&/]/\\&/g')

SED_ROOT_OBS=$(printf "%s\n" "$ROOT_OBS" | sed 's/[\&/]/\\&/g')
SED_ROOT_MODEL=$(printf "%s\n" "$ROOT_MODEL" | sed 's/[\&/]/\\&/g')
SED_ROOT_COORD=$(printf "%s\n" "$ROOT_COORD" | sed 's/[\&/]/\\&/g')
SED_ROOT_OUTPUT=$(printf "%s\n" "$ROOT_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_FILE_INFO=$(printf "%s\n" "$FILE_INFO" | sed 's/[\&/]/\\&/g')

START_YEAR=${DATE_START:0:4};	START_MONTH=${DATE_START:4:2};	START_DAY=${DATE_START:6:2}
FINAL_YEAR=${DATE_FINAL:0:4};	FINAL_MONTH=${DATE_FINAL:4:2};	FINAL_DAY=${DATE_FINAL:6:2}

# End of day switch
SWITCH_END_OF_DAY=${REF_END_OF_DAY}

if [[ ${SWITCH_END_OF_DAY} == '00:00:00' ]]; then;
        datemin=${START_YEAR}'-'${START_MONTH}'-'${START_DAY}' 00:00:01';
        datemax=${FINAL_YEAR}'-'${FINAL_MONTH}'-'$((${FINAL_DAY} + 1))' 00:00:00'
else
        datemin=${START_YEAR}'-'${START_MONTH}'-'${START_DAY}' 00:00:00';
        datemax=${FINAL_YEAR}'-'${FINAL_MONTH}'-'${FINAL_DAY}' '${REF_END_OF_DAY}
fi


#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')


NOTE=${NOTE// /_}

# Prepare the submission script
SUBMIT=$SUBMIT_ROOT'_MCTD.job'
cp $TEMPLATE_SUBMIT $SUBMIT

JOB_NAME=MCTD_${RUN_ID}

sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

echo 'cd ' `pwd` >> $SUBMIT


# [ 0.5 ] FIND THE NEAREST POINTS ON THE GRID TO EXTRACT

if [[ ${SWITCH_STATION} == 1 ]]; then
	SCRIPT=$SCRIPT_ROOT'_STN.py'
	cp $TEMPLATE_SCRIPT_STN $SCRIPT

	# Swap in the info 
	sed -i "s/VAR_FILELIST/$SED_FILELIST_OBS/g" $SCRIPT
	sed -i "s/VAR_DIR_OBS/$SED_DIR_OBS_DATA/g" $SCRIPT

	sed -i "s/VAR_FILE_COORD/$SED_REF_FILE_COORD/g" $SCRIPT
	sed -i "s/VAR_FILE_LMASK/$SED_REF_FILE_MASK/g" $SCRIPT
	sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT

	sed -i "s/VAR_OUTPUT/$SED_FILE_STATION_INFO/g" $SCRIPT

        sed -i "s/VAR_SWITCH_USEFILENAME/$SWITCH_USEFILENAME/g" $SCRIPT
        sed -i "s/VAR_STR_INSTRUMENT/$INSTRUMENT/g" $SCRIPT

	sed -i "s/VAR_DICTIONARY/$DICTIONARY/g" $SCRIPT

	sed -i "s/VAR_VAL_DX/$REF_DX/g" $SCRIPT

        python $SCRIPT 
fi



# [ 1 ] EXTRACTIONs / CONCATENATION
#       If necessary, extract the model station data from whole-domain snapshots.  Likely not needed.

if [[ ${SWITCH_EXTRACT} == 1 ]]; then

        SWITCH_SERIES=1         # if you're extracting patches, you need to concatenate them

        SCRIPT=$SCRIPT_ROOT'_EXT.sh'
        cp $TEMPLATE_SCRIPT_EXT $SCRIPT

        # Swap in the variables into the extract / series script
        sed -i "s/VAR_CONFIG/$CONFIG/g" $SCRIPT
        sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT

        sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT;        sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT;      sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT
        sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT;        sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT;      sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT

        sed -i "s/VAR_STATION_LIST/$SED_FILE_STATION_INFO/g" $SCRIPT

        sed -i "s/VAR_DIR_SOURCE/$SED_DIR_MODEL_RAW/g" $SCRIPT
        sed -i "s/VAR_DIR_WORK/$SED_DIR_WORK/g" $SCRIPT
        sed -i "s/VAR_DIR_SERIES/$SED_DIR_MODEL_SER/g" $SCRIPT

        sed -i "s/VAR_FILE_INTERIM/$SED_FILE_INTERIM/g" $SCRIPT

        sed -i "s/VAR_SWITCH_USEFILENAME/$SWITCH_USEFILENAME/g" $SCRIPT

        sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

        sed -i "s/VAR_FILE_COORD/$SED_REF_FILE_COORD/g" $SCRIPT
        sed -i "s/VAR_DIR_COORD/$SED_DIR_COORD/g" $SCRIPT

        sed -i "s/VAR_GRID_T2D/$REF_GRID_T2D/g" $SCRIPT;        sed -i "s/VAR_GRID_U2D/$REF_GRID_U2D/g" $SCRIPT;        sed -i "s/VAR_GRID_V2D/$REF_GRID_V2D/g" $SCRIPT
        sed -i "s/VAR_GRID_T/$REF_GRID_T/g" $SCRIPT;            sed -i "s/VAR_GRID_U/$REF_GRID_U/g" $SCRIPT;            sed -i "s/VAR_GRID_V/$REF_GRID_V/g" $SCRIPT

        sed -i "s/VAR_VARS_T2D/$VARS_T2D/g" $SCRIPT;            sed -i "s/VAR_VARS_U2D/$VARS_U2D/g" $SCRIPT;            sed -i "s/VAR_VARS_V2D/$VARS_V2D/g" $SCRIPT
        sed -i "s/VAR_VARS_T/$VARS_T/g" $SCRIPT;                sed -i "s/VAR_VARS_U/$VARS_U/g" $SCRIPT;                sed -i "s/VAR_VARS_V/$VARS_V/g" $SCRIPT

        sed -i "s/VAR_END_OF_DAY/$REF_END_OF_DAY/g" $SCRIPT

        sed -i "s/VAR_SWITCH_SERIES/$SWITCH_SERIES/g" $SCRIPT
        sed -i "s/VAR_SWITCH_RMINTERIM/$SWITCH_RMINTERIM/g" $SCRIPT

        sed -i "s/VAR_DELIMITER/$DELIMITER/g" $SCRIPT
        sed -i "s/VAR_INTERVAL/$INTERVAL_STR/g" $SCRIPT
        sed -i "s/VAR_MOORING_INTERVAL/$MOORING_INTERVAL/g" $SCRIPT

        sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT


        echo 'ksh '$SCRIPT >> $SUBMIT


elif [[ ${SWITCH_SERIES} == 1 ]]; then

	# Nothing has been added to the submission script, so there's no need to put this in a separate file and run it there
	# If you add anything before this that does add to the submission script, you'll need to change this.	

	# If not extracting, need to concatenate the 3x3 station files and trim to match the observations.
	# To match behaviour of template_extract_ncks.sh, keep grids separate rather than combining into a single file.

	# Load the specified dictionary linking the model domain codes with observation files

	. ${PKG_LOCATION}/ETC/DICTS/${DICTIONARY}.sh

	#for eachkey in "${!ADCPs[@]}"; do echo "${eachkey}"; done       # A1, BP1, etc
	#for eachvalue in "${ADCPs[@]}"; do echo "${eachvalue}"; done    # observation file names

	# If need to concatenate some moorings (which is a much more typical thing for current metre data:
	if [[ ${SWITCH_ISMOORING} == 1 ]]; then
		# Files are mooring files and already in 3x3 patches.  Just need to stitch and trim to the specified window

		MCTD_LIST='HEC1ca A1bc'

		#for key in "${!MCTDs[@]}"; do
		for key in ${MCTD_LIST}; do
			# This assumes that all the variables are in a single file - update as necessary
			# Moorings with multiple instruments all end with a *lowercase* letter - check if last character in key is lowercase letter.  If it is, use a trimmed variableo
			if [[ "${key:$((${#key}-1)):1}" =~ [a-z] ]]; then
				if [[ "${key:$((${#key}-2)):1}" =~ [a-z] ]]; then
					key2=${key:0:$((${#key}-2))}
				else
					key2=${key:0:$((${#key}-1))}
				fi
			else
				key2=${key}
			fi
			echo switch 1, ${key2}

	                PATCHLIST=`ls -1 ${DIR_MODEL_RAW}'/'*'_'${key2}'-09_'*`

        	        ncrcat -O -h $PATCHLIST $FILE_INTERIM
			ncks -O -h -d time_counter,"$datemin","$datemax" ${FILE_INTERIM} ${DIR_MODEL_SER}/${ROOT_MODEL}_SER_MCTD-${key}-09_${DATE_START}-${DATE_FINAL}.nc
			ncatted -a  'note',global,a,c,${NOTE} ${DIR_MODEL_SER}/${ROOT_MODEL}_SER_MCTD-${key}-09_${DATE_START}-${DATE_FINAL}.nc
			rm ${FILE_INTERIM}

		done

	else	
		# This is fine if you have hextracted slices that are just waiting to be collated.  Probably never needed, but leave as an option just in case.
		for key in "${!MCTDs[@]}"; do
			# This assumes that all the variables are in a single file - update as necessary

			if [[ "${key:$((${#key}-1)):1}" =~ [a-z] ]]; then
				key2=${key:0:$((${#key}-1))}
			else
				key2=${key}
			fi
			echo ${key2}


			echo ${key}
                	PATCHLIST=`ls -1 ${DIR_WORK}'/'*'_'${key}'_'*`
        	        ncrcat -O -h $PATCHLIST $FILE_INTERIM
			ncks -O -h -d time_counter,"$datemin","$datemax" ${FILE_INTERIM} ${DIR_MODEL_SER}/${ROOT_MODEL}_SER_MCTD-${key}_${DATE_START}-${DATE_FINAL}.nc
			ncatted -a  'note',global,a,c,${NOTE} ${DIR_MODEL_SER}/${ROOT_MODEL}_SER_MCTD-${key}_${DATE_START}-${DATE_FINAL}.nc
			rm ${FILE_INTERIM}
	
		done

	fi




fi



# [ 3 ] DATA ANALYSIS

if [[ ${SWITCH_MCTD} == 1 ]] ; then

	SCRIPT=$SCRIPT_ROOT'_MCTD.m'
	cp $TEMPLATE_SCRIPT_MCTD $SCRIPT

	# Adjust the script files

        sed -i "s/VAR_CTD_TYPE/$CTD_TYPE/g" $SCRIPT
        sed -i "s/VAR_SINGLE_FILE/$SWITCH_SINGLE_FILE/g" $SCRIPT

	sed -i "s/VAR_DIR_OBS/$SED_DIR_OBS_DATA/g" $SCRIPT

        sed -i "s/VAR_DIR_MODEL/$SED_DIR_MODEL_SER/g" $SCRIPT
        sed -i "s/VAR_ROOT_MODEL/${SED_ROOT_MODEL}_SER/g" $SCRIPT		# likely needs changing

        sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
        sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_OUTPUT/g" $SCRIPT

        sed -i "s/VAR_STATION_LIST/$SED_FILE_STATION_INFO/g" $SCRIPT

        sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

        sed -i "s/VAR_FILE_INFO/$SED_FILE_INFO/g" $SCRIPT
        sed -i "s/VAR_HEADER_LINES/$HEADER_LINES/g" $SCRIPT

        sed -i "s/VAR_DATE_START/$DATE_START/g" $SCRIPT
        sed -i "s/VAR_DATE_FINAL/$DATE_FINAL/g" $SCRIPT

        sed -i "s/VAR_COORD_X/$COORD_X/g" $SCRIPT
        sed -i "s/VAR_COORD_Y/$COORD_Y/g" $SCRIPT

        sed -i "s/VAR_MODVAR_TEMP/$VARS_MODT/g" $SCRIPT
        sed -i "s/VAR_MODVAR_SALT/$VARS_MODS/g" $SCRIPT

        sed -i "s/VAR_OBSVAR_TEMP/$VARS_OBST/g" $SCRIPT
        sed -i "s/VAR_OBSVAR_SALT/$VARS_OBSS/g" $SCRIPT
        sed -i "s/VAR_OBSVAR_PRES/$VARS_OBSP/g" $SCRIPT

        sed -i "s/VAR_VAR_DIM/$REF_DEPDIM_T/g" $SCRIPT
        sed -i "s/VAR_VAR_TIME/$REF_VAR_TIME/g" $SCRIPT
        sed -i "s/VAR_VAR_LAT/$VARS_LAT/g" $SCRIPT

        sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT
        sed -i "s/VAR_META_USER/$USER/g" $SCRIPT
fi






# [ 5 ] SET UP THE SUBMISSION FILES.


SCRIPT_FL=$SCRIPT_ROOT'_MCTD.m'
SED_SCRIPT=$(printf "%s\n" "$SCRIPT" | sed 's/[\&/]/\\&/g')

SUBMIT=$SUBMIT_ROOT'_MCTD.job'
cp $TEMPLATE_SUBMIT $SUBMIT  

JOB_NAME=${RUN_ID}_MCTD

sed -i "s/VAR_SCRIPT/$SED_SCRIPT/g" $SUBMIT
sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

RUN_COMMAND='matlab < '$SED_SCRIPT
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT

SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )
if [ $SWITCH_SUBMIT = 'true' ] ; then
	jobsub -c $CLUSTER $SUBMIT
fi

