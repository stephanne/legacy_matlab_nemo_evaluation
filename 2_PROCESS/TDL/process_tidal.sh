#!/bin/bash

# TIDAL ANALYSIS OF A 2D FIELD
# S Taylor, based on script from Xianmin Hu. stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: July 11, 2018.

# USED WITH: template_tidal.m
# REQUIRES : MATLAB 2013+, ttide
# PURPOSE  : Calculates tidal constituents for each point over a domain

# INPUT : One of more data files, bathymetry file for land mask (use mesh mask if nz > 1)

# OUTPUT: .mat and .nc files with 2d maps of amp/phase of each constituent.  Each variable
#         requested has it's own files.

# NOTE:  Currently no support for 3D variables but can be added if needed


CONFIG='NEP36';       RUN_ID='9l'
SWITCH_SUBMIT=false 

# Input files
DIR_INPUT='/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TIDE_TESTING/DATA/TDL'
ROOT_INPUT='NEP36_9l';	INTERVAL_INPUT='1h';	SUFFIX_INPUT='.nc';
SWITCH_MULTI=1;	SWITCH_OVERRIDE=1;	NFILES_OVERRIDE=5;

FILE_BATHY='/space/hall0/sitestore/dfo/odis/jpp001/ECCC/NEP36/INITIALISATION_714x1020/Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc'


# Output files
DIR_OUTPUT='~/sitestore/PACKAGE_CIOPSW/3_ANALYZE/TDL'
ROOT_OUTPUT='NEP36_9l_test';	SUFFIX_OUTPUT='.nc'
SWITCH_SERIES=0

# Parameters
CONSTITUENTS="'M2', 'N2', 'S2', 'O1', 'K1', 'K2', 'P1', 'Q1'"
VARS="'zos'";	GRIDS="'grid_T_2D'";
TIME_VAR='time_counter';	TIME_INTERVAL=1;	# time interval in hours

# Metadata
NOTE='initial test string'		# string for any relevant information
USER=`whoami`				# attribution string


# Template files
TEMPLATE_SCRIPT='/home/stt001/sitestore/PACKAGE_CIOPSW/3_ANALYZE/TDL/template_tidal.m'
TEMPLATE_SUBMIT='/home/stt001/sitestore/PACKAGE_CIOPSW/3_ANALYZE/TDL/template_submit.sh'

# Local script roots
SCRIPT_ROOT='/home/stt001/sitestore/PACKAGE_CIOPSW/3_ANALYZE/TDL/scr_'${CONFIG}_${RUN_ID}
SUBMIT_ROOT='/home/stt001/sitestore/PACKAGE_CIOPSW/3_ANALYZE/TDL/sub_'${CONFIG}_${RUN_ID}
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters -----------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [0]: Prepare variables.

SED_DIR_INPUT=$(printf "%s\n" "$DIR_INPUT" | sed 's/[\&/]/\\&/g')
SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_FILE_BATHY=$(printf "%s\n" "$FILE_BATHY" | sed 's/[\&/]/\\&/g')

CONSTITUENTS_STR=$(printf "%s\n" "$CONSTITUENTS" | sed "s/[\'&,]//g")
#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')

source /home/stt001/sitestore/PACKAGE_CIOPSW/LIB/library.scr


# [1]: List all the files that match in the input root, look for the specified dates, chop, split
# Copy and adjust each script file


SCRIPT=$SCRIPT_ROOT'_TDL.m'
cp $TEMPLATE_SCRIPT $SCRIPT


# Adjust the script files
sed -i "s/VAR_DIR_INPUT/$SED_DIR_INPUT/g" $SCRIPT

sed -i "s/VAR_ROOT_INPUT/$ROOT_INPUT/g" $SCRIPT
sed -i "s/VAR_INTERVAL_INPUT/$INTERVAL_INPUT/g" $SCRIPT
sed -i "s/VAR_SUFFIX_INPUT/$SUFFIX_INPUT/g" $SCRIPT


sed -i "s/VAR_SWITCH_MULTI/$SWITCH_MULTI/g" $SCRIPT
sed -i "s/VAR_SWITCH_NFILES/$SWITCH_OVERRIDE/g" $SCRIPT
sed -i "s/VAR_SWITCH_SERIES/$SWITCH_SERIES/g" $SCRIPT

sed -i "s/VAR_NFILES_OVERRIDE/$NFILES_OVERRIDE/g" $SCRIPT
sed -i "s/VAR_SWITCH_SERIES/$SWITCH_SERIES/g" $SCRIPT

sed -i "s/VAR_GRIDS/$GRIDS/g" $SCRIPT
sed -i "s/VAR_VARS/$VARS/g" $SCRIPT
sed -i "s/VAR_TIME_VAR/$TIME_VAR/g" $SCRIPT
sed -i "s/VAR_INTERVAL/$TIME_INTERVAL/g" $SCRIPT



sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
sed -i "s/VAR_ROOT_OUTPUT/$ROOT_OUTPUT/g" $SCRIPT
sed -i "s/VAR_SUFFIX_OUTPUT/$SUFFIX_OUTPUT/g" $SCRIPT

sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT
sed -i "s/VAR_META_USER/$USER/g" $SCRIPT

sed -i "s/VAR_FILE_BATHY/$SED_FILE_BATHY/g" $SCRIPT

sed -i "s/VAR_CONSTITUENTS_STR/$CONSTITUENTS_STR/g" $SCRIPT
sed -i "s/VAR_CONSTITUENTS/$CONSTITUENTS/g" $SCRIPT


# [3] SET UP THE SUBMISSION FILES.


SCRIPT_FL=$SCRIPT_ROOT'_TDL.m'
SED_SCRIPT=$(printf "%s\n" "$SCRIPT" | sed 's/[\&/]/\\&/g')

SUBMIT=$SUBMIT_ROOT'_TDL.job'
cp $TEMPLATE_SUBMIT $SUBMIT  

JOB_NAME=${RUN_ID}_TDL

sed -i "s/VAR_SCRIPT/$SED_SCRIPT/g" $SUBMIT
sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

RUN_COMMAND='matlab < '$SED_SCRIPT
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT

SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )
if [ $SWITCH_SUBMIT = 'true' ] ; then
	jobsub -c $CLUSTER $SUBMIT
fi

