#!/bin/bash

# PROCESS CTD DATA
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: September 6, 2018.

# USED WITH: a bunch of things 
# REQUIRES : MATLAB 2013+, GSW matlab library
# PURPOSE  : Reads in a (large!) set of CTD data, sorts by date, and finds file with that data.  Extracts daily average, compares, plots. 

# INPUT : Set of CTD casts, daily 3D T files, local bathymetry. Aggreagate stats useful for running subsets of full couple thousand casts.

# OUTPUT: 

#source ../../config.ksh
set -a; source ../../config.ksh; set +a

CONFIG='NEP36';       RUN_ID='DI02c'
SWITCH_SUBMIT=false 

# Input files

# Observations
DIR_OBS_DATA=${REF_DIR_UMBRELLA}'/DATA/CTD';	FILELIST_OBS='filelist_ctd.txt'

# Model data
DIR_MODEL_RAW=${REF_DIR_UMBRELLA}'/DATA/DI02c';	ROOT_MODEL='NEP36-DI02c_1d'
DIR_WORK=${REF_DIR_UMBRELLA}'/WORK_'${RUN_ID};		DIR_SLC=${REF_DIR_UMBRELLA}'/SLC';	FILE_INTERIM=${DIR_WORK}'/interimfile'${RUN_ID}'.nc'
DIR_COORD=${REF_DIR_UMBRELLA}'/COORD';
DELIMITER='grid_T';	

# Timing information
INTERVAL_STR='1d';		

# What needs to be run?  CASTS ONLY.  MOORINGS ARE HANDLED SEPARATELY
# presorted into groups (defined by...?) - this is a separate script under 1_CONFIGURE
# read filelist, get the nearest locations.  ie do a station script every time
# then extract to match
# and ctd to grab nearest time (ie use the date time and ignore the time - daily average is always closest since it's in the middle of the day)
# plot cast to generate 3 panel plots. use gsw to convert pressure to z
# stats to get aggreate + individual stats
SWITCH_GETPOINTS=0;	SWITCH_EXTRACT=0;	SWITCH_CTD=1;

# Info file with obs file string, short code, latlon coords, etc 
FILE_STATION_INFO='info_ctd.csv';	HEADER_LINES=1  # amend this to include header info, make ADCP robust to it.
DICTIONARY='d_ctd'					# Located in PACKAGE_LOCATION/ETC/DICTS.  Do not use the .py extension

# Output files
DIR_OUTPUT=${REF_DIR_UMBRELLA}'/TESTING_CTD';	ROOT_OUTPUT='NEP36-DI02c_1d'

# Parameters
DATE_START='20160101';	DATE_FINAL='20161231'
SWITCH_USEFILENAME=True;	# True if using filenames to generate short codes for extracted patches
INSTRUMENT='CastCTD'		# string removed from filenames if previous switch is true
SWITCH_RMINTERIM=1
SWITCH_MINMAX=0
SWITCH_SINGLE_FILE=1 		# if 1, output single giant file with everything in it as well
CTD_TYPE='casted'			# lowercase, cast or moored

# Variables
VARS_MODT='thetao';	VARS_MODS='so';			VARS_T='thetao so';	# Model variables
VARS_TEMP='Temp';	VARS_SALT='Sal';		VARS_PRES='Pres';	# Observation variables
VARS_TIME='time_instant';	VARS_LAT='nav_lat';	VARS_LON='nav_lon';
DEPTH_VAR='deptht'	# should be consistent with VEL_GRIDSET
COORD_X=2;	COORD_Y=2;		# Matlab is 1-indexed


# Metadata
NOTE='placeholder'		# string for any relevant information
USER=`whoami`				# attribution string


# Template files
TEMPLATE_SCRIPT_STN=${PKG_LOCATION}/${DIR_LIB}'/template_station_newest.py'
TEMPLATE_SCRIPT_EXT=${PKG_LOCATION}/${DIR_LIB}'/template_extract_noseries.sh'
TEMPLATE_SCRIPT_CTD=${PKG_LOCATION}/${DIR_LIB}'/template_process_ctd.m'
TEMPLATE_SUBMIT=${PKG_LOCATION}/${DIR_LIB}/${REF_FILE_JOBSUB}

# Local script roots
SCRIPT_ROOT=${PKG_LOCATION}'/2_PROCESS/CTD/scr_'${CONFIG}_${RUN_ID}
SUBMIT_ROOT=${PKG_LOCATION}'/2_PROCESS/CTD/sub_'${CONFIG}_${RUN_ID}
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [ 0 ] HOUSEKEEPING
if [ ! -d $DIR_OUTPUT ] ; then; mkdir -p ${DIR_OUTPUT}; fi
if [ ! -d $DIR_SERIES ] ; then; mkdir -p ${DIR_SERIES}; fi
if [ ! -d $DIR_WORK   ] ; then; mkdir -p ${DIR_WORK}; fi


SED_DIR_OBS_DATA=$(printf "%s\n" "$DIR_OBS_DATA" | sed 's/[\&/]/\\&/g')
SED_DIR_MODEL_RAW=$(printf "%s\n" "$DIR_MODEL_RAW" | sed 's/[\&/]/\\&/g')
SED_DIR_MODEL_SLC=$(printf "%s\n" "$DIR_SLC" | sed 's/[\&/]/\\&/g')
SED_DIR_WORK=$(printf "%s\n" "$DIR_WORK" | sed 's/[\&/]/\\&/g')
SED_DIR_COORD=$(printf "%s\n" "$DIR_COORD" | sed 's/[\&/]/\\&/g')
SED_DIR_OUTPUT=$(printf "%s\n" "$DIR_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_FILELIST_OBS=$(printf "%s\n" "$FILELIST_OBS" | sed 's/[\&/]/\\&/g')
SED_FILE_STATION_INFO=$(printf "%s\n" "$FILE_STATION_INFO" | sed 's/[\&/]/\\&/g')

SED_REF_FILE_COORD=$(printf "%s\n" "$REF_FILE_COORD" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_BATHY=$(printf "%s\n" "$REF_FILE_BATHY" | sed 's/[\&/]/\\&/g')
SED_REF_FILE_MASK=$(printf "%s\n" "$REF_FILE_MASK" | sed 's/[\&/]/\\&/g')
SED_REF_PATH_FXNS=$(printf "%s\n" "$REF_PATH_FXNS" | sed 's/[\&/]/\\&/g')

SED_FILE_INTERIM=$(printf "%s\n" "$FILE_INTERIM" | sed 's/[\&/]/\\&/g')

SED_ROOT_OBS=$(printf "%s\n" "$ROOT_OBS" | sed 's/[\&/]/\\&/g')
SED_ROOT_MODEL=$(printf "%s\n" "$ROOT_MODEL" | sed 's/[\&/]/\\&/g')
SED_ROOT_OUTPUT=$(printf "%s\n" "$ROOT_OUTPUT" | sed 's/[\&/]/\\&/g')

SED_FILE_INFO=$(printf "%s\n" "$FILE_INFO" | sed 's/[\&/]/\\&/g')

START_YEAR=${DATE_START:0:4};	START_MONTH=${DATE_START:4:2};	START_DAY=${DATE_START:6:2}
FINAL_YEAR=${DATE_FINAL:0:4};	FINAL_MONTH=${DATE_FINAL:4:2};	FINAL_DAY=${DATE_FINAL:6:2}

#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')


# Prepare the submission script
SUBMIT=$SUBMIT_ROOT'_CTD.job'
cp $TEMPLATE_SUBMIT $SUBMIT

JOB_NAME=CTD_${RUN_ID}

sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

echo 'cd ' `pwd` >> $SUBMIT


# [ 0.5 ] FIND THE NEAREST POINTS ON THE GRID TO EXTRACT

if [[ ${SWITCH_GETPOINTS} == 1 ]]; then
	SCRIPT=$SCRIPT_ROOT'_STN.py'
	cp $TEMPLATE_SCRIPT_STN $SCRIPT

	# Swap in the info 
	sed -i "s/VAR_FILELIST/$SED_FILELIST_OBS/g" $SCRIPT
	sed -i "s/VAR_DIR_OBS/$SED_DIR_OBS_DATA/g" $SCRIPT

	sed -i "s/VAR_FILE_COORD/$SED_REF_FILE_COORD/g" $SCRIPT
	sed -i "s/VAR_FILE_LMASK/$SED_REF_FILE_MASK/g" $SCRIPT
	sed -i "s/VAR_FILE_BATHY/$SED_REF_FILE_BATHY/g" $SCRIPT

	sed -i "s/VAR_SWITCH_USEFILENAME/$SWITCH_USEFILENAME/g" $SCRIPT
	sed -i "s/VAR_STR_INSTRUMENT/$INSTRUMENT/g" $SCRIPT

	sed -i "s/VAR_OUTPUT/$SED_FILE_STATION_INFO/g" $SCRIPT

	sed -i "s/VAR_DICTIONARY/$DICTIONARY/g" $SCRIPT

	sed -i "s/VAR_VAL_DX/$REF_DX/g" $SCRIPT

        python $SCRIPT 
fi


# [ 1 ] EXTRACTIONs / CONCATENATION
#       If necessary, extract the model station data from whole-domain snapshots.  Likely not needed.

if [[ ${SWITCH_EXTRACT} == 1 ]]; then

        SCRIPT=$SCRIPT_ROOT'_EXT.sh'
        cp $TEMPLATE_SCRIPT_EXT $SCRIPT

        # Swap in the variables into the extract / series script
        sed -i "s/VAR_CONFIG/$CONFIG/g" $SCRIPT
        sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT

        sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT;        sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT;      sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT
        sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT;        sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT;      sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT

        sed -i "s/VAR_STATION_LIST/$SED_FILE_STATION_INFO/g" $SCRIPT
	sed -i "s/VAR_SWITCH_USEFILENAME/$SWITCH_USEFILENAME/g" $SCRIPT

        sed -i "s/VAR_DIR_SOURCE/$SED_DIR_MODEL_RAW/g" $SCRIPT
        sed -i "s/VAR_DIR_WORK/$SED_DIR_WORK/g" $SCRIPT
        sed -i "s/VAR_DIR_SLICES/$SED_DIR_MODEL_SLC/g" $SCRIPT

        sed -i "s/VAR_FILE_INTERIM/$SED_FILE_INTERIM/g" $SCRIPT

        sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

        sed -i "s/VAR_FILE_COORD/$SED_REF_FILE_COORD/g" $SCRIPT
        sed -i "s/VAR_DIR_COORD/$SED_DIR_COORD/g" $SCRIPT

        sed -i "s/VAR_GRID_T2D/$REF_GRID_T2D/g" $SCRIPT;        sed -i "s/VAR_GRID_U2D/$REF_GRID_U2D/g" $SCRIPT;        sed -i "s/VAR_GRID_V2D/$REF_GRID_V2D/g" $SCRIPT
        sed -i "s/VAR_GRID_T/$REF_GRID_T/g" $SCRIPT;            sed -i "s/VAR_GRID_U/$REF_GRID_U/g" $SCRIPT;            sed -i "s/VAR_GRID_V/$REF_GRID_V/g" $SCRIPT

        sed -i "s/VAR_VARS_T2D/$VARS_T2D/g" $SCRIPT;            sed -i "s/VAR_VARS_U2D/$VARS_U2D/g" $SCRIPT;            sed -i "s/VAR_VARS_V2D/$VARS_V2D/g" $SCRIPT
        sed -i "s/VAR_VARS_T/$VARS_T/g" $SCRIPT;                sed -i "s/VAR_VARS_U/$VARS_U/g" $SCRIPT;                sed -i "s/VAR_VARS_V/$VARS_V/g" $SCRIPT

        sed -i "s/VAR_END_OF_DAY/$REF_END_OF_DAY/g" $SCRIPT

        sed -i "s/VAR_SWITCH_RMINTERIM/$SWITCH_RMINTERIM/g" $SCRIPT

        sed -i "s/VAR_DELIMITER/$DELIMITER/g" $SCRIPT
        sed -i "s/VAR_INTERVAL/$INTERVAL_STR/g" $SCRIPT
        sed -i "s/VAR_MOORING_INTERVAL/$MOORING_INTERVAL/g" $SCRIPT

        sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT


        echo 'ksh '$SCRIPT >> $SUBMIT


fi



# [ 3 ] DATA ANALYSIS

if [[ ${SWITCH_CTD} == 1 ]] ; then

	SCRIPT=$SCRIPT_ROOT'_CTD.m'
	cp $TEMPLATE_SCRIPT_CTD $SCRIPT

	# Adjust the script files
	sed -i "s/VAR_DIR_OBS/$SED_DIR_OBS_DATA/g" $SCRIPT

	sed -i "s/VAR_DIR_MODEL/$SED_DIR_MODEL_SLC/g" $SCRIPT
	sed -i "s/VAR_ROOT_MODEL/${SED_ROOT_MODEL}_SLC/g" $SCRIPT

	sed -i "s/VAR_DIR_OUTPUT/$SED_DIR_OUTPUT/g" $SCRIPT
	sed -i "s/VAR_ROOT_OUTPUT/$SED_ROOT_OUTPUT/g" $SCRIPT

        sed -i "s/VAR_CTD_TYPE/$CTD_TYPE/g" $SCRIPT

        sed -i "s/VAR_STATION_LIST/$SED_FILE_STATION_INFO/g" $SCRIPT

        sed -i "s/VAR_SINGLE_FILE/$SWITCH_SINGLE_FILE/g" $SCRIPT

	sed -i "s/VAR_PATH_FXNS/$SED_REF_PATH_FXNS/g" $SCRIPT

	sed -i "s/VAR_FILE_INFO/$SED_FILE_INFO/g" $SCRIPT
	sed -i "s/VAR_HEADER_LINES/$HEADER_LINES/g" $SCRIPT

	sed -i "s/VAR_DATE_START/$DATE_START/g" $SCRIPT
	sed -i "s/VAR_DATE_FINAL/$DATE_FINAL/g" $SCRIPT

	sed -i "s/VAR_VAR_DEPTH/$DEPTH_VAR/g" $SCRIPT

	sed -i "s/VAR_COORD_X/$COORD_X/g" $SCRIPT
	sed -i "s/VAR_COORD_Y/$COORD_Y/g" $SCRIPT

	sed -i "s/VAR_MODVAR_TEMP/$VARS_MODT/g" $SCRIPT
	sed -i "s/VAR_MODVAR_SALT/$VARS_MODS/g" $SCRIPT

	sed -i "s/VAR_OBSVAR_TEMP/$VARS_TEMP/g" $SCRIPT
	sed -i "s/VAR_OBSVAR_SALT/$VARS_SALT/g" $SCRIPT
	sed -i "s/VAR_OBSVAR_PRES/$VARS_PRES/g" $SCRIPT
	sed -i "s/VAR_VAR_TIME/$REF_VAR_TIME/g" $SCRIPT
	sed -i "s/VAR_VAR_LAT/$VARS_LAT/g" $SCRIPT

	sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT
	sed -i "s/VAR_META_USER/$USER/g" $SCRIPT
fi




# [ 5 ] SET UP THE SUBMISSION FILES.


SCRIPT_FL=$SCRIPT_ROOT'_CTD.m'
SED_SCRIPT=$(printf "%s\n" "$SCRIPT" | sed 's/[\&/]/\\&/g')

SUBMIT=$SUBMIT_ROOT'_CTD.job'
cp $TEMPLATE_SUBMIT $SUBMIT  

JOB_NAME=${RUN_ID}_CTD

sed -i "s/VAR_SCRIPT/$SED_SCRIPT/g" $SUBMIT
sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

RUN_COMMAND='matlab < '$SED_SCRIPT
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT

SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )
if [ $SWITCH_SUBMIT = 'true' ] ; then
	jobsub -c $CLUSTER $SUBMIT
fi

