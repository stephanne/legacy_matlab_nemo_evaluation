#!/bin/bash

# CONFIGURATION FILE FOR NEMO DATA PROCESSING PACKAGE
# Similar to environment.ksh for NEMO, this keeps track of common directories and files as well as the local location of the package itself
# This means less things need to be hardcoded, so the package is more portable and easier to use

# [ 0 ] PACKAGE
PKG_LOCATION='/space/hall0/sitestore/dfo/odis/stt001/PACKAGE_REORG'			# Root directory for all package directories / files
DIR_LIB='LIB'				# Library of script templates
DIR_ETC='ETC/FXNS'			# Any functions / scripts that are called during processing
DIR_UTIL='UTIL'				# Other tools that may be useful
REF_FILE_JOBSUB='template_submit.sh'	# Job submission file (located in the library)


# [ 1 ] CONFIGURATION
REF_CONFIG='NEP36'
REF_RUN_ID='DE01'
REF_FILE_BATHY=${PKG_LOCATION}'/ETC/REF/Bathymetry_NEP36_714x1020_SRTM30_v11_NOAA3sec.nc'
REF_FILE_COORD=${PKG_LOCATION}'/ETC/REF/coordinates_nep36.nc'
REF_FILE_MASK=${PKG_LOCATION}'/ETC/REF/tlandmask_NEP36_tmpbathy.nc'
REF_FILE_MESH=''

# Directory containing subdirectories with the raw source data as well as various stages of analyzed data.  
REF_DIR_UMBRELLA='/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TESTING'
REF_PATH_FXNS=${PKG_LOCATION}/${DIR_ETC}

# [ 2 ] PARAMETERS
REF_CONSTITUENTS="'Q1', 'O1', 'P1', 'K1', 'N2', 'M2', 'S2', 'K2'" # must be in same order as t_tide - ie, in increasing frequency
REF_GRID_T2D='grid_T_2D';	REF_GRID_U2D='grid_U_2D';	REF_GRID_V2D='grid_V_2D';
REF_GRID_T='grid_T'; 		REF_GRID_U='grid_U'; 		REF_GRID_V='grid_V'; 
REF_DEPDIM_T='deptht';		REF_DEPDIM='depthu';		REF_DEPDIM_V='depthv';
REF_STEPH_CONVENTIONS=0	# substitute dashes for underscores in individual units of info, omit -09 at the end of 3x3 patch files.  Inconsistently implemented so far.
REF_VAR_TIME='time_counter';	REF_END_OF_DAY='23:59:59'	# if '00:00:00, slice at midnight precisely is counted with the previous day. Otherwise, use 23:59:59.
REF_DX=0.1		# radius in degrees to find nearest grid point for an observation
REF_PLT_ELLGAP=20	# number of degrees gap in tidal ellipse plots
REF_PLT_NHOURS=1	# number of hours in n-hour averages


# [ 3 ] VARIABLES THAT ARE NULL BY DEFAULT
VARS_T2D='';	VARS_U2D='';	VARS_V2D='';	VARS_T='';	VARS_U='';	VARS_V=''


# [ 4 ] EXPORT THINGS BECAUSE FOR SOME REASON NOW YOU NEED TO DO THAT
#export PKG_LOCATION


# [ 3 ] Small bash functions used in process_X.sh files.

get_nb_days () 
{
   theyear=$1
   themonth=$2
      case $themonth in
        '01' | '03' | '05' | '07' | '08' | '10' | '12' )  nb=31 ;;
        '04' | '06' | '09' | '11' )                       nb=30 ;;
        '02' )
           if [ $((theyear % 4)) -ne 0 ] ; then     # not a leap year : means do nothing and use old value of isleap
             nb=28
           elif [ $((theyear % 400)) -eq 0 ] ; then # yes, it's a leap year
             nb=29
           elif [ $((theyear % 100)) -eq 0 ] ; then # not a leap year do nothing and use old value of isleap
             nb=28
           else # it is a leap year
             nb=29
           fi
           ;;
      esac
}



lowercase ()
{
        #$1 | awk '{print tolower($0)}'
        echo $1 | awk '{print tolower($0)}'
}


