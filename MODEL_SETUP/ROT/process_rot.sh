#!/bin/bash

# ROTATE VELOCITIES TO CARDINAL DIRECTIONS
# S Taylor, based on routines from Li Zhai. stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: May 3, 2018.  Harmonized with process_extract_rotate.sh

# USED WITH: template_rotate.sh
# REQUIRES : MATLAB 2013+, opa_angle_zhai.m, rot_rep_2017.m, xml2struct.m
# PURPOSE  : Amends input file by added rotated velocities.  Handles both station files and full fields (in separate scripts)

# INPUT : Raw, large domain files and/or small subdomains.  Be very careful if running concurrently with the model, since it 
#         alters the input files by adding the rotated velocities.

# OUTPUT: Small station files, with all variables requested in one file, with both original and rotated velocities present.
#         Full domain (or large region), with variables separated into files based on their grid, with both original and 
#         rotated velocities present.



# Specifics of the run configuration - used for script filenames only
CONFIG='BoF180'; 	RUN_ID='P2T2'
SWITCH_SUBMIT_JOBS=false	# 0 for files to be generated but nothing submitted to run

# Window of data to be rotated.  Is the same for both field and stations
# As it is, this assumes that the first day of the window is the first day of its relevant file, and last is the last of its.
# In general this is not the case. (Not really an issue when files have only one day in each, but if multiple days, it gets messy.)
START_YEAR='2015';	START_MONTH='12';	START_DAY='07';
FINAL_YEAR='2016';	FINAL_MONTH='01';	FINAL_DAY='15';
INTERVAL=10  # number of days grouped in each file
#RUN_START_DATE='20150923'; 	RUN_FINAL_DATE='20160618'		# these are the global run values, used only as strings in filename grabbing

## These are the directories to check which months have been completed
COORD_FILENAME='/space/hall0/sitestore/dfo/odis/stt001/coords_and_meshes/coordinates_BoF180.nc'

NPAIRS=1
U_INVARS="'uos', 'ubar'";	U_OUTVARS="'ruos', 'rubar'"
V_INVARS="'vos', 'vbar'";	V_OUTVARS="'rvos', 'rvbar'"


# Station-specific things
SWITCH_STATIONS=true		# Rotate extracted station files
STN_INSTANTANEOUS=false		# are instantaneous values included 
STN_INPUT_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2o_1d_20150401-20160613'
STN_OUTPUT_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2o_1dR_20150401-20160613'
STN_STATION_LIST='/home/stt001/sitestore/PACKAGE/1_CONFIGURE/STN/test_stations.xml'

# Field-specific things
SWITCH_FIELDS=true		# Rotate whole fields
FLD_INSTANTANEOUS=false		# are instantaneous values included 
FLD_INPUT_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2_1h_20150928-20160613_MTH'
FLD_INPUT_ROOT='/space/hall0/sitestore/dfo/odis/stt001/BoF180/BoF180-P2T2o/BoF180-P2T2o-OUT/DIMG/BoF180-P2T2o_1h_20150928-20160613'
FLD_OUTPUT_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2o_1d_20150401-20160613'
GRIDT='grid-T';         GRIDU='grid-U';         GRIDV='grid-V'; # These can change if have a large subdomain output, like harbour-T etc
XOFFSET=1;              YOFFSET=1;                              # If full domain, set both offsets to 1, since Matlab is 1-indexed.

# Metadata
NOTE=''				# string for any relevant note
USER=`whoami`			# attribution string


# Template files
TEMPLATE_ROT_STN='/home/stt001/sitestore/PACKAGE/2_PREPARE/ROT/template_rotate_stations.m'
TEMPLATE_ROT_FLD='/home/stt001/sitestore/PACKAGE/2_PREPARE/ROT/template_rotate_fields.m'
TEMPLATE_SUB='/home/stt001/sitestore/PACKAGE/2_PREPARE/ROT/template_submit.sh'

# Local script roots
SCRIPT_ROOT='/home/stt001/sitestore/PACKAGE/2_PREPARE/ROT/scr_'$CONFIG'_'$RUN_ID  
SUBMIT_ROOT='/home/stt001/sitestore/PACKAGE/2_PREPARE/ROT/sub_'$CONFIG'_'$RUN_ID  
CLUSTER='gpsc1.science.gc.ca'


#----------------------------------------------------------------------------------------------------
#--- End of input parameters. -----------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [0]: Prepare variables.

# Escape the characters for each of the directories so they work properly.

SED_COORD_FILENAME=$(printf "%s\n" "$COORD_FILENAME" | sed 's/[\&/]/\\&/g')
SED_STN_STATION_LIST=$(printf "%s\n" "$STN_STATION_LIST" | sed 's/[\&/]/\\&/g')

SED_STN_INPUT_ROOT=$(printf "%s\n" "$STN_INPUT_ROOT" | sed 's/[\&/]/\\&/g')
SED_STN_OUTPUT_ROOT=$(printf "%s\n" "$STN_OUTPUT_ROOT" | sed 's/[\&/]/\\&/g')

SED_FLD_INPUT_ROOT=$(printf "%s\n" "$FLD_INPUT_ROOT" | sed 's/[\&/]/\\&/g')
SED_FLD_OUTPUT_ROOT=$(printf "%s\n" "$FLD_OUTPUT_ROOT" | sed 's/[\&/]/\\&/g')

#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')





# [1] Copy station template, sed in variables

# Originally, this was parallelized in a hacky way - listed the files in a directory, parsed out the station codes,
# added that to a file, split the file in N pieces, concatenated a string for each file that had al the contents,
# and then swapped those strings into each of the N copies of the template that were being used.
# Instead, now only one file (can bring that back if needed - will need separate XML files for each) and stations
# are listed in an XML file that's read in and parsed in the script (ie not here).  Use the same XML scripts as for 
# extract - no need to make things more complicated than necessary.

# Old section of code is preserved at the bottom this file, commented out. 

if [ $SWITCH_STATIONS = 'true' ] ; then

	
	SCRIPT_ROT=$SCRIPT_ROOT'_ROT_STN.m'
	cp $TEMPLATE_ROT_STN $SCRIPT_ROT


	# Adjust the script files
	sed -i "s/VAR_STN_INPUT_ROOT/$SED_STN_INPUT_ROOT/g" $SCRIPT_ROT
	sed -i "s/VAR_STN_OUTPUT_ROOT/$SED_STN_OUTPUT_ROOT/g" $SCRIPT_ROT
	sed -i "s/VAR_STATION_LIST/$SED_STN_STATION_LIST/g" $SCRIPT_ROT

	sed -i "s/VAR_COORD_FILENAME/$SED_COORD_FILENAME/g" $SCRIPT_ROT

	sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT_ROT  
	sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT_ROT
	sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT_ROT

	sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT_ROT       
	sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT_ROT   
	sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT_ROT
	sed -i "s/VAR_XOFFSET/$XOFFSET/g" $SCRIPT_ROT
	sed -i "s/VAR_YOFFSET/$YOFFSET/g" $SCRIPT_ROT
	sed -i "s/VAR_STN_INSTANTANEOUS/$STN_INSTANTANEOUS/g" $SCRIPT_ROT

	sed -i "s/VAR_NPAIRS/$NPAIRS/g" $SCRIPT_ROT
	sed -i "s/VAR_U_INVARS/$U_INVARS/g" $SCRIPT_ROT
	sed -i "s/VAR_V_INVARS/$V_INVARS/g" $SCRIPT_ROT
	sed -i "s/VAR_U_OUTVARS/$U_OUTVARS/g" $SCRIPT_ROT
	sed -i "s/VAR_V_OUTVARS/$V_OUTVARS/g" $SCRIPT_ROT
	
	sed -i "s/VAR_RUN_START_DATE/$RUN_START_DATE/g" $SCRIPT_ROT  
	sed -i "s/VAR_RUN_FINAL_DATE/$RUN_FINAL_DATE/g" $SCRIPT_ROT  

	sed -i "s/VAR_INTERVAL/$INTERVAL/g" $SCRIPT_ROT
	
	sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT_ROT
	sed -i "s/VAR_META_USER/$USER/g" $SCRIPT_ROT

fi




# Rotation Field scripts

# Can't split to run U and V in separate files - if it gets unwieldy (and it might!) split it in time.  
# At this point that's more hassle than it's worth, since it runs quite quickly.

if [ $SWITCH_FIELDS = 'true' ] ; then
	SCRIPT_ROT=$SCRIPT_ROOT'_ROT_FLD.m'
	cp $TEMPLATE_ROT_FLD $SCRIPT_ROT


	# Adjust the script files
	sed -i "s/VAR_FLD_INPUT_ROOT/$SED_FLD_INPUT_ROOT/g" $SCRIPT_ROT
	sed -i "s/VAR_FLD_OUTPUT_ROOT/$SED_FLD_OUTPUT_ROOT/g" $SCRIPT_ROT

	sed -i "s/VAR_GRIDT/$GRIDT/g" $SCRIPT_ROT
	sed -i "s/VAR_GRIDU/$GRIDU/g" $SCRIPT_ROT
	sed -i "s/VAR_GRIDV/$GRIDV/g" $SCRIPT_ROT
	sed -i "s/VAR_COORD_FILENAME/$SED_COORD_FILENAME/g" $SCRIPT_ROT

	sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT_ROT
	sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT_ROT
	sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT_ROT

	sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT_ROT
	sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT_ROT
	sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT_ROT

	sed -i "s/VAR_RUN_START_DATE/$RUN_START_DATE/g" $SCRIPT_ROT  
	sed -i "s/VAR_RUN_FINAL_DATE/$RUN_FINAL_DATE/g" $SCRIPT_ROT  
        sed -i "s/VAR_XOFFSET/$XOFFSET/g" $SCRIPT_ROT
        sed -i "s/VAR_YOFFSET/$YOFFSET/g" $SCRIPT_ROT

        sed -i "s/VAR_FLD_INSTANTANEOUS/$FLD_INSTANTANEOUS/g" $SCRIPT_ROT


	sed -i "s/VAR_NPAIRS/$NPAIRS/g" $SCRIPT_ROT
	sed -i "s/VAR_U_INVARS/$U_INVARS/g" $SCRIPT_ROT
	sed -i "s/VAR_V_INVARS/$V_INVARS/g" $SCRIPT_ROT
	sed -i "s/VAR_U_OUTVARS/$U_OUTVARS/g" $SCRIPT_ROT
	sed -i "s/VAR_V_OUTVARS/$V_OUTVARS/g" $SCRIPT_ROT

	sed -i "s/VAR_INTERVAL/$INTERVAL/g" $SCRIPT_ROT

	sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT_ROT
	sed -i "s/VAR_META_USER/$USER/g" $SCRIPT_ROT

fi












# [3] SET UP ALL THE SUBMISSION FILES.

# Station files
if [ $SWITCH_STATIONS = 'true' ] ; then

	SCRIPT_ROT=$SCRIPT_ROOT'_ROT_STN.m'
	SED_SCRIPT_ROT=$(printf "%s\n" "$SCRIPT_ROT" | sed 's/[\&/]/\\&/g')

	SUBMIT_ROT=$SUBMIT_ROOT'_ROT_STN.job'
	cp $TEMPLATE_SUB $SUBMIT_ROT

	JOB_NAME=rotate_stn_${RUN_ID}

	sed -i "s/VAR_SCRIPT/$SED_SCRIPT_ROT/g" $SUBMIT_ROT
	sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT_ROT

	RUN_COMMAND='matlab < '$SED_SCRIPT_ROT
	sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT_ROT

fi


# Rotation Fields files

if [ $SWITCH_FIELDS = 'true' ] ; then
	SCRIPT_ROT=$SCRIPT_ROOT'_ROT_FLD.m'
	SED_SCRIPT_ROT=$(printf "%s\n" "$SCRIPT_ROT" | sed 's/[\&/]/\\&/g')

	SUBMIT_ROT=$SUBMIT_ROOT'_ROT_FLD.job'
	cp $TEMPLATE_SUB $SUBMIT_ROT 

	JOB_NAME=rotate_fld_${RUN_ID}

	sed -i "s/VAR_SCRIPT_ROT/$SED_SCRIPT_ROT/g" $SUBMIT_ROT
	sed -i "s/VAR_JOB_NAME/$JOB_NAME/g" $SUBMIT_ROT

	RUN_COMMAND='matlab < '$SED_SCRIPT_ROT
	sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT_ROT

fi











# [4] SUBMIT ALL THE JOBS.
if [ $SWITCH_SUBMIT_JOBS = 'true' ] ; then

	if [ $SWITCH_STATIONS = 'true' ] ; then
		SUBMIT_ROT=$SUBMIT_ROOT'_ROT_STN.job'
		jobsub -c $CLUSTER $SUBMIT_ROT
	fi

	if [ $SWITCH_FIELDS = 'true' ] ; then

		SUBMIT_ROT=$SUBMIT_ROOT'_ROT_FLD.job'
		jobsub -c $CLUSTER $SUBMIT_ROT
	fi

fi

#-----------------------------------------------------------------------------------------------------------------
#--- End of script -----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------


# This is the original station list concatination code - it's kept for posterity in case it's useful, but generally
# speaking don't use it.

#if [ $SWITCH_STATIONS ] ; then
#
#	#MONTH=$ROT_START_MONTH ;	DAY=$ROT_START_DAY
#	#typeset -RZ2 MONTH ;		typeset -RZ2 DAY
#
#	# First read in the full variable names, then chop so it's just the station numbers, divvy up into bundles for parallel processing
#	# Then read in, concatenate string to read into matlab
#	# Last two steps are redundant but I am not hassling with Matlab to read in a text file in any sensible fashion.
#	
#	ls ${ROT_STN_INPUT_ROOT}_*-09_$ROT_START_YEAR$MONTH${DAY}* > $ROT_STN_STATION_LIST 
#	# Trim off the last 21 characters of each line
#	sed -i 's/.\{24\}$//g' $ROT_STN_STATION_LIST
#	# Then sed out all the stuff before the last underscore
#	sed -i 's/.*_//g' $ROT_STN_STATION_LIST
#	
#	NSTATIONS=`wc -l $ROT_STN_STATION_LIST | awk {'print $1'}`	# Number of station
#	NL=$(($NSTATIONS / $ROT_STN_NSCRIPTS))				# Number of lines (ie stations) per individual script
#	
#	## Error checking so if there's no station files it continues on to the next thing.
#	if [ $(($NL)) -eq 0 ] ; then
#		echo "NO STATIONS FOUND. NL SET TO 1 TO AVOID DIV 0"
#		NL=1
#	fi
#
#	
#	# If the number of stations present doesn't divide evenly into the number of files specified 
#	# there's one more line in each file (expect for the last one or two etc)
#	
#	if [ $(($NSTATIONS % $NL)) -ne 0 ] ; then #|| ( [ $NL -eq 1 ] && [$NSTATIONS -gt $N_ROT_SCRIPTS] ); then 
#		NL=$(($(($NL)) + 1))
#	fi
#	
#	if [ ${NL} -eq 1 ] && [ ${NSTATIONS} -gt ${ROT_STN_NSCRIPTS} ]; then
#        	NL=$(($(($NL)) + 1))
#	fi
#
#
#	split -l $NL -d $ROT_STN_STATION_LIST tmp_rotate_
#
#	# Copy and adjust each script file
#	ROTSEQ=`seq 1 ${ROT_STN_NSCRIPTS}`
#	for rot in ${ROTSEQ}; 
#	do
#		rotchar=$(($((rot))-1));	typeset -RZ2 rotchar
#	
#		SCRIPT_ROT=$SCRIPT_ROOT'_ROT_STN_'$rotchar'.m'
#		cp $TEMPLATE_ROT_STN $SCRIPT_ROT
#
#		# Sort out the division of the stations
#		#List all the files at the start date, add to file
#
#		MONTH=$ROT_STN_START_MONTH ;	DAY=$ROT_STN_START_DAY
#		typeset -RZ2 MONTH ;		typeset -RZ2 DAY
#
#		# Take each of the input files, read in station codes, concatenate an array in a string, swap into script file.
#		STATION_FILE=tmp_rotate_$rotchar
#	
#		STATION_ARRAY="{"
#		while IFS='' read -r line || [[ -n "$line" ]]; do
#			#echo "Text read from file: $line"
#			STATION_ARRAY=$STATION_ARRAY"'"$line"', "
#		done < "$STATION_FILE"
#		STATION_ARRAY=$STATION_ARRAY"}"


