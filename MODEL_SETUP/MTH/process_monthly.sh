#!/bin/bash

# MONTHLY FILE COMPILER
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structural changes: May 1, 2018

# PURPOSE         : Takes a series of NEMO output files (either the whole domain or a subset of it) and combines them into monthly, yearly, and total files
# REQUIRED SCRIPTS: template_monthly.sh, template_submit.sh

# CONTEXT: Early processing script, through simply reorganizes data into more weildy files.  (Reading in 365 files as opposed to 1 is unnecessary, assuming
#          that the single file is a reasonable size.)  Can run either after the model is done or concurrently: does not alter or move the original files. 
#          Two categories of files can be run at once - stations and fields.  stations are subdomains (ie, the 3x3 patches for the CTDs etc) and fields are
#          the entire domain (say BoF180).  Fields have only monthly concatenation, because will be too large otherwise.  Monthly means calculations 
#          are available for both.

# AUTOMATION: Similar to BDY, some previous work done on detecting which files were present and unprocessed, but not well tested.  Wait for Maestro.

# NOTE OF CAUTION: Be careful concatentating larger (sub)domains - memory adds up quickly.  3x3 files are fine with > a year in one file.

# ASSUMPTIONS: - rotation to be done afterwards. Does not request specific variables, so if rotation already done it will be included in the concatenated file.


#------------------------------------------------------------------------------------------------------------------------------
#--- USER SPECIFIED INFORMATION -----------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------

CONFIG='BoF180';       RUN_ID='P2T2'
SWITCH_SUBMIT=false   

# STATION INFO (input and output)
STATION_FILE='/home/stt001/sitestore/PACKAGE/1_CONFIGURE/STN/stations_test_sj.txt'

INPUT_STN_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2o/BoF180-P2T2o-OUT/DIMG/BoF180-P2T2o_60ts_20150928-20160613'
INPUT_STN_ROOT='/space/hall0/sitestore/dfo/odis/stt001/BoF180/BoF180-P2T2o/BoF180-P2T2o-OUT/DIMG/BoF180-P2T2o_60ts_20150928-20160613'

OUTPUT_MTH_STN_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2_60ts_20150928-20160613_MTH'
OUTPUT_YR_STN_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2_60ts_20150928-20160613_YR'
OUTPUT_TOT_STN_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2_60ts_20150928-20160613_TOT'
OUTPUT_MMN_STN_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2_60ts_20150928-20160613_MMN'

# FIELD INFO
GRID_LIST='grid-U grid-V grid-T'

INPUT_FLD_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2o/BoF180-P2T2o-OUT/DIMG/BoF180-P2T2o_1h_20150928-20160613'
INPUT_FLD_ROOT='/space/hall0/sitestore/dfo/odis/stt001/BoF180/BoF180-P2T2o/BoF180-P2T2o-OUT/DIMG/BoF180-P2T2o_1h_20150928-20160613'

OUTPUT_MTH_FLD_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2_1h_20150928-20160613_MTH'
OUTPUT_MMN_FLD_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2_1h_20150928-20160613_MMN'

# TIME INFO
START_MONTH='12';       START_YEAR='2015'		# First month to calculate
FINAL_MONTH='01';       FINAL_YEAR='2016'		# Last month to calculate

# CALCULATIONS
SWITCH_STATION=true;	SWITCH_FIELD=true;		# Create monthly files for station files, fields
SWITCH_YEARLY=false;	SWITCH_TOTAL=false;		# Create yearly, total files for stations only
SWITCH_MEAN_MONTH=true;					# Calculation monthly means for stations and fields

INTERIM_FILE='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/INTERIMFILE.nc'

# Metadata
NOTE=''				# string for user-specified information
AUTHOR=`whoami`			# attribution string

# Templates and scripts
TEMPLATE_MTH='/home/stt001/sitestore/PACKAGE/2_PREPARE/MTH/template_monthly.sh'
TEMPLATE_SUB='/home/stt001/sitestore/PACKAGE/2_PREPARE/MTH/template_submit.sh'

SUBMIT_FILE='/home/stt001/sitestore/PACKAGE/2_PREPARE/MTH/sub_'${CONFIG}_${RUN_ID}'.job'	# specify the entire filename. switch to generated structure?
SCRIPT_FILE='/home/stt001/sitestore/PACKAGE/2_PREPARE/MTH/scr_'${CONFIG}_${RUN_ID}'.sh'		# switch to nprocs structure for lots of stations???

CLUSTER='gpsc1.science.gc.ca'


#------------------------------------------------------------------------------------------------------------------------------
#--- GENERATE AND SUBMIT THE SCRIPT -------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------

source "/fs/vnas_Hdfo/odis/stt001/PACKAGE/refined_scripts/library.scr"

# Prepare variables for sed
SED_STATION_FILE=$(printf "%s\n" "$STATION_FILE" | sed 's/[\&/]/\\&/g')
SED_INPUT_FLD_ROOT=$(printf "%s\n" "$INPUT_FLD_ROOT" | sed 's/[\&/]/\\&/g')
SED_INPUT_STN_ROOT=$(printf "%s\n" "$INPUT_STN_ROOT" | sed 's/[\&/]/\\&/g')

SED_OUTPUT_MTH_FLD_ROOT=$(printf "%s\n" "$OUTPUT_MTH_FLD_ROOT" | sed 's/[\&/]/\\&/g')
SED_OUTPUT_MTH_STN_ROOT=$(printf "%s\n" "$OUTPUT_MTH_STN_ROOT" | sed 's/[\&/]/\\&/g')
SED_OUTPUT_YR_STN_ROOT=$(printf "%s\n" "$OUTPUT_YR_STN_ROOT" | sed 's/[\&/]/\\&/g')
SED_OUTPUT_TOT_STN_ROOT=$(printf "%s\n" "$OUTPUT_TOT_STN_ROOT" | sed 's/[\&/]/\\&/g')

SED_OUTPUT_MMN_FLD_ROOT=$(printf "%s\n" "$OUTPUT_MMN_FLD_ROOT" | sed 's/[\&/]/\\&/g')
SED_OUTPUT_MMN_STN_ROOT=$(printf "%s\n" "$OUTPUT_MMN_STN_ROOT" | sed 's/[\&/]/\\&/g')

SED_INTERIM_FILE=$(printf "%s\n" "$INTERIM_FILE" | sed 's/[\&/]/\\&/g')
SED_SCRIPT_FILE=$(printf "%s\n" "$SCRIPT_FILE" | sed 's/[\&/]/\\&/g')

# Copy the template into its own script file
cp $TEMPLATE_MTH $SCRIPT_FILE

# Alter the script with specified variables
sed -i "s/VAR_STATION_FILE/$SED_STATION_FILE/g" $SCRIPT_FILE
sed -i "s/VAR_INPUT_FLD_ROOT/$SED_INPUT_FLD_ROOT/g" $SCRIPT_FILE
sed -i "s/VAR_INPUT_STN_ROOT/$SED_INPUT_STN_ROOT/g" $SCRIPT_FILE


sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT_FILE
sed -i "s/VAR_META_AUTHOR/$AUTHOR/g" $SCRIPT_FILE

sed -i "s/VAR_OUTPUT_MTH_FLD_ROOT/$SED_OUTPUT_MTH_FLD_ROOT/g" $SCRIPT_FILE
sed -i "s/VAR_OUTPUT_MTH_STN_ROOT/$SED_OUTPUT_MTH_STN_ROOT/g" $SCRIPT_FILE
sed -i "s/VAR_OUTPUT_YR_STN_ROOT/$SED_OUTPUT_YR_STN_ROOT/g" $SCRIPT_FILE
sed -i "s/VAR_OUTPUT_TOT_STN_ROOT/$SED_OUTPUT_TOT_STN_ROOT/g" $SCRIPT_FILE

sed -i "s/VAR_OUTPUT_MMN_FLD_ROOT/$SED_OUTPUT_MMN_FLD_ROOT/g" $SCRIPT_FILE
sed -i "s/VAR_OUTPUT_MMN_STN_ROOT/$SED_OUTPUT_MMN_STN_ROOT/g" $SCRIPT_FILE

sed -i "s/VAR_GRID_LIST/$GRID_LIST/g" $SCRIPT_FILE
sed -i "s/VAR_INTERIM_FILE/$SED_INTERIM_FILE/g" $SCRIPT_FILE

SWITCH_STATION=$( lowercase $SWITCH_STATION )
SWITCH_FIELD=$( lowercase $SWITCH_FIELD )
SWITCH_YEARLY=$( lowercase $SWITCH_YEARLY )
SWITCH_TOTAL=$( lowercase $SWITCH_TOTAL )

sed -i "s/VAR_SWITCH_STATION/$SWITCH_STATION/g" $SCRIPT_FILE
sed -i "s/VAR_SWITCH_FIELD/$SWITCH_FIELD/g" $SCRIPT_FILE
sed -i "s/VAR_SWITCH_YEARLY/$SWITCH_YEARLY/g" $SCRIPT_FILE
sed -i "s/VAR_SWITCH_TOTAL/$SWITCH_TOTAL/g" $SCRIPT_FILE

sed -i "s/VAR_SWITCH_MEAN_MONTH/$SWITCH_MEAN_MONTH/g" $SCRIPT_FILE

sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT_FILE
sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT_FILE
sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT_FILE
sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT_FILE
#sed -i "s//$/g" $SCRIPT_FILE


# Same for the submission file, and then submit it.
cp $TEMPLATE_SUB $SUBMIT_FILE

RUN_COMMAND='ksh '$SED_SCRIPT_FILE
sed -i "s/VAR_SCRIPT/$SED_SCRIPT_FILE/g" $SUBMIT_FILE
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT_FILE

SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )

if [ $SWITCH_SUBMIT = 'true' ]; then
	jobsub -c $CLUSTER $SUBMIT_FILE
fi


