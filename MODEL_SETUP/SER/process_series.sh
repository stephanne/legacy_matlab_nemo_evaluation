#!/bin/bash

# TIME SERIES CONCATENATOR
# S Taylor, stephanne.taylor@dfo-mpo-gc.ca
# Last structural change: May 2, 2018

# USED WITH: template_series.sh
# REQUIRES : NCO 4.4.2 (GPSC)
# PURPOSE  : Concatenates a series of station (and/or field) files into a single file, and trims it to the specified window.

# INPUT : Raw model files, or somehwat processed files.  No check on size - be careful with whole-domain files.

# OUTPUT: Single file for each station specified with all data from start to end of specified window

CONFIG='NEP36';       RUN_ID='9l'
SWITCH_SUBMIT='False'

# STATION INFO
SWITCH_STATION=true;		STATION_FILE='/home/stt001/sitestore/PACKAGE_CIOPSW/1_CONFIGURE/INFO/tidegauges_id_NEP36_9l.txt'
INPUT_STN_ROOT='/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TIDE_TESTING/EXT/NEP369l'
OUTPUT_STN_ROOT='/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TIDE_TESTING/SER/NEP369l'

# FIELD INFO
SWITCH_FIELD=false;		GRID_LIST='grid-T grid-U grid-V'
INPUT_FLD_ROOT='/space/hall0/work/dfo/odis/stt001/SJAP100/SJAP100-P2R1/SJAP100-P2R1-OUT/EXR/SJAP100-P2R1_180ts_20150???-201????'
OUTPUT_FLD_ROOT='/space/hall0/work/dfo/odis/stt001/SJAP100/SJAP100-P2R1/SJAP100-P2R1-OUT/SER/SJAP100-P2R1_180ts_20150411-20160613_TOT'

# WINDOW TO CONCATENATE
START_YEAR='2015';	START_MONTH='09';	START_DAY='15';
FINAL_YEAR='2016';	FINAL_MONTH='03';	FINAL_DAY='03';

# METADATA
NOTE=''
USER=`whoami`

INTERIM_FILE='/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TIDE_TESTING/SER/interimfile.nc'

TEMPLATE_SER='/home/stt001/sitestore/PACKAGE_CIOPSW/2_PREPARE/SER/template_series.sh'
TEMPLATE_SUB='/home/stt001/sitestore/PACKAGE_CIOPSW/2_PREPARE/SER/template_submit.sh'

SUBMIT_FILE='/home/stt001/sitestore/PACKAGE_CIOPSW/2_PREPARE/SER/sub_'${CONFIG}_${RUN_ID}'.job'
SCRIPT_FILE='/home/stt001/sitestore/PACKAGE_CIOPSW/2_PREPARE/SER/scr_'${CONFIG}_${RUN_ID}'.scr'
CLUSTER='gpsc1.science.gc.ca'

#-------------------------------------------------------------------------------------------------------------------------------
#--- END OF USER INPUT ---------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------

source "/fs/vnas_Hdfo/odis/stt001/PACKAGE/refined_scripts/library.scr"


# Prepare variables for sed
SED_STATION_FILE=$(printf "%s\n" "$STATION_FILE" | sed 's/[\&/]/\\&/g')
SED_INPUT_STN_ROOT=$(printf "%s\n" "$INPUT_STN_ROOT" | sed 's/[\&/]/\\&/g')
SED_OUTPUT_STN_ROOT=$(printf "%s\n" "$OUTPUT_STN_ROOT" | sed 's/[\&/]/\\&/g')

SED_INPUT_FLD_ROOT=$(printf "%s\n" "$INPUT_FLD_ROOT" | sed 's/[\&/]/\\&/g')
SED_OUTPUT_FLD_ROOT=$(printf "%s\n" "$OUTPUT_FLD_ROOT" | sed 's/[\&/]/\\&/g')

SED_INTERIM_FILE=$(printf "%s\n" "$INTERIM_FILE" | sed 's/[\&/]/\\&/g')
SED_SCRIPT_FILE=$(printf "%s\n" "$SCRIPT_FILE" | sed 's/[\&/]/\\&/g')

# Copy the template into its own script file
cp $TEMPLATE_SER $SCRIPT_FILE

# Swap in the variables
sed -i "s/VAR_STATION_FILE/$SED_STATION_FILE/g" $SCRIPT_FILE
sed -i "s/VAR_INPUT_STN_ROOT/$SED_INPUT_STN_ROOT/g" $SCRIPT_FILE
sed -i "s/VAR_OUTPUT_STN_ROOT/$SED_OUTPUT_STN_ROOT/g" $SCRIPT_FILE

sed -i "s/VAR_GRID_LIST/$GRID_LIST/g" $SCRIPT_FILE
sed -i "s/VAR_INPUT_FLD_ROOT/$SED_INPUT_FLD_ROOT/g" $SCRIPT_FILE
sed -i "s/VAR_OUTPUT_FLD_ROOT/$SED_OUTPUT_FLD_ROOT/g" $SCRIPT_FILE

sed -i "s/VAR_INTERIM_FILE/$SED_INTERIM_FILE/g" $SCRIPT_FILE

sed -i "s/VAR_SWITCH_STN/$SWITCH_STATION/g" $SCRIPT_FILE
sed -i "s/VAR_SWITCH_FLD/$SWITCH_FIELD/g" $SCRIPT_FILE

sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT_FILE
sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT_FILE
sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT_FILE
sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT_FILE
sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT_FILE
sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT_FILE

sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT_FILE
sed -i "s/VAR_META_USER/$USER/g" $SCRIPT_FILE

#sed -i "s//$/g" $SCRIPT_FILE


# Same for the submission file, and then submit it
cp $TEMPLATE_SUB $SUBMIT_FILE

RUN_COMMAND='ksh '$SED_SCRIPT_FILE
sed -i "s/VAR_SCRIPT/$SED_SCRIPT_FILE/g" $SUBMIT_FILE
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT_FILE

SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )


if [ $SWITCH_SUBMIT = 'true' ]; then
	jobsub -c $CLUSTER $SUBMIT_FILE
fi


