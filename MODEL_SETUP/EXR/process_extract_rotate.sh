#!/bin/bash

# EXTRACT SUBDOMAINS AND ROTATE VELOCITIES TO CARDINAL DIRECTIONS
# S Taylor, based on routines from Li Zhai and J-P Paquin. stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: May 2, 2018

# USED WITH: template_extract_rotate.sh
# REQUIRES : MATLAB 2013+, opa_angle_zhai.m, rot_rep_2017.m
# PURPOSE  : Extracts a small region (typically 3x3 grid points) from a larger domain, saves in separate file. Amends
#            the file by adding rotated velocities (assuming that input data is on an ORCA grid and not lat/lon).

# INPUT : Raw, large domain files.  Does not alter or move original input files, so if full domain velocities need to
#         be rotated use ROT instead.  Can be run concurrently with the model itself.

# OUTPUT: Small station files, with all variables requested in one file, with both original and rotated velocities present.


# Configuration details
CONFIG='BoF180';       RUN_ID='P2T2'
SWITCH_SUBMIT=false        # false for files to be generated but nothing submitted to run


# This is the window to ignore everything outside of.
# As it is, this assumes that the first day of the window is the first day of its relevant file, and last is the last of its.
# In general this is not the case. (Not really an issue when files have only one day in each, but if multiple days, it gets messy.)
START_YEAR='2015';	START_MONTH='05';	START_DAY='01';
FINAL_YEAR='2015';	FINAL_MONTH='05';	FINAL_DAY='20';
INTERVAL=10  # number of days grouped in each file

# Stations / regions to be extracted
STATION_LIST='/home/stt001/sitestore/PACKAGE/1_CONFIGURE/STN/stations_test_sj.xml'

# Input / Output directories and roots
IN_ROOT='/space/hall0/sitestore/dfo/odis/stt001/BoF180/BoF180-P2T2o/BoF180-P2T2o-OUT/DIMG/BoF180-P2T2o_1d_20150401-20160613'
OUT_ROOT='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING/BoF180-P2T2o_1d_20150401_20160613'

# All variables to be extracted
# Currently, need to specify all variables listed ininput file - leaving one or more out will result in error when schema are
# written (since unextracted variables are not ssigned smaller dimension values.)  To fix - figure out how to delete from schema.
NT=7;	T_VARS="{'thetao', 'so', 'zos', 'ssh_ib', 'mldr10_1', 'heatc', 'saltc'}";	T_3D="[1,1,0,0,0,0,0]"
NU=3;	U_VARS="{'uo', 'ubar', 'tauuo'}";			U_3D="[1,0,0]"
NV=3;	V_VARS="{'vo', 'vbar', 'tauvo'}";			V_3D="[1,0,0]"
HAS_INSTANTANEOUS=false

# Rotate variables
NPAIRS=2
U_INVARS="{'uo', 'ubar'}";        U_OUTVARS="{'ruo', 'rubar'}"
V_INVARS="{'vo', 'vbar'}";        V_OUTVARS="{'rvo', 'rvbar'}"

# Details about grids
gridT='grid-T';		gridU='grid-U'; 	gridV='grid-V';	# These can change if have a large subdomain output, like harbour-T etc
XOFFSET=1;		YOFFSET=1;				# If full domain, set both offsets to 1, since Matlab is 1-indexed.

# Metadata
NOTE=''				# string used for any relevant info user wants to add to file
USER=`whoami`			# attribution string


# Template files
TEMPLATE_SCRIPT='/home/stt001/sitestore/PACKAGE/2_PREPARE/EXR/template_extract_rotate.m'
TEMPLATE_SUBMIT='/home/stt001/sitestore/PACKAGE/2_PREPARE/EXR/template_submit.sh'

# Local script roots
SCRIPT_ROOT='/home/stt001/sitestore/PACKAGE/2_PREPARE/EXR/scr_'${CONFIG}_${RUN_ID}
SUBMIT_ROOT='/home/stt001/sitestore/PACKAGE/2_PREPARE/EXR/sub_'${CONFIG}_${RUN_ID}
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [0]: Prepare variables.

SED_IN_ROOT=$(printf "%s\n" "$IN_ROOT" | sed 's/[\&/]/\\&/g')
SED_OUT_ROOT=$(printf "%s\n" "$OUT_ROOT" | sed 's/[\&/]/\\&/g')

SED_STATION_LIST=$(printf "%s\n" "$STATION_LIST" | sed 's/[\&/]/\\&/g')

#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')






# [1]: List all the files that match in the input root, look for the specified dates, chop, split
# Copy and adjust each script file


SCRIPT=$SCRIPT_ROOT'_EXR.m'
cp $TEMPLATE_SCRIPT $SCRIPT

# Sort out the division of the stations
# List all the files at the start date, add to file

typeset -RZ2 START_MONTH; typeset -RZ2 FINAL_MONTH
typeset -RZ2 START_DAY; typeset -RZ2 FINAL_DAY


# Adjust the script files
sed -i "s/VAR_INPUT/$SED_IN_ROOT/g" $SCRIPT
sed -i "s/VAR_OUTPUT/$SED_OUT_ROOT/g" $SCRIPT

sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT
sed -i "s/VAR_META_USER/$USER/g" $SCRIPT

sed -i "s/VAR_STATION_FILE/$SED_STATION_LIST/g" $SCRIPT

sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT
sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT
sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT

sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT 
sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT
sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT

sed -i "s/VAR_INTERVAL/$INTERVAL/g" $SCRIPT

sed -i "s/VAR_XOFFSET/$XOFFSET/g" $SCRIPT
sed -i "s/VAR_YOFFSET/$YOFFSET/g" $SCRIPT

sed -i "s/VAR_INSTANTANEOUS/$HAS_INSTANTANEOUS/g" $SCRIPT

sed -i "s/VAR_NTVARS/$NT/g" $SCRIPT
sed -i "s/VAR_TVARS/$T_VARS/g" $SCRIPT
sed -i "s/VAR_T3D/$T_3D/g" $SCRIPT
sed -i "s/VAR_GRIDT/$gridT/g" $SCRIPT

sed -i "s/VAR_NUVARS/$NU/g" $SCRIPT
sed -i "s/VAR_UVARS/$U_VARS/g" $SCRIPT
sed -i "s/VAR_U3D/$U_3D/g" $SCRIPT
sed -i "s/VAR_GRIDU/$gridU/g" $SCRIPT

sed -i "s/VAR_NVVARS/$NV/g" $SCRIPT
sed -i "s/VAR_VVARS/$V_VARS/g" $SCRIPT
sed -i "s/VAR_V3D/$V_3D/g" $SCRIPT
sed -i "s/VAR_GRIDV/$gridV/g" $SCRIPT

sed -i "s/VAR_NPAIRS/$NPAIRS/g" $SCRIPT
sed -i "s/VAR_U_INVARS/$U_INVARS/g" $SCRIPT
sed -i "s/VAR_V_INVARS/$V_INVARS/g" $SCRIPT
sed -i "s/VAR_U_OUTVARS/$U_OUTVARS/g" $SCRIPT
sed -i "s/VAR_V_OUTVARS/$V_OUTVARS/g" $SCRIPT


# [2] SET UP THE SUBMISSION FILES.


SCRIPT_FL=$SCRIPT_ROOT'_EXR.m'
SED_SCRIPT=$(printf "%s\n" "$SCRIPT" | sed 's/[\&/]/\\&/g')

SUBMIT=$SUBMIT_ROOT'_EXR.job'
cp $TEMPLATE_SUBMIT $SUBMIT  

JOB_NAME='EXR ' #${RUN_ID}_EXR

sed -i "s/VAR_SCRIPT/$SED_SCRIPT/g" $SUBMIT
sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

RUN_COMMAND='matlab < '$SED_SCRIPT
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT


# [3] SUBMIT THE JOBS.
if [ $SWITCH_SUBMIT = 'true' ] ; then
	jobsub -c $CLUSTER $SUBMIT
fi

