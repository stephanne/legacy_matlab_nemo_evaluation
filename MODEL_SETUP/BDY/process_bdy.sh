#!/bin/bash

# BOUNDARY FILE GENERATION WRAPPER SCRIPT
# S Taylor, stephanne.taylor@dfo-mpo.gc.ca
# Last structural edit: May 1, 2018

# PURPOSE         : Uses specifed information to copy and modify a template to create open boundary files for NEMO.
# REQUIRED SCRIPTS: template_bdy.scr, template_submit.sh

# CONTEXT         : Priority script to run for data processing.  Needs to be run only once if run after NEMO run is complete.
#                   Can run concurrently with NEMO - does not move or alter raw input files.  Run in complete months unless 
#                   NEMO starts or ends mid-month (and then only partial months should be first / last). 
#                   If eastern boundaries present, flipbdye.scr should be run after this script once NEMO is completed.

# AUTOMATION: Script currently requires user to specify a start and end data for the data to be processed.  Earlier
#             work considered automatic detection of model output and previously processed data to determine the
#             relevant window, but this was not rigourously tested.  Given that in the foreseeable future we're
#             heading towards using Maestro, there is little sense in revisiting the automation question until
#             ST has a grasp on what's involved with it.

#--------------------------------------------------------------------------------------------------------------------------
#--- USER SPECIFIED INFORMATION -------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# Specifics of the run configuration
INPUT_CONFIG='BoF180'		# source domain
OUTPUT_CONFIG='SJAP100'		# destination domain
RUN_ID='P2T2o'			# string used to parse filenames
JOBNAME='testing'		# string used to distinguish separate copies of scripts.
META_NOTE=''			# string added to metadata with any relevant info the user wants to include

# Switches
SWITCH_SUBMIT_JOBS=0		# 0 if want to generate files but not submit them (useful to make sure everything's as it should be)
SWITCH_EVERYOTHER=1		# 1 if using instantaneous data and input data is 2x frequency of output required.  0 otherwise 

# Start and end dates of the data to be processed.  This assumes that the first / last days are complete days.
START_DAY='01';	START_MONTH='02';	START_YEAR='2016'
FINAL_DAY='29';	FINAL_MONTH='02';	FINAL_YEAR='2016'

# Input / output directories 
DIR_INPUT_DATA='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/DIMG'
DIR_OUTPUT_BDY='/space/hall0/work/dfo/odis/stt001/BoF180/BoF180-P2T2/BoF180-P2T2-OUT/TESTING'

# Details of BDY files 
LIST_BDYS='BDYW BDYS BDYE'		# Which BDY files should be run
BDY_INTERVAL='30ts'			# Interval between slices (used only as a string to parse filenames)
TIME_SLICES_PER_DAY=48

# Coordinate and mesh files
FILE_INPUT_COORD='/space/hall0/work/dfo/odis/stt001/INITIALISATION_PHASE2/BoF180/coordinates_BoF180.nc'			# Source coordinates
FILE_OUTPUT_COORD='/space/hall0/work/dfo/odis/stt001/INITIALISATION_PHASE2/SJAP100/coordinates_SJAP100_AGRIF.nc'	# Destination coordinates
FILE_OUTPUT_MESH='/space/hall0/work/dfo/odis/stt001/INITIALISATION_PHASE2/SJAP100/mesh_mask_SJAP100_PHASE2_180_for10pts_LinInt_16_AGRIF.nc'

# Interpolation / interim concatenating etc locations
DIR_WORK='/space/hall0/work/dfo/odis/stt001/WORK_BDY/BoF180/'${JOBNAME}		# Umbrella directory for work directories (one per bdy)
NAMELIST='/home/stt001/PACKAGE/2_PREPARE/BDY/NAMELISTS_SOSIE'			# Location for generic SOSIE namelists
SOSIE='/home/jpp001/soft/sosie/bin'						# Location of SOSIE


# Template files to be copied and adjusted with above info
TEMPLATE_BDY='/home/stt001/sitestore/PACKAGE/2_PREPARE/BDY/template_bdy.scr'
TEMPLATE_SUB='/home/stt001/sitestore/PACKAGE/2_PREPARE/BDY/template_submit.sh'

# Paths for generated scripts / submission files
SCRIPT_ROOT='/home/stt001/sitestore/PACKAGE/2_PREPARE/BDY/scr_'$INPUT_CONFIG'_'$OUTPUT_CONFIG'_'$RUN_ID'_'${JOBNAME}
SUBMIT_ROOT='/home/stt001/sitestore/PACKAGE/2_PREPARE/BDY/sub_'$INPUT_CONFIG'_'$OUTPUT_CONFIG'_'$RUN_ID'_'${JOBNAME}
CLUSTER='gpsc1.science.gc.ca'


#--------------------------------------------------------------------------------------------------------------------------------
#--- END OF INPUT PARAMETERS. ---------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------


# [0]: PREPARATION

# Escape the characters for each of the directories so they work properly.

SED_DIR_INPUT_DATA=$(printf "%s\n" "$DIR_INPUT_DATA" | sed 's/[\&/]/\\&/g')
SED_DIR_OUTPUT_BDY=$(printf "%s\n" "$DIR_OUTPUT_BDY" | sed 's/[\&/]/\\&/g')

SED_FILE_INPUT_COORD=$(printf "%s\n" "$FILE_INPUT_COORD" | sed 's/[\&/]/\\&/g')
SED_FILE_OUTPUT_COORD=$(printf "%s\n" "$FILE_OUTPUT_COORD" | sed 's/[\&/]/\\&/g')
SED_FILE_OUTPUT_MESH=$(printf "%s\n" "$FILE_OUTPUT_MESH" | sed 's/[\&/]/\\&/g')

SED_DIR_WORK=$(printf "%s\n" "$DIR_WORK" | sed 's/[\&/]/\\&/g')
SED_BDY_NAMELIST=$(printf "%s\n" "$NAMELIST" | sed 's/[\&/]/\\&/g')
SED_SOSIE=$(printf "%s\n" "$SOSIE" | sed 's/[\&/]/\\&/g')

#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')




# [1] COPY THE TEMPLATES, ADJUST WITH SPECIFIED VARIABLES

# Loop over the boundaries - one script for each.
for bdy in $LIST_BDYS; do

	# Leading zeros for single digit months
	typeset -RZ2 START_MONTH
	typeset -RZ2 FINAL_MONTH

	# Copy the template into its own script file
	SCRIPT_BDY=$SCRIPT_ROOT'_'${bdy}_$START_YEAR$START_MONTH'_'$FINAL_YEAR$FINAL_MONTH'.scr'
	cp $TEMPLATE_BDY $SCRIPT_BDY 

	sed -i "s/VAR_RUN_ID/$RUN_ID/g" $SCRIPT_BDY
	sed -i "s/VAR_META_NOTE/$META_NOTE/g" $SCRIPT_BDY

	# Start and end days
	sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT_BDY
	sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT_BDY
	sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT_BDY
	sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT_BDY  
	sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT_BDY
	sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT_BDY

	# Boundaries to generate
	sed -i "s/VAR_BDY_LIST/$bdy/g" $SCRIPT_BDY
	sed -i "s/VAR_BDY_INTERVAL/$BDY_INTERVAL/g" $SCRIPT_BDY
	sed -i "s/VAR_SWITCH_EVERYOTHER/$SWITCH_EVERYOTHER/g" $SCRIPT_BDY
	sed -i "s/VAR_SLICES_PER_DAY/$TIME_SLICES_PER_DAY/g" $SCRIPT_BDY

	# Source domain info
	sed -i "s/VAR_SOURCE_CONFIG/$INPUT_CONFIG/g" $SCRIPT_BDY
	sed -i "s/VAR_DIR_SOURCE_RAW_DATA/$SED_DIR_INPUT_DATA/g" $SCRIPT_BDY
	sed -i "s/VAR_FILE_SOURCE_COORD/$SED_FILE_INPUT_COORD/g" $SCRIPT_BDY

	# Destination domain info
	sed -i "s/VAR_TARGET_CONFIG/$OUTPUT_CONFIG/g" $SCRIPT_BDY
	sed -i "s/VAR_DIR_TARGET_BDY/$SED_DIR_OUTPUT_BDY/g" $SCRIPT_BDY
	sed -i "s/VAR_FILE_TARGET_COORD/$SED_FILE_OUTPUT_COORD/g" $SCRIPT_BDY
	sed -i "s/VAR_FILE_TARGET_MESH/$SED_FILE_OUTPUT_MESH/g" $SCRIPT_BDY

	# Interim work variables
	sed -i "s/VAR_DIR_WORK/$SED_DIR_WORK/g" $SCRIPT_BDY 

	# SOSIE
	sed -i "s/VAR_DIR_NAMELIST/$SED_BDY_NAMELIST/g" $SCRIPT_BDY
	sed -i "s/VAR_SOSIE/$SED_SOSIE/g" $SCRIPT_BDY

done




# [2] SET UP ALL THE SUBMISSION FILES.

# Loop over boundaries - one per side
for bdy in $LIST_BDYS; do

	BDY_ROOT=$SUBMIT_ROOT'_'${bdy}_$START_YEAR$START_MONTH'_'$FINAL_YEAR$FINAL_MONTH
	SCRIPT_BDY=$SCRIPT_ROOT'_'${bdy}_$START_YEAR$START_MONTH'_'$FINAL_YEAR$FINAL_MONTH'.scr'

	SUBMIT_BDY=$BDY_ROOT'.job'

	JOB_NAME=${RUN_ID}_${bdy}_${JOBNAME}

	SED_BDY_ROOT=$(printf "%s\n" "$BDY_ROOT" | sed 's/[\&/]/\\&/g')

	cp $TEMPLATE_SUB $SUBMIT_BDY   

	sed -i "s/VAR_SCRIPT/$SED_BDY_ROOT/g" $SUBMIT_BDY
	sed -i "s/VAR_JOB_NAME/$JOB_NAME/g" $SUBMIT_BDY

	SED_SCRIPT_BDY=$(printf "%s\n" "$SCRIPT_BDY" | sed 's/[\&/]/\\&/g')
	RUN_COMMAND='ksh '$SED_SCRIPT_BDY

	sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT_BDY

done









# [3] SUBMIT ALL THE JOBS (one for each type of bdy specified.)

if [ ${SWITCH_SUBMIT_JOBS} == 1 ] ; then
	for bdy in $LIST_BDYS; do

	SUBMIT_BDY=$SUBMIT_ROOT'_'${bdy}_$START_YEAR$START_MONTH'_'$FINAL_YEAR$FINAL_MONTH'.job'
		jobsub $SUBMIT_BDY

	done

fi


