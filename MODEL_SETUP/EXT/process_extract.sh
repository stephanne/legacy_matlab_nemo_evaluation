#!/bin/bash

# EXTRACT SUBDOMAINS
# S Taylor, based on routines from Li Zhai and J-P Paquin. stephanne.taylor@dfo-mpo.gc.ca
# Last structual change: May 2, 2018.  Harmonized with process_extract_rotate.sh

# USED WITH: template_extract_rotate.sh
# REQUIRES : MATLAB 2013+, opa_angle_zhai.m, rot_rep_2017.m
# PURPOSE  : Extracts a small region (typically 3x3 grid points) from a larger domain, saves in separate file.

# INPUT : Raw, large domain files.  Does not alter or move original input files, so if full domain velocities need to
#         be rotated use ROT instead.  Can be run concurrently with the model itself.

# OUTPUT: Small station files, with all variables requested in one file.


CONFIG='NEP36';       RUN_ID='9l'
SWITCH_SUBMIT=true

# This is the window to ignore everything outside of.
START_YEAR='2015';	START_MONTH='09';	START_DAY='15';
FINAL_YEAR='2016';	FINAL_MONTH='12';	FINAL_DAY='31';
INTERVAL=10  # number of days grouped in each file.  If >1, start and end days must coincide with first/last days in a file

# Stations / regions to be extracted
STATION_LIST='/home/stt001/sitestore/PACKAGE_CIOPSW/1_CONFIGURE/INFO/tidegauges_domains_NEP36_9l.xml'

# Input / Output directories and roots
IN_ROOT='/space/hall0/sitestore/dfo/odis/jpp001/ECCC/CIOPS-W/hindcast_nep36_9l/NEMO_244800/NEP369l_2016030300_1h'
OUT_ROOT='/space/hall0/sitestore/dfo/odis/stt001/CIOPSW/TIDE_TESTING/EXT/NEP369l_2016030300_1h'

# All variables to be extracted.  Currently must include all variables present in a file - no variables deleted from schema.
NT=4;	T_VARS="{'tos', 'sos', 'zos', 'mldr10_1'}";	T_3D="[0,0,0,0]"
NU=0;	U_VARS="{'uos', 'ubar', 'tauuo'}";			U_3D="[0,0,0]"
NV=0;	V_VARS="{'vos', 'vbar', 'tauvo'}";			V_3D="[0,0,0]"
HAS_INSTANTANEOUS=false

# Details about grids
gridT='grid_T_2D';		gridU='grid_U'; 	gridV='grid_V';	# These can change if have a large subdomain output, like harbour-T etc
XOFFSET=1;              YOFFSET=1;                              # If full domain, set both offsets to 1, since Matlab is 1-indexed.

# Metadata
NOTE=''				# string for any relevant information
USER=`whoami`			# attribution string


# Template files
TEMPLATE_SCRIPT='/home/stt001/sitestore/PACKAGE_CIOPSW/2_PREPARE/EXT/template_extract.m'
TEMPLATE_SUBMIT='/home/stt001/sitestore/PACKAGE_CIOPSW/2_PREPARE/EXT/template_submit.sh'

# Local script roots
SCRIPT_ROOT='/home/stt001/sitestore/PACKAGE_CIOPSW/2_PREPARE/EXT/scr_'${CONFIG}_${RUN_ID}
SUBMIT_ROOT='/home/stt001/sitestore/PACKAGE_CIOPSW/2_PREPARE/EXT/sub_'${CONFIG}_${RUN_ID}
CLUSTER='gpsc1.science.gc.ca'

#----------------------------------------------------------------------------------------------------
#--- End of Input Parameters ------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# [0]: Prepare variables.

SED_IN_ROOT=$(printf "%s\n" "$IN_ROOT" | sed 's/[\&/]/\\&/g')
SED_OUT_ROOT=$(printf "%s\n" "$OUT_ROOT" | sed 's/[\&/]/\\&/g')

SED_STATION_LIST=$(printf "%s\n" "$STATION_LIST" | sed 's/[\&/]/\\&/g')

#SED_=$(printf "%s\n" "$" | sed 's/[\&/]/\\&/g')

source "/space/hall0/sitestore/dfo/odis/stt001/PACKAGE_CIOPSW/LIB/library.scr"




# [1]: List all the files that match in the input root, look for the specified dates, chop, split
# Copy and adjust each script file


SCRIPT=$SCRIPT_ROOT'_EXT.m'
cp $TEMPLATE_SCRIPT $SCRIPT

# Sort out the division of the stations
# List all the files at the start date, add to file

typeset -RZ2 START_MONTH; typeset -RZ2 FINAL_MONTH
typeset -RZ2 START_DAY; typeset -RZ2 FINAL_DAY


# Adjust the script files
sed -i "s/VAR_INPUT/$SED_IN_ROOT/g" $SCRIPT
sed -i "s/VAR_OUTPUT/$SED_OUT_ROOT/g" $SCRIPT

sed -i "s/VAR_META_NOTE/$NOTE/g" $SCRIPT
sed -i "s/VAR_META_USER/$USER/g" $SCRIPT

sed -i "s/VAR_STATION_FILE/$SED_STATION_LIST/g" $SCRIPT

sed -i "s/VAR_START_YEAR/$START_YEAR/g" $SCRIPT
sed -i "s/VAR_START_MONTH/$START_MONTH/g" $SCRIPT
sed -i "s/VAR_START_DAY/$START_DAY/g" $SCRIPT

sed -i "s/VAR_FINAL_YEAR/$FINAL_YEAR/g" $SCRIPT 
sed -i "s/VAR_FINAL_MONTH/$FINAL_MONTH/g" $SCRIPT
sed -i "s/VAR_FINAL_DAY/$FINAL_DAY/g" $SCRIPT

sed -i "s/VAR_INTERVAL/$INTERVAL/g" $SCRIPT

sed -i "s/VAR_XOFFSET/$XOFFSET/g" $SCRIPT
sed -i "s/VAR_YOFFSET/$YOFFSET/g" $SCRIPT

HAS_INSTANTANEOUS=$( lowercase $HAS_INSTANTANEOUS )
sed -i "s/VAR_INSTANTANEOUS/$HAS_INSTANTANEOUS/g" $SCRIPT

sed -i "s/VAR_NTVARS/$NT/g" $SCRIPT
sed -i "s/VAR_TVARS/$T_VARS/g" $SCRIPT
sed -i "s/VAR_T3D/$T_3D/g" $SCRIPT
sed -i "s/VAR_GRIDT/$gridT/g" $SCRIPT

sed -i "s/VAR_NUVARS/$NU/g" $SCRIPT
sed -i "s/VAR_UVARS/$U_VARS/g" $SCRIPT
sed -i "s/VAR_U3D/$U_3D/g" $SCRIPT
sed -i "s/VAR_GRIDU/$gridU/g" $SCRIPT

sed -i "s/VAR_NVVARS/$NV/g" $SCRIPT
sed -i "s/VAR_VVARS/$V_VARS/g" $SCRIPT
sed -i "s/VAR_V3D/$V_3D/g" $SCRIPT
sed -i "s/VAR_GRIDV/$gridV/g" $SCRIPT



# [3] SET UP THE SUBMISSION FILES.


SCRIPT_FL=$SCRIPT_ROOT'_EXT.m'
SED_SCRIPT=$(printf "%s\n" "$SCRIPT" | sed 's/[\&/]/\\&/g')

SUBMIT=$SUBMIT_ROOT'_EXT.job'
cp $TEMPLATE_SUBMIT $SUBMIT  

JOB_NAME=EXT_${CONFIG}_${RUN_ID}

sed -i "s/VAR_SCRIPT/$SED_SCRIPT/g" $SUBMIT
sed -i "s/VAR_JOB_NAME/${JOB_NAME}/g" $SUBMIT

RUN_COMMAND='matlab < '$SED_SCRIPT
sed -i "s/VAR_RUN_COMMAND/$RUN_COMMAND/g" $SUBMIT

SWITCH_SUBMIT=$( lowercase $SWITCH_SUBMIT )
if [ $SWITCH_SUBMIT = 'true' ] ; then
	jobsub -c $CLUSTER $SUBMIT
fi

